//
//  QKProListVC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 16/02/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import SDWebImage

class QKProListData: NSObject {
    
    var id = 0
    var role = 0
    var logo = ""
    var name = ""
    var Status = "A"
    var type = 0
    var reason = ""
    var unique_code = ""
    var followed_at = ""
    var followed_on = ""
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        self.id = dict["id"] as? Int ?? 0
        self.role = dict["role"] as? Int ?? 0
        self.type = dict["type"] as? Int ?? 0
        self.logo = dict["logo"] as? String ?? ""
        self.name = dict["name"] as? String ?? ""
        self.Status = dict["status"] as? String ?? ""
        self.reason = dict["reason"] as? String ?? ""
        self.unique_code = dict["unique_code"] as? String ?? ""
        self.followed_at = dict["followed_at"] as? String ?? ""
        self.followed_on = dict["followed_on"] as? String ?? ""
    }
    
}


class QKProListVC: UIViewController {

    var arrSection = [Int]()
    var Main_Dict = NSMutableDictionary()
    @IBOutlet var ProList_Tab: UITableView!
    @IBOutlet var viewTopStatus: UIView!
    @IBOutlet var Add_btn: UIButton!
    var arr_ProfileLisData = [NSDictionary]()
    var arr_ProfileList: [QKProListData]! = [QKProListData]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        app_Delegate.isBackChange = true
        app_Delegate.strProfileScreenFrom = ""
        self.Add_btn.setImage(UIImage.init(named: "add")?.tint(with: UIColor.white)!, for: .normal)
        
        
        //=========Add Refresh Control For Pull To Refresh==============//
         self.ProList_Tab.pullTorefresh(#selector(self.callAPIForGetUserProfileListing), self)
        //==============================================================//
        //==============================================================//
        
       
        //Table Header Cell Register
        ProList_Tab.register(UINib(nibName: "ProfileTypeHeaderCell", bundle: nil), forCellReuseIdentifier: "ProfileTypeHeaderCell")
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Clear Create Profile Data
        KVProfileCreate.shared.DataDict = NSMutableDictionary()
        KVProfileCreate.shared.arr_ContactInfo.removeAll()
        KVProfileCreate.shared.arr_WebInfo.removeAll()
        KVProfileCreate.shared.arr_Founders.removeAll()
        KVProfileCreate.shared.arr_EmailInfo.removeAll()
        KVProfileCreate.shared.arr_SocialMedia.removeAll()
        KVProfileCreate.shared.arr_OtherAddress.removeAll()
        
        
        //Call API Function
        if app_Delegate.isBackChange == true {
            app_Delegate.isBackChange = false
            
            if self.arr_ProfileLisData.count > 0 {
                self.setDictionaryForProfileListWithCategoryWise()
            }
            else {
                self.callAPIForGetUserProfileListing()
            }
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedReloadProfileList(notification:)), name: Notification.Name("RELOADNOTIFICATION"), object: nil)
    }
    
    //================================================================================//
    //MARK :- Received Notification
    func methodOfReceivedReloadProfileList(notification: NSNotification){
        //Take Action on Notification
        self.callAPIForGetUserProfileListing()
    }
    //================================================================================//
    
    
    //=================================================================================================//
    //#Call API For Get User List======================================================================//
    func callAPIForGetUserProfileListing() {
        
        DispatchQueue.main.async {
            
            let dict = NSMutableDictionary()
            dict["user_id"] = KVAppDataServieces.shared.LoginUserID()
            
            KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict, api: "profile/profile_list", arrFlag: false, Hendler: { (json, status) in
                self.ProList_Tab.closeEndPullRefresh()

                if status == 1
                {
                    print(json)
                    if let arr_data = json["data"] as? [NSDictionary]
                    {
                        self.arr_ProfileLisData = arr_data
                        self.setDictionaryForProfileListWithCategoryWise()
                    }
                }
            })
            
            
        }
    }
    
    func setDictionaryForProfileListWithCategoryWise() {

        self.arrSection.removeAll()
        self.arr_ProfileList.removeAll()
        let arrFirst = NSMutableArray()
        let arrSecond = NSMutableArray()
        let arrThird = NSMutableArray()
        
        //For Type 1
        for itemType in self.arr_ProfileLisData
        {
            let Type = itemType["type"] as? Int ?? 00
            if Type == 1 {
                arrFirst.add(itemType)
            }
            else if Type == 2 {
                arrSecond.add(itemType)
            }
            else if Type == 3 {
                arrThird.add(itemType)
            }
            else {
            }
            
            if !(Type == 0) {
                if !(self.arrSection.contains(Type)) {
                    self.arrSection.append(Type)
                }
            }
        }
        
        self.Main_Dict.setValue(arrFirst, forKey: "1")
        self.Main_Dict.setValue(arrSecond, forKey: "2")
        self.Main_Dict.setValue(arrThird, forKey: "3")
        print(self.Main_Dict)
        
        
        self.arrSection = self.arrSection.sorted{$0 < $1}
        print(self.arrSection)
        self.ProList_Tab.reloadData()

    }
    //=================================================================================================//
    //=================================================================================================//
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    @IBAction func backPress(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func AddprofilePress(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddProfileVC") as! AddProfileVC
        self.navigationController?.pushViewController(obj, animated: true)
    }

}

//=================================================================================================//
//==========TableView Delegate and Datasource Method===============================================//
extension QKProListVC: UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSection.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        guard let Headercell = tableView.dequeueReusableCell(withIdentifier: "ProfileTypeHeaderCell") as? ProfileTypeHeaderCell else {
            return UIView()
        }

        Headercell.contentView.backgroundColor = UIColor.groupTableViewBackground

        let sectionStringgg = arrSection[section]
        if sectionStringgg == 0 {
            Headercell.lblHeaderTitle.text = "Default"
        }
        else if sectionStringgg == 1 {
            Headercell.lblHeaderTitle.text = "Company"
        }
        else if sectionStringgg == 2 {
            Headercell.lblHeaderTitle.text = "Enterprise"
        }
        else if sectionStringgg == 3 {
            Headercell.lblHeaderTitle.text = "Celebrity/Public Figure"
        }

        return Headercell

    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let arrKeys = arrSection[section]
        let arrP_Data = Main_Dict[arrKeys.description]! as AnyObject
        print(arrP_Data.count)
        return arrP_Data.count
        //return arr_ProfileList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let sectionStringgg = arrSection[indexPath.section]
        if sectionStringgg == 0 {

            let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            obj.isedit = false
            self.navigationController?.pushViewController(obj, animated: true)

            return
        }

        let struNae = Main_Dict[sectionStringgg.description]! as AnyObject
        let dicList = struNae.object(at:indexPath.row) as? NSDictionary ?? [:]

        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
        if dicList["role"] as? Int ?? 0 == 1 {
            obj.int_userID = -1
            obj.str_Screen_From = "OnlyViewProfile"
            obj.strBlockProfileID = dicList["id"] as? Int ?? 0
            obj.str_uniquecode = dicList["unique_code"] as? String ?? ""
        }
        else {
            if dicList["role"] as? Int ?? 0 == 2 {
                obj.str_Screen_From = "View/EditProfile"
            }
            obj.int_userID = dicList["id"] as? Int ?? -1
            app_Delegate.strProfileScreenFrom = "ViewProfile"
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ProListCell = tableView.dequeueReusableCell(withIdentifier: "ProListCell") as! ProListCell
        cell.accessoryView = nil
        cell.constraint_top_lbl.constant = 1
        cell.constraint_bottom_lbl.constant = 1
        cell.Pro_img.layer.borderWidth = 3
        cell.Pro_img.layer.borderColor = UIColor.white.withAlphaComponent(0.16).cgColor
        cell.Pro_img.layer.cornerRadius = cell.Pro_img.frame.size.width/2

        
        //let dicList = arr_ProfileList[indexPath.row]
        
        let sectionStringgg = arrSection[indexPath.section]
        let struNae = Main_Dict[sectionStringgg.description]! as AnyObject
        let dicList = struNae.object(at:indexPath.row) as! NSDictionary
        
        cell.Type_lbl.text = ""
        cell.Title_lbl.text = dicList["name"] as? String ?? ""
        
//        let p_type = dicList["type"] as? Int ?? 0
//        let strType = getProfileType(tpye: p_type)
        cell.Type_lblStatus.text = ""
        cell.img_approve.isHidden = true
        let strP_Status = dicList["status"] as? String ?? ""
        if strP_Status == "P" {
            cell.accessoryType = .disclosureIndicator
            cell.Type_lbl.text = ""
            cell.Type_lblStatus.textColor = UIColor.orange
            cell.Type_lblStatus.text = "Pending Verification"
        }
        else if strP_Status == "R" {
            let strReject_Reason = dicList["reason"] as? String ?? ""
            if strReject_Reason == "" {
                cell.accessoryType = .disclosureIndicator
            }
            else {
                let buttonInfo = UIButton.init(type: .infoDark)
                buttonInfo.tintColor = .red
                buttonInfo.tag = indexPath.row
                buttonInfo.addTarget(self, action: #selector(self.btnClkRejectDetail), for: .touchUpInside)
                cell.accessoryView = buttonInfo
            }
            cell.Type_lbl.text = ""
            cell.Type_lblStatus.text = "Rejected"
            cell.Type_lblStatus.textColor = UIColor.red
        }
        else if strP_Status == "A" {
            print(dicList["is_transfer"] as? Int ?? 0)
            print(dicList["transfer_status"] as? Int ?? 0)
            cell.Type_lblStatus.text = ""
            
            if (dicList["is_transfer"] as? Int ?? 0 == 1) && (dicList["transfer_status"] as? Int ?? 0 == 0) {
                cell.constraint_top_lbl.constant = 1
                cell.constraint_bottom_lbl.constant = 1
                cell.Type_lblStatus.text = "Transfer in Process"
                cell.Type_lblStatus.textColor = UIColor.orange
            }
            else {
                cell.constraint_top_lbl.constant = -10
                cell.constraint_bottom_lbl.constant = 10
                cell.Type_lblStatus.text = ""
            }
            cell.Type_lbl.text = ""
            cell.img_approve.isHidden = false
            cell.accessoryType = .disclosureIndicator
        }
        cell.Pro_img.image = #imageLiteral(resourceName: "logo")
        let strlogo = dicList["logo"] as? String ?? ""
        if strlogo != "" {
            cell.Pro_img.image = #imageLiteral(resourceName: "logo")
            SDWebImageManager().loadImage(with: URL.init(string: strlogo), options: SDWebImageOptions.progressiveDownload, progress: { (p1, p2, uri) in
                
            }, completed: { (img, dta, err, typw, isfinished, uri) in
                
                cell.Pro_img.image = img
            })
        }
        
        //==================Button Reject Detail=====================================================//
        //cell.btnDetail.tag = indexPath.row
        //cell.btnDetail.addTarget(self, action: #selector(self.btnClkRejectDetail), for: .touchUpInside)
        //===========================================================================================//
        //===========================================================================================//
        
        
        cell.ActionButton = {(sender) in
            
            let ActionAlert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
            
            
            let SetDefault = UIAlertAction.init(title: "Set as Default", style: .default, handler: { (action) in
                
                ActionAlert.dismiss(animated: true, completion: nil)
            })
            let edit = UIAlertAction.init(title: "Edit", style: .default, handler: { (action) in
                
                ActionAlert.dismiss(animated: true, completion: nil)
            })
            let transfer = UIAlertAction.init(title: "Transfer", style: .default, handler: { (action) in
                
                ActionAlert.dismiss(animated: true, completion: nil)
            })
            let delete = UIAlertAction.init(title: "Delete", style: .destructive, handler: { (action) in
                
                ActionAlert.dismiss(animated: true, completion: nil)
            })
            
            let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                
                ActionAlert.dismiss(animated: true, completion: nil)
            })
            
            ActionAlert.addAction(SetDefault)
            ActionAlert.addAction(edit)
            ActionAlert.addAction(transfer)
            ActionAlert.addAction(delete)
            ActionAlert.addAction(cancel)
            
           self.present(ActionAlert, animated: true, completion: nil)
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        print("Tap On DetailDisclore Button")
        
        let dicList = arr_ProfileList[indexPath.row]
        if dicList.reason == "" {
            return
        }
        //Alert Show
        let detailAlt = UIAlertController.init(title: "", message: dicList.reason, preferredStyle: .alert)
        let ok = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
        })
        detailAlt.addAction(ok)
        self.present(detailAlt, animated: true, completion: nil)
    }
    
    
    func getProfileType(tpye: Int) -> String {
        var strProfileType = "Default"
        if tpye == 0 {
            strProfileType = "Default"
        }
        else if tpye == 1 {
            strProfileType = "Company"
        }
        else if tpye == 2 {
            strProfileType = "Enterprise"
        }
        else if tpye == 3 {
            strProfileType = "Celebrity/Public Figure"
        }
        return strProfileType
    }
    
    @IBAction func btnClkRejectDetail(_ sender: UIButton) {
        let detail = (sender as AnyObject).convert(CGPoint.zero, to: ProList_Tab)
        let indexpa = ProList_Tab.indexPathForRow(at: detail)!
        
        let sectionStringgg = arrSection[indexpa.section]
        let struNae = Main_Dict[sectionStringgg.description]! as AnyObject
        let dicList = struNae.object(at:indexpa.row) as! NSDictionary
        
        
        let strRejectReason = dicList["reason"] as? String ?? ""
        if strRejectReason == "" {
            return
        }
        //Alert Show
        let detailAlt = UIAlertController.init(title: "", message: strRejectReason, preferredStyle: .alert)
        let ok = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
        })
        detailAlt.addAction(ok)
        self.present(detailAlt, animated: true, completion: nil)
    }
    
}
//=================================================================================================//
//=================================================================================================//

class ProListCell: UITableViewCell
{
    @IBOutlet var Title_lbl: UILabel!
    @IBOutlet var Type_lbl: UILabel!
    @IBOutlet var Type_lblStatus: UILabel!
    @IBOutlet var action_btn: UIButton!
    @IBOutlet var Pro_img: UIImageView!
    @IBOutlet var img_approve: UIImageView!
    @IBOutlet var constraint_top_lbl: NSLayoutConstraint!
    @IBOutlet var constraint_bottom_lbl: NSLayoutConstraint!
    
    var ActionButton: ((UIButton) -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func actionPress(_ sender: UIButton)
    {
//        if self.ActionButton != nil
//        {
//            self.ActionButton!(sender)
//        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.Pro_img.image = UIImage.init(named: "user_profile_pic")
    }
    
}

















