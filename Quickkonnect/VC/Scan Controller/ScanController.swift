//
//  ScanViewController.swift
//  Quickkonnect
//
//  Created by Dhaval Tannarana on 02/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import Quikkly


class ScanController: UIViewController, QKGetScancodeBack, ScanViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, TOCropViewControllerDelegate {

    var QKscan: ScanView!
    @IBOutlet var btnUploadQK: UIButton!
    var ImagePicker = UIImagePickerController()
    let QKEmView = UIView.fromNib("QKEmployeView") as! QKEmployeView
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var TopAreHeight = 20
        
        let TopViewStatus = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: 20))
        if (Device.IS_IPHONE_X){
            //iPhone X
            TopAreHeight = 44
            TopViewStatus.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44)
        }
        
        TopViewStatus.backgroundColor = UIColor.init(hex: "28AFB0")
        self.view.addSubview(TopViewStatus)
        
        
        if(UIImagePickerController.isSourceTypeAvailable(.camera))
        {
            self.QKscan = ScanView.init(frame: CGRect.init(x: 0, y: TopAreHeight, width: Int(self.view.bounds.width), height: Int(self.view.bounds.height) - TopAreHeight))
            self.QKscan.delegate = self
            
            self.view.addSubview(self.QKscan)
        }
        
        
        if UserDefaults.standard.bool(forKey: "ImageUploadFirstTime") == false {
            //For First Time Tool Tip
            let view = UIView.fromNib("viewToolTip") as! viewToolTip
            view.getFullFreameForToolTip(to: self)
            self.view.bringSubview(toFront: btnUploadQK)
            //btnUploadQK.isHidden = true
        }
        
    }
        
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if(UIImagePickerController.isSourceTypeAvailable(.camera)) {
            self.QKscan.start()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    
    //================================================================================================//
    //MARK : - ScanView Deleta Method
    func scanView(_ scanView: ScanView, didDetectScannables scannables: [Scannable]) {

        scanView.stop()
        
        print(scannables.last?.value ?? 0)
        
        let dict_param = NSMutableDictionary()
        dict_param["unique_code"] = (scannables.last?.value ?? 0)!
        
        KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict_param, api: "is_user_exist", arrFlag: false) { (JSON, status) in
            
            if status == 1
            {
                print(JSON)
                if JSON["type"] as? Int ?? 0 == 0
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                    
                    if KVAppDataServieces.shared.UserValue(.unique_code) as? String ?? "" == "\((scannables.last?.value ?? 0)!)"
                    {
                        vc.scanuniquecode = "\((scannables.last?.value ?? 0)!)"
                        vc.scanuserid = KVAppDataServieces.shared.LoginUserID()
                        vc.strContactProfile = ""
                        vc.isaddcontect = false
                    }
                    else
                    {
                        vc.strContactProfile = "\((scannables.last?.value ?? 0)!)"
                        vc.isaddcontect = true
                        vc.Scan_user_update = true
                    }
                     self.navigationController?.pushViewController(vc, animated: true)
                }
                else
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
                    vc.int_userID = -1
                    vc.str_Screen_From = "Unfollow_Profile"
                    vc.str_uniquecode = "\((scannables.last?.value ?? 0)!)"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else{
                KVClass.ShowNotification("", "User Not available!")
            }
        }
        
    }

     func getscancodeBack(_ scanCode: String)
     {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        vc.strContactProfile = scanCode
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //================================================================================================//
    //================================================================================================//
    
    
    //=================================================================================================//
    // MARK: UIButton Method
    @IBAction func btnUploadTapped(_ sender: UIButton) {
        
        self.ImagePicker.delegate = self
            
        let imageAlert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
            
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                
            imageAlert.dismiss(animated: true, completion: nil)
        })
            
        let Capture = UIAlertAction.init(title: "Take Photo", style: .destructive, handler: { (action) in
                
            self.ImagePicker.sourceType = .camera
            self.ImagePicker.showsCameraControls = true
            self.ImagePicker.allowsEditing = false
                
            self.present(self.ImagePicker, animated: true, completion: nil)
        })
            
        let chosefromlib = UIAlertAction.init(title: "Choose Photo", style: .default, handler: { (action) in
            
            self.ImagePicker.sourceType = .photoLibrary
            self.ImagePicker.allowsEditing = false
                
            self.present(self.ImagePicker, animated: true, completion: nil)
        })
            
        imageAlert.addAction(Capture)
        imageAlert.addAction(chosefromlib)
        imageAlert.addAction(cancel)
            
        self.present(imageAlert, animated: true, completion: nil)
    }
    //=================================================================================================//
    //=================================================================================================//
    
    // MARK: ImagePicker Delegate Datasource
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let PickedImage: UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        picker.dismiss(animated: true, completion: nil)
        //================Image Crop=====================//
        let cropController:TOCropViewController = TOCropViewController(image: PickedImage)
        cropController.delegate = self
        appDelegate.window?.rootViewController?.present(cropController, animated: true, completion: nil)
        //===============================================//
        
    }
    
    // MARK: - Cropper Delegate
    @objc(cropViewController:didCropToImage:withRect:angle:) func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        
        cropViewController.presentingViewController?.dismiss(animated: true, completion: { _ in })
        print("\(image)")
        
        let scanble = Scannable.detect(inImage: image.cgImage!)
        print(scanble.first?.value ?? 0)
        
        if scanble.first?.value ?? 0 == 0 {
            KVClass.ShowNotification("", "Please selecte proper image")
            return
        }
        
        
        let dict_param = NSMutableDictionary()
        dict_param["unique_code"] = (scanble.first?.value ?? 0)!
        
        KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict_param, api: "is_user_exist", arrFlag: false) { (JSON, status) in
            
            if status == 1
            {
                print(JSON)
                if JSON["type"] as? Int ?? 0 == 0
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                    
                    if KVAppDataServieces.shared.UserValue(.unique_code) as? String ?? "" == "\((scanble.last?.value ?? 0)!)"
                    {
                        vc.scanuniquecode = "\((scanble.first?.value ?? 0)!)"
                        vc.scanuserid = KVAppDataServieces.shared.LoginUserID()
                        vc.strContactProfile = ""
                        vc.isaddcontect = false
                    }
                    else
                    {
                        vc.strContactProfile = "\((scanble.first?.value ?? 0)!)"
                        vc.isaddcontect = true
                        vc.Scan_user_update = true
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
                    vc.int_userID = -1
                    vc.str_Screen_From = "Unfollow_Profile"
                    vc.str_uniquecode = "\((scanble.first?.value ?? 0)!)"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else{
                KVClass.ShowNotification("", "User Not available!")
            }
        }
    }
}

