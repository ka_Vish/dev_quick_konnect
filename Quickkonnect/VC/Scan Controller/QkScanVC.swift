//
//  QkScanVC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 10/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import Foundation
import Quikkly

protocol QKGetScancodeBack {
    
    func getscancodeBack(_ scanCode: String)
}

class ScanVC : ScanViewController {
    
    var delegate: QKGetScancodeBack?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
    }
    
    func scanView(_ scanView: ScanView, didDetectScannables scannables: [Scannable]) {
        
        scanView.stop()
        
        self.dismiss(animated: true) {
            
            self.delegate?.getscancodeBack("\((scannables.last?.value ?? 0)!)")
        }
        
    }
}
