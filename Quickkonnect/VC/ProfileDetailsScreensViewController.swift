//
//  ProfileDetailsScreensViewController.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 28/12/17.
//  Copyright © 2017 com.kavish. All rights reserved.
//

import UIKit
import Material
import Alamofire
import GooglePlaces
import GooglePlacePicker

class AddEditLanguageController:UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    
    var indexOfData:Int = 0
    
    @IBOutlet weak var tfLanguageTitle: TextField!
    @IBOutlet weak var floatRatingView: FloatRatingView!
    @IBOutlet weak var btnDelete: UIButton!
    
    var pickerView: UIPickerView!
    let arrLanguage:[String] = ["Abkhaz", "Afar", "Afrikaans", "Akan", "Albanian", "Amharic", "Arabic", "Aragonese", "Aragonese", "Armenian", "Avestan", "Avaric", "Aymara", "Azerbaijani", "Bambara", "Bashkir", "Avaric", "Basque", "Belarusian", "Bengali", "Bihari", "Bislama", "Bosnian", "Breton", "Bulgarian", "Burmese", "Catalan", "Chamorro", "Chechen", "Chichewa", "Chinese", "Chuvash", "Cornish", "Cree", "Croatian", "Croatian", "Danish", "Danish", "Dutch", "English", "Esperanto", "Estonian", "Ewe", "Faroese", "Fijian", "Finnish", "French", "Fula", "Galician", "Georgian", "German", "Greek", "Guaraní", "Gujarati", "Haitian", "Hausa", "Hebrew", "Herero", "Hindi", "Hiri Motu", "Hungarian", "Interlingua", "Irish", "Igbo", "Inupiaq", "Ido", "Icelandic", "Italian", "Inuktitut", "Japanese", "Javanese", "Kalaallisut", "Kannada", "Kanuri", "Kashmiri", "Kazakh", "Khmer", "Kikuyu", "Kinyarwanda", "Kirghiz", "Komi", "Kongo", "Korean", "Kurdish", "Kwanyama", "Latin", "Luxembourgish", "Luganda", "Limburgish", "Lingala", "Lao", "Lithuanian", "Luba-Katanga", "Latvian", "Manx", "Macedonian", "Malagasy", "Malay", "Malayalam", "Maltese", "Māori", "Marathi", "Marshallese", "Mongolian", "Nauru", "Navajo", "Nauru", "Norwegian Bokmål", "North Ndebele", "Nepali", "Ndonga", "Norwegian Nynorsk", "Norwegian", "Nuosu", "South Ndebele", "Occitan", "Ojibwe", "Old Church Slavonic", "Oromo", "Oriya", "Ossetian", "Panjabi", "Pāli", "Persian", "Polish", "Pashto", "Portuguese", "Quechua", "Romansh", "Kirundi", "Romanian", "Russian", "Sanskrit", "Sardinian", "Sindhi", "Northern Sami", "Samoan", "Sango", "Serbian", "Scottish", "Shona", "Sinhala", "Slovak", "Slovene", "Somali", "Slovak", "Southern Sotho", "Spanish", "Sundanese", "Swati", "Swedish", "Tamil", "Telugu", "Tajik", "Thai", "Tigrinya", "Tibetan Standard", "Tagalog", "Tagalog", "Tonga", "Turkish", "Tsonga", "Tatar", "Twi", "Tahitian", "Uighur", "Ukrainian", "Urdu", "Uzbek", "Venda", "Vietnamese", "Walloon", "Tahitian", "Welsh", "Wolof", "Western Frisian", "Xhosa", "Yiddish", "Yoruba", "Zhuang"]
    
    
    func dateTextInputPressed(sender: UITextField) {
        
        //Create the view
        let inputView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: 190.0))
        
        pickerView = UIPickerView.init(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: 190.0))
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = sender.tag
        
        if arrLanguage.contains((sender.text ?? "")) {
            pickerView.selectRow(arrLanguage.index(of: sender.text!)!, inComponent: 0, animated: false)
        } else {
            pickerView.selectRow(0, inComponent: 0, animated: false)
        }
        
        inputView.addSubview(pickerView) // add date picker to UIView
        
        if sender.text != "" {
            //SelectedRow
        }
        
        sender.inputView = inputView
        
        handleDatePicker(sender: pickerView) // Set the date on start.
    }
    
    func handleDatePicker(sender: UIPickerView) {
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.tfLanguageTitle {
            dateTextInputPressed(sender: textField)
        }
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tfLanguageTitle.setDivider()
        
        
        let toolBar = UIToolbar().ToolbarPikerteast(mySelect: #selector(dismissPicker), delegate: self)
        tfLanguageTitle.text = ""
        tfLanguageTitle.inputAccessoryView = toolBar
        tfLanguageTitle.addTarget(self, action: #selector(self.dateTextInputPressed(sender:)), for: .allTouchEvents)
        
        tfLanguageTitle.delegate = self
        self.tfLanguageTitle.text = self.arrLanguage[0]
        
        if indexOfData == 5001 {
            self.btnDelete.isHidden = true
        } else {
            self.btnDelete.isHidden = false
            if let strValue = Singleton.sharedInstance.data_details?.languageDetail![indexOfData].langauge_name, strValue.count > 0 {
                tfLanguageTitle.text = strValue
            }
            
            if let ratingValue = Singleton.sharedInstance.data_details?.languageDetail![indexOfData].rating{
                
                print(ratingValue)
                
                floatRatingView.rating = Float(ratingValue)!
            }
        }
    }
    func testDone() {
        self.view.endEditing(true)
    }
    
    //============================================================================================//
    //MARK : - UIButton Action Method
    @IBAction func btnDeleteTapped(_ sender: Any) {
        //api/delete_language_detail/" + languageid
        //getPrefixURL
        let alertView = UIAlertController(title: "", message: "Are you sure, You want to delete this Language from your Profile?", preferredStyle: .alert)
        let actionNo = UIAlertAction(title: "No", style: .cancel, handler:nil)
        
        let actionYes = UIAlertAction(title: "Yes", style: .default, handler: { (alert) in
            if let ratingValue = Singleton.sharedInstance.data_details?.languageDetail![self.indexOfData].langauge_id, ratingValue > 0{
                
                KVClass.KVShowLoader()
                
                Alamofire.request("\(self.getPrefixURL())delete_language_detail/\(ratingValue)", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
                    
                    KVClass.KVHideLoader()
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            KVAppDataServieces.shared.deleteProfileData(.language_detail, self.indexOfData)
                            print(response.result.value as Any)
                            NotificationCenter.default.post(name: Notification.Name("UPDATEPROFILE"), object: nil)
                        }
                        self.navigationController?.popViewController(animated: true)
                        break
                        
                    case .failure(_):
                        print(response.result.error as Any)
                        self.navigationController?.popViewController(animated: true)
                        break
                    }
                }
            }
        })
        alertView.addAction(actionNo)
        alertView.addAction(actionYes)
        self.present(alertView, animated: true, completion: nil)
    }
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDoneTapped(_ sender: Any) {
        
        if tfLanguageTitle.text == "" {
            KVClass.ShowNotification("", "Please Select Language")
            return
        }
        if !(arrLanguage.contains(tfLanguageTitle.text!)) {
            KVClass.ShowNotification("", "Please Select Proper Language")
            return
        }
        
        
        var dictParameter:[String:Any] = [:]
        var strURL:String = "user_langauge_detail_add"
        
        if tfLanguageTitle.text != "" {
            dictParameter["langauge_name"] = tfLanguageTitle.text
            dictParameter["rating"] = "\(floatRatingView.rating)"
            
            if indexOfData != 5001 {
                if let strValue = Singleton.sharedInstance.data_details?.languageDetail![indexOfData].langauge_id, strValue > 0 {
                    dictParameter["langauge_id"] = strValue
                    strURL = "user_langauge_detail_edit"
                }
            } else {
                dictParameter["user_id"] = KVAppDataServieces.shared.LoginUserID()
            }
            KVClass.KVShowLoader()
            Alamofire.request("\(self.getPrefixURL())\(strURL)", method: .post, parameters: dictParameter, encoding: JSONEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
                kUserDefults(true as AnyObject, key: "isprofileupdate")
               KVClass.KVHideLoader()
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value{
                        
                        if let datadict = (data as? NSDictionary ?? [:])["data"] as? NSDictionary
                        {
                            KVAppDataServieces.shared.updateProfile(.language_detail, datadict, self.indexOfData)
                            NotificationCenter.default.post(name: Notification.Name("UPDATEPROFILE"), object: nil)
                        }
                        
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                    break
                    
                case .failure(_):
                    self.navigationController?.popViewController(animated: true)
                    break
                }
            }
        } else {
            KVClass.ShowNotification("", "Please add Language Title.")
        }
        
        
    }
    //============================================================================================//
    //============================================================================================//
    
    
    //============================================================================================//
    //MARK : - UIPickerView Delegate Method
    
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrLanguage.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrLanguage[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
        tfLanguageTitle.text = arrLanguage[row]
    }
    //============================================================================================//
    //============================================================================================//
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//=================================================================================================================//


class AddEditPublicationsController:UIViewController, UITextFieldDelegate {
    
    var indexOfData:Int = 0
    @IBOutlet weak var tfPublicationTitle: TextField!
    @IBOutlet weak var tfPublisher: TextField!
    @IBOutlet weak var tfDate: TextField!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var tfUrl: TextField!
    
    
    func dateTextInputPressed(sender: UITextField) {
        
        //Create the view
        let datePickerView  : UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.tag = sender.tag
        
        if self.tfDate.text != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if dateFormatter.date(from: tfDate.text!) != nil {
                let currentDate = dateFormatter.date(from: self.tfDate.text!)!
                datePickerView.date = currentDate
            }
            else {
                tfDate.text = ""
                KVClass.ShowNotification("", "Please Select Proper Date")
                return
            }
            
        }
        else
        {
            datePickerView.date = Date()
        }
        // datePickerView.maximumDate = Date()
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.handleDatePicker(sender:)), for: UIControlEvents.valueChanged)
        
        handleDatePicker(sender: datePickerView) // Set the date on start.
    }
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        tfDate.text = dateFormatter.string(from: sender.date)
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.tfDate {
            dateTextInputPressed(sender: textField)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if dateFormatter.date(from: tfDate.text!) != nil {
            }
            else {
                tfDate.text = ""
                KVClass.ShowNotification("", "Please Select Proper Date")
                return
            }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfPublicationTitle || textField == tfPublisher {
            let maxLength = 250
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
    }
    //=============================================================================================================//
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tfUrl.setDivider()
        self.tfDate.setDivider()
        self.tfPublisher.setDivider()
        self.tfPublicationTitle.setDivider()
        
        tfPublicationTitle.delegate = self
        tfPublisher.delegate = self
        
        let toolBar = UIToolbar().ToolbarPikerteast(mySelect: #selector(dismissPicker), delegate: self)
        tfDate.delegate = self
        tfDate.inputAccessoryView = toolBar
        tfDate.addTarget(self, action: #selector(self.dateTextInputPressed(sender:)), for: .allTouchEvents)
        
        if indexOfData == 5001 {
            self.btnDelete.isHidden = true
        } else {
            self.btnDelete.isHidden = false
            
            if let strValue = Singleton.sharedInstance.data_details?.publicationDetail![indexOfData].publication_title, strValue.count > 0 {
                tfPublicationTitle.text = strValue
            }
            if let strValue = Singleton.sharedInstance.data_details?.publicationDetail![indexOfData].publisher_name, strValue.count > 0 {
                tfPublisher.text = strValue
            }
            if let strValue = Singleton.sharedInstance.data_details?.publicationDetail![indexOfData].date, strValue.count > 0 {
                
                let dateformat = DateFormatter()
                dateformat.dateFormat = "yyyy-MM-dd"
                if dateformat.date(from: strValue) != nil
                {
                    let date = dateformat.date(from: strValue)!
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let stdate = dateformat.string(from: date)
                    tfDate.text = stdate
                }
                else
                {
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let date = dateformat.date(from: strValue)!
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let stdate = dateformat.string(from: date)
                    tfDate.text = stdate
                }
                
                
            }
            if let strValue = Singleton.sharedInstance.data_details?.publicationDetail![indexOfData].url, strValue.count > 0 {
                tfUrl.text = strValue
            }
        }
    }
    
    func testDone() {
        self.view.endEditing(true)
    }
    //============================================================================================//
    
    
    //============================================================================================//
    //MARK :- UIButton Action Method
    
    @IBAction func btnDeleteTapped(_ sender: Any) {
        
        let alertView = UIAlertController(title: "", message: "Are you sure, You want to delete this Publication from your Profile?", preferredStyle: .alert)
        let actionNo = UIAlertAction(title: "No", style: .cancel, handler:nil)
        
        let actionYes = UIAlertAction(title: "Yes", style: .default, handler: { (alert) in
            if let ratingValue = Singleton.sharedInstance.data_details?.publicationDetail![self.indexOfData].publication_id, ratingValue > 0{
                kUserDefults(true as AnyObject, key: "isprofileupdate")
               KVClass.KVShowLoader()
                Alamofire.request("\(self.getPrefixURL())delete_publication_detail/\(ratingValue)", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
                    KVClass.KVHideLoader()
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            print(response.result.value as Any)
                            KVAppDataServieces.shared.deleteProfileData(.publication_detail, self.indexOfData)
                            NotificationCenter.default.post(name: Notification.Name("UPDATEPROFILE"), object: nil)
                        }
                        self.navigationController?.popViewController(animated: true)
                        break
                        
                    case .failure(_):
                        print(response.result.error as Any)
                        self.navigationController?.popViewController(animated: true)
                        break
                    }
                }
            }
        })
        alertView.addAction(actionNo)
        alertView.addAction(actionYes)
        self.present(alertView, animated: true, completion: nil)
        
        
    }
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Validation function
    func isValidData() -> Bool{
        if tfPublicationTitle.text == "" {
            KVClass.ShowNotification("", "Please Enter Publication Title")
            return false;
        }
        else if tfPublisher.text == "" {
            KVClass.ShowNotification("", "Please Enter Publisher Name")
            return false;
        }
        else if tfDate.text == "" {
            KVClass.ShowNotification("", "Please Enter Date")
            return false;
        }
        
        if !(tfUrl.text == "") {
            if (tfUrl.text?.isValidateUrl(urlString: tfUrl.text!))! {
                return true
            }
            KVClass.ShowNotification("", "Please Enter Proper URL")
            return false
        }
        
        return true
    }
    
    @IBAction func btnDoneTapped(_ sender: Any) {
        
        self.view.endEditing(true)

        if isValidData() == false {
            return
        }
        else {
        
            var dictParameter:[String:Any] = [:]
            var strURL:String = "user_publication_profile_add"
            
            dictParameter["user_id"] = KVAppDataServieces.shared.LoginUserID()
            dictParameter["publication_title"] = tfPublicationTitle.text!
            dictParameter["date"] = ""
            if (tfDate.text ?? "").count > 0 {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                
                let date = dateFormatter.date(from: tfDate.text!)!
                dateFormatter.dateFormat = "yyyy-MM-dd"
                dictParameter["date"] = dateFormatter.string(from: date)
            }
            
            dictParameter["publisher_name"] = tfPublisher.text
            dictParameter["url"] = tfUrl.text
            
            if indexOfData != 5001 {
                
                if let strValue = Singleton.sharedInstance.data_details?.publicationDetail![indexOfData].publication_id, strValue > 0 {
                    dictParameter["publication_id"] = strValue
                    strURL = "user_publication_profile_edit"
                }
            }
           KVClass.KVShowLoader()
            Alamofire.request("\(self.getPrefixURL())\(strURL)", method: .post, parameters: dictParameter, encoding: JSONEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
                kUserDefults(true as AnyObject, key: "isprofileupdate")
                KVClass.KVHideLoader()
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value{
                        
                        if let datadict = (data as? NSDictionary ?? [:])["data"] as? NSDictionary
                        {
                            KVAppDataServieces.shared.updateProfile(.publication_detail, datadict, self.indexOfData)
                            NotificationCenter.default.post(name: Notification.Name("UPDATEPROFILE"), object: nil)
                        }
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                    break
                    
                case .failure(_):
                    self.navigationController?.popViewController(animated: true)
                    break
                }
            }
        }
    }
    //============================================================================================//
    //============================================================================================//
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//================================================================================================================//

class AddEditExperienceController:UIViewController, UITextFieldDelegate {
    
    var indexOfData:Int = 0
    
    @IBOutlet weak var tfTitle: TextField!
    @IBOutlet weak var tfStartDate: TextField!
    @IBOutlet weak var tfCompany: TextField!
    @IBOutlet weak var lblLoc_Title: UILabel!
    @IBOutlet weak var lbl_Location: UILabel!
    @IBOutlet weak var tfEndDate: TextField!
    @IBOutlet weak var btnDelete: UIButton!
    var placesClient: GMSPlacesClient!
    
    
    //============================================================================================//
    //MARK : - UITextField Deletegate Method
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfStartDate || textField == tfEndDate {
            self.dateTextInputPressed(sender: textField)
        }
//        else if textField == tfLocation {
//            textField.endEditing(true)
//            self.OpenPlacePickerForSelectLocation()
//            return false
//        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == tfLocation {
//            textField.endEditing(true)
////            self.OpenPlacePickerForSelectLocation()
//        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfStartDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if dateFormatter.date(from: textField.text!) != nil {
            }
            else {
                tfStartDate.text = ""
                KVClass.ShowNotification("", "Please Select Proper Date")
                return
            }
        }
        if textField == tfEndDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if dateFormatter.date(from: textField.text!) != nil {
            }
            else {
                tfEndDate.text = ""
                KVClass.ShowNotification("", "Please Select Proper Date")
                return
            }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfTitle || textField == tfCompany {
            let maxLength = 250
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
    }
    
    //============================================================================================//
    //============================================================================================//
    
    func tapOnLocationLabel(sender:UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        
        let center = CLLocationCoordinate2D(latitude: Double(KVClass.shareinstance().Lattitude)!, longitude: Double(KVClass.shareinstance().Longitude)!)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePicker(config: config)
        
        placePicker.pickPlace(callback: {(place, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            
            if let place = place {
                self.lblLoc_Title.isHidden = false
                self.lbl_Location.textColor = UIColor.black
                self.lbl_Location.text = (place.formattedAddress ?? "")
                
                if (place.formattedAddress ?? "") == ""
                {
                    self.lbl_Location.text = "Location*"
                    self.lblLoc_Title.isHidden = true
                    self.lbl_Location.textColor = UIColor.lightGray
                }
            }
            
//            if let place = place {
//                var str_place = place.formattedAddress?.components(separatedBy: ",")
//                if (str_place?.count ?? 0)! > 2{
//                str_place?.removeLast()
//                str_place?.removeLast()
//                }
//               // str_place?.removeLast()
//
//                self.tfLocation.text = (str_place?.last ?? "")
//            }
            else {
                //self.lbl_adres.text = "No place selected"
                //self.addressLabel.text = ""
            }
        })
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblLoc_Title.isHidden = true
        lbl_Location.textColor = UIColor.lightGray
        self.tfTitle.setDivider()
        self.tfStartDate.setDivider()
        self.tfCompany.setDivider()
        //self.tfLocation.setDivider()
        self.tfEndDate.setDivider()
        
        tfTitle.delegate = self
        tfCompany.delegate = self
        
       let toolBar = UIToolbar().ToolbarPikerteast(mySelect: #selector(dismissPicker), delegate: self)
        tfStartDate.tag = 121
        tfStartDate.inputAccessoryView = toolBar
        tfStartDate.addTarget(self, action: #selector(self.dateTextInputPressed(sender:)), for: .allTouchEvents)
        
        tfEndDate.tag = 122
        tfEndDate.inputAccessoryView = toolBar
        tfEndDate.addTarget(self, action: #selector(self.dateTextInputPressed(sender:)), for: .allTouchEvents)
        
        tfStartDate.delegate = self
        tfEndDate.delegate = self
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapOnLocationLabel))
        lbl_Location.isUserInteractionEnabled = true
        lbl_Location.addGestureRecognizer(tap)
        
        
        if indexOfData == 5001 {
            self.btnDelete.isHidden = true
        } else {
            self.btnDelete.isHidden = false
            if let strTitle = Singleton.sharedInstance.data_details?.experienceDetail![indexOfData].title, strTitle.count > 0 {
                tfTitle.text = strTitle
            }
            
            if let strCompanyName = Singleton.sharedInstance.data_details?.experienceDetail![indexOfData].company_name, strCompanyName.count > 0 {
                tfCompany.text = strCompanyName
            }
            
            if let strLocation = Singleton.sharedInstance.data_details?.experienceDetail![indexOfData].location, strLocation.count > 0 {
                lbl_Location.text = strLocation
                lblLoc_Title.isHidden = false
                lbl_Location.textColor = UIColor.black
            }
            
            if let strExperienceStartDate = Singleton.sharedInstance.data_details?.experienceDetail![indexOfData].start_date, strExperienceStartDate.count > 0 {
                let dateformat = DateFormatter()
                dateformat.dateFormat = "yyyy-MM-dd"
                if dateformat.date(from: strExperienceStartDate) != nil
                {
                    let date = dateformat.date(from: strExperienceStartDate)!
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let stdate = dateformat.string(from: date)
                    tfStartDate.text = stdate
                }
                else
                {
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let date = dateformat.date(from: strExperienceStartDate)!
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let stdate = dateformat.string(from: date)
                    tfStartDate.text = stdate
                }
                
                
            }
            
            if let strExperienceEndDate = Singleton.sharedInstance.data_details?.experienceDetail![indexOfData].end_date, strExperienceEndDate.count > 0 {
                let dateformat = DateFormatter()
                dateformat.dateFormat = "yyyy-MM-dd"
                if dateformat.date(from: strExperienceEndDate) != nil
                {
                    let date = dateformat.date(from: strExperienceEndDate)!
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let stdate = dateformat.string(from: date)
                    tfEndDate.text = stdate
                }
                else
                {
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let date = dateformat.date(from: strExperienceEndDate)!
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let stdate = dateformat.string(from: date)
                    tfEndDate.text = stdate
                }
            }
        }
    }
    
    func testDone() {
        self.view.endEditing(true)
    }
    
    func dateTextInputPressed(sender: UITextField) {
        
        
        let datePickerView  : UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.tag = sender.tag
        datePickerView.maximumDate = Date()
        // inputView.addSubview(datePickerView) // add date picker to UIView
        
        if sender.text != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if dateFormatter.date(from: sender.text!) != nil {
                let currentDate:Date = dateFormatter.date(from: sender.text!)!
                datePickerView.setDate(currentDate, animated: false)
            }
            else {
                sender.text = ""
                KVClass.ShowNotification("", "Please Select Proper Date")
                return
            }
            
        }
        // datePickerView.maximumDate = Date()
        
        if sender.tag == 122 {
            if tfStartDate.text != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                if dateFormatter.date(from: tfStartDate.text!) != nil {
                    let currentDate:Date = dateFormatter.date(from: tfStartDate.text!)!
                    datePickerView.minimumDate = currentDate
                }
                else {
                    tfStartDate.text = ""
                    KVClass.ShowNotification("", "Please Select Proper Date")
                    return
                }
                
            }
        } else {
            if tfEndDate.text != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                if dateFormatter.date(from: tfStartDate.text!) != nil {
                }
                else {
                    tfStartDate.text = ""
                    KVClass.ShowNotification("", "Please Select Proper Date")
                    return
                }
                //let currentDate:Date = dateFormatter.date(from: tfEndDate.text!)!
                // datePickerView.maximumDate = currentDate
            }
        }
        
        
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.handleDatePicker(sender:)), for: UIControlEvents.valueChanged)
        
        handleDatePicker(sender: datePickerView) // Set the date on start.
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        if sender.tag == 121 {
            tfStartDate.text = dateFormatter.string(from: sender.date)
        } else {
            tfEndDate.text = dateFormatter.string(from: sender.date)
        }
    }
    //============================================================================================//
    
    
    //============================================================================================//
    //MARK :- UIButton Action Method
    
    @IBAction func btnDeleteTapped(_ sender: Any) {
        let alertView = UIAlertController(title: "", message: "Are you sure, You want to delete this Experience Detail from your Profile?", preferredStyle: .alert)
        let actionNo = UIAlertAction(title: "No", style: .cancel, handler:nil)
        
        let actionYes = UIAlertAction(title: "Yes", style: .default, handler: { (alert) in
            if let ratingValue = Singleton.sharedInstance.data_details?.experienceDetail![self.indexOfData].experience_id{
                kUserDefults(true as AnyObject, key: "isprofileupdate")
                KVClass.KVShowLoader()
                Alamofire.request("\(self.getPrefixURL())delete_experience_detail/\(ratingValue)", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
                    KVClass.KVHideLoader()
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            print(response.result.value as Any)
                            KVAppDataServieces.shared.deleteProfileData(.experience_detail, self.indexOfData)
                            NotificationCenter.default.post(name: Notification.Name("UPDATEPROFILE"), object: nil)
                        }
                        self.navigationController?.popViewController(animated: true)
                        break
                        
                    case .failure(_):
                        print(response.result.error as Any)
                        self.navigationController?.popViewController(animated: true)
                        break
                    }
                }
            }
        })
        alertView.addAction(actionNo)
        alertView.addAction(actionYes)
        
        self.present(alertView, animated: true, completion: nil)
    }
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Validation function
    func isValidData() -> Bool{
        if tfTitle.text == "" {
            KVClass.ShowNotification("", "Please Enter Title")
            return false;
        }
        else if tfCompany.text == "" {
            KVClass.ShowNotification("", "Please Enter Company Name")
            return false;
        }
        else if lbl_Location.text == "" || lbl_Location.text == "Location*" {
            KVClass.ShowNotification("", "Please Select your location")
            return false;
        }
        else if tfStartDate.text == "" {
            if !(tfEndDate.text == "") {
                KVClass.ShowNotification("", "Please Enter Start Date")
                return false;
            }
        }
        
        return true
    }
    
            
            
    
    @IBAction func btnDoneTapped(_ sender: Any) {
        
        
        if isValidData() == false {
            return
        }
        else {
            self.view.endEditing(true)
        
            var dictParameter:[String:Any] = [:]
            var strURL:String = "user_experience_profile_add"
            
            dictParameter["user_id"] = KVAppDataServieces.shared.LoginUserID()
            dictParameter["title"] = tfTitle.text!
            dictParameter["company_name"] = tfCompany.text!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            var dateStart:String = ""
            var dateEnd:String = ""
            
            if (tfStartDate.text ?? "").count > 0 {
                let date = dateFormatter.date(from: tfStartDate.text!)!
                dateFormatter.dateFormat = "yyyy-MM-dd"
                dateStart = dateFormatter.string(from: date)
            }
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            if(tfEndDate.text ?? "").count > 0 {
                let date = dateFormatter.date(from: tfEndDate.text!)!
                dateFormatter.dateFormat = "yyyy-MM-dd"
                dateEnd = dateFormatter.string(from: date)
            }
            
            dictParameter["start_date"] = dateStart
            dictParameter["end_date"] = dateEnd
            dictParameter["location"] = lbl_Location.text!
            
            if indexOfData != 5001 {
                if let strValue = Singleton.sharedInstance.data_details?.experienceDetail![indexOfData].experience_id, strValue > 0 {
                    dictParameter["experience_id"] = strValue
                    strURL = "user_experience_profile_edit"
                }
            }
            print(dictParameter)
            KVClass.KVShowLoader()
            Alamofire.request("\(self.getPrefixURL())\(strURL)", method: .post, parameters: dictParameter, encoding: JSONEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
                kUserDefults(true as AnyObject, key: "isprofileupdate")
                KVClass.KVHideLoader()
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value{
                        
                        if let datadict = (data as? NSDictionary ?? [:])["data"] as? NSDictionary
                        {
                            KVAppDataServieces.shared.updateProfile(.experience_detail, datadict, self.indexOfData)
                            NotificationCenter.default.post(name: Notification.Name("UPDATEPROFILE"), object: nil)
                        }
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                    break
                    
                case .failure(_):
                    self.navigationController?.popViewController(animated: true)
                    break
                }
            }
        }
    }
    //============================================================================================//
    //============================================================================================//
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//================================================================================================================//

class AddEditContactInfoController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    var indexOfData:Int = 0
    
    @IBOutlet weak var tfType: TextField!
    @IBOutlet weak var tfEmail: TextField!
    @IBOutlet weak var tfPhone: TextField!
    @IBOutlet weak var btnCountryCode: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    var pickerView: UIPickerView!
    let arrLanguage:[String] = ["Personal", "Business", "Others"]
    
    
    func dateTextInputPressed(sender: UITextField) {
        //Create the view
        let inputView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: 190.0))
        
        pickerView = UIPickerView.init(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: 190.0))
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = sender.tag
        inputView.addSubview(pickerView) // add date picker to UIView
        
        if arrLanguage.contains((sender.text ?? "")) {
            pickerView.selectRow(arrLanguage.index(of: sender.text!)!, inComponent: 0, animated: false)
        } else {
            pickerView.selectRow(0, inComponent: 0, animated: false)
        }
        
        
        sender.inputView = inputView
        
        handleDatePicker(sender: pickerView) // Set the date on start.
    }
    
    func donepress(_ sender: UITextField)
    {
        self.tfPhone.resignFirstResponder()
    }
    
    //============================================================================================//
    //MARK : - UIPickerVire Delegate Method
    
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrLanguage.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrLanguage[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
        tfType.text = arrLanguage[row]
    }
    //============================================================================================//
    //============================================================================================//
    
    func handleDatePicker(sender: UIPickerView) {
    }
    
    //============================================================================================//
    //MARK :- UITextField Delegate Method
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfType {
            self.dateTextInputPressed(sender: textField)
        }
        else if textField == tfPhone {
            textField.placeholder = "Phone"
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfPhone {
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_NUMBERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfPhone {
            if tfPhone.text == "" {
                textField.placeholder = ""
            }
        }
    }
    
    func didChangeTextPhone() {
        if !(tfPhone.text == "") {
            let strTest = tfPhone.text!
            let index = strTest.index(strTest.startIndex, offsetBy: 0)
            let firstCharcter = strTest[index]
            if firstCharcter == "0" {
                tfPhone.text = ""
                return
            }
        }
    }
    //============================================================================================//
    //============================================================================================//
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tfEmail.setDivider()
        self.tfType.setDivider()
        self.tfPhone.setPhoneDivider()
        
        
//        let viewLeft = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 45, height: 35))
//        viewLeft.backgroundColor = UIColor.clear
//        let btnPick = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 45, height: 35))
//        btnPick.setTitle(app_Delegate.CURRENT_COUNTRY_CODE, for: .normal)
//        btnPick.setTitleColor(UIColor.black, for: .normal)
//        btnPick.titleLabel?.font =  UIFont(name: "lato-Regular", size: 13)
//        btnPick.addTarget(self, action: #selector(self.btnCountryCodeTapped(_:)), for: .touchUpInside)
//        viewLeft.addSubview(btnPick)
//        
//        self.tfPhone.leftView = viewLeft
//        self.tfPhone.leftViewMode = .always
        
        self.tfPhone.delegate = self
        self.tfEmail.delegate = self
        
        self.tfPhone.addTarget(self, action: #selector(self.didChangeTextPhone), for: UIControlEvents.editingChanged)
        //==============================================================//
        //Get Default Country Code
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print(countryCode)
            btnCountryCode.setTitle(app_Delegate.CURRENT_COUNTRY_CODE, for: .normal)
        }
        //==============================================================//
        //==============================================================//
        
        let toolBar = UIToolbar().ToolbarPikerteast(mySelect: #selector(dismissPicker), delegate: self)
        self.tfType.text = self.arrLanguage[0]
        tfType.tag = 121
        tfType.inputAccessoryView = toolBar
        tfType.addTarget(self, action: #selector(self.dateTextInputPressed(sender:)), for: .allTouchEvents)
        tfType.delegate = self
        if indexOfData == 5001 {
            self.btnDelete.isHidden = true
            self.tfType.text = self.arrLanguage[0]
            
            let toolbar = UIToolbar().ToolbarPiker(mySelect: #selector(self.donepress(_:)))
            tfPhone.inputAccessoryView = toolbar
            tfPhone.keyboardType = .phonePad
        } else {
            self.btnDelete.isHidden = false
            if let strContactType = Singleton.sharedInstance.data_details?.contactDetails![indexOfData].contact_type, strContactType.count > 0 {
                tfType.text = strContactType
            }
            else {
                tfType.text = "Personal"
            }
            
            if let strEmail = Singleton.sharedInstance.data_details?.contactDetails![indexOfData].email, strEmail.count > 0 {
                tfEmail.text = strEmail
            }
            if let strPhone = Singleton.sharedInstance.data_details?.contactDetails![indexOfData].phone, strPhone.count > 0 {
                
                let arrP = strPhone.components(separatedBy: " ")
                if arrP.count == 2 {
                    tfPhone.text = arrP.last!
                    self.btnCountryCode.setTitle(arrP.first!, for: .normal)
                }
                else {
                    tfPhone.text = strPhone
                    self.btnCountryCode.setTitle(app_Delegate.CURRENT_COUNTRY_CODE, for: .normal)
                }

                let toolbar = UIToolbar().ToolbarPiker(mySelect: #selector(self.donepress(_:)))
                tfPhone.inputAccessoryView = toolbar
                tfPhone.keyboardType = .phonePad
            }
        }
        
        
    }
    
    func testDone() {
        self.view.endEditing(true)
    }
    //============================================================================================//
    //MARK :- UIButton Action Mehod
    @IBAction func btnCountryCodeTapped(_ sender: Any) {
        self.view.endEditing(true)
        app_Delegate.app_CountryPickpopup { (objCountry, isSuccess) in
            print(objCountry?.phoneCode ?? "")
            print(objCountry?.code ?? "")
            print(objCountry?.name ?? "")
            self.btnCountryCode.setTitle(objCountry?.phoneCode ?? "", for: .normal)
            debugPrint("check this out!! [\(objCountry?.name! ?? "0")]")
        }
    }
    
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDoneTapped(_ sender: Any) {
        if tfType.text == "" {
            KVClass.ShowNotification("", "Please select contact type")
            return
        }
        if !(arrLanguage.contains(tfType.text!)) {
            KVClass.ShowNotification("", "Please select proper contact type")
            return
        }
        if tfEmail.text == "" {
            KVClass.ShowNotification("", "Please enter email")
            return
        }
        if !(tfEmail.text?.isValidEmail())! {
            KVClass.ShowNotification("", "Please enter valid Email!")
            return
        }
        if tfPhone.text == "" {
            KVClass.ShowNotification("", "Please enter phone")
            return
        }
        if !(tfPhone.text?.isPhoneValid())! {
            KVClass.ShowNotification("", "Please enter valid Phone!")
            return
        }
        
        var dictParameter:[String:Any] = [:]
        var strURL:String = "user_contact_profile_add"
            
        dictParameter["user_id"] = KVAppDataServieces.shared.LoginUserID()
        dictParameter["contact_type"] = tfType.text
        dictParameter["email"] = tfEmail.text
        dictParameter["phone"] = (self.btnCountryCode.titleLabel?.text)! + " " + tfPhone.text!
            
        if indexOfData != 5001 {
            if let strValue = Singleton.sharedInstance.data_details?.contactDetails![indexOfData].contact_id, strValue > 0 {
                dictParameter["contact_id"] = strValue
                strURL = "user_contact_profile_edit"
            }
        }
        KVClass.KVShowLoader()
        Alamofire.request("\(self.getPrefixURL())\(strURL)", method: .post, parameters: dictParameter, encoding: JSONEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
                
            KVClass.KVHideLoader()
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                        
                    if let datadict = (data as? NSDictionary ?? [:])["data"] as? NSDictionary
                    {
                        KVAppDataServieces.shared.updateProfile(.contact_detail, datadict, self.indexOfData)
                            
                        print(KVAppDataServieces.shared.UserValue(.profile_data) as? NSDictionary ?? [:])
                            
                        Timer.scheduledTimer(withTimeInterval: 0.15, repeats: false, block: { (tim) in
                                tim.invalidate()
                            self.navigationController?.popViewController(animated: true)
                        })
                        NotificationCenter.default.post(name: Notification.Name("UPDATEPROFILE"), object: nil)
                    }
                }
                break
                
            case .failure(_):
                self.navigationController?.popViewController(animated: true)
                break
            }
        }
    }
    
    
    @IBAction func btnDeleteTapped(_ sender: Any) {
        
        let alertView = UIAlertController(title: "", message: "Are you sure, You want to delete this ContactInfo?", preferredStyle: .alert)
        let actionNo = UIAlertAction(title: "No", style: .cancel, handler:nil)
        
        let actionYes = UIAlertAction(title: "Yes", style: .default, handler: { (alert) in
            if let ratingValue = Singleton.sharedInstance.data_details?.contactDetails![self.indexOfData].contact_id, ratingValue > 0{
                
               KVClass.KVShowLoader()
                Alamofire.request("\(self.getPrefixURL())delete_contact_detail/\(ratingValue)", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
                    
                    KVClass.KVHideLoader()
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            KVAppDataServieces.shared.deleteProfileData(.contact_detail, self.indexOfData)
                            print(response.result.value as Any)
                            NotificationCenter.default.post(name: Notification.Name("UPDATEPROFILE"), object: nil)
                        }
                        self.navigationController?.popViewController(animated: true)
                        break
                        
                    case .failure(_):
                        print(response.result.error as Any)
                        self.navigationController?.popViewController(animated: true)
                        break
                        
                    }
                }
                
            }
        })
        alertView.addAction(actionNo)
        alertView.addAction(actionYes)
        self.present(alertView, animated: true, completion: nil)
    }
    //============================================================================================//
    //============================================================================================//
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//=================================================================================================================//
class AddEditEducationController: UIViewController, UITextFieldDelegate {
    
    var indexOfData:Int = 0
    
    @IBOutlet weak var tfSchoolName: TextField!
    
    @IBOutlet weak var tfDegree: TextField!
    @IBOutlet weak var tfStartDate: TextField!
    @IBOutlet weak var tfEndDate: TextField!
    @IBOutlet weak var tfActivities: TextField!
    @IBOutlet weak var tfFieldOfStudy: TextField!
    @IBOutlet weak var btnDelete: UIButton!
    

    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.tfStartDate || textField == self.tfEndDate {
            dateTextInputPressed(sender: textField)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.tfStartDate {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            if formatter.date(from: textField.text!) != nil {
            }
            else {
                self.tfStartDate.text = ""
                KVClass.ShowNotification("", "Please Select Proper Start Date")
                return
            }
        }
        else if textField == self.tfEndDate {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            if formatter.date(from: textField.text!) != nil {
            }
            else {
                self.tfEndDate.text = ""
                KVClass.ShowNotification("", "Please Select Proper End Date")
                return
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfSchoolName || textField == tfDegree || textField == tfFieldOfStudy || textField == tfActivities {
            let maxLength = 250
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tfSchoolName.setDivider()
        self.tfDegree.setDivider()
        self.tfStartDate.setDivider()
        self.tfEndDate.setDivider()
        self.tfActivities.setDivider()
        self.tfFieldOfStudy.setDivider()
        
        self.tfSchoolName.delegate = self
        self.tfDegree.delegate = self
        self.tfFieldOfStudy.delegate = self
        self.tfActivities.delegate = self
        
        
        let toolBar = UIToolbar().ToolbarPikerteast(mySelect: #selector(dismissPicker), delegate: self)
        tfStartDate.tag = 121
        tfStartDate.inputAccessoryView = toolBar
        tfStartDate.addTarget(self, action: #selector(self.dateTextInputPressed(sender:)), for: .allTouchEvents)
        tfStartDate.delegate = self
        
        tfEndDate.tag = 122
        tfEndDate.inputAccessoryView = toolBar
        tfEndDate.addTarget(self, action: #selector(self.dateTextInputPressed(sender:)), for: .allTouchEvents)
        tfEndDate.delegate = self
        
        if indexOfData == 5001 {
            self.btnDelete.isHidden = true
        } else {
            self.btnDelete.isHidden = false
            if let strSchoolName = Singleton.sharedInstance.data_details?.educationDetails![indexOfData].school_name, strSchoolName.count > 0 {
                tfSchoolName.text = strSchoolName
            }
            
            if let strValue = Singleton.sharedInstance.data_details?.educationDetails![indexOfData].field_of_study, strValue.count > 0 {
                tfFieldOfStudy.text = strValue
            }
            
            if let strValue = Singleton.sharedInstance.data_details?.educationDetails![indexOfData].degree, strValue.count > 0 {
                tfDegree.text = strValue
            }
            
            if let strEducateStartDate = Singleton.sharedInstance.data_details?.educationDetails![indexOfData].start_date, strEducateStartDate.count > 0 {
                //tfStartDate.text = strEducateStartDate
                let dateformat = DateFormatter()
                dateformat.dateFormat = "yyyy-MM-dd"
                if dateformat.date(from: strEducateStartDate) != nil
                {
                    let date = dateformat.date(from: strEducateStartDate)!
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let stdate = dateformat.string(from: date)
                    tfStartDate.text = stdate
                }
                else
                {
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let date = dateformat.date(from: strEducateStartDate)!
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let stdate = dateformat.string(from: date)
                    tfStartDate.text = stdate
                }
            }
            
            if let strEducateEndDate = Singleton.sharedInstance.data_details?.educationDetails![indexOfData].end_date, strEducateEndDate.count > 0 {
                let dateformat = DateFormatter()
                dateformat.dateFormat = "yyyy-MM-dd"
                if dateformat.date(from: strEducateEndDate) != nil
                {
                    let date = dateformat.date(from: strEducateEndDate)!
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let stdate = dateformat.string(from: date)
                    tfEndDate.text = stdate
                }
                else
                {
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let date = dateformat.date(from: strEducateEndDate)!
                    dateformat.dateFormat = "dd/MM/yyyy"
                    let stdate = dateformat.string(from: date)
                    tfEndDate.text = stdate
                }
            } //activities
            
            if let strEducateEndDate = Singleton.sharedInstance.data_details?.educationDetails![indexOfData].activities, strEducateEndDate.count > 0 {
                tfActivities.text = strEducateEndDate
            }
        }
    }
    
    func testDone() {
        self.view.endEditing(true)
    }
    
    func dateTextInputPressed(sender: UITextField) {
        
        //Create the view
        ///let inputView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: 190.0))
        
        let datePickerView  : UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.tag = sender.tag
        datePickerView.maximumDate = Date()
        // inputView.addSubview(datePickerView) // add date picker to UIView
        
        if sender.text != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if dateFormatter.date(from: sender.text!) != nil {
                let currentDate:Date = dateFormatter.date(from: sender.text!)!
                datePickerView.setDate(currentDate, animated: false)
            }
            else {
                sender.text = ""
                KVClass.ShowNotification("", "Please select Proper Date")
                return
            }
            
        }
        
        // datePickerView.maximumDate = Date()
        
        if sender.tag == 122 {
            if tfStartDate.text != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                if dateFormatter.date(from: tfStartDate.text!) != nil {
                    let currentDate:Date = dateFormatter.date(from: tfStartDate.text!)!
                    datePickerView.minimumDate = currentDate
                }
                else {
                    tfStartDate.text = ""
                    KVClass.ShowNotification("", "Please select Proper Date")
                    return
                }
                
            }
        } else {
            if tfEndDate.text != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                if dateFormatter.date(from: tfEndDate.text!) != nil {
                }
                else {
                    tfEndDate.text = ""
                    KVClass.ShowNotification("", "Please select Proper Date")
                    return
                }
                //let currentDate:Date = dateFormatter.date(from: tfEndDate.text!)!
                // datePickerView.maximumDate = currentDate
            }
        }
        
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.handleDatePicker(sender:)), for: UIControlEvents.valueChanged)
        
        handleDatePicker(sender: datePickerView) // Set the date on start.
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        if sender.tag == 121 {
            tfStartDate.text = dateFormatter.string(from: sender.date)
        } else {
            tfEndDate.text = dateFormatter.string(from: sender.date)
        }
    }
    
    @IBAction func btnDeleteClicked(_ sender: Any) {
        
        let alertView = UIAlertController(title: "", message: "Are you sure, You want to delete this Education Information?", preferredStyle: .alert)
        let actionNo = UIAlertAction(title: "No", style: .cancel, handler:nil)
        
        let actionYes = UIAlertAction(title: "Yes", style: .default, handler: { (alert) in
            if let ratingValue = Singleton.sharedInstance.data_details?.educationDetails![self.indexOfData].education_id, ratingValue > 0{
                kUserDefults(true as AnyObject, key: "isprofileupdate")
                KVClass.KVShowLoader()
                Alamofire.request("\(self.getPrefixURL())delete_education_detail/\(ratingValue)", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
                    KVClass.KVHideLoader()
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            
                            KVAppDataServieces.shared.deleteProfileData(.education_detail, self.indexOfData)
                            NotificationCenter.default.post(name: Notification.Name("UPDATEPROFILE"), object: nil)
                        }
                        self.navigationController?.popViewController(animated: true)
                        break
                        
                    case .failure(_):
                        print(response.result.error as Any)
                        self.navigationController?.popViewController(animated: true)
                        break
                        
                    }
                }
            }
        })
        alertView.addAction(actionNo)
        alertView.addAction(actionYes)
        self.present(alertView, animated: true, completion: nil)
    }
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Validation function
    func isValidData() -> Bool{
        if tfSchoolName?.text! == "" {
            KVClass.ShowNotification("", "Please Enter your School Name!")
            return false;
        }
        else if tfDegree?.text! == "" {
            KVClass.ShowNotification("", "Please Enter your Degree!")
            return false;
        }
        else if tfFieldOfStudy?.text! == "" {
            KVClass.ShowNotification("", "Please Enter your Study!")
            return false;
        }
        else if tfStartDate?.text! == "" {
            if !(tfEndDate?.text! == "") {
                KVClass.ShowNotification("", "Please Enter Start Date!")
                return false;
            }
        }
        
        return true
    }
    
    
    
    @IBAction func btnDoneTapped(_ sender: Any) {
        
        if isValidData() == false {
            return
        }
        else {
            var dictParameter:[String:Any] = [:]
            var strURL:String = "user_education_profile_add"
            
            dictParameter["user_id"] = KVAppDataServieces.shared.LoginUserID()
            dictParameter["school_name"] = tfSchoolName.text
            dictParameter["degree"] = tfDegree.text
            dictParameter["field_of_study"] = tfFieldOfStudy.text
            dictParameter["activities"] = self.tfActivities.text
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            var dateStart:String = ""
            var dateEnd:String = ""
            
            if (tfStartDate.text ?? "").count > 0 {
                let date = dateFormatter.date(from: tfStartDate.text!)!
                dateFormatter.dateFormat = "yyyy-MM-dd"
                dateStart = dateFormatter.string(from: date)
            }
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            if(tfEndDate.text ?? "").count > 0 {
                let date = dateFormatter.date(from: tfEndDate.text!)!
                dateFormatter.dateFormat = "yyyy-MM-dd"
                dateEnd = dateFormatter.string(from: date)
            }
            
            dictParameter["start_date"] = dateStart
            dictParameter["end_date"] = dateEnd
            
            if indexOfData != 5001 {
                if let strValue = Singleton.sharedInstance.data_details?.educationDetails![indexOfData].education_id, strValue > 0 {
                    dictParameter["education_id"] = strValue
                    strURL = "user_education_profile_edit"
                }
            }
            KVClass.KVShowLoader()
            Alamofire.request("\(self.getPrefixURL())\(strURL)", method: .post, parameters: dictParameter, encoding: JSONEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
                
                KVClass.KVHideLoader()
                self.navigationController?.popViewController(animated: true)
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value{
                        if let datadict = data as? NSDictionary
                        {
                            let dict = datadict["data"] as? NSDictionary ?? [:]
                            KVAppDataServieces.shared.updateProfile(.education_detail, dict, self.indexOfData)
                            print(dict)
                            NotificationCenter.default.post(name: Notification.Name("UPDATEPROFILE"), object: nil)
                        }
                    }
                    break
                    
                case .failure(_):
                    break
                }
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//===========================================================================================================//
class ProfileEducationController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate, TOCropViewControllerDelegate {
    
    var indexOfData:Int = 0
    var isUpdatePhoto:Bool = false
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var tfFirstName: TextField!
    @IBOutlet weak var tfLastName: TextField!
    @IBOutlet weak var tfStory: TextView!
    @IBOutlet weak var lblTxtChar: UILabel!
    @IBOutlet weak var lblStorTitle: UILabel!
    @IBOutlet weak var lblDeviderStory: UILabel!
    
    var ImagePicker : UIImagePickerController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblStorTitle.text = ""
        self.tfFirstName.delegate = self
        self.tfLastName.delegate = self
        self.tfStory.delegate = self
        
        self.tfStory.setDivider()
        self.tfLastName.setDivider()
        self.tfFirstName.setDivider()
        
        
        self.ImagePicker = UIImagePickerController.init()
        self.ImagePicker.delegate = self
        
        self.imgProfile.image = UIImage.init(named: "user_profile_pic")
        if let profileImageURL = Singleton.sharedInstance.data_details?.basicDetails?.profile_pic, profileImageURL.count > 0 {
            imgProfile.setImageWith(URL.init(string: profileImageURL)!, placeholderImage: #imageLiteral(resourceName: "user_profile_pic"))
        }
        
        if let firstName = Singleton.sharedInstance.data_details?.basicDetails?.firstname, firstName.count > 0 {
            tfFirstName.text = firstName
        }
        
        if let lastName = Singleton.sharedInstance.data_details?.basicDetails?.lastname, lastName.count > 0 {
            tfLastName.text =  lastName
        }
        
        if let story = Singleton.sharedInstance.data_details?.basicDetails?.qk_story, story.count > 0 {
            tfStory.text = story
            lblStorTitle.text = "Story"
            let currentString: NSString = story as NSString
            let newChar = 400 - currentString.length
            lblTxtChar.text = newChar.description
            tfStory.setContentOffset(CGPoint.zero, animated: false)
        }
        
        
        //==============================================================================================//
        //==============================================================================================//
        let toolBar = UIToolbar().ToolbarPikerteast(mySelect: #selector(dismissPicker), delegate: self)
        tfStory.inputAccessoryView = toolBar
        //==============================================================================================//
        //==============================================================================================//
        // Do any additional setup after loading the view.
    }
    
    func CloseKeyboardPress(_ sender: UITextView) {
        self.tfStory.resignFirstResponder()
    }
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnDoneTapped(_ sender: Any) {
        
        if self.tfLastName.text != "" && self.tfFirstName.text != ""
            
        {
            var strBase64:String = ""
            if let base64String = UIImageJPEGRepresentation(self.imgProfile.image!, 0.25)?.base64EncodedString() {
                // Upload base64String to your database
                strBase64 = base64String
            }
            
            let ParamDict = NSMutableDictionary()
            
            var parameters: [String : String] = [:]
            if isUpdatePhoto == true {
                
                ParamDict["user_id"] = KVAppDataServieces.shared.LoginUserID()
                ParamDict["firstname"] = self.tfFirstName.text
                ParamDict["lastname"] = self.tfLastName.text
                ParamDict["qk_story"] = self.tfStory.text
                ParamDict["profile_picture"] = strBase64
                
                parameters = ["user_id": "\(KVAppDataServieces.shared.LoginUserID())", "firstname":tfFirstName.text!, "lastname":tfLastName.text!, "qk_story":tfStory.text!, "profile_picture": strBase64]
            } else {
                
                ParamDict["user_id"] = KVAppDataServieces.shared.LoginUserID()
                ParamDict["firstname"] = self.tfFirstName.text
                ParamDict["lastname"] = self.tfLastName.text
                ParamDict["qk_story"] = self.tfStory.text
                //ParamDict["profile_picture"] = strBase64
                
                parameters = ["user_id": "\(KVAppDataServieces.shared.LoginUserID())", "firstname":tfFirstName.text!, "lastname":tfLastName.text!, "qk_story":tfStory.text!]
            }
            
            KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: ParamDict, api: "change_profile_pic", arrFlag: false, Hendler: { (json, status) in

                if status == 1
                {
                    if let datadict = json["data"] as? NSDictionary
                    {
                        
                        print(datadict)
                       // app_Delegate.isBackChange = true
                        
                        if let profiledict = (KVAppDataServieces.shared.UserValue(.profile_data) as? NSDictionary ?? [:])["basic_detail"] as? NSDictionary
                        {
                            let valuedict = NSMutableDictionary.init(dictionary: profiledict as! [AnyHashable : AnyHashable])
                            valuedict["firstname"] = datadict["firstname"]
                            valuedict["lastname"] = datadict["lastname"]
                            valuedict["profile_pic"] = datadict["profile_pic"]
                            valuedict["qk_story"] = datadict["qk_story"]
                            
                            let fulname = "\(datadict["firstname"]!) \(datadict["lastname"]!)"
                            
                            KVAppDataServieces.shared.update(.fullname, fulname)
                            KVAppDataServieces.shared.update(.profile_pic, datadict["profile_pic"] as? String ?? "")
                            
                            KVAppDataServieces.shared.updateProfile(.basic_detail, valuedict, 0)
                             NotificationCenter.default.post(name: Notification.Name("UPDATEPROFILE"), object: nil)
                        }
                    }
                }
                
                self.navigationController?.popViewController(animated: true)
                
            })
        }
        else
        {
            KVClass.ShowNotification("", "Firstname and Lastname are required !")
        }
    }
    
    //============================================================================================//
    //MARK :- UITextField Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfFirstName || textField == tfLastName {
            let maxLength = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
            
//        else if textField == tfStory {
//            let maxLength = 75
//            let currentString: NSString = textField.text! as NSString
//            let newString: NSString =
//                currentString.replacingCharacters(in: range, with: string) as NSString
//            if newString.length > maxLength {
//                return false
//            }
//            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
//            let filtered = string.components(separatedBy: cs).joined(separator: "")
//
//            return (string == filtered)
//        }
        return true
    }
    
    
    //============================================================================================//
    //MARK :- UITextView Delegate Method
    func textViewDidBeginEditing(_ textView: UITextView) {
        lblStorTitle.text = "Story"
        lblStorTitle.textColor = UIColor.init(hex: "28AFB0")
        lblDeviderStory.backgroundColor = UIColor.init(hex: "28AFB0")
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            lblStorTitle.text = ""
        }
        lblStorTitle.textColor = UIColor.lightGray
        lblDeviderStory.backgroundColor = UIColor.lightGray
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == tfStory {
            
            let maxLength = 400
            let currentString: NSString = textView.text! as NSString
            let new_Str: NSString = currentString.replacingCharacters(in: range, with: text) as NSString
            
            
            
            if new_Str.length > maxLength {
                return false
            }

            let newChar = maxLength - new_Str.length
            lblTxtChar.text = newChar.description
            
            return true

        }
        return true
    }
    //============================================================================================//
    //============================================================================================//
    
    
    
    //============================================================================================//
    //============================================================================================//
    
    @IBAction func btnGetImageTapped(_ sender: Any) {
        
        let imageAlert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
            imageAlert.dismiss(animated: true, completion: nil)
        })
        
        let Capture = UIAlertAction.init(title: "Take Photo", style: .destructive, handler: { (action) in
            
            self.ImagePicker.sourceType = .camera
            self.ImagePicker.showsCameraControls = true
            self.ImagePicker.allowsEditing = false
            
            self.present(self.ImagePicker, animated: true, completion: nil)
        })
        
        let chosefromlib = UIAlertAction.init(title: "Choose Photo", style: .default, handler: { (action) in
            
            self.ImagePicker.sourceType = .photoLibrary
            self.ImagePicker.allowsEditing = false
            
            self.present(self.ImagePicker, animated: true, completion: nil)
        })
        
        imageAlert.addAction(Capture)
        imageAlert.addAction(chosefromlib)
        imageAlert.addAction(cancel)
        
        self.present(imageAlert, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let PickedImage: UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        picker.dismiss(animated: true, completion: nil)
        //================Image Crop=====================//
        let cropController:TOCropViewController = TOCropViewController(image: PickedImage)
        cropController.delegate = self
        appDelegate.window?.rootViewController?.present(cropController, animated: true, completion: nil)
        //===============================================//
    }
    
    // MARK: - Cropper Delegate
    @objc(cropViewController:didCropToImage:withRect:angle:) func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        
        cropViewController.presentingViewController?.dismiss(animated: true, completion: { _ in })
        print("\(image)")
        
        self.imgProfile.image = image
        isUpdatePhoto = true
    }
    //=================================================================================================//
    //=================================================================================================//
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension UIViewController {
    @objc func dismissPicker() {
        self.view.endEditing(true)
    }
}

extension UIToolbar {
    
    func ToolbarPiker(mySelect : Selector) -> UIToolbar {
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
       // toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: mySelect)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
    
    func ToolbarPikerteast(mySelect : Selector, delegate:UIViewController) -> UIToolbar {
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
       // toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.done, target: delegate, action: mySelect)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
}
