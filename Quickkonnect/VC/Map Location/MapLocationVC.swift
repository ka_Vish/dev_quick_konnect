//
//  MapLocationVC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 01/03/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import GoogleMaps


class MapLocationVC: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    @IBOutlet weak var viewMapp: GMSMapView!
    @IBOutlet weak var viewZoomBG: UIView!
    @IBOutlet weak var btnzoomIn: UIButton!
    @IBOutlet weak var btnzoomOut: UIButton!
    
    var locationManager = CLLocationManager()
    
    var str_lat = "24.5944"
    var str_long = "73.6532"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //==================Google Map======================//
        viewMapp.delegate = self
        // A minimum distance a device must move before update event generated
        viewMapp.isMyLocationEnabled = true
        viewMapp.settings.myLocationButton = true
        viewMapp.settings.compassButton = true
        //====================================================//
        //====================================================//
        
        
        //============================Show Pin On Map==================================//
        viewMapp.clear()
        
        let camera = GMSCameraPosition.camera(withLatitude: Double(str_lat)!, longitude: Double(str_long)!, zoom: 12.0)
        viewMapp.isMyLocationEnabled = true
        viewMapp.camera = camera
            
        let state_marker = GMSMarker()
        state_marker.position = CLLocationCoordinate2D(latitude: Double(str_lat)!, longitude: Double(str_long)!)
        state_marker.icon = #imageLiteral(resourceName: "pin")
        state_marker.map = viewMapp
        //====================================================//
        //====================================================//
        
        
        viewZoomBG.backgroundColor = UIColor.white
        viewZoomBG.layer.masksToBounds = false
        viewZoomBG.layer.shadowColor = UIColor.lightGray.cgColor
        viewZoomBG.layer.shadowOpacity = 0.6
        viewZoomBG.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        viewZoomBG.layer.shadowRadius = 1
        
        
        btnzoomOut.setImage(#imageLiteral(resourceName: "remove").tint(with: .darkGray), for: .normal)
        btnzoomIn.setImage(#imageLiteral(resourceName: "add").tint(with: .darkGray), for: .normal)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //=================================Map Delegate Method===========================================//
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.authorizedWhenInUse) {
            viewMapp.isMyLocationEnabled = true
        }
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
        // Display the map using the default location.
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            viewMapp.isMyLocationEnabled = true
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //===============================================================================================//
    //MARK :- UIButton Action Method
    @IBAction func closePress(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //Zoom In Button Action Event
    @IBAction func zoomIn(_ sender: UIButton) {
        guard let mapView = self.viewMapp
            else {
                print("Set mapView variable")
                return
        }
        let zoomInValue = mapView.camera.zoom + 1.0
        mapView.animate(toZoom: zoomInValue)
    }
    
    //Zoom Out Button Action Event
    @IBAction func zoomOut(_ sender: UIButton) {
        guard let mapView = self.viewMapp
            else {
                print("Set mapView variable")
                return
        }
        let zoomOutValue = mapView.camera.zoom - 1.0
        mapView.animate(toZoom: zoomOutValue)
    }
    //===============================================================================================//
    //===============================================================================================//
    //===============================================================================================//
}


 
