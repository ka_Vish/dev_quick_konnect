//
//  CompanyInfoViewC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 01/03/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacePicker

protocol saveEditedChangesforQKProfileBasicInfo {
    func getResultbackEdited(_ teamsize: String, _ estadate: String, _ address: String, _ lat: String, _ long: String)
}

class CompanyInfoViewC: UIViewController, UITextFieldDelegate {

    var lattitude = ""
    var longitude = ""
    var isFirstTime = true
    
    var str_title = ""
    
    @IBOutlet weak var txtEstablishDate: TextField!
    @IBOutlet weak var txtTeamSize: TextField!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lbl_adresTitle: UILabel!
    @IBOutlet var lbl_title: UILabel!
    @IBOutlet weak var constraint_TeamSize_height: NSLayoutConstraint!
    @IBOutlet weak var constraint_Poup_Top: NSLayoutConstraint!
    @IBOutlet weak var constraint_Address_Top: NSLayoutConstraint!
    var QKProfileData: QKProfileDataList! = QKProfileDataList()
    @IBOutlet weak var tblview: UITableView!
    var placesClient: GMSPlacesClient!
    var delegate: saveEditedChangesforQKProfileBasicInfo?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view
        
        self.view.backgroundColor = .clear
        tblview.transform = CGAffineTransform.init(scaleX: 0.000, y: 0.000)
        self.lbl_title.text = self.str_title
        var height = UIScreen.main.bounds.height
        height = height/4
        height = height - 90
        self.constraint_Poup_Top.constant = height
        self.constraint_TeamSize_height.constant = self.str_title == "Comapny Information" ? 35 : 0
        self.constraint_Address_Top.constant = self.str_title == "Comapny Information" ? 35 : 0
        self.txtTeamSize.isHidden = self.str_title == "Comapny Information" ? false : true
        //Set TextField Dalegate PlaceHolder anf BgColor
        self.txtTeamSize.setDivider()
        self.txtEstablishDate.setDivider()
        self.txtTeamSize.delegate = self
        self.txtEstablishDate.delegate = self
        //=============================================//
        //=============================================//
        
        self.txtTeamSize.text = "\(self.QKProfileData.company_team_size)"
        self.txtEstablishDate.text = self.QKProfileData.establish_date
        self.lblAddress.text = self.QKProfileData.address
        if self.QKProfileData.address != "" {
            lbl_adresTitle.isHidden = false
        }else {
            lbl_adresTitle.isHidden = true
        }
        self.longitude = self.QKProfileData.longitude
        self.lattitude = self.QKProfileData.latitude
        
        self.lblAddress.isUserInteractionEnabled = true
        self.lblAddress.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(AddresspicUpPress(_:))))
    }
    
    
    @IBAction func AddresspicUpPress(_ sender: UITapGestureRecognizer) {
        
        DispatchQueue.main.async {
            
            self.view.endEditing(true)
            
            let center = CLLocationCoordinate2D(latitude: Double(KVClass.shareinstance().Lattitude)!, longitude: Double(KVClass.shareinstance().Longitude)!)
            let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
            let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            let config = GMSPlacePickerConfig(viewport: viewport)
            let placePicker = GMSPlacePicker(config: config)
            
            placePicker.pickPlace(callback: {(place, error) -> Void in
                if let error = error {
                    print("Pick Place error: \(error.localizedDescription)")
                    return
                }
                
                if let place = place {
                    self.lattitude = "\(place.coordinate.latitude)"
                    self.longitude = "\(place.coordinate.longitude)"
                    self.lbl_adresTitle.isHidden = false
                    
                    self.lblAddress.text = (place.formattedAddress ?? "")
                    
                    if (place.formattedAddress ?? "") == ""
                    {
                        self.lblAddress.text = "Address"
                        self.lbl_adresTitle.isHidden = true
                    }
                    

                    NotificationCenter.default.post(name: Notification.Name("MAINTABLERELOAD"), object: nil)
                    
                } else {
                }
            })
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFirstTime == true {
            isFirstTime = false
            
            tblview.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
            UIView.animate(withDuration: 0.3, animations: {
                self.tblview.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
                self.view.backgroundColor = UIColor.black.withAlphaComponent(0.45)
            }) { (sucess) in
                UIView.animate(withDuration: 0.3, animations: {
                    self.tblview.transform = CGAffineTransform.identity
                }) { (sucess) in
                }
            }
        }
    }
    
    //===============================================================================================//
    //MARK :- UIButton Action Method
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.tblview.transform = CGAffineTransform.identity
        UIView.animate(withDuration: 0.3, animations: {
            self.tblview.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0)
            
        }) { (sucess) in
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func btnDoneTapped(_ sender: Any) {
        if self.txtTeamSize.text == "" || self.txtEstablishDate.text == "" || self.lblAddress.text == "Address" || self.txtTeamSize.text == nil || self.txtEstablishDate.text == nil || self.lblAddress.text == nil {
            KVClass.ShowNotification("", "All fields are required!")
            return
        }

        self.delegate?.getResultbackEdited(self.txtTeamSize.text!, self.txtEstablishDate.text!, self.lblAddress.text!,self.lattitude,self.longitude)
        self.dismiss(animated: false, completion: nil)
    }
    
    //======================================================================================================//
    //======================================================================================================//
    
    
    //===============================================================================================//
    //MARK :- UIBarButton Action Method
    @IBAction func resignNumberpad(_ sender: UIBarButtonItem)
    {
        self.txtTeamSize.endEditing(true)
        self.txtEstablishDate.endEditing(true)
    }
    
    //======================================================================================================//
    //MARK :- TextField Delegate Method
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtEstablishDate
        {
            self.setDatepickerView(textField)
        }
        else if textField == self.txtTeamSize
        {
            self.setToobarView(textField)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtEstablishDate {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            if formatter.date(from: textField.text!) != nil {
            }
            else {
                textField.text = ""
                KVClass.ShowNotification("", "Please Select Proper Date")
                return
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtTeamSize {
            let maxLength = 7
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_NUMBERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
    }
    
    
    func setToobarView(_ txtfiewld: UITextField)
    {
        let toobar = UIToolbar()
        
        toobar.sizeToFit()
        
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.resignNumberpad(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toobar.setItems([ spaceButton, doneButton], animated: false)
        toobar.isUserInteractionEnabled = true
        
        txtfiewld.inputAccessoryView = toobar
    }
    //===============================================================================================//
    //===============================================================================================//
    //===============================================================================================//
    
    
    func setDatepickerView(_ txtfield: UITextField)
    {
        let toobar = UIToolbar()
        
        toobar.sizeToFit()
        
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.resignNumberpad(_:)))
        doneButton.tag = txtfield.tag
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toobar.setItems([ spaceButton, doneButton], animated: false)
        toobar.isUserInteractionEnabled = true
        
        txtfield.inputAccessoryView = toobar
        txtfield.clearButtonMode = .never
        
        
        let datepicker = UIDatePicker()
        datepicker.date = Date()
        datepicker.datePickerMode = .date
        datepicker.maximumDate = Date()
        
        
        let formatter = DateFormatter()
        
        if txtfield.text == "" || txtfield.text == nil
        {
            formatter.dateFormat = "yyyy-MM-dd"
            
            txtfield.text = formatter.string(from: datepicker.date)
            
        }
        else
        {
            formatter.dateFormat = "yyyy-MM-dd"
            let dt = formatter.date(from: txtfield.text!)
            
            if formatter.date(from: txtfield.text!) != nil {
                let stdt = formatter.string(from: dt!)
                datepicker.date = formatter.date(from: stdt)!
            }
            else {
                txtfield.text = ""
                KVClass.ShowNotification("", "Please Select Proper Date")
                return
            }
            
            
        }
        
        datepicker.addTarget(self, action: #selector(self.handleDatePicker(_:)), for: .valueChanged)
        txtfield.inputView = datepicker
        
    }
    @IBAction func handleDatePicker(_ sender: UIDatePicker)
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        self.txtEstablishDate.text = formatter.string(from: sender.date)
    }
    
}

