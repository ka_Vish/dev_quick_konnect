//
//  ProfileVC.swift
//  Quickkonnect
//
//  Created by Kavi on 22/12/17.
//  Copyright © 2017 com.kavish. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import FloatRatingView
import SDWebImage
import ContactsUI

class ProfileDataList: NSObject {
    
    dynamic var user_blocked = Bool()
    dynamic var basic_detail = NSDictionary()
    dynamic var contact_detail = [NSDictionary]()
    dynamic var iscontact = [NSDictionary]()
    dynamic var education_detail = [NSDictionary]()
    dynamic var experience_detail = [NSDictionary]()
    dynamic var publication_detail = [NSDictionary]()
    dynamic var language_detail = [NSDictionary]()
    dynamic var social_detail = [NSDictionary]()
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        
        self.user_blocked = dict["is_blocked"] as? Bool ?? false
        self.basic_detail = dict["basic_detail"] as? NSDictionary ?? [:]
        self.iscontact = dict["contact_detail"] as? [NSDictionary] ?? []
        if KVAppDataServieces.shared.LoginUserID() == (dict["basic_detail"] as? NSDictionary ?? [:])["user_id"] as? Int ?? 0
        {
            self.contact_detail = dict["contact_detail"] as? [NSDictionary] ?? []
        }
        else if KVAppDataServieces.shared.LoginUserID() == Int((dict["basic_detail"] as? NSDictionary ?? [:])["user_id"] as? String ?? "0")
        {
           self.contact_detail = dict["contact_detail"] as? [NSDictionary] ?? []
        }
        else
        {
            self.contact_detail = dict["approved_contacts"] as? [NSDictionary] ?? []
        }
        self.education_detail = dict["education_detail"] as? [NSDictionary] ?? []
        self.experience_detail = dict["experience_detail"] as? [NSDictionary] ?? []
        self.publication_detail = dict["publication_detail"] as? [NSDictionary] ?? []
        self.language_detail = dict["language_detail"] as? [NSDictionary] ?? []
        self.social_detail = dict["connected_social_media"] as? [NSDictionary] ?? []
        
    }
}

class ProfileVC: UIViewController {
    
    //<<========IBOutlet===========>>//
    var isedit = false
    var isaddcontect = false
    var Scan_user_update = false
    var ScreenFrom = ""
    var scanuniquecode = ""
    var strContactProfile = ""
    var numberindecofedit = 0
    var lastContentOffset: CGFloat = 0
    var ProfileData: ProfileDataList! = ProfileDataList()
    var scanuserid = KVAppDataServieces.shared.LoginUserID()
    var PeraHeaderTitleArr:[String] = ["BASIC INFORMATION","EDUCATION", "CONTACT INFO", "EXPERIENCES", "PUBLICATIONS", "LANGUAGES"]
    
    let arrKeys = ["user_blocked", "basic_detail", "contact_detail", "iscontact", "education_detail", "experience_detail", "publication_detail", "language_detail"]
    
    @IBOutlet var SKIP_btn: UIButton!
    
    @IBOutlet var Scrolltop: UIButton!
    @IBOutlet var Error_labl: UILabel!
    @IBOutlet var Profile_tab: UITableView!
    
     private var myContext = 0
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.updateViewConstraints()
        self.view.layoutIfNeeded()
        self.Profile_tab.reloadData()
    }
    
    //=================Get Observer Value====================================================//
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &myContext {
            print("changed. \(String(describing: change?[NSKeyValueChangeKey.newKey]))")
        }
    }
    //======================================================================================//
    //======================================================================================//
    //======================================================================================//
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.isedit
        {
            self.SKIP_btn.isHidden = false
        }
        else
        {
            self.SKIP_btn.isHidden = true
        }
        if self.strContactProfile == ""
        {
            let dicti = KVAppDataServieces.shared.UserValue(.profile_data) as? NSDictionary ?? [:]
            self.saveToJsonFile(dicti)
            self.retrieveFromJsonFile()
            self.getProfilefunc()
        }
        
        if self.strContactProfile != "" && !self.Scan_user_update
        {
            
            if KVAppDataServieces.shared.getScanUser(self.strContactProfile) != nil
            {
                self.ProfileData = ProfileDataList.init(KVAppDataServieces.shared.getScanUser(self.strContactProfile)!)
            }
            else
            {
                self.getProfilefunc()
            }
            self.Profile_tab.reloadData()
        }
        if Scan_user_update
        {
            self.getProfilefunc()
        }
    }
    
    

    //==================================================================================================//
    //MARK - Update Local Profile=======================================================================//
    func updateLocalProfile()
    {
        
        var urlapi = ""
        
        if self.scanuserid == KVAppDataServieces.shared.LoginUserID()
        {
            urlapi = "get_user_detail/\(self.scanuserid)?timezone=\(KVClass.KVTimeZone())"
        }
        else
        {
           urlapi = "get_user_detail/\(self.scanuserid)/\(KVAppDataServieces.shared.LoginUserID())?timezone=\(KVClass.KVTimeZone())"
        }
        KVClass.KVServiece(methodType: "GET", processView: self.navigationController, baseView: self, processLabel: "", params: nil, api: urlapi, arrFlag: false) { (JSON, status) in
            self.Profile_tab.closeEndPullRefresh()
            print(JSON)
            if self.scanuserid == KVAppDataServieces.shared.LoginUserID()
            {
                if let dataDict = JSON["data"] as? NSDictionary
                {
                    KVAppDataServieces.shared.updateFullProfile(dataDict)
                    
                    self.viewWillAppear(false)
                }
            }
            else
            {
                if let dataDict = JSON["data"] as? NSDictionary
                {
                    KVAppDataServieces.shared.setScanUser(self.scanuniquecode, dataDict)
                    self.ProfileData = ProfileDataList.init(dataDict)
                    app_Delegate.arrSocialData = (dataDict["connected_social_media"]! as! NSArray).mutableCopy() as! NSMutableArray
                    print(app_Delegate.arrSocialData)
                    self.Profile_tab.reloadData()
                }
            }
            
        }
    }
    
    func saveToJsonFile(_ dict: NSDictionary) {
        // Get the url of Persons.json in document directory
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("Profile.json")
        
        // Transform array into data and save it into file
        do {
            let data = try JSONSerialization.data(withJSONObject: dict, options: [])
            try data.write(to: fileUrl, options: [])
        } catch {
            print(error)
        }
    }
    
    func retrieveFromJsonFile() {
        // Get the url of Persons.json in document directory
        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentsDirectoryUrl.appendingPathComponent("Profile.json")
        
        Alamofire.request(fileUrl).responseObject { (resp: DataResponse<dataDetails>) in
            
            if resp.result.error == nil {
                
                Singleton.sharedInstance.data_details = resp.result.value
                
            }
        }
    }
    //==================================================================================================//
    //==================================================================================================//
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        app_Delegate.isBackChange = false
        app_Delegate.arrSocialData.removeAllObjects()
        self.Scrolltop.isHidden = true
        self.Profile_tab.estimatedSectionFooterHeight = 4
        self.Profile_tab.estimatedRowHeight = self.view.bounds.height
        
        if self.strContactProfile != ""
        {
            //=========Add Refresh Control For Pull To Refresh==============//
            self.Profile_tab.pullTorefresh(#selector(self.getProfilefunc), self)
            //==============================================================//
            //==============================================================//
        }
        else if self.strContactProfile == ""
        {
          //  self.getProfilefunc()
            
            //=========Add Refresh Control For Pull To Refresh==============//
            self.Profile_tab.pullTorefresh(#selector(self.updateLocalProfile), self)
            //==============================================================//
            //==============================================================//
        }
        
        self.Profile_tab.reloadData()
        
        self.scrolltotoppress(self.Scrolltop)
        
        if self.strContactProfile == ""
        {
            NotificationCenter.default.addObserver(self, selector: #selector(self.receivedNotification(notification:)), name: Notification.Name("UPDATEPROFILE"), object: nil)

        }
        
        
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    
    func receivedNotification(notification: NSNotification){
        
        let dictParam = NSMutableDictionary()
        dictParam["user_id"] = KVAppDataServieces.shared.LoginUserID()
        dictParam["device_id"] = kUserDefults_("fcmtoken")
        
        KVClass.KVServiece(methodType: "POST", processView: nil, baseView: self, processLabel: "", params: dictParam, api: "profile_update_notification", arrFlag: false) { (JSON, status) in
            
            if status == 1{
                print("success")
            }
        }
        
    }
    
    func getProfilefunc()
    {
         let userID = KVAppDataServieces.shared.LoginUserID()
            
            if self.strContactProfile == ""
            {
                
                if self.scanuserid == KVAppDataServieces.shared.LoginUserID()
                {
                    self.ProfileData = ProfileDataList.init(KVAppDataServieces.shared.UserValue(.profile_data) as? NSDictionary ?? [:])
                    
                    print(self.ProfileData.basic_detail)
                    
                    self.Profile_tab.reloadData()
                }
                else
                {
                    if KVAppDataServieces.shared.getScanUser(self.scanuniquecode) == nil || self.Scan_user_update
                    {
                        var urlapi = ""
                        
                        if self.scanuserid == KVAppDataServieces.shared.LoginUserID()
                        {
                            urlapi = "get_user_detail/\(self.scanuserid)?timezone=\(KVClass.KVTimeZone())"
                        }
                        else
                        {
                            urlapi = "get_user_detail/\(self.scanuserid)/\(KVAppDataServieces.shared.LoginUserID())?timezone=\(KVClass.KVTimeZone())"
                        }
                        
                        KVClass.KVServiece(methodType: "GET", processView: self.navigationController, baseView: self, processLabel: "", params: nil, api: urlapi, arrFlag: false) { (JSON, status) in
                            
                            if let dataDict = JSON["data"] as? NSDictionary
                            {
                                KVAppDataServieces.shared.setScanUser(self.scanuniquecode, dataDict)
                                self.ProfileData = ProfileDataList.init(dataDict)
                                app_Delegate.arrSocialData = (dataDict["connected_social_media"]! as! NSArray).mutableCopy() as! NSMutableArray
                                self.Profile_tab.reloadData()
                            }
                            
                            
                        }
                    }
                    else
                    {
                        self.ProfileData = ProfileDataList.init(KVAppDataServieces.shared.getScanUser(self.scanuniquecode)!)
                        let dicSocial = self.ProfileData.social_detail
                        app_Delegate.arrSocialData = (dicSocial as NSArray).mutableCopy() as! NSMutableArray
                        self.Profile_tab.reloadData()
                    }
                    
                }
                
                
                
            }
            else
            {

                let dictiadd = NSMutableDictionary()
                dictiadd["user_id"] = userID
                dictiadd["unique_code"] = self.strContactProfile
                dictiadd["timezone"] = KVClass.KVTimeZone()
                dictiadd["latitude"] = KVClass.shareinstance().Lattitude
                dictiadd["longitude"] = KVClass.shareinstance().Longitude
                dictiadd["device_type"] = "iOS"
                dictiadd["model"] = UIDevice.current.model
                dictiadd["version"] = UIDevice.current.systemVersion
                
                KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dictiadd, api: "scan_user_store", arrFlag: false, Hendler: { (json, status) in
                    self.Profile_tab.closeEndPullRefresh()
                    print(json)
                    
                    if status == 1 {
                        
                        if let datadict = json["contact_count"] as? NSDictionary
                        {
                            KVAppDataServieces.shared.update(.contact_all, datadict["contact_all"]!)
                            KVAppDataServieces.shared.update(.contact_week, datadict["contact_week"]!)
                            KVAppDataServieces.shared.update(.contact_month, datadict["contact_month"]!)
                            
                        }
                        if let dataDict = json["data"] as? NSDictionary
                        {
                            let isBlock = dataDict["is_blocked"] as? Int ?? 0
                            if isBlock == 1 {
                                self.ScreenFrom = "BlockList"
                            }
                            KVAppDataServieces.shared.setScanUser(self.strContactProfile, dataDict)
                            self.ProfileData = ProfileDataList.init(dataDict)
                            app_Delegate.arrSocialData = (dataDict["connected_social_media"]! as! NSArray).mutableCopy() as! NSMutableArray
                            print(app_Delegate.arrSocialData)
                            self.Profile_tab.reloadData()
                        }
                        appDelegate.updateContactList()
                        
                    }
                    else {
                        
                        KVClass.ShowNotification("", json["msg"] as? String ?? "User not found")
                        
                    }
                    
                    
                    
                })
            }
    }
    //==================================================================================================//
    //==================================================================================================//
    
    
    //==================================================================================================//
    //MARK :- UIButton Action Method====================================================================//
    @IBAction func profileMaineditpress(_ sender: UIButton)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProfileEducationController") as! ProfileEducationController
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func addInformationCellAction(_ sender: UIButton)
    {
        
        switch sender.tag {
        case 1:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "BaseviewVC") as! BaseviewVC
            self.navigationController?.pushViewController(obj, animated: true)
        case 2:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditEducationController") as! AddEditEducationController
            obj.indexOfData = 5001
            self.navigationController?.pushViewController(obj, animated: true)
        case 3:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditContactInfoController") as! AddEditContactInfoController
            obj.indexOfData = 5001
            self.navigationController?.pushViewController(obj, animated: true)
        case 4:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditExperienceController") as! AddEditExperienceController
            obj.indexOfData = 5001
            self.navigationController?.pushViewController(obj, animated: true)
        case 5:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditPublicationsController") as! AddEditPublicationsController
            obj.indexOfData = 5001
            self.navigationController?.pushViewController(obj, animated: true)
        case 6:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditLanguageController") as! AddEditLanguageController
            obj.indexOfData = 5001
            self.navigationController?.pushViewController(obj, animated: true)
        default:
            print("")
        }
    }
    
    @IBAction func editUserDetailAction(_ sender: UIButton)
    {
        
        let section = sender.tag / 100
        var row = sender.tag % 100
        row = row - 1
        
        switch section {
        case 1:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "BaseviewVC") as! BaseviewVC
            self.navigationController?.pushViewController(obj, animated: true)
        case 2:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditEducationController") as! AddEditEducationController
            obj.indexOfData = row
            self.navigationController?.pushViewController(obj, animated: true)
        case 3:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditContactInfoController") as! AddEditContactInfoController
            obj.indexOfData = row
            self.navigationController?.pushViewController(obj, animated: true)
        case 4:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditExperienceController") as! AddEditExperienceController
            obj.indexOfData = row
            self.navigationController?.pushViewController(obj, animated: true)
        case 5:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditPublicationsController") as! AddEditPublicationsController
            obj.indexOfData = row
            self.navigationController?.pushViewController(obj, animated: true)
        case 6:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditLanguageController") as! AddEditLanguageController
            obj.indexOfData = row
            self.navigationController?.pushViewController(obj, animated: true)
        default:
            print("")
        }
    }
    
    @IBAction func addInformationAction(_ sender: UIButton)
    {
        
        switch sender.tag {
        case 1:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "BaseviewVC") as! BaseviewVC
            self.navigationController?.pushViewController(obj, animated: true)
        case 2:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditEducationController") as! AddEditEducationController
            obj.indexOfData = 5001
            self.navigationController?.pushViewController(obj, animated: true)
        case 3:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditContactInfoController") as! AddEditContactInfoController
            obj.indexOfData = 5001
            self.navigationController?.pushViewController(obj, animated: true)
        case 4:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditExperienceController") as! AddEditExperienceController
            obj.indexOfData = 5001
            self.navigationController?.pushViewController(obj, animated: true)
        case 5:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditPublicationsController") as! AddEditPublicationsController
            obj.indexOfData = 5001
            self.navigationController?.pushViewController(obj, animated: true)
        case 6:
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditLanguageController") as! AddEditLanguageController
            obj.indexOfData = 5001
            self.navigationController?.pushViewController(obj, animated: true)
        default:
            print("")
        }
    }
    //==================================================================================================//
    //==================================================================================================//
}

//==================================================================================================//
//MARK :- Table View Delegate Datasource Method=====================================================//
extension ProfileVC: UITableViewDelegate, UITableViewDataSource
{
    func noProfileFound(_ ishide: Bool) {
        self.Error_labl.isHidden = ishide
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        noProfileFound(true)
        if self.ProfileData.basic_detail == [:]
        {
            noProfileFound(false)
            return 0
        }
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 1
        case 1:
            if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
            {
                return 2
            }
            return 1
        case 2:
            if (self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()) && self.ProfileData.education_detail.count == 0
            {
                return 0
            }
            return self.ProfileData.education_detail.count + 1
        case 3:
            if (self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()) && self.ProfileData.contact_detail.count == 0
            {
                return 2
            }
            return self.ProfileData.contact_detail.count + 1
        case 4:
            if (self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()) && self.ProfileData.experience_detail.count == 0
            {
                return 0
            }
            return self.ProfileData.experience_detail.count + 1
        case 5:
            if (self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()) && self.ProfileData.publication_detail.count == 0
            {
                return 0
            }
            return self.ProfileData.publication_detail.count + 1
        case 6:
            if (self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()) && self.ProfileData.language_detail.count == 0
            {
                return 0
            }
            return self.ProfileData.language_detail.count + 1
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let secView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: 70))
        secView.KVCorneredius = 4
        secView.backgroundColor = UIColor.groupTableViewBackground
        if section == 1
        {
            if self.strContactProfile == "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
            {
                
                let ShowLabel = UILabel.init(frame: CGRect.init(x: 30, y: 0, width: tableView.bounds.width - 60, height: 70))
                ShowLabel.text = "Having a detailed and complete profile creates a better impression both professionally and socially."
                ShowLabel.textAlignment = .center
                ShowLabel.numberOfLines = 0
                ShowLabel.font = UIFont.init(name: "lato-Regular", size: 14)
                ShowLabel.textColor = UIColor.darkGray
                
                secView.addSubview(ShowLabel)
            }
            return secView
        }
        else if section == 0
        {
            secView.backgroundColor = .clear
            return secView
        }
        return secView
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 1
        {
            if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
            {
                return 10
            }
            return 70
        }
        else if section == 0
        {
            return 1
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let secView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: 0))
        secView.KVCorneredius = 4
        secView.backgroundColor = UIColor.groupTableViewBackground
        return secView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if section == (tableView.numberOfSections - 1) {
            return 70
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
            {
                return 280
            }
            return 220
        case 1:
            if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
            {
                if indexPath.row == 0
                {
                    if (self.ProfileData.basic_detail["birthdate"] as? String ?? "" == "" || self.ProfileData.basic_detail["dob"] as? String ?? "" == "") && self.ProfileData.basic_detail["hometown"] as? String ?? "" == ""
                    {
                        return 0
                    }
                    return 120
                }
                else {
                    if app_Delegate.arrSocialData.count == 0 {
                        return 0
                    }
                    return 172
                }
            }
            return 120
        case 2:
            if self.ProfileData.education_detail.count > 0
            {
                if indexPath.row == 0
                {
                    return 44
                }
                return UITableViewAutomaticDimension
            }
            return 120
        case 3:
            if self.ProfileData.contact_detail.count > 0
            {
                if indexPath.row == 0
                {
                    return 44
                }
                return UITableViewAutomaticDimension
            }
            else if (self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()) && self.ProfileData.contact_detail.count == 0
            {
                if indexPath.row == 0
                {
                    return 44
                }
                return UITableViewAutomaticDimension
            }
            return 120
        case 4:
            if self.ProfileData.experience_detail.count > 0
            {
                if indexPath.row == 0
                {
                    return 44
                }
                return UITableViewAutomaticDimension
            }
            return 120
        case 5:
            if self.ProfileData.publication_detail.count > 0
            {
                if indexPath.row == 0
                {
                    return 44
                }
                return UITableViewAutomaticDimension
            }
            return 120
        case 6:
            if self.ProfileData.language_detail.count > 0
            {
                if indexPath.row == 0
                {
                    return 44
                }
                return 72
            }
            return 120
        default:
            return 120
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            
            let cell : ProFirstSectionCell = tableView.dequeueReusableCell(withIdentifier: "ProFirstSectionCell") as! ProFirstSectionCell
            cell.strScreen_From = self.ScreenFrom
            if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
            {
                cell.tag = 22
            }
            else
            {
                cell.tag = 14
            }
            cell.reusecell()
            cell.Profile_img.image = UIImage.init(named: "user_profile_pic")
            DispatchQueue.main.async {
                let imgdata = UIImageJPEGRepresentation(UIImage.init(named: "user_profile_pic")!, 0.25)!
                KVAppDataServieces.shared.update(.contact_image, imgdata)
            }
            if self.ProfileData.basic_detail["profile_pic"] as? String ?? "" != ""
            {
                print(self.ProfileData.basic_detail["profile_pic"] as? String ?? "")
                
                SDWebImageManager.shared().loadImage(with: URL.init(string: self.ProfileData.basic_detail["profile_pic"] as? String ?? "")!, options: SDWebImageOptions.refreshCached, progress: { (p1, p2, uri) in
                }, completed: { (img, dta, err, type, isfinish, uri) in
                    
                    cell.Profile_img.image = (img ?? UIImage.init(named: "user_profile_pic"))
                    if err == nil
                    {
                        DispatchQueue.main.async {
                            let imgdata = UIImageJPEGRepresentation(img!, 0.25)!
                            KVAppDataServieces.shared.update(.contact_image, imgdata)
                        }
                    }
                })
                
            }
            cell.User_name.text = "\(self.ProfileData.basic_detail["firstname"] as? String ?? "".capitalized) \(self.ProfileData.basic_detail["lastname"] as? String ?? "".capitalized)"
            cell.Story_lbl.text = ""
            cell.lbl_Story.text = self.ProfileData.basic_detail["qk_story"] as? String ?? ""
            
            let getAbout_Height = self.estimatedHeightOfAboutLabel(text: cell.lbl_Story.text!)
            if getAbout_Height > 36 {
                cell.lbl_ReadMore.isHidden = false
            }
            else {
                cell.lbl_ReadMore.isHidden = true
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.AboutTextTapPress))
            cell.lbl_Story.isUserInteractionEnabled = true
            cell.lbl_Story.addGestureRecognizer(tap)
            
            let tapReadMore = UITapGestureRecognizer(target: self, action: #selector(self.AboutTextTapPress))
            cell.lbl_ReadMore.isUserInteractionEnabled = true
            cell.lbl_ReadMore.addGestureRecognizer(tapReadMore)
            
            
            cell.Profile_btn.addTarget(self, action: #selector(profileMaineditpress(_:)), for: .touchUpInside)
            cell.isUserBlocked = self.ProfileData.user_blocked
            cell.Action_Collect.reloadData()
            cell.ActionBtnPress = {(sender) in
                
                
                
                DispatchQueue.main.async {
                    
                    var ActionStatus = ""
                    var ActonName = ""
                    var AlertMsg = ""
                    var ActionTitlest = ""
                    
                    if sender.tag == 1
                    {
                        ActionStatus = "U"
                        ActonName = "Unmatch"
                        AlertMsg = "Are you sure you want to unmatch \(self.ProfileData.basic_detail["firstname"] as? String ?? "") \(self.ProfileData.basic_detail["lastname"] as? String ?? "")?"
                        ActionTitlest = "Warning !"
                        
                    }
                    else if sender.tag == 2
                    {
                        if self.ProfileData.user_blocked == true
                        {
                            ActionStatus = "UB"
                            ActonName = "Unblock"
                            
                        }
                        else
                        {
                            ActionStatus = "B"
                            ActonName = "Block"
                            
                            let strF_Name = self.ProfileData.basic_detail["firstname"] as? String ?? ""
                            let strL_Name = self.ProfileData.basic_detail["lastname"] as? String ?? ""
                            
                            ActionTitlest = "Are you sure you want to block \(strF_Name.capitalized) \(strL_Name.capitalized)?"
                            
                            AlertMsg = "\n\(strF_Name.capitalized) \(strL_Name.capitalized) will no longer be able to connect with you, view your profile or any other data, nor start a conversation.\n\nYou will no longer be  able view \(strF_Name.capitalized) \(strL_Name.capitalized) profile and his contact details will be deleted from your Quickkonnect contact list."

                        }
                    }
                    
                    let AlertView = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
                    
                    let attributedTitle = NSMutableAttributedString(string: ActionTitlest, attributes:
                        [NSFontAttributeName: UIFont.init(name: "lato-Semibold", size: 16)!])
                    
                    let attributedMessage = NSMutableAttributedString(string: AlertMsg, attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 13)!])
                    AlertView.setValue(attributedTitle, forKey: "attributedTitle")
                    AlertView.setValue(attributedMessage, forKey: "attributedMessage")
                    
                    
                    var actionOK: UIAlertAction?
                    
                    if ActonName != "Unblock"
                    {
                        actionOK = UIAlertAction.init(title: ActonName, style: .destructive, handler: { (action) in
                            
                            self.unblockUserAlert(ActionStatus,ActonName,0)
                        })
                        
                        let cancelact = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                            
                            AlertView.dismiss(animated: true, completion: nil)
                        })
                        
                        AlertView.addAction(cancelact)
                        AlertView.addAction(actionOK!)
                        
                        self.present(AlertView, animated: true, completion: nil)
                        
                        
                    }
                    else
                    {
                        //actionOK = UIAlertAction.init(title: ActonName, style: .destructive, handler: { (action) in
                        
                        let alertOption = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
                        
                        //                            let attributedTitle = NSMutableAttributedString(string: "Alert !", attributes:
                        //                                [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
                        //
                        //                            let attributedMessage = NSMutableAttributedString(string: "Are you sure you want to Unblock this person with send Match request..?", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12.5)!])
                        //                            alertOption.setValue(attributedTitle, forKey: "attributedTitle")
                        //                            alertOption.setValue(attributedMessage, forKey: "attributedMessage")
                        
                        
                        let action1 = UIAlertAction.init(title: "Unblock with Match", style: .destructive, handler: { (action) in
                            self.unblockUserAlert(ActionStatus,ActonName,1)
                        })
                        let action2 = UIAlertAction.init(title: "Unblock Only", style: .default, handler: { (action) in
                            self.unblockUserAlert(ActionStatus,ActonName,0)
                        })
                        
                        let action3 = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                            
                            alertOption.dismiss(animated: true, completion: nil)
                        })
                        
                        alertOption.addAction(action1)
                        alertOption.addAction(action2)
                        alertOption.addAction(action3)
                        self.present(alertOption, animated: true, completion: nil)
                        //  })
                    }
                    
                }
            }
            
            return cell
            
        case 1:
            
            if self.ProfileData.basic_detail != [:] || self.ProfileData.basic_detail.allKeys.count > 0
            {
                if indexPath.row == 0
                {
                    
                    if (self.ProfileData.basic_detail["birthdate"] as? String ?? "" != "" || self.ProfileData.basic_detail["dob"] as? String ?? "" != "") && self.ProfileData.basic_detail["hometown"] as? String ?? "" != ""
                    {
                        
                        let cell: BasicInfoCell = tableView.dequeueReusableCell(withIdentifier: "BasicInfoCell", for: indexPath) as! BasicInfoCell
                        if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
                        {
                            cell.tag = 22
                        }
                        else
                        {
                            cell.tag = 14
                        }
                        cell.reusecell()
                        cell.Dob_lbl.text = self.ProfileData.basic_detail["birthdate"] as? String ?? "\(self.ProfileData.basic_detail["dob"] as? String ?? "")"
                        cell.title_lbl.text = "BASIC INFORMATION"
                        cell.hometown_lbl.text = self.ProfileData.basic_detail["hometown"] as? String ?? "N/A"
                        cell.edit_btn.tag = (indexPath.section*100)+indexPath.row
                        
                        cell.edit_btn.addTarget(self, action: #selector(editUserDetailAction(_:)), for: .touchUpInside)
                        
                        // cell.edit_btn.addTarget(self, action: #selector(self.editUserDetailAction(sender:tagvalue:)), for: .touchUpInside)
                        
                        return cell
                    }
                    else
                    {
                        let cell: AddInformCell = tableView.dequeueReusableCell(withIdentifier: "AddInformCell", for: indexPath) as! AddInformCell
                        if self.strContactProfile != ""
                        {
                            cell.tag = 22
                        }
                        else
                        {
                            cell.tag = 14
                        }
                        cell.Title_lbl.text = self.PeraHeaderTitleArr[indexPath.section - 1]
                        cell.Add_btn.addTarget(self, action: #selector(addInformationCellAction(_:)), for: .touchUpInside)
                        cell.Add_btn.tag = indexPath.section
                        
                        
                        return cell
                        
                    }
                }
                let cell: SocialMediaCell = tableView.dequeueReusableCell(withIdentifier: "SocialMediaCell", for: indexPath) as! SocialMediaCell
                    cell.Social_collection.reloadData()
                    cell.page_controll.numberOfPages = app_Delegate.arrSocialData.count
                
                cell.ActionSocialConnectBtnPress = {(sender) in
                        print("Twitter, Facebook, linkedin, Instagram")
                    
                    var socialDic: NSDictionary = [:]
                    socialDic = app_Delegate.arrSocialData.object(at:sender.tag) as! NSDictionary
                    let strSocialMediaID = socialDic["social_media_id"] as? Int ?? 0
                    
                    if strSocialMediaID == 1 {
                        print("Facebook")
                        
                        let strAppID = socialDic["social_media_user_id"] as? String ?? ""
                        let strURLlink = "http://www.facebook.com/" + strAppID
                        
                        DispatchQueue.main.async {
                            let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpSettingWebVC") as! HelpSettingWebVC
                            obj.urlstring = strURLlink
                            obj.titlestring = "Facebook"
                            self.present(obj, animated: true, completion: nil)
                        }
                    }
                        
                    else if strSocialMediaID == 2 {
                        print("Linkedin")
                        
                        let strLinkdinProfileURL = socialDic["publicProfileUrl"] as? String ?? ""
                        if !(strLinkdinProfileURL == "") {
                            DispatchQueue.main.async {
                                let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpSettingWebVC") as! HelpSettingWebVC
                                obj.urlstring = strLinkdinProfileURL
                                obj.titlestring = "Linkedin"
                                self.present(obj, animated: true, completion: nil)
                            }
                        }
                    }
                        
                    else if strSocialMediaID == 3 {
                        print("Twitter")
                        let strUsername = socialDic["username"] as? String ?? ""
                        let sreURLlink = "http://www.twitter.com/" + strUsername
            
                        DispatchQueue.main.async {
                            let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpSettingWebVC") as! HelpSettingWebVC
                            obj.urlstring = sreURLlink
                            obj.titlestring = "Twitter"
                            self.present(obj, animated: true, completion: nil)
                        }
                    }
                        
                    else if strSocialMediaID == 4 {
                        print("Instagram")
                        let strUsername = socialDic["username"] as? String ?? ""
                        let sreURLlink = "http://www.instagram.com/" + strUsername
                        
                        DispatchQueue.main.async {
                            let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpSettingWebVC") as! HelpSettingWebVC
                            obj.urlstring = sreURLlink
                            obj.titlestring = "Instagram"
                            self.present(obj, animated: true, completion: nil)
                        }
                    }
                    
                    
                    
                    
                    }
                
                
                return cell
                
            }
            let cell: AddInformCell = tableView.dequeueReusableCell(withIdentifier: "AddInformCell", for: indexPath) as! AddInformCell
            if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
            {
                cell.tag = 22
            }
            else
            {
                cell.tag = 14
            }
            cell.Title_lbl.text = self.PeraHeaderTitleArr[indexPath.section - 1]
            cell.Add_btn.addTarget(self, action: #selector(addInformationCellAction(_:)), for: .touchUpInside)
            cell.Add_btn.tag = indexPath.section
            
            
            return cell
            
        case 2:
            
            if self.ProfileData.education_detail.count > 0
            {
                if indexPath.row == 0
                {
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
                    {
                        cell.tag = 22
                    }
                    else
                    {
                        cell.tag = 14
                    }
                    cell.reusecell()
                    cell.title_lbl.text = self.PeraHeaderTitleArr[indexPath.section - 1]
                    cell.add_btn.tag = indexPath.section
                    cell.add_btn.addTarget(self, action: #selector(addInformationAction(_:)), for: .touchUpInside)
                    
                    return cell
                }
                let cell: EduExpCell = tableView.dequeueReusableCell(withIdentifier: "EduExpCell") as! EduExpCell
                if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
                {
                    cell.tag = 22
                }
                else
                {
                    cell.tag = 14
                }
                cell.reusecell()
                cell.school_lbl.text = self.ProfileData.education_detail[indexPath.row - 1]["school_name"] as? String ?? ""
                cell.degree_lbl.text = self.ProfileData.education_detail[indexPath.row - 1]["degree"] as? String ?? ""
                cell.field_lbl.text = self.ProfileData.education_detail[indexPath.row - 1]["field_of_study"] as? String ?? ""
                
                if self.ProfileData.education_detail[indexPath.row - 1]["end_date"] as? String ?? "" != "" && self.ProfileData.education_detail[indexPath.row - 1]["start_date"] as? String ?? "" != ""
                {
                    cell.date_lbl.text = "\(self.ProfileData.education_detail[indexPath.row - 1]["start_date"] as? String ?? "") - \(self.ProfileData.education_detail[indexPath.row - 1]["end_date"] as? String ?? "")"
                }
                else
                {
                    if self.ProfileData.education_detail[indexPath.row - 1]["end_date"] as? String ?? "" == "" && self.ProfileData.education_detail[indexPath.row - 1]["start_date"] as? String ?? "" != ""
                    {
                        cell.date_lbl.text = "\(self.ProfileData.education_detail[indexPath.row - 1]["start_date"] as? String ?? "") - Present"
                    }
                    else
                    {
                        cell.date_lbl.text = ""
                    }
                }
                
                cell.edit_btn.tag = (indexPath.section*100)+indexPath.row
                
                cell.edit_btn.addTarget(self, action: #selector(editUserDetailAction(_:)), for: .touchUpInside)
                
                return cell
            }
            let cell: AddInformCell = tableView.dequeueReusableCell(withIdentifier: "AddInformCell", for: indexPath) as! AddInformCell
            if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
            {
                cell.tag = 22
            }
            else
            {
                cell.tag = 14
            }
            cell.Title_lbl.text = self.PeraHeaderTitleArr[indexPath.section - 1]
            cell.Add_btn.addTarget(self, action: #selector(addInformationCellAction(_:)), for: .touchUpInside)
            cell.Add_btn.tag = indexPath.section
            return cell
            
        case 3:
            
            if self.ProfileData.contact_detail.count > 0
            {
                if indexPath.row == 0
                {
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
                    {
                        cell.tag = 22
                    }
                    else
                    {
                        cell.tag = 14
                    }
                    cell.reusecell()
                    cell.title_lbl.text = self.PeraHeaderTitleArr[indexPath.section - 1]
                    cell.add_btn.tag = indexPath.section
                    cell.add_btn.addTarget(self, action: #selector(addInformationAction(_:)), for: .touchUpInside)
                    return cell
                }
                let cell: ContactInfoCell = tableView.dequeueReusableCell(withIdentifier: "ContactInfoCell") as! ContactInfoCell
                if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
                {
                    cell.tag = 22
                }
                else
                {
                    cell.tag = 14
                }
                cell.reusecell()
                cell.contacttype_lbl.text = self.ProfileData.contact_detail[indexPath.row - 1]["contact_type"] as? String ?? ""
                cell.email_lbl.text = self.ProfileData.contact_detail[indexPath.row - 1]["email"] as? String ?? ""
                cell.phone_lbl.text = self.ProfileData.contact_detail[indexPath.row - 1]["phone"] as? String ?? ""
                cell.edit_btn.tag = indexPath.section
                self.numberindecofedit = indexPath.row - 1
                cell.edit_btn.tag = (indexPath.section*100)+indexPath.row
                
                cell.edit_btn.addTarget(self, action: #selector(editUserDetailAction(_:)), for: .touchUpInside)
                 cell.addcontact_btn.isHidden = true
                self.iscontactInContactbook(self.ProfileData.contact_detail[indexPath.row - 1]["phone"] as? String ?? "", Hendler: { (isfound) in
                    
                    DispatchQueue.main.async {
                        if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
                        {
                            cell.addcontact_btn.isHidden = isfound
                        }
                    }
                })
                
                cell.addcontactpres = {(sender) in
                    
                    let contactalert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
                    
                    let attributedTitle = NSMutableAttributedString(string: "Add Contact !", attributes:
                        [NSFontAttributeName: UIFont.init(name: "lato-Bold", size: 15)!])
                    
                    let attributedMessage = NSMutableAttributedString(string: "Are you sure to add this contact to your Phone Book ?", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 13)!])
                    contactalert.setValue(attributedTitle, forKey: "attributedTitle")
                    contactalert.setValue(attributedMessage, forKey: "attributedMessage")
                    
                    
                    let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                        
                        contactalert.dismiss(animated: true, completion: nil)
                    })
                    
                    let addcontact = UIAlertAction.init(title: "Add To Contact", style: UIAlertActionStyle.destructive, handler: { (actio) in
                        
                        
                        self.iscontactNameInCobntactbook("\(self.ProfileData.basic_detail["firstname"] as? String ?? "") \(self.ProfileData.basic_detail["lastname"] as? String ?? "")", Hendler: { (isfound,fContact) in
                            
                            DispatchQueue.main.async {
                                
                                if isfound
                                {
                                    do {
                                        let contactToUpdate = fContact.mutableCopy() as! CNMutableContact
                                        
                                        var phone: [CNLabeledValue<CNPhoneNumber>] = [CNLabeledValue.init(label: self.ProfileData.contact_detail[indexPath.row - 1]["contact_type"] as? String ?? "home", value: CNPhoneNumber.init(stringValue: self.ProfileData.contact_detail[indexPath.row - 1]["phone"] as? String ?? ""))]
                                        
                                        for item in fContact.phoneNumbers
                                        {
                                            phone.append(item)
                                        }
                                        contactToUpdate.phoneNumbers = phone
                                        
                                        if let contactimagedata = KVAppDataServieces.shared.UserValue(.contact_image) as? Data
                                        {
                                            if contactToUpdate.imageDataAvailable
                                            {
                                                contactToUpdate.imageData = fContact.imageData
                                            }
                                            else
                                            {
                                                contactToUpdate.imageData = contactimagedata
                                            }
                                        }
                                        
                                        var email: [CNLabeledValue<NSString>] = [CNLabeledValue.init(label: self.ProfileData.contact_detail[indexPath.row - 1]["contact_type"] as? String ?? "home", value: (self.ProfileData.contact_detail[indexPath.row - 1]["email"] as? String ?? "") as NSString)]
                                        
                                        for item in fContact.emailAddresses
                                        {
                                            email.append(item)
                                        }
                                        contactToUpdate.emailAddresses = email
                                        
                                        var DOB: [DateComponents?] = [fContact.birthday]
                                        var dob = DateComponents()
                                        let formatter = DateFormatter()
                                        formatter.dateFormat = "dd/MM/yyyy"
                                        if self.ProfileData.basic_detail["birthdate"] as? String ?? "" != ""
                                        {
                                            let db = formatter.date(from: self.ProfileData.basic_detail["birthdate"] as? String ?? "")
                                            
                                            dob.year = Calendar.current.component(.year, from: db!)
                                            dob.month = Calendar.current.component(.month, from: db!)
                                            dob.day = Calendar.current.component(.day, from: db!)
                                            
                                            DOB.append(dob)
                                        }
                                        if DOB.count > 0
                                        {
                                            contactToUpdate.birthday = (DOB.last)!
                                        }
                                        let saveRequest = CNSaveRequest()
                                        saveRequest.update(contactToUpdate)
                                        try CNContactStore().execute(saveRequest)
                                        KVClass.ShowNotification("", "Contact Succesfully Updated!")
                                        sender.isHidden = true
                                    } catch let error{
                                        print(error)
                                        KVClass.ShowNotification("", error.localizedDescription)
                                        sender.isHidden = false
                                    }
                                }
                                else
                                {
                                    let contact = CNMutableContact()
                                    contact.givenName = self.ProfileData.basic_detail["firstname"] as? String ?? ""
                                    contact.familyName = self.ProfileData.basic_detail["lastname"] as? String ?? ""
                                    contact.phoneNumbers = [CNLabeledValue.init(label: self.ProfileData.contact_detail[indexPath.row - 1]["contact_type"] as? String ?? "home", value: CNPhoneNumber.init(stringValue: self.ProfileData.contact_detail[indexPath.row - 1]["phone"] as? String ?? ""))]
                                    
                                    if let contactimagedata = KVAppDataServieces.shared.UserValue(.contact_image) as? Data
                                    {
                                        contact.imageData = contactimagedata
                                    }
                                    
                                    contact.emailAddresses = [CNLabeledValue.init(label: self.ProfileData.contact_detail[indexPath.row - 1]["contact_type"] as? String ?? "home", value: (self.ProfileData.contact_detail[indexPath.row - 1]["email"] as? String ?? "") as NSString)]
                                    
                                    var dob = DateComponents()
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "dd/MM/yyyy"
                                    if self.ProfileData.basic_detail["birthdate"] as? String ?? "" != ""
                                    {
                                        let db = formatter.date(from: self.ProfileData.basic_detail["birthdate"] as? String ?? "")
                                        
                                        dob.year = Calendar.current.component(.year, from: db!)
                                        dob.month = Calendar.current.component(.month, from: db!)
                                        dob.day = Calendar.current.component(.day, from: db!)
                                        
                                        contact.birthday = dob
                                    }
                                    
                                    let request = CNSaveRequest()
                                    request.add(contact, toContainerWithIdentifier: nil)
                                    
                                    do{
                                        try CNContactStore().execute(request)
                                        KVClass.ShowNotification("", "Contact Succesfully Added!")
                                        sender.isHidden = true
                                    } catch let error{
                                        print(error)
                                        KVClass.ShowNotification("", error.localizedDescription)
                                        sender.isHidden = false
                                    }
                                }
                                
                            }
                            
                        })
                    })
                    
                    
                    contactalert.addAction(cancel)
                    contactalert.addAction(addcontact)
                    self.present(contactalert, animated: true, completion: nil)
                    
                    // let contactViewController = CNContactViewController(forNewContact: contact)
                    
                    
                }
                
                
                return cell
            }
            else if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
            {
                if indexPath.row == 0
                {
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
                    {
                        cell.tag = 22
                    }
                    else
                    {
                        cell.tag = 14
                    }
                    cell.reusecell()
                    cell.title_lbl.text = self.PeraHeaderTitleArr[indexPath.section - 1]
                    cell.add_btn.tag = indexPath.section
                    cell.add_btn.addTarget(self, action: #selector(addInformationAction(_:)), for: .touchUpInside)
                    return cell
                }
                
                let cell: DownLoadContactCell = tableView.dequeueReusableCell(withIdentifier: "DownloadCell", for: indexPath) as! DownLoadContactCell
                
                
                //btn.center = view.center
                
                let titilest = NSMutableAttributedString.init(string: "     Download Contact     ")
                titilest.addAttributes([NSFontAttributeName : UIFont.init(name: "lato-Medium", size: 15)!, NSForegroundColorAttributeName : UIColor.white], range: NSRange.init(location: 0, length: titilest.length))
                cell.btn_download.setAttributedTitle(titilest, for: .normal)
                
                cell.btn_download.addTarget(self, action: #selector(downloadContactPress(_:)), for: .touchUpInside)
                
                if self.ProfileData.iscontact != [] || self.ProfileData.iscontact.count > 0
                {
                    cell.btn_download.isHidden = false
                    cell.lbl_err.isHidden = true
                    
                }
                else
                {
                    cell.btn_download.isHidden = true
                    cell.lbl_err.isHidden = false
                    
                }
                if ScreenFrom == "BlockList" {
                    cell.btn_download.isEnabled = false
                    cell.btn_download.backgroundColor = UIColor.lightGray
                }
                else {
                    cell.btn_download.isEnabled = true
                }
                
                
                return cell
            }
            let cell: AddInformCell = tableView.dequeueReusableCell(withIdentifier: "AddInformCell", for: indexPath) as! AddInformCell
            if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
            {
                cell.tag = 22
            }
            else
            {
                cell.tag = 14
            }
            cell.Title_lbl.text = self.PeraHeaderTitleArr[indexPath.section - 1]
            
            cell.Add_btn.addTarget(self, action: #selector(addInformationCellAction(_:)), for: .touchUpInside)
            cell.Add_btn.tag = indexPath.section
            
            return cell
            
        case 4:
            
            if self.ProfileData.experience_detail.count > 0
            {
                if indexPath.row == 0
                {
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
                    {
                        cell.tag = 22
                    }
                    else
                    {
                        cell.tag = 14
                    }
                    cell.reusecell()
                    cell.title_lbl.text = self.PeraHeaderTitleArr[indexPath.section - 1]
                    cell.add_btn.tag = indexPath.section
                    cell.add_btn.addTarget(self, action: #selector(addInformationAction(_:)), for: .touchUpInside)
                    return cell
                }
                let cell: EduExpCell = tableView.dequeueReusableCell(withIdentifier: "EduExpCell") as! EduExpCell
                if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
                {
                    cell.tag = 22
                }
                else
                {
                    cell.tag = 14
                }
                cell.reusecell()
                cell.school_lbl.text = self.ProfileData.experience_detail[indexPath.row - 1]["title"] as? String ?? ""
                cell.degree_lbl.text = self.ProfileData.experience_detail[indexPath.row - 1]["company_name"] as? String ?? ""
                cell.field_lbl.text = self.ProfileData.experience_detail[indexPath.row - 1]["location"] as? String ?? ""
                
                if self.ProfileData.experience_detail[indexPath.row - 1]["end_date"] as? String ?? "" != "" && self.ProfileData.experience_detail[indexPath.row - 1]["start_date"] as? String ?? "" != ""
                {
                    cell.date_lbl.text = "\(self.ProfileData.experience_detail[indexPath.row - 1]["start_date"] as? String ?? "") - \(self.ProfileData.experience_detail[indexPath.row - 1]["end_date"] as? String ?? "")"
                }
                else
                {
                    if self.ProfileData.experience_detail[indexPath.row - 1]["end_date"] as? String ?? "" == "" && self.ProfileData.experience_detail[indexPath.row - 1]["start_date"] as? String ?? "" != ""
                    {
                        cell.date_lbl.text = "\(self.ProfileData.experience_detail[indexPath.row - 1]["start_date"] as? String ?? "") - Present"
                    }
                    else{
                        cell.date_lbl.text = ""
                    }
                }
                
                cell.edit_btn.tag = indexPath.section
                self.numberindecofedit = indexPath.row - 1
                cell.edit_btn.tag = (indexPath.section*100)+indexPath.row
                
                cell.edit_btn.addTarget(self, action: #selector(editUserDetailAction(_:)), for: .touchUpInside)
                
                return cell
            }
            let cell: AddInformCell = tableView.dequeueReusableCell(withIdentifier: "AddInformCell", for: indexPath) as! AddInformCell
            if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
            {
                cell.tag = 22
            }
            else
            {
                cell.tag = 14
            }
            cell.Title_lbl.text = self.PeraHeaderTitleArr[indexPath.section - 1]
            cell.Add_btn.addTarget(self, action: #selector(addInformationCellAction(_:)), for: .touchUpInside)
            cell.Add_btn.tag = indexPath.section
            return cell
            
        case 5:
            
            if self.ProfileData.publication_detail.count > 0
            {
                if indexPath.row == 0
                {
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
                    {
                        cell.tag = 22
                    }
                    else
                    {
                        cell.tag = 14
                    }
                    cell.reusecell()
                    cell.title_lbl.text = self.PeraHeaderTitleArr[indexPath.section - 1]
                    cell.add_btn.tag = indexPath.section
                    cell.add_btn.addTarget(self, action: #selector(addInformationAction(_:)), for: .touchUpInside)
                    return cell
                }
                let cell: PublicationCell = tableView.dequeueReusableCell(withIdentifier: "PublicationCell") as! PublicationCell
                if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
                {
                    cell.tag = 22
                }
                else
                {
                    cell.tag = 14
                }
                cell.reusecell()
                cell.ptitle_lbl.text = self.ProfileData.publication_detail[indexPath.row - 1]["publication_title"] as? String ?? ""
                cell.pname_lbl.text = self.ProfileData.publication_detail[indexPath.row - 1]["publisher_name"] as? String ?? ""
                cell.date_lbl.text = self.ProfileData.publication_detail[indexPath.row - 1]["date"] as? String ?? ""
                cell.edit_btn.tag = (indexPath.section*100)+indexPath.row
                
                cell.edit_btn.addTarget(self, action: #selector(editUserDetailAction(_:)), for: .touchUpInside)
                
                if self.ProfileData.publication_detail[indexPath.row-1]["publication_url"] as? String ?? "" != ""
                {
                    cell.openurl_btn.isHidden = false
                }
                else
                {
                    cell.openurl_btn.isHidden = true
                }
                cell.urlpress = {(sender) in
                    DispatchQueue.main.async {
                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpSettingWebVC") as! HelpSettingWebVC
                    obj.urlstring = self.ProfileData.publication_detail[indexPath.row-1]["publication_url"] as? String ?? ""
                    obj.titlestring = self.ProfileData.publication_detail[indexPath.row - 1]["publication_title"] as? String ?? ""
                    self.present(obj, animated: true, completion: nil)
                    }
                }
                
                return cell
            }
            let cell: AddInformCell = tableView.dequeueReusableCell(withIdentifier: "AddInformCell", for: indexPath) as! AddInformCell
            if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
            {
                cell.tag = 22
            }
            else
            {
                cell.tag = 14
            }
            cell.Title_lbl.text = self.PeraHeaderTitleArr[indexPath.section - 1]
            cell.Add_btn.addTarget(self, action: #selector(addInformationCellAction(_:)), for: .touchUpInside)
            cell.Add_btn.tag = indexPath.section
            return cell
            
        case 6:
            
            if self.ProfileData.language_detail.count > 0
            {
                if indexPath.row == 0
                {
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
                    {
                        cell.tag = 22
                    }
                    else
                    {
                        cell.tag = 14
                    }
                    cell.reusecell()
                    cell.title_lbl.text = self.PeraHeaderTitleArr[indexPath.section - 1]
                    cell.add_btn.tag = indexPath.section
                    cell.add_btn.addTarget(self, action: #selector(addInformationAction(_:)), for: .touchUpInside)
                    return cell
                }
                let cell: LanguageCell = tableView.dequeueReusableCell(withIdentifier: "LanguageCell") as! LanguageCell
                if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
                {
                    cell.tag = 22
                }
                else
                {
                    cell.tag = 14
                }
                cell.reusecell()
                cell.language_lbl.text = self.ProfileData.language_detail[indexPath.row - 1]["langauge_name"] as? String ?? ""
                cell.rating_view.rating = Float(self.ProfileData.language_detail[indexPath.row - 1]["rating"] as? String ?? "0")!
                
                cell.edit_btn.tag = (indexPath.section*100)+indexPath.row
                
                cell.edit_btn.addTarget(self, action: #selector(editUserDetailAction(_:)), for: .touchUpInside)
                
                
                return cell
            }
            let cell: AddInformCell = tableView.dequeueReusableCell(withIdentifier: "AddInformCell", for: indexPath) as! AddInformCell
            if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
            {
                cell.tag = 22
            }
            else
            {
                cell.tag = 14
            }
            cell.Title_lbl.text = self.PeraHeaderTitleArr[indexPath.section - 1]
            cell.Add_btn.addTarget(self, action: #selector(addInformationCellAction(_:)), for: .touchUpInside)
            cell.Add_btn.tag = indexPath.section
            return cell
            
        default:
            
            let cell: AddInformCell = tableView.dequeueReusableCell(withIdentifier: "AddInformCell", for: indexPath) as! AddInformCell
            if self.strContactProfile != "" || self.scanuserid != KVAppDataServieces.shared.LoginUserID()
            {
                cell.tag = 22
            }
            else
            {
                cell.tag = 14
            }
            cell.Title_lbl.text = self.PeraHeaderTitleArr[indexPath.section - 1]
            cell.Add_btn.addTarget(self, action: #selector(addInformationCellAction(_:)), for: .touchUpInside)
            cell.Add_btn.tag = indexPath.section
            
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if !self.Scrolltop.isHidden
        {
        cell.transform = CGAffineTransform.init(scaleX: 0.7, y: 0.7)
        UIView.animate(withDuration: 0.3, animations: {
            cell.transform = CGAffineTransform.identity
        }) { (sucess) in
        }
        }
    }
    //==================================================================================================//
    //==================================================================================================//
    
    
    // MARK: Tap About Label Text
    func AboutTextTapPress(sender:UITapGestureRecognizer) {
        
        let strAboutText = self.ProfileData.basic_detail["qk_story"] as? String ?? ""
        let getAbout_Height = self.estimatedHeightOfAboutLabel(text: strAboutText)
        
        if getAbout_Height > 22 {
            
            DispatchQueue.main.async {
                
                let Alert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
                
                let attributedMessage = NSMutableAttributedString(string: strAboutText, attributes: [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
                Alert.setValue(attributedMessage, forKey: "attributedMessage")
                
                let no = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                    Alert.dismiss(animated: true, completion: nil)
                })
                Alert.addAction(no)
                self.present(Alert, animated: true, completion: nil)
            }
        }
        print("tap working")
    }
    
    //Get Estimate Height User About Height
    func estimatedHeightOfAboutLabel(text: String) -> CGFloat {
        let size = CGSize(width: UIScreen.main.bounds.width - 76, height: 10000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let attributes = [NSFontAttributeName: UIFont(name: "Lato-Regular", size: 15)]
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        return rectangleHeight
    }
    
    //==================================================================================================//
    //MARK :- ScrollView Delegate Method
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset < scrollView.contentOffset.y) {
            
            self.Scrolltop.isHidden = false
            
            // moved to top
        } else if (self.lastContentOffset > scrollView.contentOffset.y) {
            self.Scrolltop.isHidden = true
            // moved to bottom
        } else {
            self.Scrolltop.isHidden = true
            // didn't move
        }
    }
    
    @IBAction func scrolltotoppress(_ sender: UIButton)
    {
        self.Scrolltop.isHidden = true
        self.Profile_tab.setContentOffset(CGPoint.zero, animated: true)
        
        self.lastContentOffset = 0
    }
    //==================================================================================================//
    //==================================================================================================//
    
    
    
    func unblockUserAlert(_ ActionStatus: String,_ ActonName: String, _ option: Int)
    {
        let actionDict = NSMutableDictionary()
        actionDict["user_id"] = KVAppDataServieces.shared.LoginUserID()
        actionDict["requested_user_id"] = self.ProfileData.basic_detail["user_id"]
        actionDict["status"] = ActionStatus
        actionDict["timezone"] = KVClass.KVTimeZone()
        if ActonName == "Unblock"
        {
            actionDict["is_match"] = option
        }
        KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: actionDict, api: "user_contact_block_unmatch", arrFlag: false, Hendler: { (json, status) in
            print(json)
            KVClass.ShowNotification("", json["msg"] as? String ?? "")
            
            if status == 1
            {
                app_Delegate.isBackChange = true
                if let value = json["data"] as? [NSDictionary]
                {
                    let datavalue = NSKeyedArchiver.archivedData(withRootObject: value)
                    
                    KVAppDataServieces.shared.update(.contact_user_list, datavalue)
                    
                }
                if let countdatadict = json["contact_count"] as? NSDictionary
                {
                    KVAppDataServieces.shared.update(.contact_all, countdatadict["contact_all"]!)
                    KVAppDataServieces.shared.update(.contact_week, countdatadict["contact_week"]!)
                    KVAppDataServieces.shared.update(.contact_month, countdatadict["contact_month"]!)
                }
                
                kUserDefults(true as AnyObject, key: "updateblocklist")
                
                
                if ActionStatus == "U" || ActionStatus == "B"
                {
                    KVAppDataServieces.shared.unMatchScanuser(self.strContactProfile)
                    KVAppDataServieces.shared.unMatchScanuser(self.scanuniquecode)
                    self.navigationController?.popViewController(animated: true)
                }
                else if ActionStatus == "UB"
                {
                     KVAppDataServieces.shared.unMatchScanuser(self.scanuniquecode)
                    KVAppDataServieces.shared.unMatchScanuser(self.strContactProfile)
                    //self.getProfilefunc()
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
        })
        
    }
    
    

    
    func iscontactNameInCobntactbook(_ contactName: String, Hendler complition:@escaping (_ isfind: Bool, _ fContact: CNContact) -> Void )
    {
        CNContactStore().requestAccess(for: CNEntityType.contacts, completionHandler: { (isgranted, err) in
            
            if isgranted{
                
                let predicate = CNContact.predicateForContacts(matchingName: contactName)
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactEmailAddressesKey, CNContactBirthdayKey, CNContactImageDataKey, CNContactNoteKey, CNContactTypeKey, CNContactDatesKey, CNContactViewController.descriptorForRequiredKeys()] as [Any]
                var contacts = [CNContact]()
                
                
                let contactsStore = CNContactStore()
                do {
                    contacts = try contactsStore.unifiedContacts(matching: predicate, keysToFetch: keys as! [CNKeyDescriptor])
                    
                    if contacts.count == 0 {
                        
                        complition(false, CNContact())
                    }
                    else
                    {
                        complition(true,(contacts.last)!)
                    }
                }
                catch {
                    // message = "Unable to fetch contacts."
                    KVClass.ShowNotification("", "Unable to fetch contacts.")
                }
            }
        })
    }
    
    func iscontactInContactbook(_ phoneNumber: String, Hendler complition:@escaping (_ isfind: Bool) -> Void )
    {
        CNContactStore().requestAccess(for: CNEntityType.contacts, completionHandler: { (isgranted, err) in
            
            if isgranted{
                
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactImageDataKey, CNContactPhoneNumbersKey]
                var contacts = [CNContact]()
                
                let contactsStore = CNContactStore()
                do {
                    try contactsStore.enumerateContacts(with: CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])) {
                        (contact, cursor) -> Void in
                        if (!contact.phoneNumbers.isEmpty) {
                            let phoneNumberToCompareAgainst = phoneNumber.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                            
                            //phoneNumber.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet).joinWithSeparator("")
                            
                            for fonnumber in contact.phoneNumbers
                            {
                                if let phoneNumberStruct = fonnumber.value as? CNPhoneNumber {
                                    let phoneNumberString = phoneNumberStruct.stringValue
                                    let phoneNumberToCompare = phoneNumberString.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                                    if phoneNumberToCompare == phoneNumberToCompareAgainst {
                                        contacts.append(contact)
                                    }
                                }
                            }
                            
                        }
                    }
                    
                    if contacts.count == 0 {
                        complition(false)
                    }
                    else
                    {
                        complition(true)
                    }
                }
                catch {
                    
                    
                    KVClass.ShowNotification("", "Unable to fetch contacts.")
                    
                }
                
                
                
            }
            
        })
    }
    //==============================================================================================//
    //==============================================================================================//
    
    
    //==============================================================================================//
    //MARK : - UIButton Action Method===============================================================//
    @IBAction func downloadContactPress(_ sender: UIButton)
    {
        // sender.setAttributedTitle(NSMutableAttributedString.init(string: "Download Request Sent"), for: .selected)
        
        let titilest2 = NSMutableAttributedString.init(string: "     Download Request Sent     ")
        titilest2.addAttributes([NSFontAttributeName : UIFont.init(name: "lato-Medium", size: 12)!, NSForegroundColorAttributeName : UIColor.white], range: NSRange.init(location: 0, length: titilest2.length))
        sender.setAttributedTitle(titilest2, for: .selected)
        sender.setAttributedTitle(titilest2, for: .normal)
        sender.backgroundColor = UIColor.lightGray
        
        sender.isEnabled = false
        // sender.isHidden = true
        let dict = NSMutableDictionary()
        dict["user_id"] = kUserDefults_("kLoginusrid") as? Int ?? 0
        dict["scan_user_id"] = self.ProfileData.basic_detail["user_id"]
        KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "Please Wait", params: dict, api: "download_contact", arrFlag: false) { (JSON, status) in
            
            print(JSON)
            
            KVClass.ShowNotification("", JSON["msg"] as? String ?? "")
            
        }
    }
    
    @IBAction func closePress(_ sender: UIButton)
    {
        
        if ScreenFrom == "Present" {
            self.dismiss(animated: true, completion: {
            })
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
        NotificationCenter.default.removeObserver(self, name: Notification.Name("UPDATEPROFILE"), object: nil)
    }
    
    @IBAction func skipPress(_ sender: UIButton)
    {
        let strBase64Image = (UIImageJPEGRepresentation(#imageLiteral(resourceName: "logo"), 0.25)?.base64EncodedString())!
        
        //Push Update QK Tag VC
        let vc = storyboard?.instantiateViewController(withIdentifier: "UpdateQKTagViewController") as! UpdateQKTagViewController
        vc.strScreenFrom = "Dashboard"
        vc.strMiddleBase64 = strBase64Image
        navigationController?.pushViewController(vc, animated: true)
    }
    //==============================================================================================//
    //==============================================================================================//
}


class ProFirstSectionCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var Profile_img: UIImageView!
    @IBOutlet var Profile_btn: UIButton!
    @IBOutlet var User_name: UILabel!
    @IBOutlet var Story_lbl: UITextView!
    @IBOutlet var lbl_Story: UILabel!
    @IBOutlet var lbl_ReadMore: UILabel!
    @IBOutlet var Action_Collect: UICollectionView!
    
    var strScreen_From = ""
    var isUserBlocked = false
    
    var ActionBtnPress: ((UIButton)->Void)? = nil
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.Action_Collect.layoutSubviews()
        self.Action_Collect .layoutIfNeeded()
        self.Action_Collect.reloadData()
    }
    
    override func awakeFromNib() {
        
//        self.layoutSubviews()
//        self.layoutIfNeeded()
        
        self.Action_Collect.delegate = self
        self.Action_Collect.dataSource = self
        
        self.Profile_btn.setImage(UIImage.init(named: "edit")?.tint(with: UIColor.init(hex: "28AFB0")), for: .normal)
        if self.tag == 22
        {
            self.Profile_btn.isHidden = true
            self.Action_Collect.isHidden = false
        }
        else
        {
            self.Profile_btn.isHidden = false
            self.Action_Collect.isHidden = true
        }
    }
    
    func reusecell()
    {
        if self.tag == 22
        {
            self.Profile_btn.isHidden = true
            self.Action_Collect.isHidden = false
        }
        else
        {
            self.Profile_btn.isHidden = false
            self.Action_Collect.isHidden = true
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
      //  self.layoutIfNeeded()
        self.reusecell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.isUserBlocked
        {
            return 1
        }
        else {
            if self.strScreen_From == "FollowersList" {
                return 1
            }
            return 3
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.isUserBlocked
        {
          return CGSize.init(width: (self.Action_Collect.frame.size.width/1), height: self.Action_Collect.frame.size.height)
        }
        else {
            if self.strScreen_From == "FollowersList" {
                return CGSize.init(width: (self.Action_Collect.frame.size.width/1), height: self.Action_Collect.frame.size.height )
            }
            return CGSize.init(width: (self.Action_Collect.frame.size.width/3), height: self.Action_Collect.frame.size.height )
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var Title_lbl_arr: [String]! = [String]()
        var Title_img: [UIImage?]! = [UIImage?]()
        if self.isUserBlocked
        {
            Title_lbl_arr.removeAll()
            Title_lbl_arr = ["Unblock"]
            Title_img.removeAll()
            Title_img = [UIImage.init(named: "punblock_ic")]
        }
        else
        {
            Title_lbl_arr.removeAll()
            if self.strScreen_From == "FollowersList" {
                Title_lbl_arr = ["Message"]
            }
            else {
                Title_lbl_arr = ["Message","Unmatch","Block"]
            }
            Title_img.removeAll()
            Title_img = [UIImage.init(named: "pchat_ic")!,UIImage.init(named: "punmatch_ic")!,UIImage.init(named: "pblock_ic")!]
        }
        
        
        let cell: ActionCollectCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActionCollectCell", for: indexPath) as! ActionCollectCell
        cell.Title_title.text = Title_lbl_arr[indexPath.row]
        cell.Title_img.image = Title_img[indexPath.row]
       // cell.backgroundColor = UIColor.yellow
    
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 && !self.isUserBlocked
        {
            KVClass.ShowNotification("", "Coming soon!")
            return
        }
            if ActionBtnPress != nil
            {
                let button = UIButton.init(type: UIButtonType.infoDark)
                if self.isUserBlocked
                {
                    button.tag = 2
                }
                else
                {
                button.tag = indexPath.row
                }
                self.ActionBtnPress!(button)
            }
        //}
    }
    
}
class ActionCollectCell: UICollectionViewCell {
    
    @IBOutlet var Title_img: UIImageView!
    @IBOutlet var Title_title: UILabel!
    
}

class AddInformCell: UITableViewCell {
    
    @IBOutlet var Title_lbl: UILabel!
    @IBOutlet var Add_btn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //self.Add_btn.setAttributedTitle(KVClass.KVattributeIconString(inputImage: "add", stringName: "  Add Information"), for: .normal)
    }
}

class BasicInfoCell: UITableViewCell {
    
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var Dob_lbl: UILabel!
    @IBOutlet var hometown_lbl: UILabel!
    @IBOutlet var edit_btn: UIButton!
    
    override func awakeFromNib() {
        self.edit_btn.setImage(UIImage.init(named: "edit")?.tint(with: UIColor.init(hex: "28AFB0")), for: .normal)
        if self.tag == 22
        {
            self.edit_btn.isHidden = true
        }
        else
        {
            self.edit_btn.isHidden = false
        }
    }
    func reusecell()
    {
        if self.tag == 22
        {
            self.edit_btn.isHidden = true
        }
        else
        {
            self.edit_btn.isHidden = false
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.reusecell()
    }
}

class EduExpCell: UITableViewCell {
    
    @IBOutlet var school_lbl: UILabel!
    @IBOutlet var degree_lbl: UILabel!
    @IBOutlet var field_lbl: UILabel!
    @IBOutlet var date_lbl: UILabel!
    @IBOutlet var edit_btn: UIButton!
    
    var editpress: ((UIButton)->Void)? = nil
    
    override func awakeFromNib() {
        self.edit_btn.setImage(UIImage.init(named: "edit")?.tint(with: UIColor.init(hex: "28AFB0")), for: .normal)
        if self.tag == 22
        {
            self.edit_btn.isHidden = true
        }
        else
        {
            self.edit_btn.isHidden = false
        }
    }
    func reusecell()
    {
        if self.tag == 22
        {
            self.edit_btn.isHidden = true
        }
        else
        {
            self.edit_btn.isHidden = false
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.reusecell()
        // self.editpress = nil
        self.edit_btn.removeTarget(nil, action: nil, for: .allEvents)
    }
    
    @IBAction func editinfopress(_ sender: UIButton)
    {
        if editpress != nil
        {
            self.editpress!(sender)
        }
    }
}

class ContactInfoCell: UITableViewCell {
    
    @IBOutlet var contacttype_lbl: UILabel!
    @IBOutlet var email_lbl: UILabel!
    @IBOutlet var phone_lbl: UILabel!
    @IBOutlet var edit_btn: UIButton!
    @IBOutlet var addcontact_btn: UIButton!
    
    var addcontactpres: ((UIButton)->Void)? = nil
    
    override func awakeFromNib() {
        self.addcontact_btn.isHidden = true
        self.addcontact_btn.setImage(nil, for: .normal)
        self.edit_btn.setImage(UIImage.init(named: "edit")?.tint(with: UIColor.init(hex: "28AFB0")), for: .normal)
        if self.tag == 22
        {
            self.edit_btn.isHidden = true
            self.addcontact_btn.isHidden = false
            self.addcontact_btn.setImage(#imageLiteral(resourceName: "addcontact_ic"), for: .normal)
        }
        else
        {
            self.edit_btn.isHidden = false
            self.addcontact_btn.isHidden = true
            self.addcontact_btn.setImage(nil, for: .normal)
        }
    }
    func reusecell()
    {
        if self.tag == 22
        {
            self.edit_btn.isHidden = true
            self.addcontact_btn.isHidden = false
            self.addcontact_btn.setImage(#imageLiteral(resourceName: "addcontact_ic"), for: .normal)
        }
        else
        {
            self.edit_btn.isHidden = false
            self.addcontact_btn.isHidden = true
            self.addcontact_btn.setImage(nil, for: .normal)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.addcontact_btn.isHidden = true
        self.reusecell()
    }
    
    @IBAction func addcontactPress(_ sender: UIButton)
    {
        if self.addcontactpres != nil
        {
            self.addcontactpres!(sender)
        }
    }
    
}
class PublicationCell: UITableViewCell {
    
    @IBOutlet var ptitle_lbl: UILabel!
    @IBOutlet var pname_lbl: UILabel!
    @IBOutlet var date_lbl: UILabel!
    @IBOutlet var edit_btn: UIButton!
    @IBOutlet var openurl_btn: UIButton!
    var urlpress:((UIButton)->Void)? = nil
    
    override func awakeFromNib() {
        self.edit_btn.setImage(UIImage.init(named: "edit")?.tint(with: UIColor.init(hex: "28AFB0")), for: .normal)
        if self.tag == 22
        {
            self.edit_btn.isHidden = true
        }
        else
        {
            self.edit_btn.isHidden = false
        }
        self.openurl_btn.isHidden = true
        self.openurl_btn.addTarget(self, action: #selector(openUrl(_:)), for: .touchUpInside)
        
        let buttontitle = NSMutableAttributedString.init(string: "Open Publication")
        buttontitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Medium", size: 14.5)!, range: NSRange.init(location: 0, length: buttontitle.length))
        buttontitle.addAttribute(NSUnderlineColorAttributeName, value: UIColor.blue, range: NSRange.init(location: 0, length: buttontitle.length))
        buttontitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range: NSRange.init(location: 0, length: buttontitle.length))
        buttontitle.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange.init(location: 0, length: buttontitle.length))
        
        self.openurl_btn.setAttributedTitle(buttontitle, for: .normal)
        
    }
    func reusecell()
    {
        if self.tag == 22
        {
            self.edit_btn.isHidden = true
        }
        else
        {
            self.edit_btn.isHidden = false
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.reusecell()
    }
    
    @IBAction func openUrl(_ sender: UIButton)
    {
        if urlpress != nil
        {
            self.urlpress!(sender)
        }
    }
}
class LanguageCell: UITableViewCell {
    
    @IBOutlet var language_lbl: UILabel!
    @IBOutlet var rating_view: FloatRatingView!
    @IBOutlet var edit_btn: UIButton!
    
    override func awakeFromNib() {
        self.edit_btn.setImage(UIImage.init(named: "edit")?.tint(with: UIColor.init(hex: "28AFB0")), for: .normal)
        if self.tag == 22
        {
            self.edit_btn.isHidden = true
        }
        else
        {
            self.edit_btn.isHidden = false
        }
    }
    func reusecell()
    {
        if self.tag == 22
        {
            self.edit_btn.isHidden = true
        }
        else
        {
            self.edit_btn.isHidden = false
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.reusecell()
    }
}

class TitleHeaderCell: UITableViewCell {
    
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var add_btn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if self.tag == 22
        {
            self.add_btn.isHidden = true
        }
        else
        {
            self.add_btn.isHidden = false
        }
    }
    func reusecell()
    {
        if self.tag == 22
        {
            self.add_btn.isHidden = true
        }
        else
        {
            self.add_btn.isHidden = false
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.reusecell()
    }
}

class SocialMediaCell: UITableViewCell, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate
{
    @IBOutlet var Social_collection: UICollectionView!
    @IBOutlet var page_controll: UIPageControl!
    
    var ActionSocialConnectBtnPress: ((UIButton)->Void)? = nil
    
    //var titlearr = ["facebook","twitter","Linked in","Instagram"]
    //var colorarr = ["4066b4","3fcdfd","0077B5","B21876"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.Social_collection.delegate = self
        self.Social_collection.dataSource = self
        
        
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let visibleRect = CGRect.init(origin: self.Social_collection.contentOffset, size: self.Social_collection.bounds.size)
        let visiblePoint = CGPoint.init(x: visibleRect.midX, y: visibleRect.midY)
        let visibleindex = self.Social_collection.indexPathForItem(at: visiblePoint)
        
        self.page_controll.currentPage = (visibleindex?.row)!
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return app_Delegate.arrSocialData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return collectionView.bounds.size
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: SocialCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SocialCollectionCell", for: indexPath) as! SocialCollectionCell
        
        var socialDic: NSDictionary = [:]
        socialDic = app_Delegate.arrSocialData.object(at:indexPath.row) as! NSDictionary
        
        let strSocialMediaID = socialDic["social_media_id"] as? Int ?? 0
        if strSocialMediaID == 1 {
            cell.btn_connect.isHidden = false
            cell.lbl_connect.isHidden = false
            cell.img_Social.image = #imageLiteral(resourceName: "facebook-logo")
            cell.backgroundColor = UIColor.init(hex: "4066b4")
        }
        else if strSocialMediaID == 2 {
            
            let strLinkdinProfileURL = socialDic["publicProfileUrl"] as? String ?? ""
            if strLinkdinProfileURL == "" {
                cell.btn_connect.isHidden = true
                cell.lbl_connect.isHidden = true
            }
            else {
                cell.btn_connect.isHidden = false
                cell.lbl_connect.isHidden = false
            }
            cell.img_Social.image = #imageLiteral(resourceName: "linkedin-logo")
            cell.backgroundColor = UIColor.init(hex: "0077B5")
        }
        else if strSocialMediaID == 3 {
            cell.btn_connect.isHidden = false
            cell.lbl_connect.isHidden = false
            cell.img_Social.image = #imageLiteral(resourceName: "twitter-logo")
            cell.backgroundColor = UIColor.init(hex: "3fcdfd")
        }
        else if strSocialMediaID == 4 {
            cell.btn_connect.isHidden = false
            cell.lbl_connect.isHidden = false
            cell.img_Social.image = #imageLiteral(resourceName: "instagram-logo")
            cell.backgroundColor = UIColor.init(hex: "B21876")
        }
        
        cell.Title_lbl.text = ""
        let strFriends = socialDic["friend_list_count"] as? Int ?? 0
        if strFriends == 0 {
            cell.view_Friends.isHidden = true
            cell.constraint_email_top.constant = -25
        }
        else {
            cell.view_Friends.isHidden = false
            cell.lbl_friends_Title.text = "Friends"
            cell.lbl_friends.text = strFriends.description
            cell.constraint_email_top.constant = 2
        }
        
        let strEmail = socialDic["email"] as? String ?? ""
        if strEmail == "" {
            cell.lbl_email.text = socialDic["username"] as? String ?? ""
            cell.lbl_email_Title.text = "User Name :"
        }
        else {
            cell.lbl_email.text = strEmail
            cell.lbl_email_Title.text = "Email :"
        }

        
        cell.btn_connect.tag = indexPath.row
        cell.btn_connect.addTarget(self, action: #selector(btnSocialConnectPress(_:)), for: .touchUpInside)
        
        //cell.backgroundColor = UIColor.init(hex: self.colorarr[indexPath.row])
        
        return cell
    }
    
    @IBAction func btnSocialConnectPress(_ sender: UIButton) {
    
        print(sender.tag)
        let btnS_Press = (sender as AnyObject).convert(CGPoint.zero, to: Social_collection)
        let indexpa = Social_collection.indexPathForItem(at: btnS_Press)
        
        let currentCell = Social_collection.cellForItem(at: indexpa!) as! SocialCollectionCell
        
        
        
        if ActionSocialConnectBtnPress != nil
        {
            self.ActionSocialConnectBtnPress!(currentCell.btn_connect)
        }
        
    }
    
    
}

class SocialCollectionCell: UICollectionViewCell
{
    @IBOutlet var btn_connect: UIButton!
    @IBOutlet var lbl_connect: UILabel!
    @IBOutlet var Title_lbl: UILabel!
    @IBOutlet var img_Social: UIImageView!
    @IBOutlet var view_Friends: UIView!
    @IBOutlet var lbl_email: UILabel!
    @IBOutlet var lbl_email_Title: UILabel!
    @IBOutlet var lbl_friends: UILabel!
    @IBOutlet var lbl_friends_Title: UILabel!
    @IBOutlet var constraint_friend_height: NSLayoutConstraint!
    @IBOutlet var constraint_email_top: NSLayoutConstraint!
    
}

class DownLoadContactCell: UITableViewCell {
    
    @IBOutlet var lbl_err: UILabel!
    @IBOutlet var btn_download: UIButton!
}


class ClosureSleeve {
    let closure: () -> ()
    
    init(attachTo: AnyObject, closure: @escaping () -> ()) {
        self.closure = closure
        objc_setAssociatedObject(attachTo, "[\(arc4random())]", self, .OBJC_ASSOCIATION_RETAIN)
    }
    
    @objc func invoke() {
        closure()
    }
}

extension UIControl {
    func addAction(for controlEvents: UIControlEvents, action: @escaping () -> ()) {
        let sleeve = ClosureSleeve(attachTo: self, closure: action)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
    }
}

class MyButton: UIButton {
    
    var editindex = 0
    
    
}

extension ProfileVC : CNContactViewControllerDelegate
{
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        viewController.dismiss(animated: true, completion: nil)
    }
    func contactViewController(_ viewController: CNContactViewController, shouldPerformDefaultActionFor property: CNContactProperty) -> Bool {
        return true
    }
}


