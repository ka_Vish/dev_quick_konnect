//
//  AddMemberInProfileVC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 10/04/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit



class AddMemberInProfileVC: UIViewController, UITextFieldDelegate {

    var P_Type = ""
    var strScreenFrom = ""
    var ProfileID = 0
    var SelectedUserID = 0
    var NotificationID = 0
    var strBackChanges = ""
    @IBOutlet var viewTop: UIView!
    @IBOutlet var img_user_center: UIImageView!
    @IBOutlet var img_user_leading: UIImageView!
    @IBOutlet var img_geadient_bg: UIImageView!
    @IBOutlet var lbl_user_name: UILabel!
    @IBOutlet var lbl_user_email: UILabel!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var btnAccept: UIButton!
    @IBOutlet var btnReject: UIButton!
    @IBOutlet var lbl_Title: UILabel!
    @IBOutlet var txt_emp_no: TextField!
    @IBOutlet var txt_Department: TextField!
    @IBOutlet var txt_Desigation: TextField!
    @IBOutlet var txt_ValidFrom: TextField!
    @IBOutlet var txt_ValidTo: TextField!
    @IBOutlet var lblProfile_Name: UILabel!
    @IBOutlet var lblConfirmation: UILabel!
    @IBOutlet var viewImageBG: UIView!
    @IBOutlet var viewCenterImageBG: UIView!
    @IBOutlet var viewEnterContentBG: UIView!
    @IBOutlet var viewDisplayContentBG: UIView!
    @IBOutlet var lblEmpNO: UILabel!
    @IBOutlet var lbl_Department: UILabel!
    @IBOutlet var lblDepartment_Title: UILabel!
    @IBOutlet var lbl_Designation: UILabel!
    @IBOutlet var lblDesignation_Title: UILabel!
    @IBOutlet var lblValidFromDate: UILabel!
    @IBOutlet var lblValidToDate: UILabel!
    @IBOutlet var constraint_user_name_top: NSLayoutConstraint!
    @IBOutlet var constraint_center_img_top: NSLayoutConstraint!
    @IBOutlet var constraint_center_img_height: NSLayoutConstraint!
    @IBOutlet var constraint_btn_accept_height: NSLayoutConstraint!
    @IBOutlet var constraint_name_leading: NSLayoutConstraint!
    @IBOutlet var constraint_lblFromDate_top: NSLayoutConstraint!
    @IBOutlet var constraint_lbldesignation_top: NSLayoutConstraint!
    
    
    var dic_UserDetail: QKUserListDATA! = QKUserListDATA()
    var dic_EmployeeDetail: EmployeesListInProfileData! = EmployeesListInProfileData()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        strBackChanges = ""
//        txt_emp_no.setDivider()
//        txt_ValidTo.setDivider()
//        txt_ValidFrom.setDivider()
//        txt_Department.setDivider()
//        txt_Desigation.setDivider()
        btnReject.layer.cornerRadius = 5
        btnAccept.layer.cornerRadius = 5
        btnSubmit.layer.cornerRadius = 5
        //........Set Delegate.............//
        txt_emp_no.delegate = self
        txt_ValidTo.delegate = self
        txt_ValidFrom.delegate = self
        txt_Department.delegate = self
        txt_Desigation.delegate = self
        
        //.....................TextField PlaceHolder......................................//
        let strEmp_no = NSAttributedString(string: "Employee Number*", attributes: [NSForegroundColorAttributeName: UIColor.lightGray])
        txt_emp_no.attributedPlaceholder! = strEmp_no
        
        let strDepartment = NSAttributedString(string: "Department", attributes: [NSForegroundColorAttributeName: UIColor.lightGray])
        txt_Department.attributedPlaceholder! = strDepartment
        
        let strDesignation = NSAttributedString(string: "Designation", attributes: [NSForegroundColorAttributeName: UIColor.lightGray])
        txt_Desigation.attributedPlaceholder! = strDesignation
        
        let strValidFrom = NSAttributedString(string: "Valid From*", attributes: [NSForegroundColorAttributeName: UIColor.lightGray])
        txt_ValidFrom.attributedPlaceholder! = strValidFrom
        
        let strValidTo = NSAttributedString(string: "Valid to*", attributes: [NSForegroundColorAttributeName: UIColor.lightGray])
        self.txt_ValidTo.attributedPlaceholder! = strValidTo
        //............................................................................................//
        
        self.txt_emp_no.layer.borderWidth = 1;
        self.txt_emp_no.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        self.txt_ValidTo.layer.borderWidth = 1;
        self.txt_ValidTo.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        self.txt_ValidFrom.layer.borderWidth = 1;
        self.txt_ValidFrom.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        self.txt_Department.layer.borderWidth = 1;
        self.txt_Department.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        self.txt_Desigation.layer.borderWidth = 1;
        self.txt_Desigation.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        //............................................................................................//
        //............................................................................................//
        
        
        //..............................................................................//
        //Set Date From Entered Search Email Through....................................//
        img_user_center.image = #imageLiteral(resourceName: "user_profile_pic")
        let strImgURL = self.dic_UserDetail.profile_pic
        if strImgURL != "" {
            img_user_center.setImageWith(URLRequest.init(url: URL.init(string: strImgURL)!), placeholderImage: nil, success: { (request, response, image) in
                self.img_user_center.image = image
            }, failure: { (request, response, error) in
                self.img_user_center.image = #imageLiteral(resourceName: "user_profile_pic")
            })
        }
        SelectedUserID = self.dic_UserDetail.id
        lbl_user_email.text = self.dic_UserDetail.email
        lbl_user_name.text = self.dic_UserDetail.f_name.capitalized + " " + self.dic_UserDetail.l_name.capitalized
        
        
        if strScreenFrom == "Update" {
            viewTop.isHidden = false
            viewCenterImageBG.isHidden = false
            viewEnterContentBG.isHidden = false
            self.setDataFromDictEmployeeDetail()
        }
        
        else if strScreenFrom == "Notification" {
            lbl_user_name.textAlignment = .left
            lbl_user_email.textAlignment = .left
            constraint_name_leading.constant = 128
            constraint_user_name_top.constant = -25
            constraint_center_img_height.constant = 75
            img_geadient_bg.isHidden = false
            self.CallAPIForGetMemberDetail()
        }
        else {
            viewTop.isHidden = false
            viewCenterImageBG.isHidden = false
            viewEnterContentBG.isHidden = false
        }
        
        //..............................................................................//
        //..............................................................................//
    
        
        //For Date Picker
        let toolBar = UIToolbar().ToolbarPikerteast(mySelect: #selector(dismissPicker), delegate: self)
        txt_ValidFrom.tag = 1200
        txt_ValidFrom.inputAccessoryView = toolBar
        txt_ValidFrom.addTarget(self, action: #selector(self.DatePickTextInputPressed(sender:)), for: .allTouchEvents)
        
        txt_ValidTo.tag = 1201
        txt_ValidTo.inputAccessoryView = toolBar
        txt_ValidTo.addTarget(self, action: #selector(self.DatePickTextInputPressed(sender:)), for: .allTouchEvents)
        //',',',',',,',',',,',',',',',,',,',',',',',,'',,',',',',',',',',',',',',',',',',',',',',',',',',',',',',//
    }

    func setDataFromDictEmployeeDetail() {
        SelectedUserID = self.dic_EmployeeDetail.user_id
        txt_emp_no.text = self.dic_EmployeeDetail.employee_no
        txt_Desigation.text = self.dic_EmployeeDetail.designation
        txt_Department.text = self.dic_EmployeeDetail.department
        btnSubmit.setTitle("Update", for: .normal)
        
        lblEmpNO.text = self.dic_EmployeeDetail.employee_no
        
        if !(self.dic_EmployeeDetail.department == "") {
            lblDepartment_Title.text = "Department:"
            constraint_lbldesignation_top.constant = 12
            lbl_Department.text = self.dic_EmployeeDetail.department
        }
        
        if !(self.dic_EmployeeDetail.designation == "") {
            constraint_lblFromDate_top.constant = 12
            lblDesignation_Title.text = "Designation:"
            lbl_Designation.text = self.dic_EmployeeDetail.designation
        }
        //=============================================================================//
        //Convert Date Format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        let ValidToDate = dateFormatter.date(from: self.dic_EmployeeDetail.valid_to)
        let ValidFromDate = dateFormatter.date(from: self.dic_EmployeeDetail.valid_from)
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        //=============================================================================//
        txt_ValidTo.text = dateFormatter.string(from: ValidToDate!)
        txt_ValidFrom.text = dateFormatter.string(from: ValidFromDate!)
        
        lblValidToDate.text = dateFormatter.string(from: ValidToDate!)
        lblValidFromDate.text = dateFormatter.string(from: ValidFromDate!)
        
        let strImgURL = self.dic_EmployeeDetail.profile_pic
        if strImgURL != "" {
            img_user_center.setImageWith(URLRequest.init(url: URL.init(string: strImgURL)!), placeholderImage: nil, success: { (request, response, image) in
                self.img_user_center.image = image
            }, failure: { (request, response, error) in
                self.img_user_center.image = #imageLiteral(resourceName: "user_profile_pic")
            })
            
            img_user_leading.setImageWith(URLRequest.init(url: URL.init(string: strImgURL)!), placeholderImage: nil, success: { (request, response, image) in
                self.img_user_leading.image = image
            }, failure: { (request, response, error) in
                self.img_user_leading.image = #imageLiteral(resourceName: "user_profile_pic")
            })
        }
        lbl_user_email.text = self.dic_EmployeeDetail.email
        lbl_Title.text = self.dic_EmployeeDetail.Emp_Fname.capitalized + " " + self.dic_EmployeeDetail.Emp_Lname.capitalized
        lbl_user_name.text = self.dic_EmployeeDetail.Emp_Fname.capitalized + " " + self.dic_EmployeeDetail.Emp_Lname.capitalized
    }
    
    func CallAPIForGetMemberDetail() {
        DispatchQueue.main.async {
            
            //Dict For Pass in API For Get Employee Detail===============//
            let dict_param = NSMutableDictionary()
            dict_param["notification_id"] = self.NotificationID
            
            //Employee Detail
            KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self.navigationController, processLabel: "", params: dict_param, api: "member/member_detail", arrFlag: false) { (JSON, status) in
                if status == 1 {
                    print(JSON)
                    if let dataDict = JSON["data"] as? NSDictionary
                    {
                        self.dic_EmployeeDetail = EmployeesListInProfileData.init(dataDict)
                        if self.dic_EmployeeDetail.status == "A" || self.dic_EmployeeDetail.status == "R" || self.dic_EmployeeDetail.status == "B" {
                            self.viewTop.isHidden = false
                            self.btnAccept.isHidden = true
                            self.btnReject.isHidden = true
                            self.viewImageBG.isHidden = false
                            self.viewEnterContentBG.isHidden = true
                            self.viewDisplayContentBG.isHidden = false
                            self.constraint_btn_accept_height.constant = 0
                            self.lblConfirmation.text = " "
                            self.lblProfile_Name.text = self.dic_EmployeeDetail.profilename
                        }
                        else {
                            self.viewTop.isHidden = false
                            self.btnAccept.isHidden = false
                            self.btnReject.isHidden = false
                            self.viewImageBG.isHidden = false
                            self.viewEnterContentBG.isHidden = true
                            self.viewDisplayContentBG.isHidden = false
                            self.lblConfirmation.text = "wants to add you as an employee\n"
                            self.lblProfile_Name.text = self.dic_EmployeeDetail.profilename
                        }
                        self.setDataFromDictEmployeeDetail()
                    }
                }
                else {
                    kUserDefults(true as AnyObject, key: "isnotificationrefresh")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UITextField Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txt_ValidFrom || textField == txt_ValidTo {
            strBackChanges = "Change"
            self.DatePickTextInputPressed(sender: textField)
        }
//        else if textField == self.txt_emp_no
//        {
//            self.setToolbarViewForEmpNo(textField)
//        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txt_ValidFrom {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if dateFormatter.date(from: textField.text!) != nil {
            }
            else {
                txt_ValidFrom.text = ""
                KVClass.ShowNotification("", "Please Select Proper Date")
                return
            }
        }
        if textField == txt_ValidTo {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if dateFormatter.date(from: textField.text!) != nil {
            }
            else {
                txt_ValidTo.text = ""
                KVClass.ShowNotification("", "Please Select Proper Date")
                return
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        strBackChanges = "Change"
        if textField == self.txt_Department || textField == self.txt_Desigation {
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        else if textField == self.txt_emp_no {
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_NUMBERSANDCHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        }
        return true
    }
    //',',',',',',',',',,',,',',',,',',,',',',,',',,',',',,',',',,',',,',',,',',,',,',',,',,',',',,',',,',//
    //',',',',',',',',',,',,',',',,',',,',',',,',',,',',',,',',',,',',,',',,',',,',,',',,',,',',',,',',,',//
    
    
    // MARK: - UIButton Event Action
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        app_Delegate.dictAddMemberInProfile = [:]
        
        if strBackChanges == "" {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            let Discart = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
                
            let attributedTitle = NSMutableAttributedString(string: "Are you sure ? ", attributes:
                    [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
            
            let attributedMessage = NSMutableAttributedString(string: "Do you really want Discard Changes ?", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
            Discart.setValue(attributedTitle, forKey: "attributedTitle")
            Discart.setValue(attributedMessage, forKey: "attributedMessage")
            
            let ok = UIAlertAction.init(title: "Yes", style: .destructive, handler: { (action) in
                
                self.navigationController?.popViewController(animated: true)
            })
            
            let no = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                Discart.dismiss(animated: true, completion: nil)
            })
            
            Discart.addAction(ok)
            Discart.addAction(no)
            
            self.present(Discart, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnAcceptAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            let Discart = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
            
            let attributedTitle = NSMutableAttributedString(string: "Are you sure ? ", attributes:
                [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
            
            let attributedMessage = NSMutableAttributedString(string: "you want to Accept this request ?", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
            Discart.setValue(attributedTitle, forKey: "attributedTitle")
            Discart.setValue(attributedMessage, forKey: "attributedMessage")
            
            let ok = UIAlertAction.init(title: "Accept", style: .destructive, handler: { (action) in
                
                let dict_param = NSMutableDictionary()
                dict_param["status"] = "A"
                dict_param["user_id"] = self.SelectedUserID
                dict_param["profile_id"] = self.dic_EmployeeDetail.profile_id
                //Accept Employee Request
                KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self.navigationController, processLabel: "", params: dict_param, api: "member/status", arrFlag: false) { (JSON, status) in
                    if status == 1 {
                        print(JSON)
                        self.btnReject.isHidden = true
                        self.btnAccept.isHidden = true
                        self.lblConfirmation.text = ""
                        self.constraint_btn_accept_height.constant = 0
                        kUserDefults(true as AnyObject, key: "isnotificationrefresh")
                    }
                }
            })
            
            let no = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                Discart.dismiss(animated: true, completion: nil)
            })
            
            Discart.addAction(ok)
            Discart.addAction(no)
            
            self.present(Discart, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnRejectAction(_ sender: UIButton) {
        
        let alertView = UIAlertController.init(title: "Reason of rejection", message: nil, preferredStyle: .actionSheet)
        
        let FirstAction = UIAlertAction.init(title: KVClass.strReason1, style: .destructive) { (action) in
            
            self.CallApiForRejectRequest(strBlock: "R", strReason: "1")
            
        }
        
        let SecondAction = UIAlertAction.init(title: KVClass.strReason2, style: .destructive) { (action) in
            
            self.CallApiForRejectRequest(strBlock: "R", strReason: "2")
            
        }
        
        let ThirdAction = UIAlertAction.init(title: KVClass.strReason3, style: .destructive) { (action) in
            
            self.CallApiForRejectRequest(strBlock: "R", strReason: "3")
        }
        
        let FourthAction = UIAlertAction.init(title: KVClass.strReasonFourth + self.dic_EmployeeDetail.profilename.capitalized, style: .destructive) { (action) in
            
            DispatchQueue.main.async {
                let Discart = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
                
                let attributedTitle = NSMutableAttributedString(string: "Are you sure ? ", attributes:
                    [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
                
                let attributedMessage = NSMutableAttributedString(string: "you want to Block " + self.dic_EmployeeDetail.profilename.capitalized + " ?", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
                Discart.setValue(attributedTitle, forKey: "attributedTitle")
                Discart.setValue(attributedMessage, forKey: "attributedMessage")
                
                let ok = UIAlertAction.init(title: "Block", style: .destructive, handler: { (action) in
                    
                    self.CallApiForRejectRequest(strBlock: "B", strReason: "")
                    
                })
                
                let no = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                    Discart.dismiss(animated: true, completion: nil)
                })
                
                Discart.addAction(ok)
                Discart.addAction(no)
                
                self.present(Discart, animated: true, completion: nil)
            }
            
            
            
            
        }
        //Block <name of the profile>
        let CancelAction = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            alertView.dismiss(animated: true, completion: nil)
        }
        
        alertView.addAction(FirstAction)
        alertView.addAction(SecondAction)
        alertView.addAction(ThirdAction)
        alertView.addAction(FourthAction)
        
        alertView.addAction(CancelAction)
        
        self.present(alertView, animated: true, completion: nil)
    }
    
    
    
    func CallApiForRejectRequest(strBlock: String, strReason: String) {
        let dict_param = NSMutableDictionary()
        if !(strReason == "") {
            dict_param["reason"] = strReason
        }
        dict_param["status"] = strBlock
        dict_param["user_id"] = self.SelectedUserID
        dict_param["profile_id"] = self.dic_EmployeeDetail.profile_id
        //Accept Employee Request
        KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self.navigationController, processLabel: "", params: dict_param, api: "member/status", arrFlag: false) { (JSON, status) in
            if status == 1 {
                print(JSON)
                self.btnAccept.isHidden = true
                self.btnReject.isHidden = true
                self.lblConfirmation.text = " "
                self.constraint_btn_accept_height.constant = 0
                kUserDefults(true as AnyObject, key: "isnotificationrefresh")
            }
        }
    }
    
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if txt_emp_no.text == "" {
            KVClass.ShowNotification("", "Please enter employee no")
            return
        }
        if txt_ValidFrom.text == "" {
            KVClass.ShowNotification("", "Please select valid from Date")
            return
        }
        if txt_ValidTo.text == "" {
            KVClass.ShowNotification("", "Please select valid to Date")
            return
        }
        if txt_ValidTo.text == txt_ValidFrom.text {
            KVClass.ShowNotification("", "Please select valid Date")
            return
        }
        
        DispatchQueue.main.async {
            
            var strOKTitle = "Confirm"
            var strMsg = "you want to add " + self.dic_UserDetail.f_name.capitalized + " " + self.dic_UserDetail.l_name + " as your employee?"
            
            //=============================================================================//
            //Convert Date Format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let ValidToDate = dateFormatter.date(from: self.txt_ValidTo.text!)
            let ValidFromDate = dateFormatter.date(from: self.txt_ValidFrom.text!)
            
            dateFormatter.dateFormat = "yyyy/MM/dd"
            let strValidTo = dateFormatter.string(from: ValidToDate!)
            let strValidFrom = dateFormatter.string(from: ValidFromDate!)
            //=============================================================================//
            
            //Dict For Pass in API For Add Employee===============//
            let dict_param = NSMutableDictionary()
            dict_param["valid_to"] = strValidTo
            dict_param["valid_from"] = strValidFrom
            dict_param["profile_id"] = self.ProfileID
            dict_param["user_id"] = self.SelectedUserID
            dict_param["employee_no"] = self.txt_emp_no.text!
            dict_param["department"] = self.txt_Department.text!
            dict_param["designation"] = self.txt_Desigation.text!
            //===================================================//
            
            if self.strScreenFrom == "Update" {
                strOKTitle = "Update"
                dict_param["id"] = self.dic_EmployeeDetail.id
                strMsg = "you want to edit " + self.dic_EmployeeDetail.Emp_Fname.capitalized + " " + self.dic_EmployeeDetail.Emp_Lname + " details as your employee?"
            }
            
            
            let Alert = UIAlertController.init(title: "Are you sure ?", message: strMsg, preferredStyle: .alert)
            
            let attributedTitle = NSMutableAttributedString(string: "Are you sure ?", attributes:
                [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
            
            let attributedMessage = NSMutableAttributedString(string: strMsg, attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
            Alert.setValue(attributedTitle, forKey: "attributedTitle")
            Alert.setValue(attributedMessage, forKey: "attributedMessage")
            

            let ok = UIAlertAction.init(title: strOKTitle, style: .destructive, handler: { (action) in
                
                
                
                //Add Employee
                KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self.navigationController, processLabel: "", params: dict_param, api: "member/add_edit_member", arrFlag: false) { (JSON, status) in
                    if status == 1 {
                        print(JSON)
                        if let dataDict = JSON["data"] as? NSDictionary
                        {
                            app_Delegate.dictAddMemberInProfile = dataDict
                        }
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            })
            
            let no = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                Alert.dismiss(animated: true, completion: nil)
            })
            Alert.addAction(ok)
            Alert.addAction(no)
            self.present(Alert, animated: true, completion: nil)
        }
    }
    
    
    
    //',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',//
    //',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',//
    // MARK: - Create Tool Bar For Number Keyboard
    func setToolbarViewForEmpNo(_ txtfield: UITextField) {
        let toobar = UIToolbar()
        toobar.sizeToFit()
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.dismissNumberpad(_:)))
        doneButton.tag = 1
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toobar.setItems([ spaceButton, doneButton], animated: false)
        toobar.isUserInteractionEnabled = true
        
        txtfield.inputAccessoryView = toobar
        txtfield.clearButtonMode = .whileEditing
    }
    
    @IBAction func dismissNumberpad(_ sender: UIBarButtonItem) {
        switch sender.tag {
        case 1:
            self.txt_emp_no.resignFirstResponder()
        default:
            break
        }
    }
    //',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',//
    
    
    //',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',//
    // MARK: - Create Tool Bar For Date Picker
    func DatePickTextInputPressed(sender: UITextField) {
        let datePickerView  : UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.tag = sender.tag
        
    
        if sender.text != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if dateFormatter.date(from: sender.text!) != nil {
                let currentDate:Date = dateFormatter.date(from: sender.text!)!
                datePickerView.setDate(currentDate, animated: false)
            }
            else {
                sender.text = ""
                KVClass.ShowNotification("", "Please Select Proper Date")
                return
            }
        }
    
        if sender.tag == 1201 {
            if txt_ValidFrom.text != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                if dateFormatter.date(from: txt_ValidFrom.text!) != nil {
                    let currentDate:Date = dateFormatter.date(from: txt_ValidFrom.text!)!
                    datePickerView.minimumDate = currentDate
                }
                else {
                    txt_ValidFrom.text = ""
                    KVClass.ShowNotification("", "Please Select Proper Date")
                    return
                }
            }
        }
        else {
            datePickerView.maximumDate = Date()
            if txt_ValidTo.text != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                if dateFormatter.date(from: txt_ValidFrom.text!) != nil {
                }
                else {
                    txt_ValidFrom.text = ""
                    KVClass.ShowNotification("", "Please Select Proper Date")
                    return
                }
            }
        }
    
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.handleDatePicker(sender:)), for: UIControlEvents.valueChanged)
        handleDatePicker(sender: datePickerView) // Set the date on start.
    }

    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        if sender.tag == 1200 {
            txt_ValidFrom.text = dateFormatter.string(from: sender.date)
        } else {
            txt_ValidTo.text = dateFormatter.string(from: sender.date)
        }
    }
    //',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',//
    //',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',//
}
