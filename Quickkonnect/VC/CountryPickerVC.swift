//
//  CountryPickerVC.swift
//  RedButton
//
//  Created by Zignuts Technolab on 30/03/18.
//  Copyright © 2018 Zignuts Technolab. All rights reserved.
//

import UIKit
import Material

class CountryPickerVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var txtsearch: UITextField!
    @IBOutlet weak var viewInnerPopup: UIView!
    @IBOutlet weak var tblCountry: UITableView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var viewPopupBottomLayout: NSLayoutConstraint!
    
    var completionPickCountry: ((Country?,Bool)->Void)?
    var arrCountry = [Country]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        txtsearch.appearanceSetup()
//        txtsearch.dividerActiveColor = UIColor.AppThemeRed()
//        txtsearch.dividerNormalColor = UIColor.AppThemeRed()
//        txtsearch.font = UIFont.AppFontRegular(14)
       btnClose.setImage(#imageLiteral(resourceName: "close_ic").tint(with: KVClass.themeColor()), for: .normal)
        tblCountry.tableFooterView = UIView(frame: .zero)
        
        viewInnerPopup.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        self.perform(#selector(show_animation), with: nil, afterDelay: 0.1)
        
        arrCountry =  SMCountry.shared.getAllCountry(withreload: true)
        tblCountry.reloadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func show_animation() {
        viewInnerPopup.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.viewInnerPopup.transform = CGAffineTransform.identity
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        }, completion: {(finished: Bool) -> Void in
        })
    }
    
    // MARK:- KEYBOARD METHODS
    @objc func keyboardWillShow(notification: NSNotification) {
        print("keyboardWillShow")
        let userinfo:NSDictionary = (notification.userInfo as NSDictionary?)!
        if let keybordsize = (userinfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.viewPopupBottomLayout.constant = (keybordsize.height - 40)
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        print("keyboardWillHide")
        self.viewPopupBottomLayout.constant = 40
        self.view.layoutIfNeeded()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - FUNCTIONS
    @IBAction func fun_close(_ sender: Any) {
        
        removeFromParent { (animated) in
        
        }
    }
    
    func removeFromParent(completion: @escaping ((Bool) -> Void)) {
        viewInnerPopup.transform = CGAffineTransform.identity
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.viewInnerPopup.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        }, completion: {(finished: Bool) -> Void in
            // do something once the animation finishes, put it here
            self.willMove(toParentViewController: nil)
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
            completion(true)
        })
    }
    
    
    // MARK: - TEXTFIELD DELEGATE
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func textfieldTextDidchange(_ sender: Any) {
        tblCountry.reloadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CountryPickerVC:UITableViewDelegate,UITableViewDataSource {
    
    
    func getfiltered() -> [Country]? {
       
        if txtsearch.text == "" || txtsearch.text == nil{
            return arrCountry
        }
        
        if let strSearch = txtsearch.text {
            return arrCountry.filter { (objcon) -> Bool in
                if let name = objcon.name {
                    return (name.lowercased().range(of: strSearch.lowercased()) != nil)
                }
                return false
            }
        }
        return arrCountry
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getfiltered()!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellCountry")
        
        let imgflag = cell?.viewWithTag(100) as? UIImageView
        let lblTitle = cell?.viewWithTag(101) as? UILabel
        let lblCode = cell?.viewWithTag(102) as? UILabel
        
        let Country = getfiltered()![indexPath.row]
        
        imgflag?.image = Country.flag
        lblTitle?.text = Country.name! + "(\(Country.code!))"
        lblCode?.text  = Country.phoneCode
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let Country = getfiltered()![indexPath.row]
        removeFromParent { (animated) in
            if let callBack = self.completionPickCountry {
                callBack(Country,true)
            }
        }
    }
    
}
