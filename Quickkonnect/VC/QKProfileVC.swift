//
//  QKProfileVC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 28/02/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import SDWebImage
import GooglePlaces
import GooglePlacePicker
import CoreTelephony


class QKProfileDataList: NSObject {
    
    var role = 0
    dynamic var id = 0
    dynamic var tag = 0
    dynamic var type = 0
    dynamic var sub_tag = 0
    dynamic var user_id = 0
    var is_transfer = 0
    var transfer_status = 0
    dynamic var company_team_size = 0
    dynamic var section_UserProfile = 0
    dynamic var section_ScanProfile = 0
    
    var transfer_to = ""
    dynamic var name = ""
    dynamic var logo = ""
    dynamic var about = ""
    dynamic var Status = ""
    dynamic var qr_code = ""
    dynamic var address = ""
    dynamic var latitude = ""
    dynamic var longitude = ""
    dynamic var middle_tag = ""
    dynamic var logobase64 = ""
    dynamic var unique_code = ""
    dynamic var border_color = ""
    dynamic var pattern_color = ""
    dynamic var establish_date = ""
    dynamic var background_color = ""
    
    
    
    dynamic var phone = [NSDictionary]()
    dynamic var email = [NSDictionary]()
    dynamic var weblink = [NSDictionary]()
    dynamic var founders = [NSDictionary]()
    dynamic var social_media = [NSDictionary]()
    dynamic var other_address = [NSDictionary]()
    
    private var kvoContext: UInt8 = 1
    private let dict: NSDictionary = [:]

    convenience init(_ dict: NSDictionary) {
        self.init()
        
        self.section_UserProfile = 7
        self.section_ScanProfile = 7
        
        self.id = dict["id"] as? Int ?? 0
        self.role = dict["role"] as? Int ?? 0
        self.tag = dict["tag"] as? Int ?? 0
        self.type = dict["type"] as? Int ?? 0
        self.user_id = dict["user_id"] as? Int ?? 0
        self.sub_tag = dict["sub_tag"] as? Int ?? 0
        self.is_transfer = dict["is_transfer"] as? Int ?? 0
        self.transfer_status = dict["transfer_status"] as? Int ?? 0
        self.company_team_size = dict["company_team_size"] as? Int ?? 0
        
        self.logobase64 = ""
        self.name = dict["name"] as? String ?? ""
        self.logo = dict["logo"] as? String ?? ""
        self.about = dict["about"] as? String ?? ""
        self.Status = dict["status"] as? String ?? ""
        self.qr_code = dict["qr_code"] as? String ?? ""
        self.address = dict["address"] as? String ?? ""
        self.latitude = dict["latitude"] as? String ?? ""
        self.longitude = dict["longitude"] as? String ?? "0"
        self.middle_tag = dict["middle_tag"] as? String ?? ""
        self.transfer_to = dict["transfer_to"] as? String ?? ""
        self.unique_code = dict["unique_code"] as? String ?? ""
        self.border_color = dict["border_color"] as? String ?? ""
        self.pattern_color = dict["pattern_color"] as? String ?? ""
        self.establish_date = dict["type"] as? Int != 3 ? dict["establish_date"] as? String ?? "" : dict["dob"] as? String ?? ""
        self.background_color = dict["background_color"] as? String ?? ""
        
        self.phone = dict["phone"] as? [NSDictionary] ?? []
        self.email = dict["email"] as? [NSDictionary] ?? []
        self.weblink = dict["weblink"] as? [NSDictionary] ?? []
        self.founders = dict["founders"] as? [NSDictionary] ?? []
        self.social_media = dict["social_media"] as? [NSDictionary] ?? []
        self.other_address = dict["otheraddress"] as? [NSDictionary] ?? []
    }


}


class QKProfileFollowersListData: NSObject {
    
    var name = ""
    var email = ""
    var user_id = 0
    var lastname = ""
    var firstname = ""
    var unique_code = ""
    var profile_pic = ""
    var profile_name = ""
    var selectionData = [String : Any]()
    
    private let dict: NSDictionary = [:]
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        
        self.name = dict["name"] as? String ?? ""
        self.email = dict["email"] as? String ?? ""
        self.user_id = dict["user_id"] as? Int ?? 0
        self.lastname = dict["lastname"] as? String ?? ""
        self.firstname = dict["firstname"] as? String ?? ""
        self.unique_code = dict["unique_code"] as? String ?? ""
        self.profile_pic = dict["profile_pic"] as? String ?? ""
        self.profile_name = dict["profile_name"] as? String ?? ""
        self.selectionData = dict as! [String:Any]
    }
}

class EmployeesListInProfileData: NSObject {
    
    var id = 0
    var role = 0
    var reason = 0
    var user_id = 0
    var profile_id = 0
    var logo = ""
    var Emp_Fname = ""
    var Emp_Lname = ""
    var role_name = ""
    var email = ""
    var status = ""
    var valid_to = ""
    var valid_from = ""
    var department = ""
    var designation = ""
    var employee_no = ""
    var profile_pic = ""
    var profilename = ""
    var checkStatus = ""
    var UniqueCode = ""
    private let dict: NSDictionary = [:]
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        
        self.id = dict["id"] as? Int ?? 0
        self.role = dict["role"] as? Int ?? 0
        self.reason = dict["reason"] as? Int ?? 0
        self.user_id = dict["user_id"] as? Int ?? 0
        self.profile_id = dict["profile_id"] as? Int ?? 0
        self.status = dict["status"] as? String ?? ""
        self.role_name = dict["role_name"] as? String ?? ""
        self.valid_to = dict["valid_to"] as? String ?? ""
        self.valid_from = dict["valid_from"] as? String ?? ""
        self.department = dict["department"] as? String ?? ""
        self.designation = dict["designation"] as? String ?? ""
        self.employee_no = dict["employee_no"] as? String ?? ""
        self.checkStatus = dict["created_at"] as? String ?? ""
        
        if let dataDict = dict["user"] as? NSDictionary
        {
            self.email = dataDict["email"] as? String ?? ""
            self.Emp_Fname = dataDict["firstname"] as? String ?? ""
            self.Emp_Lname = dataDict["lastname"] as? String ?? ""
            self.profile_pic = dataDict["profile_pic"] as? String ?? ""
        }
        
        if let dataDict = dict["profile"] as? NSDictionary
        {
            self.logo = dataDict["logo"] as? String ?? ""
            self.profilename = dataDict["name"] as? String ?? ""
            self.UniqueCode = dataDict["unique_code"] as? String ?? ""
        }
        
    }
}


class QKProfileVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, saveEditedChangesforQKProfile, saveEditedChangesforQKProfileBasicInfo {
    
    //<<----------IBOutlets---------->>//
    var int_userID = -1
    var int_socialID = 1
    var Notification_ID = 0
    var strBlockProfileID = 0
    var str_uniquecode = ""
    var str_Screen_From = ""
    var removeAnimationInSelectedTime = ""
    var picker = UIPickerView()
    var selectIndex = IndexPath()
    var lastContentOffset: CGFloat = 0
    var SocialAlert: UIAlertController?
    var arrType = ["Facebook","Linkedin","Twitter","Instagram"]
    var arrHeaderType = [String]()
    var arrSettingOptions = ["Set as Default","Transfer Profile","Delete"]
    var QKProfileData: QKProfileDataList! = QKProfileDataList()
    var arr_EmployeessList: [EmployeesListInProfileData]! = [EmployeesListInProfileData]()
    var arr_filter_EmployeessList: [EmployeesListInProfileData]! = [EmployeesListInProfileData]()
    var arr_FollowersData: [QKProfileFollowersListData]! = [QKProfileFollowersListData]()
    var arr_filter_FollowersData: [QKProfileFollowersListData]! = [QKProfileFollowersListData]()
    var arrKeys = [String]()
    var arr_SectionNumber = [[String : Any]]()
    
    
    @IBOutlet var btn_back: UIButton!
    @IBOutlet var Scrolltop: UIButton!
    @IBOutlet var btn_svachanges: UIButton!
    @IBOutlet var btn_FilterEmp: UIButton!
    @IBOutlet var btn_editBasicInfo: UIButton!
    @IBOutlet var viewUnblock: UIView!
    @IBOutlet var btnUnblock: UIButton!
    @IBOutlet var imgUnblock: UIImageView!
    @IBOutlet var lblUnblock: UILabel!
    @IBOutlet var lblNoEmployess: UILabel!
    
    var arrData:[[String:Any]] = []
    var arrMainData:[[String:Any]] = []
    
    //New IBOUTLETS
    var strAbout = ""
    var strUserName = ""
    var strScreenFrom = ""
    var strFollowersCheck = ""
    var str_email = ""
    var strF_name = ""
    var strL_name = ""
    var str_designation = ""
    var strPhone_ADD = ""
    var strTypedPhone = ""
    var strSelectCountryCode = ""
    var PhoneSenderTag: Int = 0
    var currentPage: Int = 0
    var Search_Height: CGFloat = 75
    var scrolling_Height: CGFloat = 0
    var minimumHeaderHeight: CGFloat = 60
    var maximumHeaderHeight: CGFloat = 270
    var selectedIndexRowForAddUpdateEmp = 0
    
    @IBOutlet weak var img_user_profile: UIImageView!
    @IBOutlet weak var lbluser_Name: UILabel!
    @IBOutlet weak var lbl_About: UILabel!
    @IBOutlet weak var lbl_ReadMore: UILabel!
    @IBOutlet weak var lbl_follow_Txt: UILabel!
    @IBOutlet weak var collection_Header: UICollectionView!
    @IBOutlet var constraint_header_height: NSLayoutConstraint!
    @IBOutlet var constraint_tbl_leading: NSLayoutConstraint!
    @IBOutlet var constraint_tbl_trelling: NSLayoutConstraint!
    @IBOutlet var constraint_search_bar_trelling: NSLayoutConstraint!
    @IBOutlet var constraint_view_unblock_height: NSLayoutConstraint!
    @IBOutlet var tab_Profile: UITableView!
    @IBOutlet weak var followersearchBar: UISearchBar!
    @IBOutlet weak var view_Accept_Reject: UIView!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject : UIButton!
    @IBOutlet weak var viewHeaderBG : UIView!
    @IBOutlet weak var viewTopBG : UIView!
    @IBOutlet weak var lblTopUnderline : UILabel!
    @IBOutlet weak var viewSearchBG : UIView!
    
    private var myContext = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        strFollowersCheck = ""
        app_Delegate.strOpenView = ""
        app_Delegate.strAssignRole = ""
        viewUnblock.isHidden = true
        lblNoEmployess.isHidden = true
        app_Delegate.dictAddMemberInProfile = [:]
        constraint_view_unblock_height.constant = 0
        arr_EmployeessList.removeAll()
        arr_filter_EmployeessList.removeAll()
        btn_FilterEmp.setImage(#imageLiteral(resourceName: "filter").tint(with: KVClass.themeColor()), for: .normal)
        btn_editBasicInfo.setImage(#imageLiteral(resourceName: "edit").tint(with: KVClass.themeColor()), for: .normal)
        self.btn_svachanges.isHidden = self.str_uniquecode != "" ? true : false
        self.btn_editBasicInfo.isHidden = self.str_uniquecode != "" ? true : false
        self.collection_Header.isHidden = true
        self.Scrolltop.isHidden = true
        viewSearchBG.isHidden = true
        followersearchBar.delegate = self
        self.viewTopBG.isHidden = true
        //self.viewHeaderBG.isHidden = true
        
        
        //add Label About Tap Gesture
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.TapdetailAboutTexttap))
        lbl_About.isUserInteractionEnabled = true
        lbl_About.addGestureRecognizer(tap)
        
        let tapRread = UITapGestureRecognizer(target: self, action: #selector(self.TapdetailAboutTexttap))
        lbl_ReadMore.isUserInteractionEnabled = true
        lbl_ReadMore.addGestureRecognizer(tapRread)
        //==================================//
        
        //Get Default Country Code
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print(countryCode)
            self.strSelectCountryCode = app_Delegate.CURRENT_COUNTRY_CODE
        }
        
        
        self.Scrolltop.isHidden = false
        view_Accept_Reject.isHidden = true
        if str_Screen_From == "AcceptReject" {
            self.Scrolltop.isHidden = true
            view_Accept_Reject.isHidden = false
        }
        else if str_Screen_From == "BlockProfile" {
            viewUnblock.isHidden = false
            imgUnblock.image = #imageLiteral(resourceName: "punblock_ic")
            lbl_follow_Txt.text = ""
            lblUnblock.text = "Unblock"
            btn_svachanges.isHidden = true
            btn_editBasicInfo.isHidden = true
            constraint_view_unblock_height.constant = 95
            maximumHeaderHeight = maximumHeaderHeight + 95
        }
        else if str_Screen_From == "Unfollow_Profile" {
            viewUnblock.isHidden = false
            imgUnblock.image = #imageLiteral(resourceName: "unfollow_ic")
            lblUnblock.text = "Unfollow"
            btn_svachanges.isHidden = true
            btn_editBasicInfo.isHidden = true
            constraint_view_unblock_height.constant = 122
            maximumHeaderHeight = maximumHeaderHeight + 122
        }
        
        //=======================================================================================================//
        //=======================================================================================================//
        //================================================================//
        //================================================================//
        self.tab_Profile.contentInset = UIEdgeInsetsMake(self.maximumHeaderHeight, 0, 0, 0)
        self.constraint_header_height.constant = self.maximumHeaderHeight
        tab_Profile.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.none)
        //================================================================//
        //================================================================//
        
        
        //==============================================================//
        //Call Api For Profile Detail
        
        if self.int_userID != -1
        {
            
            if KVAppDataServieces.shared.getUserProfileDetail(self.int_userID) != nil
            {
                self.QKProfileData = QKProfileDataList.init(KVAppDataServieces.shared.getUserProfileDetail(self.int_userID)!)
                //Set Data in Table View
                self.setProfileDetailFromDatawise()
                //=================================================================================//
                //=================================================================================//
            }
        }
        else if self.str_uniquecode != ""
        {
            
            if KVAppDataServieces.shared.getUserProfileDetailFromUniqueCode(self.str_uniquecode) != nil
            {
                self.QKProfileData = QKProfileDataList.init(KVAppDataServieces.shared.getUserProfileDetailFromUniqueCode(self.str_uniquecode)!)
                //Set Data in Table View
                self.setProfileDetailFromDatawise()
                //=================================================================================//
                //=================================================================================//
            }
            
            
        }
        
        
        self.callAPIforProfileData()
        //==============================================================//
        //==============================================================//
        
        //=========Add Refresh Control For Pull To Refresh==============//
        self.tab_Profile.pullTorefresh(#selector(self.callAPIforProfileData), self)
        //==============================================================//
        //==============================================================//
        
        //=======================================================================================================//
        //Table Header Cell Register
        tab_Profile.register(UINib(nibName: "FollowersTableCell", bundle: nil), forCellReuseIdentifier: "FollowersTableCell")
        
        tab_Profile.register(UINib(nibName: "EmployeesTableCell", bundle: nil), forCellReuseIdentifier: "EmployeesTableCell")
        
        tab_Profile.register(UINib(nibName: "AnalyticsComingSoonTableCell", bundle: nil), forCellReuseIdentifier: "AnalyticsComingSoonTableCell")
        
        tab_Profile.register(UINib(nibName: "SettingProfileTableCell", bundle: nil), forCellReuseIdentifier: "SettingProfileTableCell")
        
        
        tab_Profile.estimatedRowHeight = 70.0
        tab_Profile.rowHeight = UITableViewAutomaticDimension
        //=======================================================================================================//
        
        
        //Add Shodow in ViewMAin Accept and Reject
        // ** ===== Card Shadow ===== ** //
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1, execute: {
            let shadowSize : CGFloat = 5
            let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                       y: -shadowSize / 2,
                                                       width: self.view_Accept_Reject.frame.size.width + shadowSize,
                                                       height: self.view_Accept_Reject.frame.size.height + shadowSize))
            self.view_Accept_Reject.layer.masksToBounds = false
            self.view_Accept_Reject.layer.cornerRadius = 4
            self.view_Accept_Reject.layer.shadowColor = UIColor.black.cgColor
            self.view_Accept_Reject.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            self.view_Accept_Reject.layer.shadowOpacity = 0.1
            self.view_Accept_Reject.layer.shadowPath = shadowPath.cgPath
            
        })
        
//        // ** ===== Card Shadow ===== ** //
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1, execute: {
//            let shadowSize : CGFloat = 3
//            let shadowPath = UIBezierPath(rect: CGRect(x: 0,
//                                                       y: 0,
//                                                       width: self.viewTopBG.frame.size.width + 0,
//                                                       height: self.viewTopBG.frame.size.height + shadowSize))
//            self.viewTopBG.layer.masksToBounds = false
//            self.viewTopBG.layer.shadowColor = UIColor.black.cgColor
//            self.viewTopBG.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//            self.viewTopBG.layer.shadowOpacity = 0.1
//            self.viewTopBG.layer.shadowPath = shadowPath.cgPath
//        })
//        // ** =====  ===== ** //
        
        // ** =====  ===== ** //
        
        
        viewTopBG.isHidden = true
        //viewHeaderBG.isHidden = true
        lbl_About.isHidden = true
        lbl_ReadMore.isHidden = true
        lbluser_Name.isHidden = true
        img_user_profile.isHidden = true
        viewUnblock.isHidden = true
        
        //=======================================================================================================//
        //=======================================================================================================//
        collection_Header.reloadData()
        
        //===================================================================//
        //==========================Right Side Swipe=========================//
        let rightSideSwipe = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeeRight(_:)))
        rightSideSwipe.direction = .right
        self.view.addGestureRecognizer(rightSideSwipe)
        //===================================================================//
        //===================================================================//
        
        //===================================================================//
        //==========================Left Side Swipe=========================//
        let leftSideSwipe = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeeLeft(_:)))
        leftSideSwipe.direction = .left
        self.view.addGestureRecognizer(leftSideSwipe)
        //===================================================================//
        //===================================================================//
    }
    
    
    
    //=================Get Observer Value====================================================//
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    
        if context == &myContext {
            app_Delegate.isBackChange = true
            print("changed. \(String(describing: change?[NSKeyValueChangeKey.newKey]))")
        }
    }
    //================================================================================================//
    
    // MARK: Tap About Label Text
    func TapdetailAboutTexttap(sender:UITapGestureRecognizer) {
        
        let getAbout_Height = self.estimatedHeightOfAboutLabel(text: lbl_About.text!)
        
        if getAbout_Height > 22 {

            DispatchQueue.main.async {
                
                let Alert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
                
                let attributedMessage = NSMutableAttributedString(string: self.lbl_About.text!, attributes: [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
                Alert.setValue(attributedMessage, forKey: "attributedMessage")
                
                let no = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                    Alert.dismiss(animated: true, completion: nil)
                })
                Alert.addAction(no)
                self.present(Alert, animated: true, completion: nil)
            }
        }
        print("tap working")
    }
    //================================================================================================//
    //================================================================================================//
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.viewDidLayoutSubviews()
        
        //Instant add New Employee In Employee List',',',',',,',',,',',,',',,',',','','','','',,'','','','','','//
        
        if !(app_Delegate.dictAddMemberInProfile.count == 0) {
            let createandUpdatedID = app_Delegate.dictAddMemberInProfile["id"] as? Int ?? 0
            if createandUpdatedID == self.arr_EmployeessList[selectedIndexRowForAddUpdateEmp].id {
                arr_EmployeessList.remove(at: selectedIndexRowForAddUpdateEmp)
            }
            self.arr_EmployeessList.append(EmployeesListInProfileData.init(app_Delegate.dictAddMemberInProfile))
            self.arr_filter_EmployeessList = self.arr_EmployeessList
            self.arr_EmployeessList = self.arr_EmployeessList.sorted(by: { $0.Emp_Fname < $1.Emp_Lname } )
            tab_Profile.reloadData()
        }
        
        if app_Delegate.strAssignRole == "AssignRole" {
            self.arr_EmployeessList[selectedIndexRowForAddUpdateEmp].role = app_Delegate.roleID
            self.arr_EmployeessList[selectedIndexRowForAddUpdateEmp].role_name = app_Delegate.roleNAME
            self.arr_filter_EmployeessList[selectedIndexRowForAddUpdateEmp].role = app_Delegate.roleID
            self.arr_filter_EmployeessList[selectedIndexRowForAddUpdateEmp].role_name = app_Delegate.roleNAME
            tab_Profile.reloadData()
        }
        
        //,',',',',,',',,',',,',',,',',','',',',',',',,',',',,',',,',',,',',,',',','','','','',,'','','','','','//
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedTransferedProfile(notification:)), name: Notification.Name("TRANSFERPROFILESUCCESS"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedReloadFollowersAndEmployees(notification:)), name: Notification.Name("RELOADNOTIFICATION"), object: nil)
    }
    
    //================================================================================//
    //MARK :- Received Notification
    func methodOfReceivedReloadFollowersAndEmployees(notification: NSNotification){
        //Take Action on Notification
        self.callAPIforFollowerListData()
        self.callAPIforEmployeesListData()
    }
    //================================================================================//

    
    
    func methodOfReceivedTransferedProfile(notification: NSNotification){
        //Take Action on Notification
        app_Delegate.isBackChange = true
        self.QKProfileData.is_transfer = 1
        self.QKProfileData.transfer_status = 0
        self.tab_Profile.reloadData()
        self.strScreenFrom = "AppDelegate"
        //self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    
    
    
    
    
    //=========================================================================================================//
    //MARK : Swipe Gesture
    //=========================================================================================================//
    @IBAction func swipeeRight(_ sender: UISwipeGestureRecognizer)
    {
        if self.QKProfileData.type == 3 || !(self.QKProfileData.Status == "A") {
            if currentPage == 4 || currentPage == 3 {
                currentPage = currentPage - 1
            }
        }
        if self.currentPage > 0
        {
            // Disable interaction during animation
            view.isUserInteractionEnabled = false
            
            
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.currentPage = self.currentPage - 1
                
            }, completion: { (finished) in
                
                if self.QKProfileData.Status == "A" {
                    self.arrSettingOptions = ["Set as Default","Transfer Profile","Delete"]
                }
                else {
                    self.arrSettingOptions = ["Delete"]
                }
                
                let strTitle_H = self.getSelectedTitleFromSelectPageForSwipe(page: self.currentPage)
                self.currentPage = self.getSelectedCurrentPage(strTitle: strTitle_H)
                self.TappedSelectIndexInHeaderCell(PAGED: self.currentPage)
                self.collection_Header.scrollToItem(at: self.selectIndex, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
                self.tab_Profile.reloadData()
                self.collection_Header.reloadData()
                self.view.isUserInteractionEnabled = true
            })
        }
    }
    
    @IBAction func swipeeLeft(_ sender: UISwipeGestureRecognizer)
    {
        if self.QKProfileData.type == 3 || !(self.QKProfileData.Status == "A") {
            if currentPage == 3 {
                currentPage = currentPage - 1
            }
        }
        if currentPage < (arrHeaderType.count)-1
        {
            
            // Disable interaction during animation
            view.isUserInteractionEnabled = false
            
            
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.currentPage = self.currentPage + 1
                
            }, completion: { (finished) in
                
                if self.QKProfileData.Status == "A" {
                    self.arrSettingOptions = ["Set as Default","Transfer Profile","Delete"]
                }
                else {
                    self.arrSettingOptions = ["Delete"]
                }
                
                let strTitle_H = self.getSelectedTitleFromSelectPageForSwipe(page: self.currentPage)
                self.currentPage = self.getSelectedCurrentPage(strTitle: strTitle_H)
                self.TappedSelectIndexInHeaderCell(PAGED: self.currentPage)
                self.collection_Header.scrollToItem(at: self.selectIndex, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
                self.tab_Profile.reloadData()
                self.collection_Header.reloadData()
                self.view.isUserInteractionEnabled = true
            })
        }
    }
    //=========================================================================================================//
    
    func TappedSelectIndexInHeaderCell(PAGED: Int) {
        self.viewSearchBG.isHidden = true
        switch PAGED {
        case 0:
            self.lblNoEmployess.isHidden = true
            self.constraint_tbl_leading.constant = 12
            self.constraint_tbl_trelling.constant = 12
            self.view.backgroundColor = UIColor.groupTableViewBackground
            self.tab_Profile.backgroundColor = UIColor.groupTableViewBackground
            self.tab_Profile.pullTorefresh(#selector(self.callAPIforProfileData), self)
            
        case 1:
            if self.strFollowersCheck == "" {
                self.strFollowersCheck = "Date"
                //Call API=======================//
                
                var USER_ID = 0
                
                //Get Followers User List From Local Database
                if self.str_Screen_From == "OnlyViewProfile" {
                    USER_ID = strBlockProfileID
                }
                else {
                    USER_ID = self.int_userID
                }
                
                if KVAppDataServieces.shared.getFollowersUserList(USER_ID) != nil
                {
                    for item in KVAppDataServieces.shared.getFollowersUserList(USER_ID)! {
                        self.arr_FollowersData.append(QKProfileFollowersListData.init(item))
                        self.arr_filter_FollowersData.append(QKProfileFollowersListData.init(item))
                    }
                    tab_Profile.reloadData()
                }
                self.callAPIforFollowerListData()
            }
            self.lblNoEmployess.isHidden = true
            self.followersearchBar.text = ""
            self.viewSearchBG.isHidden = false
            self.constraint_search_bar_trelling.constant = 0
            self.constraint_tbl_leading.constant = 0
            self.constraint_tbl_trelling.constant = 0
            self.tab_Profile.refreshControl = nil
            self.view.backgroundColor = UIColor.white
            self.tab_Profile.backgroundColor = UIColor.white
            self.followersearchBar.placeholder = "Search Followers..."
            self.tab_Profile.pullTorefresh(#selector(self.callAPIforFollowerListData), self)
            
        case 2:
            if arr_EmployeessList.count == 0 {
                self.arr_EmployeessList.removeAll()
                
                if self.str_Screen_From == "OnlyViewProfile" || self.str_Screen_From == "View/EditProfile" {
                }
                else {
                    let dictCreateEmp : NSDictionary = [ "created_at" : ""]
                    self.arr_EmployeessList.append(EmployeesListInProfileData.init(dictCreateEmp))
                    self.arr_filter_EmployeessList.append(EmployeesListInProfileData.init(dictCreateEmp))
                }
                
                //Call API=======================//
                self.callAPIforEmployeesListData()
            }
            self.lblNoEmployess.isHidden = arr_EmployeessList.count == 0 ? false : true
            
            self.followersearchBar.text = ""
            self.viewSearchBG.isHidden = false
            self.constraint_search_bar_trelling.constant = 40
            self.constraint_tbl_leading.constant = 0
            self.constraint_tbl_trelling.constant = 0
            self.tab_Profile.refreshControl = nil
            self.view.backgroundColor = UIColor.white
            self.tab_Profile.backgroundColor = UIColor.white
            self.followersearchBar.placeholder = "Search Employees..."
            self.tab_Profile.pullTorefresh(#selector(self.callAPIforEmployeesListData), self)
            
        case 3:
            self.lblNoEmployess.isHidden = true
            constraint_tbl_leading.constant = 0
            constraint_tbl_trelling.constant = 0
            self.tab_Profile.refreshControl = nil
            self.view.backgroundColor = UIColor.white
            tab_Profile.backgroundColor = UIColor.white
            
        case 4:
            self.lblNoEmployess.isHidden = true
            self.constraint_tbl_leading.constant = 0
            self.constraint_tbl_trelling.constant = 0
            self.tab_Profile.refreshControl = nil
            self.view.backgroundColor = UIColor.white
            self.tab_Profile.backgroundColor = UIColor.white
            
        default:
            break
        }
    }
    //=========================================================================================================//
    //=========================================================================================================//
    //=========================================================================================================//
    //=========================================================================================================//
    
    
    
    
    
    
    
    
    
    
    
    
    // MARK :- PROTOCALL
    //===============================================================================================//
    //============== Button Action Method in PROTO Table Cell========================================//
    
    func protocallSavechangesProfile(_ imgstr: String, _ profilename: String, _ about: String) {
        
        self.QKProfileData.logobase64 = imgstr
        self.QKProfileData.name = profilename
        self.QKProfileData.about = about
        lbl_About.text = self.QKProfileData.about
        lbl_ReadMore.isHidden = estimatedHeightOfAboutLabel(text: lbl_About.text!) > 36 ? false : true
        self.tab_Profile.reloadData()
    }
    
    func getResultbackEdited(_ teamsize: String, _ estadate: String, _ address: String, _ lat: String, _ long: String) {
        
        self.QKProfileData.company_team_size = Int(teamsize)!
        self.QKProfileData.establish_date = estadate
        self.QKProfileData.address = address
        self.QKProfileData.longitude = long
        self.QKProfileData.latitude = lat
        
        self.tab_Profile.reloadData()
    }
    
    //===============================================================================================//
    //===============================================================================================//
    
    //
    //
    //
    //
    //
    //
    //
    //===============================================================================================//
    //===========API Call For Profile Data===========================================================//
    // MARK : - API CALL
    
    func callAPIforProfileData()
    {
        if self.int_userID != -1
        {
            let dict_param = NSMutableDictionary()
            dict_param["profile_id"] = self.int_userID
            dict_param["user_id"] = KVAppDataServieces.shared.LoginUserID()
            KVClass.KVServiece(methodType: "POST", processView: KVAppDataServieces.shared.getUserProfileDetail(self.int_userID) != nil ? nil : self.navigationController, baseView: self, processLabel: "", params: dict_param, api: "profile/profile_view", arrFlag: false) { (JSON, status) in
                self.tab_Profile.closeEndPullRefresh()
                if status == 1
                {
                    if let dataDict = JSON["data"] as? NSDictionary
                    {
                        print(JSON)
                        app_Delegate.isBackChange = false
                        //self.QKProfileData = QKProfileDataList.init(dataDict)
                        
                        KVAppDataServieces.shared.setUserProfileData(self.int_userID, dataDict)
                        self.QKProfileData = QKProfileDataList.init(dataDict)
                        self.setProfileDetailFromDatawise()
                        //Insert Profile Detail in Local Store
                        
                        //=============================================================//
                        
                        //Set Data in Table View
                        
                        //=================================================================================//
                        //=================================================================================//
                    }
                }
            }
        }
        else if self.str_uniquecode != ""
        {
            let dict_param = NSMutableDictionary()
            dict_param["user_id"] = KVAppDataServieces.shared.LoginUserID()
            dict_param["timezone"] = KVClass.KVTimeZone()
            dict_param["unique_code"] = self.str_uniquecode
            dict_param["latitude"] = KVClass.shareinstance().Lattitude
            dict_param["longitude"] = KVClass.shareinstance().Longitude
            dict_param["device_type"] = "iOS"
            dict_param["model"] = UIDevice.current.model
            dict_param["version"] = UIDevice.current.systemVersion
            if str_Screen_From == "BlockProfile" {
                dict_param["follow"] = "1"
            }
            
            KVClass.KVServiece(methodType: "POST", processView: KVAppDataServieces.shared.getUserProfileDetailFromUniqueCode(self.str_uniquecode) != nil ? nil : self.navigationController, baseView: self, processLabel: "", params: dict_param, api: "scan_user_store", arrFlag: false, Hendler: { (JSON, status) in
                self.tab_Profile.closeEndPullRefresh()
                if status == 1
                {
                    if let dataDict = JSON["data"] as? NSDictionary
                    {
                        
                        KVAppDataServieces.shared.setUserProfileDataFromUniqueCode(self.str_uniquecode, dataDict)
                        self.QKProfileData = QKProfileDataList.init(dataDict)
                        self.setProfileDetailFromDatawise()
                    }
                }
                else {
                    KVClass.ShowNotification("", JSON["msg"] as? String ?? "")
                    return
                }
                
            })
            
        }
    }
    
    
    func setProfileDetailFromDatawise() {

        if self.int_userID != -1
        {

            app_Delegate.isBackChange = false
            
            _ = self.estimatedHeightOfLabel(text: self.QKProfileData.name)
            _ = self.estimatedHeightOfAboutLabel(text: self.QKProfileData.about)
            self.constraint_header_height.constant = self.maximumHeaderHeight
                        
            self.arrKeys = ["id", "user_id", "type", "name", "address", "latitude", "longitude", "tag", "sub_tag", "logo", "about", "company_team_size", "establish_date", "social_media", "phone", "email", "weblink", "founders","other_address","other_address"]
                        
            //============Add Observer for Change Value========================================//
            for iteam in self.arrKeys {
                self.QKProfileData.addObserver(self, forKeyPath: iteam, options: .new, context: &self.myContext)
            }
            self.arr_SectionNumber.removeAll()
            self.arr_SectionNumber.append(["sec_id":"profil_info","title":"", "row":[1]])
            self.arr_SectionNumber.append(["sec_id":"company_info","title":"", "row":[1]])
            self.arr_SectionNumber.append(["sec_id":"other_address","title":"OTHER ADDRESS", "row":NSArray()])
            if self.QKProfileData.type != 3  {
                self.arr_SectionNumber.append(["sec_id":"founder","title":"FOUNDERS", "row":NSArray()])
            }
            self.arr_SectionNumber.append(["sec_id":"social_media","title":"SOCIAL MEDIA", "row":NSArray()])
            self.arr_SectionNumber.append(["sec_id":"phone","title":"PHONE", "row":NSArray()])
            self.arr_SectionNumber.append(["sec_id":"email","title":"EMAIL", "row":NSArray()])
            self.arr_SectionNumber.append(["sec_id":"web_link","title":"WEB LINK", "row":NSArray()])
                        
            //=================================================================================//
            //=================================================================================//
                        
            //Check Accept and Reject Status
            if self.strScreenFrom == "AppDelegate" {
                if (self.QKProfileData.is_transfer == 1) && (self.QKProfileData.transfer_status == 0) {
                    self.view_Accept_Reject.isHidden = false
                }
                else {
                    self.strScreenFrom = "AppDelegate"
                    self.view_Accept_Reject.isHidden = true
                }
            }
            if (self.QKProfileData.is_transfer == 1) && (self.QKProfileData.transfer_status == 1) {
                self.strScreenFrom = "AppDelegate"
            }
            //=================================================================================//
            self.tab_Profile.reloadData()
        }
        
        else if self.str_uniquecode != ""
        {
            
            self.arr_SectionNumber.removeAll()
            
            self.arr_SectionNumber.append(["sec_id":"profil_info","title":"", "row":[1]])
            self.arr_SectionNumber.append(["sec_id":"company_info","title":"", "row":[1]])
            if self.QKProfileData.type != 3  {
                self.arr_SectionNumber.append(["sec_id":"other_address","title":"OTHER ADDRESS", "row":NSArray()])
            }
            self.arr_SectionNumber.append(["sec_id":"founder","title":"FOUNDERS", "row":NSArray()])
            self.arr_SectionNumber.append(["sec_id":"social_media","title":"SOCIAL MEDIA", "row":NSArray()])
            self.arr_SectionNumber.append(["sec_id":"phone","title":"PHONE", "row":NSArray()])
            self.arr_SectionNumber.append(["sec_id":"email","title":"EMAIL", "row":NSArray()])
            self.arr_SectionNumber.append(["sec_id":"web_link","title":"WEB LINK", "row":NSArray()])
            
            if self.str_Screen_From == "Unfollow_Profile" {
                self.lbl_follow_Txt.text = "You just started following " + self.QKProfileData.name
            }
            
            self.tab_Profile.reloadData()
            
        }
    }
    //===============================================================================================//
    //===============================================================================================//
 
    
    //===============================================================================================//
    //===========API Call For Followers List Data===========================================================//
    // MARK : - Followers LIST API CALL
    func callAPIforFollowerListData()
    {
        var UUSER_ID = 0
        
        let dict_param = NSMutableDictionary()
        dict_param["type"] = "follower"
        
        if self.str_Screen_From == "OnlyViewProfile" {
            UUSER_ID = self.strBlockProfileID
            dict_param["profile_id"] = self.strBlockProfileID
        }
        else {
            UUSER_ID = self.int_userID
            dict_param["profile_id"] = self.int_userID
        }
        
        KVClass.KVServiece(methodType: "POST", processView: KVAppDataServieces.shared.getFollowersUserList(UUSER_ID) != nil ? nil : self.navigationController, baseView: self, processLabel: "", params: dict_param, api: "profile/followers_list", arrFlag: false) { (JSON, status) in
            self.tab_Profile.closeEndPullRefresh()
            if status == 1
            {
               
                if let dataDict = JSON["data"] as? [NSDictionary]
                {
                    self.arr_FollowersData.removeAll()
                    self.arr_filter_FollowersData.removeAll()
                    print(JSON)
                    for item in dataDict {
                        self.arr_FollowersData.append(QKProfileFollowersListData.init(item))
                        self.arr_filter_FollowersData.append(QKProfileFollowersListData.init(item))
                    }
                    
                    //Set Followers User List in Local Database
                    if self.str_Screen_From == "OnlyViewProfile" {
                        KVAppDataServieces.shared.setFollowersUserListFromProfile_ID(self.strBlockProfileID, JSON)
                    }
                    else {
                        KVAppDataServieces.shared.setFollowersUserListFromProfile_ID(self.int_userID, JSON)
                    }
                    //================================================================================//
                    
                    if self.currentPage == 1 {
                        self.tab_Profile.reloadData()
                    }
                }
            }
            else {
                self.arr_FollowersData.removeAll()
            }
        }
    }
    //===============================================================================================//
    //===============================================================================================//
    //===============================================================================================//
    //===============================================================================================//
    
    
    //===============================================================================================//
    //===========API Call For All Employees in Current Profile=======================================//
    // MARK : - Employees LIST API CALL
    func callAPIforEmployeesListData() {
        var UUSER_ID = 0
        
        let dict_param = NSMutableDictionary()
        
        if self.str_Screen_From == "OnlyViewProfile" {
            UUSER_ID = self.strBlockProfileID
            dict_param["profile_id"] = self.strBlockProfileID
        }
        else {
            UUSER_ID = self.int_userID
            dict_param["profile_id"] = self.int_userID
        }
        dict_param["user_id"] = KVAppDataServieces.shared.LoginUserID()
        KVClass.KVServiece(methodType: "POST", processView: KVAppDataServieces.shared.getEmployeesUserList(UUSER_ID) != nil ? nil : self.navigationController, baseView: self, processLabel: "", params: dict_param, api: "member/member_list", arrFlag: false) { (JSON, status) in
            self.tab_Profile.closeEndPullRefresh()
            if status == 1
            {

                if let dataDict = JSON["data"] as? [NSDictionary]
                {
                    self.arr_EmployeessList.removeAll()
                    self.arr_filter_EmployeessList.removeAll()
                    print(JSON)

                    if self.str_Screen_From == "OnlyViewProfile" || self.str_Screen_From == "View/EditProfile" {
                    }
                    else {
                        let dictCreateEmp : NSDictionary = [ "created_at" : ""]
                        self.arr_EmployeessList.append(EmployeesListInProfileData.init(dictCreateEmp))
                        self.arr_filter_EmployeessList.append(EmployeesListInProfileData.init(dictCreateEmp))
                    }
                    
                    
                    for item in dataDict {
                        self.arr_EmployeessList.append(EmployeesListInProfileData.init(item))
                        self.arr_filter_EmployeessList.append(EmployeesListInProfileData.init(item))
                    }
                    
                    //========================================//
                    //Set Employees User List in Local Database
                    if self.str_Screen_From == "OnlyViewProfile" {
                        KVAppDataServieces.shared.setEmployeesUserListFromProfile_ID(self.strBlockProfileID, JSON)
                    }
                    else {
                        KVAppDataServieces.shared.setEmployeesUserListFromProfile_ID(self.int_userID, JSON)
                    }
                    //================================================================================//
                    
                    
                    
                    if self.currentPage == 2 {
                        self.lblNoEmployess.isHidden = self.arr_EmployeessList.count == 0 ? false : true
                        self.tab_Profile.reloadData()
                    }
                }
            }
            else {
                if self.str_Screen_From == "OnlyViewProfile" || self.str_Screen_From == "View/EditProfile" {
                }
                else {
                    self.arr_EmployeessList.removeAll()
                    let dictCreateEmp : NSDictionary = [ "created_at" : ""]
                    self.arr_EmployeessList.append(EmployeesListInProfileData.init(dictCreateEmp))
                    self.arr_filter_EmployeessList.append(EmployeesListInProfileData.init(dictCreateEmp))
                }
                
            }
        }
    }
    //===============================================================================================//
    //===============================================================================================//
    //===============================================================================================//
    //===============================================================================================//
    
    
    
    
    
    
    //===============================================================================================//
    //UIButton Action Event
    // MARK :- SAVE PROFILE
    func getJSONarray(_ object: [NSDictionary]) -> NSMutableArray
    {
        
        let array = NSMutableArray()
        for item in object
        {
            array.add(item)
        }
        return array
    }
    
    
    // MARK : UIButton Action Method
    @IBAction func btnUnblockProfile_Action(_ sender: UIButton) {

        var strAltAction = "Unblock"
        var strAltMsg = "you want to unblock this profile ?"
        if self.str_Screen_From == "Unfollow_Profile" {
            
            strAltAction = "Unfollow"
            strAltMsg = "you want to unfollow this profile ?"
            
            if self.lblUnblock.text == "Follow" {
                
                //=============================================================//
                //MARK : Call API for Follow Unfollow Profile anf Unblock Profile
                self.CallApiForUnblock_UnfollowAndFollowProfile()
                return
                //=============================================================//
                //=============================================================//
            }
        }
        
        
        DispatchQueue.main.async {
            
            let Alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
            
            let attributedTitle = NSMutableAttributedString(string: "Are you sure?", attributes:
                [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
            
            let attributedMessage = NSMutableAttributedString(string: strAltMsg, attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
            Alert.setValue(attributedTitle, forKey: "attributedTitle")
            Alert.setValue(attributedMessage, forKey: "attributedMessage")
            
            let ok = UIAlertAction.init(title: strAltAction, style: .destructive, handler: { (action) in
                
                //MARK : Call API for Follow Unfollow Profile anf Unblock Profile
                self.CallApiForUnblock_UnfollowAndFollowProfile()
                //=============================================================//
            })
            
            let no = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                Alert.dismiss(animated: true, completion: nil)
            })
            Alert.addAction(ok)
            Alert.addAction(no)
            self.present(Alert, animated: true, completion: nil)
        }
        
    }
    
    //============//=================///=================///====================//=//=========================//
    //MARK : Call API for Follow Unfollow Profile anf Unblock Profile
    func CallApiForUnblock_UnfollowAndFollowProfile() {
        //Dictionary for Unblock and unfollow profolie for in Pass in API
        var strAPIName = ""
        let dict_param = NSMutableDictionary()
        if self.str_Screen_From == "Unfollow_Profile" {
            strAPIName = "profile/follow_unfollow"
            if self.lblUnblock.text == "Unfollow" {
                dict_param["status"] = "0"
            }
            else {
                dict_param["status"] = "1"
            }
            dict_param["profile_id"] = self.QKProfileData.id
            dict_param["user_id"] = KVAppDataServieces.shared.LoginUserID()
        }
        else {
            strAPIName = "member/unblock_profile"
            dict_param["profile_id"] = self.strBlockProfileID
            dict_param["user_id"] = KVAppDataServieces.shared.LoginUserID()
        }
        
        KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self.navigationController, processLabel: "", params: dict_param, api: strAPIName, arrFlag: false) { (JSON, status) in
            if status == 1
            {
                if self.str_Screen_From == "Unfollow_Profile" {
                    if self.lblUnblock.text == "Unfollow" {
                        self.imgUnblock.image = #imageLiteral(resourceName: "follow_ic")
                        self.lbl_follow_Txt.text = ""
                        self.lblUnblock.text = "Follow"
                    }
                    else {
                        self.imgUnblock.image = #imageLiteral(resourceName: "unfollow_ic")
                        self.lblUnblock.text = "Unfollow"
                        self.lbl_follow_Txt.text = "You just started following " + self.QKProfileData.name
                    }
                }
                else {
                    KVClass.ShowNotification("", JSON["msg"] as? String ?? "")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    //============//=================///=================///====================//=//=========================//
    //============//=================///=================///====================//=//=========================//
    
    
    // MARK : UIButton Action Method
    @IBAction func btnFilterEmp_Action(_ sender: UIButton) {
            
            let Alert = UIAlertController.init(title: "", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let attributedTitle = NSMutableAttributedString(string: "Filter by", attributes:
                [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
            
            Alert.setValue(attributedTitle, forKey: "attributedTitle")
            
            
            let action_all = UIAlertAction.init(title: "Ascending", style: .default) { (action) in
                
                self.arr_EmployeessList = self.arr_EmployeessList.sorted(by: { $0.Emp_Fname < $1.Emp_Lname } )

                self.tab_Profile.reloadData()
                
            }
            
            let action_history = UIAlertAction.init(title: "Descending", style: .default) { (action) in
                
                self.arr_EmployeessList = self.arr_EmployeessList.sorted(by: { $0.Emp_Fname > $1.Emp_Lname } )
                
                if self.str_Screen_From == "OnlyViewProfile" || self.str_Screen_From == "View/EditProfile" {
                }
                else {
                    self.arr_EmployeessList.removeLast()
                    let dictCreateEmp : NSDictionary = [ "created_at" : ""]
                    self.arr_EmployeessList.insert(EmployeesListInProfileData.init(dictCreateEmp), at: 0)
                }

                self.tab_Profile.reloadData()
                
            }
            
            let action_cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
                
//                self.arrData = self.arrMainData
//                self.tblView.reloadData()
                Alert.dismiss(animated: true, completion: nil)
            }
            
            Alert.addAction(action_all)
            Alert.addAction(action_history)
            Alert.addAction(action_cancel)
            
            self.present(Alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnSaveProfileChanges(_ sender: UIButton) {
        
        self.view.endEditing(true)

        if self.QKProfileData.address == "" {
            KVClass.ShowNotification("", "Please enter Address!")
            return
        }
        else if self.QKProfileData.email.count < 1 {
            KVClass.ShowNotification("", "Please enter email address!")
            return
        }
        else
        {
            let dict_param = NSMutableDictionary()
            
            dict_param["id"] = self.QKProfileData.id
            dict_param["user_id"] = self.QKProfileData.user_id
            dict_param["type"] = self.QKProfileData.type
            dict_param["name"] = self.QKProfileData.name
            dict_param["address"] = self.QKProfileData.address
            dict_param["latitude"] = self.QKProfileData.latitude
            dict_param["longitude"] = self.QKProfileData.longitude
            dict_param["tag"] = self.QKProfileData.tag
            dict_param["sub_tag"] = self.QKProfileData.sub_tag
            dict_param["logo"] = self.QKProfileData.logobase64
            dict_param["about"] = self.QKProfileData.about
            dict_param["company_team_size"] = self.QKProfileData.company_team_size
            if self.QKProfileData.type != 3{
            dict_param["establish_date"] = self.QKProfileData.establish_date
            }
            else{
            dict_param["dob"] = self.QKProfileData.establish_date
            }
            dict_param["social_media"] = self.getJSONarray(self.QKProfileData.social_media)//self.QKProfileData.social_media
            dict_param["phone"] = self.getJSONarray(self.QKProfileData.phone)//self.QKProfileData.phone
            dict_param["email"] = self.getJSONarray(self.QKProfileData.email)//self.QKProfileData.email
            dict_param["weblink"] = self.getJSONarray(self.QKProfileData.weblink)//self.QKProfileData.weblink
            dict_param["founders"] =  self.getJSONarray(self.QKProfileData.founders)//self.QKProfileData.founders
            
           // if self.QKProfileData.type != 3{
               dict_param["otheraddress"] =  self.getJSONarray(self.QKProfileData.other_address)
            //}
            
            KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict_param, api: "profile/create", arrFlag: false, Hendler: { (JSON, status) in
                
                if status == 1
                {
                    app_Delegate.isBackChange = true
                    self.navigationController?.popViewController(animated: true)
                }
                
            })
            
        }
        
    }
    
    
    //=========================================================================================================//
    //=========================================================================================================//
    // MARK : UIButton Approve Action Method
    @IBAction func btnAcceptRequest_Action(_ sender: UIButton) {
        DispatchQueue.main.async {
            
            let Alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
                
            let attributedTitle = NSMutableAttributedString(string: "Are you sure?", attributes:
                    [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
                
            let attributedMessage = NSMutableAttributedString(string: "you want to Accept this profile ?", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
            Alert.setValue(attributedTitle, forKey: "attributedTitle")
            Alert.setValue(attributedMessage, forKey: "attributedMessage")
                
            let ok = UIAlertAction.init(title: "Accept", style: .destructive, handler: { (action) in
                
                //Accept Profile
                let dict_param = NSMutableDictionary()
                dict_param["status"] = "A"
                dict_param["notification_id"] = self.Notification_ID
                dict_param["user_id"] = KVAppDataServieces.shared.LoginUserID()
                KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self.navigationController, processLabel: "", params: dict_param, api: "profile/transfer_request_status", arrFlag: false) { (JSON, status) in
                    if status == 1
                    {
                        app_Delegate.isBackChange = true
                        kUserDefults(true as AnyObject, key: "isnotificationrefresh")
                        KVClass.ShowNotification("", JSON["msg"] as? String ?? "")
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                    
            })
                
            let no = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                Alert.dismiss(animated: true, completion: nil)
            })
            Alert.addAction(ok)
            Alert.addAction(no)
            self.present(Alert, animated: true, completion: nil)
        }
    }
    
    
    // MARK : UIButton Reject Action Method
    @IBAction func btnRejectRequest_Action(_ sender: UIButton) {
        DispatchQueue.main.async {
            
            let Alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
            
            let attributedTitle = NSMutableAttributedString(string: "Are you sure?", attributes:
                [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
            
            let attributedMessage = NSMutableAttributedString(string: "you want to Reject this profile ?", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
            Alert.setValue(attributedTitle, forKey: "attributedTitle")
            Alert.setValue(attributedMessage, forKey: "attributedMessage")
            
            let ok = UIAlertAction.init(title: "Reject", style: .destructive, handler: { (action) in
                
                //Reject Profile
                let dict_param = NSMutableDictionary()
                dict_param["status"] = "R"
                dict_param["notification_id"] = self.Notification_ID
                dict_param["user_id"] = KVAppDataServieces.shared.LoginUserID()
                KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self.navigationController, processLabel: "", params: dict_param, api: "profile/transfer_request_status", arrFlag: false) { (JSON, status) in
                    if status == 1
                    {
                        kUserDefults(true as AnyObject, key: "isnotificationrefresh")
                        KVClass.ShowNotification("", JSON["msg"] as? String ?? "")
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                
                
                
                
            })
            
            let no = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                Alert.dismiss(animated: true, completion: nil)
            })
            Alert.addAction(ok)
            Alert.addAction(no)
            self.present(Alert, animated: true, completion: nil)
        }
    }
    //=========================================================================================================//
    //=========================================================================================================//
    
    
    //=========================================================================================================//
    //=========================================================================================================//
    //=========================================================================================================//
    //=========================================================================================================//
    //=========================================================================================================//
    //Delete Profile
    func DeleteProfilePress()
    {
        let alertView = UIAlertController.init(title: "Are you sure, You want to Delete Profile?", message: nil, preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction.init(title: "Delete", style: .destructive) { (action) in
            
            DispatchQueue.main.async {
                
                let dict_param = NSMutableDictionary()
                dict_param["profile_id"] = self.int_userID
                KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict_param, api: "profile/profile_delete", arrFlag: false) { (JSON, status) in
                    if status == 1
                    {
                        app_Delegate.isBackChange = true
                        KVClass.ShowNotification("", JSON["msg"] as? String ?? "")
                        if self.strScreenFrom == "Slider" {
                            //=====================Specific View Controll PoP====================//
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "tabbar")
                            let navi = UINavigationController.init(rootViewController: vc!)
                            navi.setNavigationBarHidden(true, animated: false)
                            app_Delegate.window?.rootViewController = navi
                            //====================================================================//
                        }
                        else {
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    }
                }
            }
        }
        
        let CancelAction = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            alertView.dismiss(animated: true, completion: nil)
        }
        
        alertView.addAction(deleteAction)
        alertView.addAction(CancelAction)
        
        self.present(alertView, animated: true, completion: nil)
    }
    //=========================================================================================================//
    //=========================================================================================================//
    //=========================================================================================================//
    
    
    //===============================================================================================//
    // MARK : - Back Button ACTION Method
    @IBAction func btnBackpress(_ sender: UIButton) {
        app_Delegate.strProfileScreenFrom = ""
        if self.str_uniquecode == "" {
        if app_Delegate.isBackChange == true {
            self.BackTimeAlert()
            return
        }
        
        if self.strScreenFrom == "AppDelegate" {
            app_Delegate.isBackChange = true
        }
        self.navigationController?.popViewController(animated: true)
        for item in self.arrKeys{
            self.QKProfileData.removeObserver(self, forKeyPath: item, context: &myContext)
        }
            KVProfileCreate.shared.DataDict = NSMutableDictionary()
            KVProfileCreate.shared.arr_ContactInfo.removeAll()
            KVProfileCreate.shared.arr_WebInfo.removeAll()
            KVProfileCreate.shared.arr_Founders.removeAll()
            KVProfileCreate.shared.arr_EmailInfo.removeAll()
            KVProfileCreate.shared.arr_SocialMedia.removeAll()
            KVProfileCreate.shared.arr_OtherAddress.removeAll()
            return
        }
        else {
            if self.strScreenFrom == "AppDelegate" {
                app_Delegate.isBackChange = true
            }
            self.navigationController?.popViewController(animated: true)
            for item in self.arrKeys{
                self.QKProfileData.removeObserver(self, forKeyPath: item, context: &myContext)
            }
            KVProfileCreate.shared.DataDict = NSMutableDictionary()
            KVProfileCreate.shared.arr_ContactInfo.removeAll()
            KVProfileCreate.shared.arr_WebInfo.removeAll()
            KVProfileCreate.shared.arr_Founders.removeAll()
            KVProfileCreate.shared.arr_EmailInfo.removeAll()
            KVProfileCreate.shared.arr_SocialMedia.removeAll()
            KVProfileCreate.shared.arr_OtherAddress.removeAll()
        }
    }
    
    func BackTimeAlert() {
        let Discart = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        
        let attributedTitle = NSMutableAttributedString(string: "Warning !", attributes:
            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
        
        let attributedMessage = NSMutableAttributedString(string: "Do you really want Discard Changes ?", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
        Discart.setValue(attributedTitle, forKey: "attributedTitle")
        Discart.setValue(attributedMessage, forKey: "attributedMessage")
        
        let ok = UIAlertAction.init(title: "Yes", style: .destructive, handler: { (action) in
            
            self.navigationController?.popViewController(animated: true)
            
            KVProfileCreate.shared.DataDict = NSMutableDictionary()
            
        })
        
        let no = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            Discart.dismiss(animated: true, completion: nil)
        })
        
        Discart.addAction(ok)
        Discart.addAction(no)
        
        self.present(Discart, animated: true, completion: nil)
    }
    //===============================================================================================//
    //===============================================================================================//

    
    
    //===============================================================================================//
    // MARK : - SCROLLView Delegate Method
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView is UITableView {
            
            let y = self.maximumHeaderHeight - (scrollView.contentOffset.y + self.maximumHeaderHeight)
            scrolling_Height = min(max(y, self.minimumHeaderHeight), self.maximumHeaderHeight)
            print("ScrollingHeight=======>>\(scrolling_Height)")
            
            if scrolling_Height/self.maximumHeaderHeight < 0.7 {
                self.lbl_About.isHidden = true
                self.lbluser_Name.isHidden = true
                self.lbl_ReadMore.isHidden = true
                self.lblTopUnderline.isHidden = true
                if self.str_Screen_From == "BlockProfile" || self.str_Screen_From == "Unfollow_Profile" {
                    self.viewUnblock.isHidden = true
                }
                self.constraint_header_height.constant = self.minimumHeaderHeight
                //self.tab_Profile.contentInset = UIEdgeInsetsMake(self.maximumHeaderHeight - scrolling_Height, 0, 0, 0)
            }
            else {
                constraint_header_height.constant = scrolling_Height
                //self.tab_Profile.contentInset = UIEdgeInsetsMake(self.maximumHeaderHeight, 0, 0, 0)
            }
            
            
            
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
                self.img_user_profile.layer.cornerRadius = self.img_user_profile.frame.size.width / 2
                self.img_user_profile.clipsToBounds = true
                self.img_user_profile.layer.borderWidth = 3
                self.img_user_profile.layer.borderColor = UIColor.white.withAlphaComponent(0.16).cgColor
                
                if self.scrolling_Height > 110 {
                    if self.scrolling_Height/self.maximumHeaderHeight < 0.7 {
                        self.lbl_About.isHidden = true
                        self.lbluser_Name.isHidden = true
                        self.lbl_ReadMore.isHidden = true
                        self.lblTopUnderline.isHidden = true
                    }
                    else {
                        self.lbl_About.isHidden = false
                        self.lbluser_Name.isHidden = false
                        self.lblTopUnderline.isHidden = false
                        self.lbl_ReadMore.isHidden = self.estimatedHeightOfAboutLabel(text: self.lbl_About.text!) > 36 ? false : true
                    }
                }
                else {
                    self.lbl_About.isHidden = true
                    self.lbluser_Name.isHidden = true
                    self.lbl_ReadMore.isHidden = true
                    self.lblTopUnderline.isHidden = true
                }
                
                if self.str_Screen_From == "BlockProfile" || self.str_Screen_From == "Unfollow_Profile" {
                    if self.scrolling_Height > 220 {
                        self.viewUnblock.isHidden = false
                        if self.scrolling_Height/self.maximumHeaderHeight < 0.7 {
                            self.viewUnblock.isHidden = true
                        }
                        else {
                            self.viewUnblock.isHidden = false
                        }
                    }
                    else {
                        self.viewUnblock.isHidden = true
                    }
                }
                
                
                
            }
            
            
        }
        
        
        if !(str_Screen_From == "AcceptReject") {
            if (self.lastContentOffset < scrollView.contentOffset.y) {
                
                self.Scrolltop.isHidden = false
                
                // moved to top
            } else if (self.lastContentOffset > scrollView.contentOffset.y) {
                self.Scrolltop.isHidden = true
                // moved to bottom
            } else {
                self.Scrolltop.isHidden = true
                // didn't move
            }
        }
        
        
        
        
        
        
        
    }
    
    @IBAction func btnscrolltotoppress(_ sender: UIButton)
    {
//        self.Scrolltop.isHidden = true
//        self.tab_Profile.setContentOffset(CGPoint.zero, animated: true)
//        constraint_header_height.constant = maximumHeaderHeight
        
        
//        self.tab_Profile.contentInset = UIEdgeInsetsMake(self.maximumHeaderHeight, 0, 0, 0)
//        self.constraint_header_height.constant = self.maximumHeaderHeight
        
        self.tab_Profile.setContentOffset(CGPoint.zero, animated: true)
//        tab_Profile.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: UITableViewScrollPosition.top)
        self.lastContentOffset = 0
    }
    //===============================================================================================//
    //===============================================================================================//

    
    
    
    
    //===============================================================================================//
    // MARK : - ADD/EDIT Button ACTION Event
    
    @IBAction func btnAddInfopress(_ sender: UIButton)
    {
        let section = sender.tag / 100
        var row = sender.tag % 100
        row = row - 1
        
        let dict = self.arr_SectionNumber[section]
        let strID = dict["sec_id"] as? String ?? ""
        
       let btn = UIButton.init(type: UIButtonType.custom)
        
        switch strID {
        case "founder":
//            btn.tag = 5001
            self.strPhone_ADD = "NewAdd"
            self.AddFounderpress(sender)
        case "social_media":
           // btn.tag = 5001
            self.AddSocialMediapress(sender)
        case "phone":
           // btn.tag = 5001
            self.strPhone_ADD = "NewAdd"
            self.AddPhonepress(sender)
        case "email":
            //btn.tag = 5001
            self.AddEmailpress(sender)
        case "web_link":
            //btn.tag = 5001
            self.AddWebLinkpress(sender)
        case "other_address":
            //btn.tag = 5001
            self.AddOtherAddress(sender)
        default:
            break
        }
    }
    
    @IBAction func btnEditInfopress(_ sender: UIButton)
    {
        let section = sender.tag / 100
        var row = sender.tag % 100
        row = row - 1
        
        let dict = self.arr_SectionNumber[section]
        let strID = dict["sec_id"] as? String ?? ""
        
        switch strID {
        case "founder":
            self.QKProfileData.founders.remove(at: row)
            self.tab_Profile.reloadSections(IndexSet.init(integer: section), with: .fade)
        case "social_media":
            self.QKProfileData.social_media.remove(at: row)
            self.tab_Profile.reloadSections(IndexSet.init(integer: section), with: .fade)
        case "phone":
            self.QKProfileData.phone.remove(at: row)
            self.tab_Profile.reloadSections(IndexSet.init(integer: section), with: .fade)
        case "email":
            self.QKProfileData.email.remove(at: row)
            self.tab_Profile.reloadSections(IndexSet.init(integer: section), with: .fade)
        case "web_link":
            self.QKProfileData.weblink.remove(at: row)
            self.tab_Profile.reloadSections(IndexSet.init(integer: section), with: .fade)
        case "other_address":
            self.QKProfileData.other_address.remove(at: row)
            self.tab_Profile.reloadSections(IndexSet.init(integer: section), with: .fade)
        default:
            break
        }
    }
    //===============================================================================================//
    //===============================================================================================//
    
    //===============================================================================================//
    // MARK :- ADD INFO ALERT Button Action
    
    func AddFounderpress(_ sender: UIButton)
    {
        DispatchQueue.main.async {
          
        let section = sender.tag / 100
        var row = sender.tag % 100
        row = row - 1
            
        var str_action_title = ""
        if row < 0
        {
            str_action_title = "Add Founders"
        }
        else
        {
            str_action_title = "Edit Founders"
        }
        
        self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
        
        let attributedTitle = NSMutableAttributedString(string: str_action_title, attributes:
            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
        
            self.SocialAlert?.setValue(attributedTitle, forKey: "attributedTitle")
            self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.tag = 2
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.tintColor = UIColor.lightGray
            textfield.clearButtonMode = .whileEditing
            textfield.autocorrectionType = .no
            textfield.autocapitalizationType = .words
            textfield.returnKeyType = .next
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Firstname"
            textfield.accessibilityHint = "name"
            
            if row > -1
            {
                textfield.text = self.QKProfileData.founders[row]["firstname"] as? String ?? ""
            }
                if self.strPhone_ADD == "Exists" {
                    textfield.text = self.strF_name
                }
            
        })
            self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.tintColor = UIColor.lightGray
            textfield.clearButtonMode = .whileEditing
            textfield.autocorrectionType = .no
            textfield.autocapitalizationType = .words
            textfield.returnKeyType = .next
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Lastname"
            textfield.accessibilityHint = "name"
            
            if row > -1
            {
                textfield.text = self.QKProfileData.founders[row]["lastname"] as? String ?? ""
            }
                if self.strPhone_ADD == "Exists" {
                    textfield.text = self.strL_name
                }
        })
            self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.tintColor = UIColor.lightGray
            textfield.clearButtonMode = .whileEditing
            textfield.autocorrectionType = .no
            textfield.autocapitalizationType = .sentences
            textfield.returnKeyType = .next
            textfield.keyboardType = .emailAddress
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Email"
            textfield.accessibilityHint = "nameEmail"
                
            if row > -1
            {
                textfield.text = self.QKProfileData.founders[row]["email"] as? String ?? ""
            }
                if self.strPhone_ADD == "Exists" {
                    textfield.text = self.str_email
                }
        })
            self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
                
                //textfield.tag = 2
                textfield.borderStyle = .roundedRect
                textfield.delegate = self
                textfield.tintColor = UIColor.lightGray
                textfield.clearButtonMode = .whileEditing
                textfield.autocorrectionType = .no
                textfield.autocapitalizationType = .words
                textfield.returnKeyType = .done
                textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
                textfield.placeholder = "Designation"
                textfield.accessibilityHint = "name"
                
                if row > -1
                {
                    textfield.text = self.QKProfileData.founders[row]["designation"] as? String ?? ""
                }
                if self.strPhone_ADD == "Exists" {
                    textfield.text = self.str_designation
                }
                
            })
        
        let addaction = UIAlertAction.init(title: str_action_title, style: .destructive) { (action) in
            
            if self.SocialAlert?.textFields![0].text == "" || self.SocialAlert?.textFields![1].text == "" || self.SocialAlert?.textFields![3].text == ""
            {
                KVClass.ShowNotification("", "Please enter first name, last name and designation !")
                
                self.strPhone_ADD = "Exists"
                self.str_email = (self.SocialAlert?.textFields![2].text!)!
                self.strF_name = (self.SocialAlert?.textFields![0].text!)!
                self.strL_name = (self.SocialAlert?.textFields![1].text!)!
                self.str_designation = (self.SocialAlert?.textFields![3].text!)!
                self.AddFounderpress(sender)
                
                return
            }
            else if !(self.SocialAlert?.textFields![2].text == "") {
                if !(self.SocialAlert?.textFields![2].text?.isValidEmail())!{
                    
                    KVClass.ShowNotification("", "Enter valid Email !")
                    self.strPhone_ADD = "Exists"
                    self.str_email = (self.SocialAlert?.textFields![2].text!)!
                    self.strF_name = (self.SocialAlert?.textFields![0].text!)!
                    self.strL_name = (self.SocialAlert?.textFields![1].text!)!
                    self.str_designation = (self.SocialAlert?.textFields![3].text!)!
                    self.AddFounderpress(sender)
                    
                    return
                }
            }
            else {
                let dict = NSMutableDictionary()
                dict["firstname"] = self.SocialAlert?.textFields![0].text
                dict["lastname"] = self.SocialAlert?.textFields![1].text
                dict["designation"] = self.SocialAlert?.textFields![3].text
                dict["email"] = self.SocialAlert?.textFields![2].text
                if row < 0
                {
                    self.QKProfileData.founders.append(dict)
                }
                else
                {
                    self.QKProfileData.founders.remove(at: row)
                    self.QKProfileData.founders.insert(dict, at: row)
                }
                self.tab_Profile.reloadSections(IndexSet.init(integer: section), with: .fade)
            }
            
            
            
            
            
        }
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            self.SocialAlert?.dismiss(animated: true, completion: nil)
        }
        
            self.SocialAlert?.addAction(addaction)
            self.SocialAlert?.addAction(cancel)
        
            self.present(self.SocialAlert!, animated: true, completion: nil)
        
            for textfield: UIView in (self.SocialAlert?.textFields!)! {
            let container: UIView = textfield.superview!
            
            let effectView: UIView = container.superview!.subviews[0]
            container.backgroundColor = UIColor.clear
            effectView.removeFromSuperview()
        }
      }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.accessibilityHint == "name" {
            let maxLength = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        
        else if textField.accessibilityHint == "phone" {
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_NUMBERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
    }
    
    
    func didChangeTextPhone() {
        let strPhone_Text = self.SocialAlert?.textFields![0].text ?? ""
        if !(strPhone_Text == "") {
            let strTest = strPhone_Text
            let index = strTest.index(strTest.startIndex, offsetBy: 0)
            let firstCharcter = strTest[index]
            if firstCharcter == "0" {
                self.SocialAlert?.textFields![0].text = ""
                return
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.accessibilityHint == "name" || textField.accessibilityHint == "nameEmail" {
            if textField == self.SocialAlert?.textFields![0] {
                textField.resignFirstResponder()
                self.SocialAlert?.textFields![1].becomeFirstResponder()
                return false
            }
            else if textField == self.SocialAlert?.textFields![1] {
                textField.resignFirstResponder()
                self.SocialAlert?.textFields![2].becomeFirstResponder()
                return false
            }
            else if textField == self.SocialAlert?.textFields![2] {
                textField.resignFirstResponder()
                self.SocialAlert?.textFields![3].becomeFirstResponder()
                return false
            }
            textField.resignFirstResponder()
            return true
        }
        textField.resignFirstResponder()
        return true
    }
    //===========================================================================================================//
    //===========================================================================================================//
    //===========================================================================================================//
    
    
    
    
    
    
    func AddSocialMediapress(_ sender: UIButton)
    {
         DispatchQueue.main.async {
            
            let section = sender.tag / 100
            var row = sender.tag % 100
            row = row - 1
            
        var str_action_title = ""
        if row < 0
        {
            str_action_title = "Add Social Media"
        }
        else
        {
            str_action_title = "Edit Social Media"
        }
        
        self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
        let attributedTitle = NSMutableAttributedString(string: str_action_title, attributes:
            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
        
            self.SocialAlert?.setValue(attributedTitle, forKey: "attributedTitle")
            self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.tag = 1
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.tintColor = .clear
            textfield.clearButtonMode = .never
            textfield.autocorrectionType = .no
            textfield.returnKeyType = .next
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Social Media Type"
            textfield.rightViewMode = .always
            if row > -1
            {
                textfield.text = self.arrType[(self.QKProfileData.social_media[row]["type"] as? Int ?? 1) - 1]
                self.int_socialID = (self.QKProfileData.social_media[row]["type"] as? Int ?? 1)
               
            }
            
            let rightView = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 30, height: 30))
            rightView.setImage(UIImage.init(named: "downar_ic")!, for: .normal)
            rightView.tag = textfield.tag
            //rightView.addTarget(self, action: #selector(self.editpress(_:)), for: .touchUpInside)
            textfield.rightView = rightView
            
        })
            self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.isHidden = true
        })
            self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.clearButtonMode = .whileEditing
            textfield.autocorrectionType = .no
            textfield.tintColor = UIColor.lightGray
            
            textfield.returnKeyType = .done
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Social Media URL"
            if row > -1
            {
                textfield.text = self.QKProfileData.social_media[row]["name"] as? String ?? ""
            }
            
        })
        
        let addaction = UIAlertAction.init(title: str_action_title, style: .destructive) { (action) in
            
            if self.SocialAlert?.textFields![2].text == "" || self.SocialAlert?.textFields![2].text == nil
            {
                 KVClass.ShowNotification("", "Please enter valid Media URL !")
                return
            }
            
            let strUrl = self.SocialAlert?.textFields![2].text!
            if (self.SocialAlert?.textFields![2].text?.isValidateUrl(urlString: strUrl!))! {
            }
            else {
                KVClass.ShowNotification("", "Please Enter Proper Media URL")
                return
            }
            
            
            
            
            let dict = NSMutableDictionary()
            dict["name"] = self.SocialAlert?.textFields![2].text
            dict["type"] = self.int_socialID
            if row < 0
            {
                for item in self.QKProfileData.social_media
                {
                    if item["type"] as? Int ?? 1 == self.int_socialID
                    {
                        KVClass.ShowNotification("", "This Social Media is already Added !")
                        return
                    }
                }
                
                self.QKProfileData.social_media.append(dict)
            }
            else
            {
                self.QKProfileData.social_media.remove(at: row)
                self.QKProfileData.social_media.insert(dict, at: row)
            }
            self.tab_Profile.reloadSections(IndexSet.init(integer: section), with: .fade)
            
        }
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            self.SocialAlert?.dismiss(animated: true, completion: nil)
        }
        
            self.SocialAlert?.addAction(addaction)
            self.SocialAlert?.addAction(cancel)
        
            appDelegate.window?.rootViewController?.present(self.SocialAlert!, animated: true, completion: nil)
        
            for textfield: UIView in (self.SocialAlert?.textFields!)! {
            let container: UIView = textfield.superview!
            
            let effectView: UIView = container.superview!.subviews[0]
            container.backgroundColor = UIColor.clear
            effectView.removeFromSuperview()
        }
        }
    }
    
    func AddPhonepress(_ sender: UIButton)
    {
         DispatchQueue.main.async {
            if self.strPhone_ADD == "NewAdd" {
                self.strSelectCountryCode = app_Delegate.CURRENT_COUNTRY_CODE
                self.PhoneSenderTag = sender.tag
            }
            
            let section = self.PhoneSenderTag / 100
            var row = self.PhoneSenderTag % 100
            row = row - 1
            
        var str_action_title = ""
        if row < 0
        {
            str_action_title = "Add Phone"
        }
        else
        {
            str_action_title = "Edit Phone"
        }
        
        self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
        let attributedTitle = NSMutableAttributedString(string: str_action_title, attributes:
            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 16.5)!])
        
            self.SocialAlert?.setValue(attributedTitle, forKey: "attributedTitle")
        
            self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.tag = 0
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.tintColor = UIColor.lightGray
            textfield.clearButtonMode = .whileEditing
            textfield.autocorrectionType = .no
            textfield.keyboardType = .numberPad
            textfield.returnKeyType = .next
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Phone"
                textfield.accessibilityHint = "phone"
                textfield.addTarget(self, action: #selector(self.didChangeTextPhone), for: UIControlEvents.editingChanged)
                
                let viewLeft = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 45, height: 30))
                viewLeft.backgroundColor = UIColor.clear
                let btnPick = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 45, height: 30))
                btnPick.setTitle(self.strSelectCountryCode, for: .normal)
                btnPick.setTitleColor(UIColor.black, for: .normal)
                btnPick.titleLabel?.font =  UIFont(name: "lato-Regular", size: 13)
                btnPick.addTarget(self, action: #selector(self.openCountryPicker), for: .touchUpInside)
                viewLeft.addSubview(btnPick)
                
                textfield.leftView = viewLeft
                textfield.leftViewMode = .always
                
            if row > -1
            {
                self.strTypedPhone = self.QKProfileData.phone[row]["name"] as? String ?? ""
                if self.strTypedPhone != "" {
                    let arrP = self.strTypedPhone.components(separatedBy: " ")
                    if arrP.count == 2 {
                        self.strSelectCountryCode = arrP.first!
                        self.strTypedPhone = arrP.last!
                        btnPick.setTitle(self.strSelectCountryCode, for: .normal)
                    }
                    else {
                        self.strTypedPhone = arrP.first!
                    }
                }
                textfield.text = self.strTypedPhone
                
            }
            if self.strPhone_ADD == "AddCountry" {
                textfield.text = self.strTypedPhone
            }
            self.settxtInputview(textfield)
            
        })
        let addaction = UIAlertAction.init(title: str_action_title, style: .destructive) { (action) in
            
            if !(self.SocialAlert?.textFields![0].text?.isPhoneValid())!
            {
                 KVClass.ShowNotification("", "Please enter valid Phone!")
                self.strTypedPhone = self.SocialAlert?.textFields![0].text ?? ""
                self.strPhone_ADD = "AddCountry"
                self.AddPhonepress(sender)
                return
            }
            if self.SocialAlert?.textFields![0].text == "" || self.SocialAlert?.textFields![0].text == nil
            {
                return
            }
            let dict = NSMutableDictionary()
            dict["name"] = self.strSelectCountryCode + " " + (self.SocialAlert?.textFields![0].text)!
            if row < 0
            {
                self.QKProfileData.phone.append(dict)
            }
            else
            {
                self.QKProfileData.phone.remove(at: row)
                self.QKProfileData.phone.insert(dict, at: row)
            }
            self.tab_Profile.reloadSections(IndexSet.init(integer: section), with: .fade)
            
        }
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            self.SocialAlert?.dismiss(animated: true, completion: nil)
        }
        
            self.SocialAlert?.addAction(addaction)
            self.SocialAlert?.addAction(cancel)
        
            appDelegate.window?.rootViewController?.present(self.SocialAlert!, animated: true, completion: nil)
        
            for textfield: UIView in (self.SocialAlert?.textFields!)! {
            let container: UIView = textfield.superview!
            
            let effectView: UIView = container.superview!.subviews[0]
            container.backgroundColor = UIColor.clear
            effectView.removeFromSuperview()
        }
        }
    }
    
    
    func openCountryPicker(_ sender: UIButton) {
        self.strTypedPhone = self.SocialAlert?.textFields![0].text ?? ""
        print(self.strTypedPhone)
        
        self.SocialAlert?.dismiss(animated: false, completion: {
            app_Delegate.app_CountryPickpopup { (objCountry, isSuccess) in
                print(objCountry?.phoneCode ?? "")
                print(objCountry?.code ?? "")
                print(objCountry?.name ?? "")
                self.strPhone_ADD = "AddCountry"
                self.strSelectCountryCode = objCountry?.phoneCode ?? ""
                debugPrint("check this out!! [\(objCountry?.name! ?? "0")]")
                self.AddPhonepress(sender)
            }
        })
    }
    
    
    func AddEmailpress(_ sender: UIButton)
    {
         DispatchQueue.main.async {
        
            let section = sender.tag / 100
            var row = sender.tag % 100
            row = row - 1
            
        var str_action_title = ""
        if row < 0
        {
            str_action_title = "Add Email"
        }
        else
        {
            str_action_title = "Edit Email"
        }
        self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
        let attributedTitle = NSMutableAttributedString(string: str_action_title, attributes:
            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 16.5)!])
        
            self.SocialAlert?.setValue(attributedTitle, forKey: "attributedTitle")
        
            self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.tag = 0
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.tintColor = UIColor.lightGray
            textfield.clearButtonMode = .whileEditing
            textfield.autocorrectionType = .no
            // textfield.keyboardType = .numberPad
            textfield.returnKeyType = .done
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Email"
            if row > -1
            {
                textfield.text = self.QKProfileData.email[row]["name"] as? String ?? ""
            }
            
            
            
        })
        let addaction = UIAlertAction.init(title: str_action_title, style: .destructive) { (action) in
            
            if !(self.SocialAlert?.textFields![0].text?.isValidEmail())!
            {
                 KVClass.ShowNotification("", "Please enter valid Email!")
                return
            }
            if self.SocialAlert?.textFields![0].text == "" || self.SocialAlert?.textFields![0].text == nil
            {
                KVClass.ShowNotification("", "Please enter valid Email!")
                return
            }
            let dict = NSMutableDictionary()
            dict["name"] = self.SocialAlert?.textFields![0].text
            if row < 0
            {
                self.QKProfileData.email.append(dict)
            }
            else
            {
                self.QKProfileData.email.remove(at: row)
                self.QKProfileData.email.insert(dict, at: row)
            }
            self.tab_Profile.reloadSections(IndexSet.init(integer: section), with: .fade)
            
        }
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            self.SocialAlert?.dismiss(animated: true, completion: nil)
        }
        
            self.SocialAlert?.addAction(addaction)
            self.SocialAlert?.addAction(cancel)
        
            appDelegate.window?.rootViewController?.present(self.SocialAlert!, animated: true, completion: nil)
        
            for textfield: UIView in (self.SocialAlert?.textFields!)! {
            let container: UIView = textfield.superview!
            
            let effectView: UIView = container.superview!.subviews[0]
            container.backgroundColor = UIColor.clear
            effectView.removeFromSuperview()
        }
        }
    }
    
    func AddOtherAddress(_ sender: UIButton){
        
        DispatchQueue.main.async {
            
            let section = sender.tag / 100
            var row = sender.tag % 100
            row = row - 1
            print(section)
            print(row)
            
            var latitude = 0.0
            var longitude = 0.0
            if row < 0{
                latitude = Double(KVClass.shareinstance().Lattitude)!
                longitude = Double(KVClass.shareinstance().Longitude)!
            }
            else{
                latitude = Double(self.QKProfileData.other_address[row]["lati"] as? CGFloat ?? 0)
                longitude = Double(self.QKProfileData.other_address[row]["longi"] as? CGFloat ?? 0)
            }
            
            
            let center = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
            let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            let config = GMSPlacePickerConfig(viewport: viewport)
            let placePicker = GMSPlacePicker(config: config)
            
            placePicker.pickPlace(callback: {(place, error) -> Void in
                if let error = error {
                    print("Pick Place error: \(error.localizedDescription)")
                    return
                }
                
                if let place = place {
                    
                    print(place.coordinate.latitude)
                    print(place.coordinate.longitude)
                    //print(place.
                    
                    if (place.formattedAddress ?? "") != ""
                    {
                        let dataDict = NSMutableDictionary()
                        dataDict["lati"] =  CGFloat(place.coordinate.latitude)
                        dataDict["longi"] = CGFloat(place.coordinate.longitude)
                        dataDict["name"] = place.formattedAddress
                        
                        if row < 0{
                            self.QKProfileData.other_address.append(dataDict)
                        }
                        else{
                            self.QKProfileData.other_address.remove(at: row)
                            self.QKProfileData.other_address.insert(dataDict, at: row)
                        }
                        
                        self.tab_Profile.reloadSections(IndexSet.init(integer: section), with: .fade)
                    }
                    
                    
                } else {
                    
                }
            })
            
        }
    }
    
    
    func AddWebLinkpress(_ sender: UIButton)
    {
         DispatchQueue.main.async {
            
            let section = sender.tag / 100
            var row = sender.tag % 100
            row = row - 1
            
        var str_action_title = ""
        if row < 0
        {
            str_action_title = "Add Web Link"
        }
        else
        {
            str_action_title = "Edit Web Link"
        }
        self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
        let attributedTitle = NSMutableAttributedString(string: str_action_title, attributes:
            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 16.5)!])
        
            self.SocialAlert?.setValue(attributedTitle, forKey: "attributedTitle")
            self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.tag = 0
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.tintColor = UIColor.lightGray
            textfield.clearButtonMode = .whileEditing
            textfield.autocorrectionType = .no
            // textfield.keyboardType = .numberPad
            textfield.returnKeyType = .done
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Web Link"
            if row > -1
            {
                textfield.text = self.QKProfileData.weblink[row]["name"] as? String ?? ""
            }
            
        })
        let addaction = UIAlertAction.init(title: str_action_title, style: .destructive) { (action) in
            
            if self.SocialAlert?.textFields![0].text == "" || self.SocialAlert?.textFields![0].text == nil
            {
                 KVClass.ShowNotification("", "Please enter valid Web Link!")
                return
            }
            
            let strUrl = self.SocialAlert?.textFields![0].text!
            if (self.SocialAlert?.textFields![0].text?.isValidateUrl(urlString: strUrl!))! {
            }
            else {
                KVClass.ShowNotification("", "Please Enter Proper URL")
                return
            }
            
            
            let dict = NSMutableDictionary()
            dict["name"] = self.SocialAlert?.textFields![0].text
            if row < 0
            {
                self.QKProfileData.weblink.append(dict)
                self.tab_Profile.reloadSections(IndexSet.init(integer: section), with: .fade)
            }
            else
            {
                self.QKProfileData.weblink.remove(at: row)
                self.QKProfileData.weblink.insert(dict, at: row)
                self.tab_Profile.reloadSections(IndexSet.init(integer: section), with: .top)
            }
            
            
        }
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            self.SocialAlert?.dismiss(animated: true, completion: nil)
        }
        
            self.SocialAlert?.addAction(addaction)
            self.SocialAlert?.addAction(cancel)
        
            appDelegate.window?.rootViewController?.present(self.SocialAlert!, animated: true, completion: nil)
        
            for textfield: UIView in (self.SocialAlert?.textFields!)! {
            let container: UIView = textfield.superview!
            
            let effectView: UIView = container.superview!.subviews[0]
            container.backgroundColor = UIColor.clear
            effectView.removeFromSuperview()
        }
       }
    }
    //===============================================================================================//
    //===============================================================================================//

    
    
    
    
    
    //===============================================================================================//
    // MARK :- TEXTINPUT ACTION
    
    func settxtInputview(_ txtfield: UITextField)
    {
        picker = UIPickerView.init()
        picker.delegate = self
        picker.dataSource = self
        picker.tag = txtfield.tag
        let toobar = UIToolbar()
        
        toobar.sizeToFit()
        
        if txtfield.tag != 0 {
            self.int_socialID = 1
            picker.selectRow(0, inComponent: 0, animated: true)
        }
        
        
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.doneTxt(_:)))
        doneButton.tag = txtfield.tag
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toobar.setItems([ spaceButton, doneButton], animated: false)
        toobar.isUserInteractionEnabled = true
        
        if txtfield.tag != 0
        {
            txtfield.inputView = picker
            txtfield.inputAccessoryView = toobar
            txtfield.clearButtonMode = .never
        }
        else
        {
            txtfield.inputAccessoryView = toobar
        }
    
    }
    
    @IBAction func doneTxt(_ sender: UIBarButtonItem)
    {
        if sender.tag == 1
        {
            self.SocialAlert?.textFields![0].resignFirstResponder()
            return
        }
        self.SocialAlert?.textFields![0].resignFirstResponder()
        print("hy")
    }
    
    
    //===============================================================================================//
    //===============================================================================================//
    // MARK :- PICKERView Delegate and Datasource Method
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrType.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrType[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
        if self.arrType.count > 0
        {
            if pickerView.tag == 1
            {
                self.SocialAlert?.textFields![0].text = arrType[row]
                self.int_socialID = row + 1
            }
        }
    }
    
    
    //===============================================================================================//
    //===============================================================================================//
    //,,.........................................................................................//
    func getSelectedCurrentPage(strTitle: String) -> Int {
        var selectedPAGE = 0
        if strTitle == "Basic Details" {
            selectedPAGE = 0
        }
        else if strTitle == "Followers" {
            selectedPAGE = 1
        }
        else if strTitle == "Employees" {
            selectedPAGE = 2
        }
        else if strTitle == "Analytics" {
            selectedPAGE = 3
        }
        else if strTitle == "Settings" {
            selectedPAGE = 4
        }
        return selectedPAGE
    }
    
    func getSelectedTitleFromSelectPageForSwipe(page: Int) -> String {
        var selectedTitle = ""
        
        if page == 0 {
            selectedTitle = "Basic Details"
        }
        else if page == 1 {
            selectedTitle = "Followers"
        }
        else if page == 2 {
            if self.QKProfileData.type == 3 || !(self.QKProfileData.Status == "A") {
                selectedTitle = "Analytics"
            }
            else {
                selectedTitle = "Employees"
            }
        }
        else if page == 3 {
            if self.QKProfileData.type == 3 || !(self.QKProfileData.Status == "A") {
                selectedTitle = "Settings"
            }
            else {
                selectedTitle = "Analytics"
            }
        }
        else if page == 4 {
            selectedTitle = "Settings"
        }
        return selectedTitle
    }
    
    func getSelectedTitleFromSelectPage(page: Int) -> String {
        var selectedTitle = ""
        
        if page == 0 {
            selectedTitle = "Basic Details"
        }
        else if page == 1 {
            selectedTitle = "Followers"
        }
        else if page == 2 {
            selectedTitle = "Employees"
        }
        else if page == 3 {
            selectedTitle = "Analytics"
        }
        else if page == 4 {
            selectedTitle = "Settings"
        }
        return selectedTitle
    }
    //,,.........................................................................................//
    //,,.........................................................................................//
    
    //===============================================================================================//
    //===============================================================================================//
    
    //Get Estimate Height User Name Height
    func estimatedHeightOfLabel(text: String) -> CGFloat {
        let size = CGSize(width: UIScreen.main.bounds.width - 44, height: 10000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let attributes = [NSFontAttributeName: UIFont(name: "Lato-Semibold", size: 18)]
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        if rectangleHeight > 22 {
            maximumHeaderHeight = maximumHeaderHeight + 0
        }
        return rectangleHeight
    }
    
    //Get Estimate Height User About Height
    func estimatedHeightOfAboutLabel(text: String) -> CGFloat {
        let size = CGSize(width: UIScreen.main.bounds.width - 44, height: 10000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let attributes = [NSFontAttributeName: UIFont(name: "Lato-Regular", size: 15)]
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        if rectangleHeight > 22 {
            maximumHeaderHeight = maximumHeaderHeight + 0
        }
        return rectangleHeight
    }
}


//===============================================================================================//
//===============================================================================================//
// MARK : - TableView Delegate Datasource Method


extension QKProfileVC: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if (self.SocialAlert?.textFields!.count ?? 0)! > 0
        {
            if textField == self.SocialAlert?.textFields![0] && textField.tag == 1
            {
                if textField.text == nil || textField.text == ""
                {
                    textField.text = self.arrType[0]
                }
                self.settxtInputview(textField)
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if currentPage == 0 {
            if self.str_uniquecode == ""
            {
                self.btn_svachanges.isHidden = self.arr_SectionNumber.count < 1 ? true : false
                self.btn_editBasicInfo.isHidden = self.arr_SectionNumber.count < 1 ? true : false
            }
    
            return self.arr_SectionNumber.count
        }
        else {
            return 1
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch currentPage {
        case 0:

            //For First Page BasicInformation
            let dict = self.arr_SectionNumber[section]
            let strID = dict["sec_id"] as? String ?? ""
            
            switch strID {
            case "profil_info":
                return 0
            case "company_info":
                return 1
            case "founder":
                if self.str_uniquecode != ""
                {
                    return self.QKProfileData.founders.count > 0 ? self.QKProfileData.founders.count + 1 : 0
                }
                return self.QKProfileData.founders.count + 1
            case "social_media":
                if self.str_uniquecode != ""
                {
                    return self.QKProfileData.social_media.count > 0 ? self.QKProfileData.social_media.count + 1 : 0
                }
                return self.QKProfileData.social_media.count + 1
            case "phone":
                if self.str_uniquecode != ""
                {
                    return self.QKProfileData.phone.count > 0 ? self.QKProfileData.phone.count + 1 : 0
                }
                return self.QKProfileData.phone.count + 1
            case "email":
                if self.str_uniquecode != ""
                {
                    return self.QKProfileData.email.count > 0 ? self.QKProfileData.email.count + 1 : 0
                }
                return self.QKProfileData.email.count + 1
            case "web_link":
                if self.str_uniquecode != ""
                {
                    return self.QKProfileData.weblink.count > 0 ? self.QKProfileData.weblink.count + 1 : 0
                }
                return self.QKProfileData.weblink.count + 1
            case "other_address":
                if self.str_uniquecode != ""
                {
                    return self.QKProfileData.other_address.count > 0 ? self.QKProfileData.other_address.count + 1 : 0
                }
                return self.QKProfileData.other_address.count + 1
            default:
                return 0
            }
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
        
        case 1:
            
            //For Second Page Followers
            if self.arr_FollowersData.count == 0 {
                return  1
            }
            return arr_FollowersData.count
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 2:
            
            //For Third Page Employees
            return arr_EmployeessList.count
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 3:
            
            //For Third Page Employees
            return  1
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 4:
            
            //For Third Page Employees
            return  arrSettingOptions.count
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch currentPage {
        case 0:
            
            //For First Page BasicInformation
            let dict = self.arr_SectionNumber[indexPath.section]
            let strID = dict["sec_id"] as? String ?? ""
            
            switch strID {
            case "profil_info":
                return 280
            case "company_info":
                return 280
            case "founder":
                return 280
            case "social_media":
                return 280
            case "phone":
                return 280
            case "email":
                return 280
            case "web_link":
                return 280
            case "other_address":
                return 280
            default:
                return 0
            }
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 1:
            
            //For Second Page Followers
            return 0
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 2:
            
            //For Third Page Employess
            return 0
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 3:
            
            //For Third Page Analycits
            return 0
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 4:
            
            //For Fourth Page Settings
            return 60
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch currentPage {
        case 0:
            
            //For First Page BasicInformation
            return UITableViewAutomaticDimension
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 1:
            
            //For Second Page Followers
//            if self.arr_FollowersData.count == 0 {
//                return 100
//            }
            return UITableViewAutomaticDimension
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 2:
            
            //For Second Page Employees
            return UITableViewAutomaticDimension
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 3:
            
            //For Second Page Analytics
            return 100
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 4:
            
            //For Second Page Settings
            return 60
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        switch currentPage {
        case 0:
            
            //For First Page BasicInformation

            let dict = self.arr_SectionNumber[indexPath.section]
            let strID = dict["sec_id"] as? String ?? ""
            
            switch strID {
            case "profil_info":
                let cell: QKProfileFirstCell = tableView.dequeueReusableCell(withIdentifier: "QKProfileFirstCell", for: indexPath) as! QKProfileFirstCell
                
                
                //cell.collection_action.reloadData()
                if self.QKProfileData.logobase64 != ""
                {
                    cell.img_profile.image = UIImage.init(data: Data.init(base64Encoded: self.QKProfileData.logobase64)!)
                    img_user_profile.image = UIImage.init(data: Data.init(base64Encoded: self.QKProfileData.logobase64)!)
                }
                else if self.QKProfileData.logo != ""
                {
                    SDWebImageManager.shared().loadImage(with: URL.init(string: self.QKProfileData.logo)!, options: SDWebImageOptions.refreshCached, progress: { (pro1, pro2, url) in
                        
                    }, completed: { (img, data, err, type, finish, url) in
                        
                        if err == nil
                        {
                            cell.img_profile.image = (img ?? UIImage.init(named: "logo"))
                            self.QKProfileData.logobase64 = (UIImageJPEGRepresentation(cell.img_profile.image!, 0.25)?.base64EncodedString())!
                            
                            self.img_user_profile.image = (img ?? UIImage.init(named: "logo"))
                            self.QKProfileData.logobase64 = (UIImageJPEGRepresentation(self.img_user_profile.image!, 0.25)?.base64EncodedString())!
                        }
                    })
                }
                else
                {
                    cell.img_profile.image = UIImage.init(named: "logo")
                    img_user_profile.image = UIImage.init(named: "logo")
                }
                
                
                viewTopBG.isHidden = false
                lbl_About.isHidden = false
                if str_Screen_From == "BlockProfile" || str_Screen_From == "Unfollow_Profile" {
                    viewUnblock.isHidden = false
                }
                lbluser_Name.isHidden = false
                img_user_profile.isHidden = false
                lbluser_Name.text = self.QKProfileData.name
                lbl_About.text = self.QKProfileData.about
                cell.lbl_companyname.text = self.QKProfileData.name
                cell.lbl_about.text = self.QKProfileData.about
                lbl_ReadMore.isHidden = estimatedHeightOfAboutLabel(text: lbl_About.text!) > 36 ? false : true
                
                
                cell.btn_edit.isHidden = self.str_uniquecode != "" ? true : false
                //Open Edit Basic Info
                cell.btn_edit.addTarget(self, action: #selector(self.openEditBasicInformation(_:)), for: .touchUpInside)
                //===============================================================================//
                
                return cell
            case "company_info":
                let cell: QKProfileBasicInfoCell = tableView.dequeueReusableCell(withIdentifier: "QKProfileBasicInfoCell", for: indexPath) as! QKProfileBasicInfoCell
                
                
                //=============================================================================================//
                //For USER Profile Info
                //=============================================================================================//
                if self.QKProfileData.logobase64 != "" {
                    img_user_profile.image = UIImage.init(data: Data.init(base64Encoded: self.QKProfileData.logobase64)!)
                }
                else if self.QKProfileData.logo != "" {
                    SDWebImageManager.shared().loadImage(with: URL.init(string: self.QKProfileData.logo)!, options: SDWebImageOptions.refreshCached, progress: { (pro1, pro2, url) in
                    }, completed: { (img, data, err, type, finish, url) in
                        if err == nil {
                            self.img_user_profile.image = (img ?? UIImage.init(named: "logo"))
                            self.QKProfileData.logobase64 = (UIImageJPEGRepresentation(self.img_user_profile.image!, 0.25)?.base64EncodedString())!
                        }
                    })
                }
                else {
                    img_user_profile.image = UIImage.init(named: "logo")
                }
                
                if self.str_uniquecode == "" {
                    print("normal")
                    if self.QKProfileData.type == 3 || !(self.QKProfileData.Status == "A") {
                        arrHeaderType = ["Basic Details","Followers", "Analytics", "Settings"]
                    }
                    else {
                        arrHeaderType = ["Basic Details","Followers", "Employees", "Analytics", "Settings"]
                    }
                    
                    if !(QKProfileData.role == 0) {
                        arrHeaderType.removeLast()
                    }
                    
                }
                else {
                    print("scan")
                    
                    arrHeaderType = ["Basic Details","Followers"]
                    
                    if str_Screen_From == "OnlyViewProfile" {
                        arrHeaderType = ["Basic Details","Followers", "Employees", "Analytics"]
                    }
                    
                    
                }
                
                viewTopBG.isHidden = false
                lbl_About.isHidden = false
                lbluser_Name.isHidden = false
                if str_Screen_From == "BlockProfile" || str_Screen_From == "Unfollow_Profile" {
                    viewUnblock.isHidden = false
                }
                img_user_profile.isHidden = false
                collection_Header.isHidden = false
                lbluser_Name.text = self.QKProfileData.name
                lbl_About.text = self.QKProfileData.about
                lbl_ReadMore.isHidden = estimatedHeightOfAboutLabel(text: lbl_About.text!) > 36 ? false : true
                collection_Header.reloadData()
                //=============================================================================================//
                //=============================================================================================//
                
                
                cell.lbl_titile.text = self.QKProfileData.type != 3 ? "COMPANY INFORMATION" : "BASIC INFORMATION"
                cell.lbl_estadateTitle.text = self.QKProfileData.type != 3 ? "Established Date :" : "Date of Birth :"
                cell.lbl_teamTitle.text = self.QKProfileData.type != 3 ? "Team Size :" : ""
                cell.lbl_compnysize.text = self.QKProfileData.type != 3 ? "\(self.QKProfileData.company_team_size)" : ""
                cell.lbl_estadate.text = self.QKProfileData.establish_date
                cell.btn_edit.isHidden = self.str_uniquecode != "" ? true : false
                cell.lbl_address.text = self.QKProfileData.address
                
                //Open Edit Company Info
                cell.btn_edit.addTarget(self, action: #selector(self.openEditCompanyInformation(_:)), for: .touchUpInside)
                //===============================================================================//
                
                return cell
                
            case "founder":
                
                switch indexPath.row{
                case 0:
                    
                    if self.QKProfileData.founders.count == 0
                    {
                        let cell: AddInformCell = tableView.dequeueReusableCell(withIdentifier: "AddInformCell", for: indexPath) as! AddInformCell
                        cell.Title_lbl.text = "FOUNDERS/ KEY MEMBERS"
                        cell.Add_btn.removeTarget(nil, action: nil, for: .allEvents)
                        cell.Add_btn.isHidden = self.str_uniquecode != "" ? true : false
                        cell.Add_btn.tag = (indexPath.section*100)+indexPath.row
                        cell.Add_btn.addTarget(self, action: #selector(self.btnAddInfopress(_:)), for: .touchUpInside)
                        
                        return cell
                    }
                    
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    cell.title_lbl.text = "FOUNDERS/ KEY MEMBERS"
                    cell.add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.add_btn.isHidden = self.str_uniquecode != "" ? true : false
                    cell.add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.add_btn.addTarget(self, action: #selector(self.btnAddInfopress(_:)), for: .touchUpInside)
                    
                    if self.str_uniquecode == ""
                    {
                        cell.add_btn.isHidden = self.QKProfileData.founders.count > 4 ? true : false
                    }
                    
                    return cell
                    
                default :
                    
                    let cell: QKProfileFounderCell = tableView.dequeueReusableCell(withIdentifier: "QKProfileFounderCell", for: indexPath) as! QKProfileFounderCell
                    
                    let dataDict = self.QKProfileData.founders[indexPath.row - 1]
                    
                    cell.lbl_name.text = "\(dataDict["firstname"] as? String ?? "") \(dataDict["lastname"] as? String ?? "")"
                    let strEmail = dataDict["email"] as? String ?? ""
                    if strEmail == "" {
                        cell.lbl_email.text = ""
                        cell.lbl_email_Title.text = ""
                    }
                    else {
                        cell.lbl_email.text = strEmail
                        cell.lbl_email_Title.text = "Email :"
                    }
                    //cell.lbl_email.text = dataDict["email"] as? String ?? ""
                    cell.lbl_designation.text = dataDict["designation"] as? String ?? ""
                    cell.btn_edit.isHidden = self.str_uniquecode != "" ? true : false
                    cell.btn_edit.tag = (indexPath.section*100)+indexPath.row
                    cell.btn_edit.addTarget(self, action: #selector(btnEditInfopress(_:)), for: .touchUpInside)
                    
                    return cell
                    
                }
            case "social_media":
                
                switch indexPath.row{
                case 0:
                    
                    if self.QKProfileData.social_media.count == 0
                    {
                        let cell: AddInformCell = tableView.dequeueReusableCell(withIdentifier: "AddInformCell", for: indexPath) as! AddInformCell
                        cell.Title_lbl.text = "SOCIAL MEDIA"
                        cell.Add_btn.removeTarget(nil, action: nil, for: .allEvents)
                        cell.Add_btn.isHidden = self.str_uniquecode != "" ? true : false
                        cell.Add_btn.tag = (indexPath.section*100)+indexPath.row
                        cell.Add_btn.addTarget(self, action: #selector(self.btnAddInfopress(_:)), for: .touchUpInside)
                        
                        return cell
                    }
                    
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    cell.title_lbl.text = "SOCIAL MEDIA"
                    cell.add_btn.isHidden = true
                    cell.add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.add_btn.isHidden = self.str_uniquecode != "" ? true : false
                    cell.add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.add_btn.addTarget(self, action: #selector(self.btnAddInfopress(_:)), for: .touchUpInside)
                    
                    return cell
                    
                default :
                    
                    let cell: QKProfileSocialCell = tableView.dequeueReusableCell(withIdentifier: "QKProfileSocialCell", for: indexPath) as! QKProfileSocialCell
                    
                    let dataDict = self.QKProfileData.social_media[indexPath.row - 1]
                    
                    cell.lbl_title.text = self.arrType[(dataDict["type"] as? Int ?? 1) - 1]//dataDict["type"] as? String ?? ""
                    
                    cell.btn_openurl.isHidden = dataDict["name"] as? String != "" ? false : true
                    cell.btn_openurl.isEnabled = dataDict["name"] as? String != "" ? true : false
                    
                    cell.btn_edit.isHidden = self.str_uniquecode != "" ? true : false
                    cell.btn_edit.tag = (indexPath.section*100)+indexPath.row
                    cell.btn_edit.addTarget(self, action: #selector(btnEditInfopress(_:)), for: .touchUpInside)
                    
                    cell.urlpress = {(sender) in
                        
                        DispatchQueue.main.async {
                            let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpSettingWebVC") as! HelpSettingWebVC
                            obj.urlstring = dataDict["name"] as? String ?? ""
                            obj.titlestring = self.arrType[(dataDict["type"] as? Int ?? 1) - 1]
                            self.present(obj, animated: true, completion: nil)
                        }
                    }
                    
                    return cell
                    
                }
                
            case "phone":
                
                switch indexPath.row{
                case 0:
                    
                    if self.QKProfileData.phone.count == 0
                    {
                        let cell: AddInformCell = tableView.dequeueReusableCell(withIdentifier: "AddInformCell", for: indexPath) as! AddInformCell
                        cell.Title_lbl.text = "PHONE"
                        cell.Add_btn.removeTarget(nil, action: nil, for: .allEvents)
                        cell.Add_btn.isHidden = self.str_uniquecode != "" ? true : false
                        cell.Add_btn.tag = (indexPath.section*100)+indexPath.row
                        cell.Add_btn.addTarget(self, action: #selector(self.btnAddInfopress(_:)), for: .touchUpInside)
                        
                        return cell
                    }
                    
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    cell.title_lbl.text = "PHONE"
                    cell.add_btn.isHidden = true
                    cell.add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.add_btn.isHidden = self.str_uniquecode != "" ? true : false
                    cell.add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.add_btn.addTarget(self, action: #selector(self.btnAddInfopress(_:)), for: .touchUpInside)
                    
                    if self.str_uniquecode == ""
                    {
                        cell.add_btn.isHidden = self.QKProfileData.phone.count > 4 ? true : false
                    }
                    
                    return cell
                    
                default :
                    
                    let cell: QKProfilePhoneCell = tableView.dequeueReusableCell(withIdentifier: "QKProfilePhoneCell", for: indexPath) as! QKProfilePhoneCell
                    
                    let dataDict = self.QKProfileData.phone[indexPath.row - 1]
                    
                    let strPhone = dataDict["name"] as? String ?? ""
                    cell.lbl_title.text = strPhone
                    // cell.lbl_designation.text = dataDict["designation"] as? String ?? ""
                    cell.btn_edit.isHidden = self.str_uniquecode != "" ? true : false
                    cell.btn_edit.tag = (indexPath.section*100)+indexPath.row
                    cell.btn_edit.addTarget(self, action: #selector(btnEditInfopress(_:)), for: .touchUpInside)
                    
                    return cell
                    
                }
                
            case "email":
                
                switch indexPath.row{
                case 0:
                    
                    if self.QKProfileData.email.count == 0
                    {
                        let cell: AddInformCell = tableView.dequeueReusableCell(withIdentifier: "AddInformCell", for: indexPath) as! AddInformCell
                        cell.Title_lbl.text = "EMAIL*"
                        cell.Add_btn.removeTarget(nil, action: nil, for: .allEvents)
                        cell.Add_btn.isHidden = self.str_uniquecode != "" ? true : false
                        cell.Add_btn.tag = (indexPath.section*100)+indexPath.row
                        cell.Add_btn.addTarget(self, action: #selector(self.btnAddInfopress(_:)), for: .touchUpInside)
                        
                        return cell
                    }
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    cell.title_lbl.text = "EMAIL*"
                    cell.add_btn.isHidden = true
                    cell.add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.add_btn.isHidden = self.str_uniquecode != "" ? true : false
                    cell.add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.add_btn.addTarget(self, action: #selector(self.btnAddInfopress(_:)), for: .touchUpInside)
                    
                    if self.str_uniquecode == ""
                    {
                        cell.add_btn.isHidden = self.QKProfileData.email.count > 4 ? true : false
                    }
                    
                    return cell
                    
                default :
                    
                    let cell: QKProfileEmailCell = tableView.dequeueReusableCell(withIdentifier: "QKProfileEmailCell", for: indexPath) as! QKProfileEmailCell
                    
                    let dataDict = self.QKProfileData.email[indexPath.row - 1]
                    
                    cell.lbl_title.text = dataDict["name"] as? String ?? ""
                    cell.img_emailimg.image =  #imageLiteral(resourceName: "ic_message").tint(with: UIColor.init(hex: "28AFB0"))
                    // cell.lbl_designation.text = dataDict["designation"] as? String ?? ""
                    cell.btn_edit.isHidden = self.str_uniquecode != "" ? true : false
                    cell.btn_edit.tag = (indexPath.section*100)+indexPath.row
                    cell.btn_edit.addTarget(self, action: #selector(btnEditInfopress(_:)), for: .touchUpInside)
                    
                    return cell
                    
                }
                
            case "other_address":
                
                switch indexPath.row{
                case 0:
                    
                    if self.QKProfileData.other_address.count == 0
                    {
                        let cell: AddInformCell = tableView.dequeueReusableCell(withIdentifier: "AddInformCell", for: indexPath) as! AddInformCell
                        cell.Title_lbl.text = "OTHER ADDRESS"
                        cell.Add_btn.removeTarget(nil, action: nil, for: .allEvents)
                        cell.Add_btn.isHidden = self.str_uniquecode != "" ? true : false
                        cell.Add_btn.tag = (indexPath.section*100)+indexPath.row
                        cell.Add_btn.addTarget(self, action: #selector(self.btnAddInfopress(_:)), for: .touchUpInside)
                        
                        return cell
                    }
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    cell.title_lbl.text = "OTHER ADDRESS"
                    cell.add_btn.isHidden = true
                    cell.add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.add_btn.isHidden = self.str_uniquecode != "" ? true : false
                    cell.add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.add_btn.addTarget(self, action: #selector(self.btnAddInfopress(_:)), for: .touchUpInside)
                    
                    if self.str_uniquecode == ""
                    {
                        cell.add_btn.isHidden = self.QKProfileData.email.count > 4 ? true : false
                    }
                    
                    return cell
                    
                default :
                    
                    let cell: QKProfileEmailCell = tableView.dequeueReusableCell(withIdentifier: "QKProfileEmailCell", for: indexPath) as! QKProfileEmailCell
                    
                    let dataDict = self.QKProfileData.other_address[indexPath.row - 1]
                    
                    cell.lbl_title.text = dataDict["name"] as? String ?? ""
                    cell.img_emailimg.image =  UIImage.init(named: "pin")?.tint(with: UIColor.init(hex: "28AFB0"))
                    // cell.lbl_designation.text = dataDict["designation"] as? String ?? ""
                    cell.btn_edit.isHidden = self.str_uniquecode != "" ? true : false
                    cell.btn_edit.tag = (indexPath.section*100)+indexPath.row
                    cell.btn_edit.addTarget(self, action: #selector(btnEditInfopress(_:)), for: .touchUpInside)
                    
                    return cell
                    
                }
                
            case "web_link":
                
                switch indexPath.row{
                case 0:
                    
                    if self.QKProfileData.weblink.count == 0
                    {
                        let cell: AddInformCell = tableView.dequeueReusableCell(withIdentifier: "AddInformCell", for: indexPath) as! AddInformCell
                        cell.Title_lbl.text = "WEB LINK"
                        cell.Add_btn.removeTarget(nil, action: nil, for: .allEvents)
                        cell.Add_btn.isHidden = self.str_uniquecode != "" ? true : false
                        cell.Add_btn.tag = (indexPath.section*100)+indexPath.row
                        cell.Add_btn.addTarget(self, action: #selector(self.btnAddInfopress(_:)), for: .touchUpInside)
                        
                        return cell
                    }
                    
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    cell.title_lbl.text = "WEB LINK"
                    cell.add_btn.isHidden = true
                    cell.add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.add_btn.isHidden = self.str_uniquecode != "" ? true : false
                    cell.add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.add_btn.addTarget(self, action: #selector(self.btnAddInfopress(_:)), for: .touchUpInside)
                    
                    if self.str_uniquecode == ""
                    {
                        cell.add_btn.isHidden = self.QKProfileData.weblink.count > 4 ? true : false
                    }
                    
                    return cell
                    
                default :
                    
                    let cell: QKProfileWeblinkCell = tableView.dequeueReusableCell(withIdentifier: "QKProfileWeblinkCell", for: indexPath) as! QKProfileWeblinkCell
                    
                    let dataDict = self.QKProfileData.weblink[indexPath.row - 1]
                    
                    cell.lbl_title.text = dataDict["name"] as? String ?? ""
                    
                    cell.btn_openurl.isHidden = dataDict["name"] as? String != "" ? false : true
                    cell.btn_openurl.isEnabled = dataDict["name"] as? String != "" ? true : false
                    
                    cell.btn_edit.isHidden = self.str_uniquecode != "" ? true : false
                    cell.btn_edit.tag = (indexPath.section*100)+indexPath.row
                    cell.btn_edit.addTarget(self, action: #selector(btnEditInfopress(_:)), for: .touchUpInside)
                    
                    cell.urlpress = {(sender) in
                        
                        DispatchQueue.main.async {
                            let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpSettingWebVC") as! HelpSettingWebVC
                            obj.urlstring = dataDict["name"] as? String ?? ""
                            obj.titlestring = "WEB LINK"//"Facebook"//["publication_title"] as? String ?? ""
                            self.present(obj, animated: true, completion: nil)
                        }
                    }
                    
                    
                    return cell
                    
                }
                
            default:
                return UITableViewCell()
            }
            
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 1:
            
            //For Second Page Followers
            
            let O_cell = tableView.dequeueReusableCell(withIdentifier: "FollowersTableCell", for: indexPath as IndexPath) as! FollowersTableCell
            O_cell.backgroundColor = UIColor.white
            O_cell.selectionStyle = .none
            O_cell.separatorInset = UIEdgeInsets.zero
            if self.arr_FollowersData.count == 0 {
                O_cell.lbl_UserName.isHidden = true
                O_cell.lbl_Underline.isHidden = true
                O_cell.imgUser_Image.isHidden = true
                O_cell.lbl_ResultNotFound.isHidden = false
            }
            else {
                O_cell.lbl_UserName.isHidden = false
                O_cell.lbl_Underline.isHidden = false
                O_cell.imgUser_Image.isHidden = false
                O_cell.lbl_ResultNotFound.isHidden = true
            
                O_cell.imgUser_Image.image = #imageLiteral(resourceName: "user_profile_pic")
                O_cell.lbl_UserName.text = self.arr_FollowersData[indexPath.row].name
                let strImgURL = self.arr_FollowersData[indexPath.row].profile_pic
                if strImgURL != "" {
                    O_cell.imgUser_Image.setImageWith(URLRequest.init(url: URL.init(string: strImgURL)!), placeholderImage: nil, success: { (request, response, image) in
                        O_cell.imgUser_Image.image = image
                    }, failure: { (request, response, error) in
                        O_cell.imgUser_Image.image = #imageLiteral(resourceName: "user_profile_pic")
                    })
                }
            }
                
            return O_cell
            
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 2:
            
            //For Second Page Employees
            
            if self.arr_EmployeessList[indexPath.row].checkStatus == "" {
                
                let cell3: createEmployeeInProfile = tableView.dequeueReusableCell(withIdentifier: "createEmployeeInProfile", for: indexPath) as! createEmployeeInProfile
                
                return cell3
            }
            else {
                let Employeecell = tableView.dequeueReusableCell(withIdentifier: "EmployeesTableCell", for: indexPath as IndexPath) as! EmployeesTableCell
                Employeecell.imgApproveReject.isHidden = true
                Employeecell.imgApproveReject.image = #imageLiteral(resourceName: "noti_contactaprove")
                Employeecell.backgroundColor = UIColor.white
                Employeecell.selectionStyle = .none
                Employeecell.separatorInset = UIEdgeInsets.zero
                
                var strStatus = self.arr_EmployeessList[indexPath.row].status
                let strE_Name = self.arr_EmployeessList[indexPath.row].Emp_Fname.capitalized + " " + self.arr_EmployeessList[indexPath.row].Emp_Lname.capitalized
            
                var strMsg = ""
                var strRoleName = ""
                if strStatus == "P" {
                    strStatus = "Pending"
                    Employeecell.imgApproveReject.isHidden = true
                    strMsg = "\((strE_Name.capitalized))\n\(strStatus)"
                }
                else if strStatus == "R" {
                    strStatus = "Rejected     Details"
                    Employeecell.imgApproveReject.image = #imageLiteral(resourceName: "noti_rejected")
                    Employeecell.imgApproveReject.isHidden = false
                    strMsg = "\((strE_Name.capitalized))\n\(strStatus)"
                }
                else if strStatus == "A" {
                    strStatus = ""
                    Employeecell.imgApproveReject.image = #imageLiteral(resourceName: "noti_contactaprove")
                    Employeecell.imgApproveReject.isHidden = false
                    
                    if !(self.arr_EmployeessList[indexPath.row].role == 0) {
                        strRoleName = self.arr_EmployeessList[indexPath.row].role_name
                        strMsg = "\((strE_Name.capitalized))\n\(strRoleName)"
                    }
                    else {
                        strMsg = "\((strE_Name.capitalized))"
                    }
                    
                }
                else {
                    strStatus = ""
                    Employeecell.imgApproveReject.isHidden = true
                    strMsg = "\((strE_Name.capitalized))"
                }
                
                
                
                let rangestring: NSString = "\((strMsg))" as NSString
                let rangeStatus = rangestring.range(of: "\(strStatus)")
                let rangeRole = rangestring.range(of: "\(strRoleName)")
                let rangeReject = rangestring.range(of: "Rejected")
                let rangeDetails = rangestring.range(of: "Details")
                let style = NSMutableParagraphStyle()
                style.lineSpacing = 0
                
                let strAtributedTitle = NSMutableAttributedString.init(string: rangestring as String)
                strAtributedTitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Semibold", size: 15)!, range: rangeStatus)
                strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.orange, range: rangeStatus)
                
                //Role Assign
                strAtributedTitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Semibold", size: 15)!, range: rangeRole)
                strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: KVClass.themeColor(), range: rangeRole)
                
                //Reject
                strAtributedTitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Bold", size: 15)!, range: rangeReject)
                strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.red, range: rangeReject)
                
                //Details
                strAtributedTitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Semibold", size: 15)!, range: rangeDetails)
                strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGray, range: rangeDetails)
                    
                Employeecell.lbl_UserName.attributedText = strAtributedTitle
                
                //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;//
                //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;//
                Employeecell.imgUser_Image.image = #imageLiteral(resourceName: "user_profile_pic")
                let strImgURL = self.arr_EmployeessList[indexPath.row].profile_pic
                if strImgURL != "" {
                    Employeecell.imgUser_Image.setImageWith(URLRequest.init(url: URL.init(string: strImgURL)!), placeholderImage: nil, success: { (request, response, image) in
                        Employeecell.imgUser_Image.image = image
                    }, failure: { (request, response, error) in
                        Employeecell.imgUser_Image.image = #imageLiteral(resourceName: "user_profile_pic")
                    })
                }
                //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;//
                //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;//
                if self.str_Screen_From == "OnlyViewProfile" || self.str_Screen_From == "View/EditProfile" {
                    Employeecell.btnOptions.isHidden = true
                    Employeecell.btnOptions.isHidden = true
                }
                else {
                    
                    Employeecell.lbl_UserName.tag = indexPath.row
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapRejectClickForReason))
                    Employeecell.lbl_UserName.isUserInteractionEnabled = true
                    Employeecell.lbl_UserName.addGestureRecognizer(tap)
                    
                    Employeecell.btnOptions.isHidden = false
                    Employeecell.btnOptions.tag = indexPath.row
                    Employeecell.btnOptions.addTarget(self, action: #selector(self.btnOptionInEmployeeList(_:)), for: .touchUpInside)
                }
                
                
                
                
                
                return Employeecell
            }
            
            
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 3:
            
            //For Second Page Analytics
            
            let Analytics_cell = tableView.dequeueReusableCell(withIdentifier: "AnalyticsComingSoonTableCell", for: indexPath as IndexPath) as! AnalyticsComingSoonTableCell
            Analytics_cell.selectionStyle = .none
            Analytics_cell.separatorInset = UIEdgeInsets.zero
            
            return Analytics_cell
            
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 4:
            
            //For Second Page Settings
            
            let S_cell = tableView.dequeueReusableCell(withIdentifier: "SettingProfileTableCell", for: indexPath as IndexPath) as! SettingProfileTableCell
            S_cell.lbl_SubTitle.text = ""
            S_cell.backgroundColor = UIColor.white
            S_cell.selectionStyle = .none
            S_cell.separatorInset = UIEdgeInsets.zero
            
            S_cell.lbl_Options_Title.text = arrSettingOptions[indexPath.row]
            if arrSettingOptions[indexPath.row] == "Delete" {
                S_cell.lbl_Options_Title.textColor = UIColor.red
            }
            else if arrSettingOptions[indexPath.row] == "Transfer Profile" {
                
                if (self.QKProfileData.is_transfer == 1) && (self.QKProfileData.transfer_status == 0) {
                    S_cell.lbl_SubTitle.text = "Transfer in Process"
                    S_cell.lbl_SubTitle.textColor = UIColor.orange
                }
                else {
                    S_cell.lbl_SubTitle.text = ""
                }
            }
            else {
                S_cell.lbl_Options_Title.textColor = UIColor.black
            }
            
            return S_cell
            
            
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let secView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: 70))
        secView.KVCorneredius = 4
        secView.backgroundColor = UIColor.groupTableViewBackground
        
        if section == 0
        {
            secView.backgroundColor = UIColor.groupTableViewBackground
        }
        return secView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch currentPage {
        case 0:
            
            //For First Page BasicInformation
            if section == 0 {
                return 1
            }
            return 10
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 1:
            
            //For Second Page Followers
            return 60
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 2:
            
            //For Second Page Employees
            return 60
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 3:
            
            //For Second Page Analytics
            return 10
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 4:
            
            //For Second Page Settings
            return 10
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let secView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: 0))
        secView.KVCorneredius = 4
        secView.backgroundColor = UIColor.groupTableViewBackground
        if !(currentPage == 0) {
            secView.backgroundColor = UIColor.white
        }
        return secView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if section == (tableView.numberOfSections - 1)
        {
            return 70
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        switch currentPage {
        case 0:
            
            //For First Page BasicInformation
            let dict = self.arr_SectionNumber[indexPath.section]
            let strID = dict["sec_id"] as? String ?? ""
                
            if self.str_uniquecode == "" {
                if indexPath.row != 0 {
                    let btn = UIButton.init(type: UIButtonType.custom)
                    switch strID {
                    case "founder":
                        btn.tag = (indexPath.section*100)+indexPath.row
                        self.strPhone_ADD = "NewAdd"
                        self.AddFounderpress(btn)
                    case "social_media":
                        btn.tag = (indexPath.section*100)+indexPath.row
                        self.AddSocialMediapress(btn)
                    case "phone":
                        btn.tag = (indexPath.section*100)+indexPath.row
                        self.strPhone_ADD = "NewAdd"
                        self.AddPhonepress(btn)
                    case "email":
                        btn.tag = (indexPath.section*100)+indexPath.row
                        self.AddEmailpress(btn)
                    case "web_link":
                        btn.tag = (indexPath.section*100)+indexPath.row
                        self.AddWebLinkpress(btn)
                    case "other_address":
                        btn.tag = (indexPath.section*100)+indexPath.row
                        self.AddOtherAddress(btn)
                    default:
                        break
                    }
                }
            }
            else {
                if strID == "phone" {
                    if indexPath.row != 0 {
                        guard let number = URL(string: "tel://" + "\(self.QKProfileData.phone[indexPath.row - 1]["name"] as? String ?? "")") else { return }
                        UIApplication.shared.open(number)
                    }
                }
                else if strID == "company_info" {
                    let objMap = self.storyboard?.instantiateViewController(withIdentifier: "MapLocationVC") as! MapLocationVC
                    objMap.str_lat = self.QKProfileData.latitude
                    objMap.str_long = self.QKProfileData.longitude
                    self.navigationController?.pushViewController(objMap, animated: true)
                }
                else if strID == "other_address" {
                    if indexPath.row != 0 {
                        let objMap = self.storyboard?.instantiateViewController(withIdentifier: "MapLocationVC") as! MapLocationVC
                        objMap.str_lat = self.QKProfileData.other_address[indexPath.row - 1]["lati"] as? String ?? "0.00"
                        objMap.str_long = self.QKProfileData.other_address[indexPath.row - 1]["longi"] as? String ?? "0.00"
                            self.navigationController?.pushViewController(objMap, animated: true)
                    }
                }
            }
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 1:
            //For Second Page Followers
            self.view.endEditing(true)
            break
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 2:
            //For Second Page Employees
            self.view.endEditing(true)
            if self.str_Screen_From == "OnlyViewProfile" || self.str_Screen_From == "View/EditProfile" {
                return
            }
            else {
                selectedIndexRowForAddUpdateEmp = indexPath.row
                if self.arr_EmployeessList[indexPath.row].checkStatus == "" {
                    app_Delegate.strOpenView = "Basic"
                    let view = UIView.fromNib("QKAllUserView") as! QKAllUserView
                    view.tag = indexPath.section
                    view.strTitle = "Add Employee"
                    view.strViewFrom = "CreateEmployee"
                    view.Profile_ID = self.int_userID
                    view.strProfileType = self.QKProfileData.type == 1 ? "Company" : "Enterprise"
                    view.getQuickKonnectUserView(to: self)
                }
                else {
                    let objAddEmp = self.storyboard?.instantiateViewController(withIdentifier: "AddMemberInProfileVC") as! AddMemberInProfileVC
                    objAddEmp.strScreenFrom = "Update"
                    objAddEmp.ProfileID = self.arr_EmployeessList[indexPath.row].profile_id
                    objAddEmp.P_Type = self.QKProfileData.type == 1 ? "Company" : "Enterprise"
                    objAddEmp.dic_EmployeeDetail = self.arr_EmployeessList[indexPath.row]
                    self.navigationController?.pushViewController(objAddEmp, animated: true)
                }
            }
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 3:
            break
            //For Third Page Analytics
            //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        case 4:
            
            //For Fourth Page Settings

            if arrSettingOptions[indexPath.row] == "Delete" {
                //For Delete Profile
                self.DeleteProfilePress()
            }
            else if arrSettingOptions[indexPath.row] == "Transfer Profile" {
                //Transfer Profile
                if (self.QKProfileData.is_transfer == 1) && (self.QKProfileData.transfer_status == 0) {
                    //Already Transferred Profile

                    DispatchQueue.main.async {
                        let Alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
                            
                        let attributedTitle = NSMutableAttributedString(string: "Warning!", attributes:
                                [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
                            
                        let strMsg = "Your Profile Transfer to " + self.QKProfileData.transfer_to + " is in progress, Do you want to cancel Transfer request ?"
                            
                        let attributedMessage = NSMutableAttributedString(string: strMsg, attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
                        Alert.setValue(attributedTitle, forKey: "attributedTitle")
                        Alert.setValue(attributedMessage, forKey: "attributedMessage")
                            
                        let ok = UIAlertAction.init(title: "Yes", style: .destructive, handler: { (action) in
                                
                            //Cancel Request Profile
                            let dict_param = NSMutableDictionary()
                            dict_param["profile_id"] = self.int_userID
                            KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self.navigationController, processLabel: "", params: dict_param, api: "profile/cancel_transfer_request", arrFlag: false) { (JSON, status) in
                                if status == 1
                                {
                                    app_Delegate.isBackChange = true
                                    self.QKProfileData.is_transfer = 0
                                    self.QKProfileData.transfer_status = 0
                                    self.tab_Profile.reloadData()
                                    kUserDefults(true as AnyObject, key: "isnotificationrefresh")
                                }
                            }
                        })
                            
                        let no = UIAlertAction.init(title: "No", style: .cancel, handler: { (action) in
                            Alert.dismiss(animated: true, completion: nil)
                        })
                        Alert.addAction(ok)
                        Alert.addAction(no)
                        self.present(Alert, animated: true, completion: nil)
                    }
                    //=====================================================================================//
                    //=====================================================================================//
                    //=====================================================================================//
                }
                else {
                    DispatchQueue.main.async {
                        self.TransferProfile_Press(indexPath)
                    }
                }
            }
            else if arrSettingOptions[indexPath.row] == "Set as Default" {
                KVClass.ShowNotification("", "Coming Soon!")
            }
        //=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//=====//
            
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !(currentPage  == 2) {
            if !self.Scrolltop.isHidden
            {
                cell.transform = CGAffineTransform.init(scaleX: 0.7, y: 0.7)
                UIView.animate(withDuration: 0.3, animations: {
                    cell.transform = CGAffineTransform.identity
                }) { (sucess) in
                }
            }
        }
    }
    
    
    
    // MARK : - TRANSFER PROFILE PRESS
    func TransferProfile_Press(_ indexPath: IndexPath) {
        app_Delegate.strOpenView = "Basic"
        let view = UIView.fromNib("QKAllUserView") as! QKAllUserView
        view.tag = indexPath.section
        view.strTitle = "Transfer Profile"
        view.Profile_ID = self.int_userID
        view.getQuickKonnectUserView(to: self)
    }
    
    // MARK : - User Name Label Press For Reject Reason IN Employee List
    func tapRejectClickForReason(sender:UITapGestureRecognizer) {
        print("tap working")
        let tapLabel = (sender.view as AnyObject).convert(CGPoint.zero, to: tab_Profile)
        let index = tab_Profile.indexPathForRow(at: tapLabel)
        
        if (self.arr_EmployeessList[(index?.row)!]).status == "R" {
            
            var str_REASON = ""
            let strRejectReason = (self.arr_EmployeessList[(index?.row)!]).reason
            if strRejectReason == 1 {
                str_REASON = KVClass.strReason1
            }
            else if strRejectReason == 2 {
                str_REASON = KVClass.strReason2
            }
            else if strRejectReason == 3 {
                str_REASON = KVClass.strReason3
            }
            
            let Discart = UIAlertController.init(title: "Reason of rejection", message: str_REASON, preferredStyle: .alert)
            
            let no = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                Discart.dismiss(animated: true, completion: nil)
            })
            
            Discart.addAction(no)
            
            self.present(Discart, animated: true, completion: nil)
        }
    }
    
    // MARK : - Button OPTIONS IN Employee List
    @IBAction func btnOptionInEmployeeList(_ sender: UIButton) {
        let btnOption = (sender as AnyObject).convert(CGPoint.zero, to: tab_Profile)
        let index = tab_Profile.indexPathForRow(at: btnOption)
        
        let Alert = UIAlertController.init(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let action_Roles = UIAlertAction.init(title: "Manage Roles", style: .default) { (action) in
            
            self.selectedIndexRowForAddUpdateEmp = (index?.row)!
            let objRoles = self.storyboard?.instantiateViewController(withIdentifier: "ManageRoleVC") as! ManageRoleVC
            objRoles.dic_MemberDetail = (self.arr_EmployeessList[(index?.row)!])
            self.navigationController?.pushViewController(objRoles, animated: true)
        }
        
        let action_edit = UIAlertAction.init(title: "Edit", style: .default) { (action) in
            
            self.selectedIndexRowForAddUpdateEmp = (index?.row)!
            let objAddEmp = self.storyboard?.instantiateViewController(withIdentifier: "AddMemberInProfileVC") as! AddMemberInProfileVC
            objAddEmp.strScreenFrom = "Update"
            objAddEmp.ProfileID = (self.arr_EmployeessList[(index?.row)!]).profile_id
            objAddEmp.P_Type = self.QKProfileData.type == 1 ? "Company" : "Enterprise"
            objAddEmp.dic_EmployeeDetail = (self.arr_EmployeessList[(index?.row)!])
            self.navigationController?.pushViewController(objAddEmp, animated: true)
        }
        
        let action_delete = UIAlertAction.init(title: "Delete", style: .destructive) { (action) in
            
            //For Delete Employee
            
            self.ActionEventForDeleteMember(indexP: index!)
            
        }
        
        let action_cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            Alert.dismiss(animated: true, completion: nil)
        }
        
        
        if (self.arr_EmployeessList[(index?.row)!]).status == "A" {
            Alert.addAction(action_Roles)
        }
        Alert.addAction(action_edit)
        Alert.addAction(action_delete)
        Alert.addAction(action_cancel)
        self.present(Alert, animated: true, completion: nil)
    }
    
    func ActionEventForDeleteMember(indexP: IndexPath) {
        let Discart = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        
        let attributedTitle = NSMutableAttributedString(string: "Are you sure ?", attributes:
            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
        
        let attributedMessage = NSMutableAttributedString(string: "You want to delete this employee ?", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
        Discart.setValue(attributedTitle, forKey: "attributedTitle")
        Discart.setValue(attributedMessage, forKey: "attributedMessage")
        
        let ok = UIAlertAction.init(title: "Delete", style: .destructive, handler: { (action) in
            
            self.removeAnimationInSelectedTime = "Delete"
            let delete_emp_ID = self.arr_EmployeessList[indexP.row].id
            //For Delete Profile
            self.tab_Profile.beginUpdates()
            self.arr_EmployeessList.remove(at: indexP.row)
            self.arr_filter_EmployeessList.remove(at: indexP.row)
            self.tab_Profile.deleteRows(at: [indexP], with: .fade)
            self.tab_Profile.endUpdates()
            self.tab_Profile.reloadData()
            
            //Call ApI for Delete Member
            let dict_param = NSMutableDictionary()
            dict_param["id"] = delete_emp_ID
            KVClass.KVServiece(methodType: "POST", processView: nil, baseView: nil, processLabel: "", params: dict_param, api: "member/delete_member", arrFlag: false) { (JSON, status) in
            }
            //=========================================================================================//
            //=========================================================================================//
            
        })
        
        let no = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            Discart.dismiss(animated: true, completion: nil)
        })
        
        Discart.addAction(ok)
        Discart.addAction(no)
        
        self.present(Discart, animated: true, completion: nil)
        
    }
    //======================================================================================================//
    //======================================================================================================//
    //======================================================================================================//
    
    
    //# Mark Edit Basic Info
    //=================Edit Basic Information Controlle========================//
    @IBAction func openEditBasicInformation(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "QKCompanyInfoVC") as! QKCompanyInfoVC
        obj.delegate = self
        obj.QKProfileData = self.QKProfileData
        obj.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        obj.view.backgroundColor = UIColor.clear
        self.present(obj, animated: false, completion: nil)
    }
    
    
    @IBAction func openEditCompanyInformation(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "CompanyInfoViewC") as! CompanyInfoViewC
        obj.delegate = self
        obj.str_title = self.QKProfileData.type != 3 ? "Comapny Information" : "Basic Information"
        obj.QKProfileData = self.QKProfileData
        obj.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        obj.view.backgroundColor = UIColor.clear
        self.present(obj, animated: false, completion: nil)
    }
    
}

//===============================================================================================//
//===============================================================================================//


//===============================================================================================//
// MARK :- QKProfileFirst Table Cell

class QKProfileFirstCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var img_profile: UIImageView!
    @IBOutlet var btn_edit: UIButton!
    @IBOutlet var lbl_companyname: UILabel!
    @IBOutlet var lbl_about: UILabel!
    @IBOutlet var collection_action: UICollectionView!
    
    var isUserBlocked = false
    
    var ActionBtnPress: ((UIButton)->Void)? = nil
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.collection_action.layoutSubviews()
        self.collection_action.layoutIfNeeded()
        self.collection_action.reloadData()
    }
    
    
    override func awakeFromNib() {
        
        self.btn_edit.isHidden = true
//        self.layoutSubviews()
//        self.layoutIfNeeded()
        
        self.collection_action.delegate = self
        self.collection_action.dataSource = self
        
        self.btn_edit.setImage(UIImage.init(named: "edit")?.tint(with: UIColor.init(hex: "28AFB0")), for: .normal)
        if self.tag == 22
        {
            self.btn_edit.isHidden = true
            self.collection_action.isHidden = false
        }
        else
        {
            self.btn_edit.isHidden = false
            self.collection_action.isHidden = true
        }
        self.collection_action.isHidden = false
        self.collection_action.reloadData()
    }
    
    func reusecell()
    {
        if self.tag == 22
        {
            self.btn_edit.isHidden = true
            self.collection_action.isHidden = false
        }
        else
        {
            self.btn_edit.isHidden = false
            self.collection_action.isHidden = true
        }
        self.collection_action.isHidden = false
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.layoutIfNeeded()
        self.reusecell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: (self.collection_action.frame.size.width), height: self.collection_action.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var Title_lbl_arr: [String]! = [String]()
        var Title_img: [UIImage?]! = [UIImage?]()

        Title_img.removeAll()
        Title_lbl_arr.removeAll()
        if app_Delegate.strProfileScreenFrom == "ViewProfile" {
            Title_lbl_arr = ["Followers"]
            Title_img = [UIImage.init(named: "unfollow_ic")]
        }
        else {
            Title_lbl_arr = ["Follow"]
            Title_img = [UIImage.init(named: "follow_ic")]
        }
        
        
        let cell: ActionCollectCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActionCollectCell", for: indexPath) as! ActionCollectCell
        cell.Title_title.text = Title_lbl_arr[indexPath.row]
        cell.Title_img.image = Title_img[indexPath.row]
        // cell.backgroundColor = UIColor.yellow
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
}
//===============================================================================================//
//===============================================================================================//


//===============================================================================================//
//MARK :- QK Profile Basic Info Table Cell====================================================//

class QKProfileBasicInfoCell: UITableViewCell
{
    @IBOutlet var lbl_titile: UILabel!
    @IBOutlet var lbl_estadate: UILabel!
     @IBOutlet var lbl_estadateTitle: UILabel!
    @IBOutlet var lbl_teamTitle: UILabel!
    @IBOutlet var lbl_address: UILabel!
    @IBOutlet var btn_edit: UIButton!
    @IBOutlet var lbl_compnysize: UILabel!
    
    @IBOutlet var titileHeightConstant: NSLayoutConstraint!
    
    override func awakeFromNib() {
        self.btn_edit.setImage(UIImage.init(named: "edit")?.tint(with: UIColor.init(hex: "28AFB0")), for: .normal)
        
    }
    func reusecell() {
        self.btn_edit.isHidden = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.reusecell()
    }
}
//===============================================================================================//
//===============================================================================================//



//===============================================================================================//
//MARK :- QK Profile Founder Table Cell==========================================================//
class QKProfileFounderCell: UITableViewCell {
  
    @IBOutlet var lbl_name: UILabel!
    @IBOutlet var lbl_designation: UILabel!
    @IBOutlet var lbl_email: UILabel!
    @IBOutlet var lbl_email_Title: UILabel!
    @IBOutlet var btn_edit: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.btn_edit.setImage(#imageLiteral(resourceName: "dlt_ic").tint(with: UIColor.lightGray), for: .normal)
        
    }
    
}
//===============================================================================================//
//===============================================================================================//


//===============================================================================================//
//MARK :- QK Profile Social Table Cell ==========================================================//
class QKProfileSocialCell: UITableViewCell{
    
    @IBOutlet var lbl_title: UILabel!
    @IBOutlet var btn_openurl: UIButton!
    @IBOutlet var btn_edit: UIButton!
    var urlpress:((UIButton)->Void)? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
         self.btn_edit.isHidden = true
         self.btn_edit.setImage(#imageLiteral(resourceName: "dlt_ic").tint(with: UIColor.lightGray), for: .normal)
        
        self.btn_openurl.isHidden = true
        self.btn_openurl.addTarget(self, action: #selector(openUrl(_:)), for: .touchUpInside)
        
        let buttontitle = NSMutableAttributedString.init(string: "Open Social Media URL")
        buttontitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Medium", size: 14.5)!, range: NSRange.init(location: 0, length: buttontitle.length))
        buttontitle.addAttribute(NSUnderlineColorAttributeName, value: UIColor.blue, range: NSRange.init(location: 0, length: buttontitle.length))
        buttontitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range: NSRange.init(location: 0, length: buttontitle.length))
        buttontitle.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange.init(location: 0, length: buttontitle.length))
        
        self.btn_openurl.setAttributedTitle(buttontitle, for: .normal)
        
    }
    
    @IBAction func openUrl(_ sender: UIButton)
    {
        if urlpress != nil
        {
            self.urlpress!(sender)
        }
    }
    
}
//===============================================================================================//
//===============================================================================================//



//===============================================================================================//
//MARK :- QK Profile Phone Cell==================================================================//
class QKProfilePhoneCell: UITableViewCell{
    
    @IBOutlet var lbl_title: UILabel!
    @IBOutlet var img_phoneimg: UIImageView!
    @IBOutlet var btn_edit: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
         self.btn_edit.isHidden = true
        self.img_phoneimg.image = self.img_phoneimg.image?.tint(with: UIColor.init(hex: "28AFB0"))
         self.btn_edit.setImage(#imageLiteral(resourceName: "dlt_ic").tint(with: UIColor.lightGray), for: .normal)
    }
}
//===============================================================================================//
//===============================================================================================//


//===============================================================================================//
//MARK :- QK Profile Email Table Cell============================================================//
class QKProfileEmailCell: UITableViewCell{
    
    @IBOutlet var lbl_title: UILabel!
    @IBOutlet var img_emailimg: UIImageView!
    @IBOutlet var btn_edit: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
         self.btn_edit.isHidden = true
        self.img_emailimg.image = #imageLiteral(resourceName: "ic_message").tint(with: UIColor.init(hex: "28AFB0"))
         self.btn_edit.setImage(#imageLiteral(resourceName: "dlt_ic").tint(with: UIColor.lightGray), for: .normal)
        
    }
}
//===============================================================================================//
//===============================================================================================//


//===============================================================================================//
//MARK :- QK Profile weblink Table Cell==========================================================//
class QKProfileWeblinkCell: UITableViewCell{
    
    @IBOutlet var lbl_title: UILabel!
    @IBOutlet var btn_openurl: UIButton!
    @IBOutlet var btn_edit: UIButton!
    var urlpress:((UIButton)->Void)? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
         self.btn_edit.isHidden = true
         self.btn_edit.setImage(#imageLiteral(resourceName: "dlt_ic").tint(with: UIColor.lightGray), for: .normal)
        self.btn_openurl.isHidden = true
        self.btn_openurl.addTarget(self, action: #selector(openUrl(_:)), for: .touchUpInside)
        
        let buttontitle = NSMutableAttributedString.init(string: "Open Web Link")
        buttontitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Medium", size: 14.5)!, range: NSRange.init(location: 0, length: buttontitle.length))
        buttontitle.addAttribute(NSUnderlineColorAttributeName, value: UIColor.blue, range: NSRange.init(location: 0, length: buttontitle.length))
        buttontitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range: NSRange.init(location: 0, length: buttontitle.length))
        buttontitle.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange.init(location: 0, length: buttontitle.length))
        
        self.btn_openurl.setAttributedTitle(buttontitle, for: .normal)
        
    }
    
    @IBAction func openUrl(_ sender: UIButton)
    {
        if urlpress != nil
        {
            self.urlpress!(sender)
        }
    }
}
//===============================================================================================//
//===============================================================================================//





// MARK: New Work From Deepak
//=============================================================================================================//
//=============================================================================================================//
//=============================================================================================================//
//Collection View Delegate Datasource Method
extension QKProfileVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collection_Header {
            return arrHeaderType.count
        }
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let H_cell: Profile_HeaderCollectCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Profile_HeaderCollectCell", for: indexPath) as! Profile_HeaderCollectCell
        
        H_cell.lbl_Underline.backgroundColor = KVClass.themeColor()
        
        let selected_Title = getSelectedTitleFromSelectPage(page: currentPage)
        
        if self.arrHeaderType[indexPath.row] == selected_Title {
            selectIndex = indexPath
            H_cell.lbl_Underline.isHidden = false
            H_cell.lbl_header_Title.textColor = KVClass.themeColor()
        }
        else {
            H_cell.lbl_Underline.isHidden = true
            H_cell.lbl_header_Title.textColor = UIColor.darkGray
        }
            
        H_cell.lbl_header_Title.text = self.arrHeaderType[indexPath.row]
        return H_cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.str_uniquecode == "" || self.str_Screen_From == "OnlyViewProfile" {
            let size = CGSize.init(width: 115, height: self.collection_Header.frame.size.height)
            return size
        }
        else {
            let size = CGSize.init(width: self.collection_Header.frame.size.width/CGFloat(arrHeaderType.count), height: self.collection_Header.frame.size.height)
            return size
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        
        if self.QKProfileData.Status == "A" {
            arrSettingOptions = ["Set as Default","Transfer Profile","Delete"]
        }
        else {
            arrSettingOptions = ["Delete"]
        }
        selectIndex = indexPath
        let currentCell = collection_Header.cellForItem(at: indexPath) as! Profile_HeaderCollectCell
        currentPage = getSelectedCurrentPage(strTitle: currentCell.lbl_header_Title.text!)
        
        viewSearchBG.isHidden = true
        collectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
        
        switch currentPage {
        case 0:
            
            self.lblNoEmployess.isHidden = true
            constraint_tbl_leading.constant = 12
            constraint_tbl_trelling.constant = 12
            self.view.backgroundColor = UIColor.groupTableViewBackground
            tab_Profile.backgroundColor = UIColor.groupTableViewBackground
            self.tab_Profile.pullTorefresh(#selector(self.callAPIforProfileData), self)
            
        case 1:
            
            if self.strFollowersCheck == "" {
                self.strFollowersCheck = "Date"
                //Call API=======================//
                var USER_ID = 0
                
                //Get Followers User List From Local Database
                if self.str_Screen_From == "OnlyViewProfile" {
                    USER_ID = strBlockProfileID
                }
                else {
                    USER_ID = self.int_userID
                }
                
                if KVAppDataServieces.shared.getFollowersUserList(USER_ID) != nil
                {
                    for item in KVAppDataServieces.shared.getFollowersUserList(USER_ID)! {
                        self.arr_FollowersData.append(QKProfileFollowersListData.init(item))
                        self.arr_filter_FollowersData.append(QKProfileFollowersListData.init(item))
                    }
                    tab_Profile.reloadData()
                }
                self.callAPIforFollowerListData()
            }

            
            viewSearchBG.isHidden = false
            self.lblNoEmployess.isHidden  = true
            constraint_tbl_leading.constant = 0
            constraint_tbl_trelling.constant = 0
            constraint_search_bar_trelling.constant = 0
            self.tab_Profile.refreshControl = nil
            self.view.backgroundColor = UIColor.white
            tab_Profile.backgroundColor = UIColor.white
            followersearchBar.placeholder = "Search Followers..."
            self.tab_Profile.pullTorefresh(#selector(self.callAPIforFollowerListData), self)
            self.tab_Profile.reloadData()
            
        case 2:
            
            if arr_EmployeessList.count == 0 {
                self.arr_EmployeessList.removeAll()
                
                if self.str_Screen_From == "OnlyViewProfile" || self.str_Screen_From == "View/EditProfile" {
                }
                else {
                    let dictCreateEmp : NSDictionary = [ "created_at" : ""]
                    self.arr_EmployeessList.append(EmployeesListInProfileData.init(dictCreateEmp))
                }
                
                
                //Call API=======================//
                var USERID = 0
                
                //Get Followers User List From Local Database
                if self.str_Screen_From == "OnlyViewProfile" {
                    USERID = strBlockProfileID
                }
                else {
                    USERID = self.int_userID
                }
                
                if KVAppDataServieces.shared.getEmployeesUserList(USERID) != nil
                {
                    for item in KVAppDataServieces.shared.getEmployeesUserList(USERID)! {
                        self.arr_EmployeessList.append(EmployeesListInProfileData.init(item))
                        self.arr_filter_EmployeessList.append(EmployeesListInProfileData.init(item))
                    }
                    tab_Profile.reloadData()
                }
                self.callAPIforEmployeesListData()
                
                
                
            }
            
            self.lblNoEmployess.isHidden = arr_EmployeessList.count == 0 ? false : true
            
            self.followersearchBar.text = ""
            viewSearchBG.isHidden = false
            constraint_tbl_leading.constant = 0
            constraint_tbl_trelling.constant = 0
            constraint_search_bar_trelling.constant = 40
            self.tab_Profile.refreshControl = nil
            self.view.backgroundColor = UIColor.white
            tab_Profile.backgroundColor = UIColor.white
            followersearchBar.placeholder = "Search Employees..."
            self.tab_Profile.pullTorefresh(#selector(self.callAPIforEmployeesListData), self)
            self.tab_Profile.reloadData()
            
        case 3:
            
            self.lblNoEmployess.isHidden = true
            self.followersearchBar.text = ""
            constraint_tbl_leading.constant = 0
            constraint_tbl_trelling.constant = 0
            self.tab_Profile.refreshControl = nil
            self.view.backgroundColor = UIColor.white
            tab_Profile.backgroundColor = UIColor.white
            
        case 4:
            
            self.lblNoEmployess.isHidden = true
            constraint_tbl_leading.constant = 0
            constraint_tbl_trelling.constant = 0
            self.tab_Profile.refreshControl = nil
            self.view.backgroundColor = UIColor.white
            tab_Profile.backgroundColor = UIColor.white

        default:
            break
        }
        
        ///..........................Check Scrolling Height.........................///
        if scrolling_Height > 110 {
            if self.scrolling_Height/self.maximumHeaderHeight < 0.7 {
                self.lbl_About.isHidden = true
                self.lbluser_Name.isHidden = true
                self.lbl_ReadMore.isHidden = true
                self.lblTopUnderline.isHidden = true
            }
            else {
                self.lbl_About.isHidden = false
                self.lbluser_Name.isHidden = false
                self.lblTopUnderline.isHidden = false
                lbl_ReadMore.isHidden = estimatedHeightOfAboutLabel(text: lbl_About.text!) > 36 ? false : true
            }
        }
        else {
            self.lbl_About.isHidden = true
            self.lbluser_Name.isHidden = true
            self.lbl_ReadMore.isHidden = true
            self.lblTopUnderline.isHidden = true
        }
        
        if str_Screen_From == "BlockProfile" || str_Screen_From == "Unfollow_Profile" {
            if self.scrolling_Height > 220 {
                if self.scrolling_Height/self.maximumHeaderHeight < 0.7 {
                    self.viewUnblock.isHidden = true
                }
                else {
                    self.viewUnblock.isHidden = false
                }
            }
            else {
                self.viewUnblock.isHidden = true
            }
        }
        ///.........................................................................///
        ///.........................................................................///
        tab_Profile.reloadData()
        collection_Header.reloadData()
    }
    
    
  
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsetsMake(0, 0, 1, 1)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 1
//    }
    
}
//=============================================================================================================//
//=============================================================================================================//
//=============================================================================================================//



class Profile_HeaderCollectCell: UICollectionViewCell {
    @IBOutlet var lbl_Underline: UILabel!
    @IBOutlet var lbl_header_Title: UILabel!
}



//=============================================================================================================//
//=============================================================================================================//
//=============================================================================================================//
// MARK : Followers User Search
extension QKProfileVC:UISearchBarDelegate {
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if currentPage == 1 {
            self.arr_FollowersData = searchText.isEmpty ? self.arr_filter_FollowersData : self.arr_filter_FollowersData.filter { (item) -> Bool in
                // If dataItem matches the searchText, return true to include it
                return item.name.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
            }
        }
        else {
            self.arr_EmployeessList = searchText.isEmpty ? self.arr_filter_EmployeessList : self.arr_filter_EmployeessList.filter { (item) -> Bool in
                // If dataItem matches the searchText, return true to include it
                let strValue = item.Emp_Fname + " " + item.Emp_Lname
                return strValue.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
            }
        }
        
        tab_Profile.reloadData()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        if currentPage == 1 {
            self.arr_FollowersData = self.arr_filter_FollowersData
        }
        else {
            self.arr_EmployeessList = self.arr_filter_EmployeessList
        }
        self.view.endEditing(true)
        tab_Profile.reloadData()
    }
    

}
//=============================================================================================================//
//=============================================================================================================//
//=============================================================================================================//



class createEmployeeInProfile: UITableViewCell{
    @IBOutlet var btn_add: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //viewBG.layer.cornerRadius = 15
        //imgAdd.image = #imageLiteral(resourceName: "add").tint(with: .white)
        btn_add.layer.cornerRadius = btn_add.frame.size.width/2
        self.btn_add.setImage(#imageLiteral(resourceName: "add").tint(with: .white), for: .normal)
    }
    
}

