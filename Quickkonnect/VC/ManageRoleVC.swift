//
//  ManageRoleVC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 16/04/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import WebKit
import SDWebImage

class QKRolesListData: NSObject {
    
    var role_ID = 0
    var roleName = ""
    var roleDescription = ""
    
    private let dict: NSDictionary = [:]
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        
        self.role_ID = dict["id"] as? Int ?? 0
        self.roleName = dict["name"] as? String ?? ""
        self.roleDescription = dict["description"] as? String ?? ""
    }
}


class ManageRoleVC: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgView_BG: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lbl_RoleName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet var imageTopContraint: NSLayoutConstraint!
    @IBOutlet var imageBottomConstraint: NSLayoutConstraint!
    @IBOutlet var layoutHeightHeaderView: NSLayoutConstraint!
    var minimumHeaderHeight: CGFloat = 75
    var maximumHeaderHeight: CGFloat = 110
    
    var SelectedRole = 0
    var SelectedRoleNaem = ""
    var imageScale: CGFloat = 6.2
    var dic_MemberDetail: EmployeesListInProfileData! = EmployeesListInProfileData()
    var arrRoles: [QKRolesListData]! = [QKRolesListData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        app_Delegate.strAssignRole = ""
        
        imageTopContraint.constant = -self.imgView.frame.size.width / 2
        imageBottomConstraint.constant = self.imgView.frame.size.width / 2
        
        self.imgView.layer.cornerRadius = self.imgView.frame.size.width / 2
        self.imgView.clipsToBounds = true
        
        self.imgView_BG.layer.cornerRadius = self.imgView_BG.frame.size.width / 2
        self.imgView_BG.clipsToBounds = true
        
        self.imgView_BG.layer.borderWidth = 3
        self.imgView_BG.layer.borderColor = UIColor.white.withAlphaComponent(0.36).cgColor

           //===================================================================================================================//
        //Register Table Cell in NIB
        tableView.register(UINib(nibName: "ManageRoleTableCell", bundle: nil), forCellReuseIdentifier: "ManageRoleTableCell")
        
        tableView.estimatedRowHeight = 70.0
        tableView.rowHeight = UITableViewAutomaticDimension
        //===================================================================================================================//
        //===================================================================================================================//
        
        
        //self.tableView.contentInset = UIEdgeInsetsMake(self.maximumHeaderHeight, 0, 0, 0)
        //self.layoutHeightHeaderView.constant = self.maximumHeaderHeight
        //tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.none)
        self.arrRoles.removeAll()
        self.callAPIforGetRolesList()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.imgView.image = #imageLiteral(resourceName: "user_profile_pic")
        let strURL = dic_MemberDetail.profile_pic
        if strURL != "" {
            SDWebImageManager.shared().loadImage(with: URL.init(string: strURL )!, options: SDWebImageOptions.refreshCached, progress: { (pro1, pro2, url) in
            }, completed: { (img, data, err, type, finish, url) in
                if err == nil {
                    self.imgView.image = img
                }
            })
        }
        if dic_MemberDetail.role_name.capitalized == "None" {
            self.lbl_RoleName.text = ""
        }
        else {
            self.lbl_RoleName.text = dic_MemberDetail.role_name.capitalized
        }
        self.lblName.text = dic_MemberDetail.Emp_Fname.capitalized + " " + dic_MemberDetail.Emp_Lname.capitalized
    }

    
    func callAPIforGetRolesList() {

        KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self.navigationController, processLabel: "", params: nil, api: "profile/role_list", arrFlag: false) { (JSON, status) in
            if status == 1 {
                print(JSON)
                if let dataDict = JSON["data"] as? [NSDictionary]
                {
                    self.arrRoles.removeAll()
                    print(JSON)
                    
                    for item in dataDict {
                        self.arrRoles.append(QKRolesListData.init(item))
                    }
                    
                    self.SelectedRole = self.dic_MemberDetail.role
                    self.SelectedRoleNaem = self.dic_MemberDetail.role_name
                }
                
                self.tableView.reloadData()
            }
        }
    }
    
    
    
    
    //MARK: UIBUtton Action Method
    @IBAction func btnBack_Action(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmit_Action(_ sender: UIButton) {
        
        let dict_param = NSMutableDictionary()
        dict_param["role"] = self.SelectedRole
        dict_param["user_id"] = self.dic_MemberDetail.user_id
        dict_param["profile_id"] = self.dic_MemberDetail.profile_id
        //Assign Role Request
        KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self.navigationController, processLabel: "", params: dict_param, api: "profile/assign_role", arrFlag: false) { (JSON, status) in
            if status == 1 {
                print(JSON)
                app_Delegate.roleID = self.SelectedRole
                app_Delegate.roleNAME = self.SelectedRoleNaem
                app_Delegate.strAssignRole = "AssignRole"
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}


//=============================================================================================================//
//=============================================================================================================//
//=============================================================================================================//


/*
 Extention of `NavigationMenuViewController` class, implements table view delegates methods.
 */
extension ManageRoleVC: UITableViewDelegate, UITableViewDataSource {
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        
//        var imgscale = self.imageScale
//        
//        let y = self.maximumHeaderHeight - (scrollView.contentOffset.y + self.maximumHeaderHeight)
//        let height = min(max(y, self.minimumHeaderHeight), self.maximumHeaderHeight + 30)
//        
//        layoutHeightHeaderView.constant = height
//        
//        let range = self.maximumHeaderHeight - self.minimumHeaderHeight
//        let openAmount = self.layoutHeightHeaderView.constant - self.minimumHeaderHeight
//        let scale = (openAmount/range)
//        
//        imgscale = imgscale - scale
//        
//        self.imageBottomConstraint.constant = self.layoutHeightHeaderView.constant / imgscale
//        self.imageTopContraint.constant = self.layoutHeightHeaderView.constant / imgscale
//        
//        
//        UIView.animate(withDuration: 0.1) {
//            self.view.layoutIfNeeded()
//            self.imgView.layer.cornerRadius = self.imgView.frame.size.width / 2
//            self.imgView.clipsToBounds = true
//            self.imgView_BG.layer.cornerRadius = self.imgView_BG.frame.size.width / 2
//            self.imgView_BG.clipsToBounds = true
//            
//            let percentage = self.layoutHeightHeaderView.constant * 0.3
//            
//            // if percentage > 0.7{
//            
//            self.lblName.font = UIFont.init(name: "lato-Medium", size: percentage)
//        }
//
//    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRoles.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension//52.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let role_Cell = tableView.dequeueReusableCell(withIdentifier: "ManageRoleTableCell", for: indexPath as IndexPath) as! ManageRoleTableCell
        role_Cell.backgroundColor = UIColor.clear
        role_Cell.selectionStyle = .none
        role_Cell.separatorInset = UIEdgeInsets.zero
        
        if self.arrRoles[indexPath.row].role_ID == SelectedRole {
            role_Cell.btnSelectUnselect.isSelected = true
            role_Cell.viewOuterBG.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9921568627, blue: 0.9882352941, alpha: 1)
        }
        else {
            role_Cell.btnSelectUnselect.isSelected = false
            role_Cell.viewOuterBG.backgroundColor = UIColor.white
        }
        
        let strTitle = self.arrRoles[indexPath.row].roleName
        role_Cell.lblTitle.text = strTitle
        
        let strDescriprion = self.arrRoles[indexPath.row].roleDescription
        role_Cell.lblSubTitle.text = strDescriprion
        
        role_Cell.btnSelectUnselect.tag = indexPath.row
        role_Cell.btnSelectUnselect.addTarget(self, action: #selector(self.btnSelectRole_Action(_:)), for: .touchUpInside)
        
            
        return role_Cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        SelectedRole = self.arrRoles[indexPath.row].role_ID
        SelectedRoleNaem = self.arrRoles[indexPath.row].roleName
        tableView.reloadData()
    }
    
    
    // MARK : - Button Select Unselect Role
    @IBAction func btnSelectRole_Action(_ sender: UIButton) {
        let btnRole = (sender as AnyObject).convert(CGPoint.zero, to: tableView)
        let index = tableView.indexPathForRow(at: btnRole)
        
        SelectedRole = self.arrRoles[(index?.row)!].role_ID
        SelectedRoleNaem = self.arrRoles[(index?.row)!].roleName
        tableView.reloadData()
    }
    
}









