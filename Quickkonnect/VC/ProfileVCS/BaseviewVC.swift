//
//  BaseviewVC.swift
//  Quickkonnect
//
//  Created by Kavi on 26/12/17.
//  Copyright © 2017 com.kavish. All rights reserved.
//

import UIKit
import Material
import Alamofire
import RealmSwift
import ObjectMapper

class CountryListData: NSObject {
    
    var code = 0
    var name = ""
    
    convenience init(_ Code: Int, _ Name: String) {
        self.init()
        self.code = Code
        self.name = Name
    }
}

class StateListData: NSObject {
    
    var id = 0
    var name = ""
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        self.id = dict["id"] as? Int ?? 0
        self.name = dict["name"] as? String ?? ""
    }
}

class CityListData: NSObject {
    
    var id = 0
    var name = ""
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        self.id = dict["id"] as? Int ?? 0
        self.name = dict["name"] as? String ?? ""
    }
}
//===============================================================================================//
//===============================================================================================//
//===============================================================================================//

class BaseviewVC: UIViewController, UITextFieldDelegate, MLPAutoCompleteTextFieldDelegate, MLPAutoCompleteTextFieldDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var tfDOB: TextField!
    @IBOutlet weak var txtGender: TextField!
    @IBOutlet var Title_lbl: UILabel!
    @IBOutlet weak var tfHomeTown: TextField!
    @IBOutlet weak var tfCountry: KVtxtTextField!
    @IBOutlet weak var tfState: KVtxtTextField!
    @IBOutlet weak var tfCity: KVtxtTextField!
    
    var arrCheckCity = [String]()
    var arrCheckState = [String]()
    var arrCheckCountry = [String]()
    var CountryList: [CountryListData]! = [CountryListData]()
    var StateList: [StateListData]! = [StateListData]()
    var CityList: [CityListData]! = [CityListData]()
    var arrGender = ["Male", "Female", "Other", "Unknown"]
    
    @IBOutlet var country: MLPAutoCompleteTextField!
    
    var SelectedRowType = 0
    var indexLocationSelection:Int = 0
    var strCountryName:String = ""
    var strCountryId:String = ""
    var strStateName: String = ""
    var strStateId: String = ""
    var strCityName: String = ""
    var strCityId: String = ""
    var strSelectGender = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set TextField PlaceHolder and BgColor================//
        self.tfDOB.setDivider()
        self.txtGender.setDivider()
        self.tfHomeTown.setDivider()
        self.tfCountry.setDivider()
        self.tfState.setDivider()
        self.tfCity.setDivider()
        
        self.tfCountry.autoCompleteTableView.backgroundColor = UIColor.white
        self.tfCountry.autoCompleteTableBorderColor = #colorLiteral(red: 0, green: 0.6777210236, blue: 0.7377243042, alpha: 1)
        self.tfCountry.autoCompleteTableBorderWidth = 0.5
        self.tfCountry.showAutoCompleteTableWhenEditingBegins = true
        self.tfCountry.autoCompleteTableView.separatorInset.left = 0
        self.tfCountry.autoCompleteTableView.separatorInset.right = 0
        
        self.tfCity.autoCompleteTableView.backgroundColor = UIColor.white
        self.tfCity.autoCompleteTableBorderColor = #colorLiteral(red: 0, green: 0.6777210236, blue: 0.7377243042, alpha: 1)
        self.tfCity.autoCompleteTableBorderWidth = 0.5
        self.tfCity.showAutoCompleteTableWhenEditingBegins = true
        self.tfCity.autoCompleteTableView.separatorInset.left = 0
        self.tfCity.autoCompleteTableView.separatorInset.right = 0
        
        self.tfState.autoCompleteTableView.backgroundColor = UIColor.white
        self.tfState.autoCompleteTableBorderColor = #colorLiteral(red: 0, green: 0.6777210236, blue: 0.7377243042, alpha: 1)
        self.tfState.autoCompleteTableBorderWidth = 0.5
        self.tfState.showAutoCompleteTableWhenEditingBegins = true
        self.tfState.autoCompleteTableView.separatorInset.left = 0
        self.tfState.autoCompleteTableView.separatorInset.right = 0
        
        
        let countrlist = RealmServieces.shared.realm.objects(RealmCountryList.self)
        
        self.CountryList.removeAll()
        self.arrCheckCountry.removeAll()
        for item in countrlist {
            arrCheckCountry.append(item.name)
            self.CountryList.append(CountryListData.init(item.code, item.name))
        }
        
        KVClass.KVServiece(methodType: "GET", processView: nil, baseView: self, processLabel: "", params: nil, api: "get_countries", arrFlag: false) { (countryL, status) in
            
            if let data = countryL["data"] as? [NSDictionary]
            {
                if data.count != self.CountryList.count
                {
                    for item in data {
                        let obj = RealmCountryList.init(item)
                        RealmServieces.shared.create(obj)
                    }
                    
                    self.CountryList.removeAll()
                    self.arrCheckCountry.removeAll()
                    for item in countrlist
                    {
                        self.arrCheckCountry.append(item.name)
                        self.CountryList.append(CountryListData.init(item.code, item.name))
                    }
                    
                    self.tfCountry.reloadData()
                }
            }
            
        }
        
        //Already loaded state and city
        
        
        
        
        if let story = Singleton.sharedInstance.data_details?.basicDetails?.hometown, story != ""  {
            tfHomeTown.text = story
        }
        
        //BaseviewVC
        if let story = Singleton.sharedInstance.data_details?.basicDetails?.birthdate, story != "" {
            let dateformat = DateFormatter()
            dateformat.dateFormat = "yyyy-MM-dd"
            if dateformat.date(from: story) != nil
            {
                let date = dateformat.date(from: story)!
                dateformat.dateFormat = "dd/MM/yyyy"
                let stdate = dateformat.string(from: date)
                tfDOB.text = stdate
            }
            else
            {
                dateformat.dateFormat = "dd/MM/yyyy"
                let date = dateformat.date(from: story)!
                dateformat.dateFormat = "dd/MM/yyyy"
                let stdate = dateformat.string(from: date)
                tfDOB.text = stdate
            }
        }
        
        //Gender
        if let story = Singleton.sharedInstance.data_details?.basicDetails?.gender, story != ""  {
            txtGender.text = getFullGenderString(firstWord_Gender: story)
        }
        
        //BaseviewVC
        if let story = Singleton.sharedInstance.data_details?.basicDetails?.country, story != "" {
            tfCountry.text = story
            strCountryName = story
        }
        if let story = Singleton.sharedInstance.data_details?.basicDetails?.country_id {
            strCountryId = "\(story)"
            DispatchQueue.main.async {
                self.getStatesListFromCountry(county_id: self.strCountryId)
            }
        }
        
        //BaseviewVC
        if let story = Singleton.sharedInstance.data_details?.basicDetails?.state, story != "" {
            tfState.text = story
            strStateName = story
        }
        if let story = Singleton.sharedInstance.data_details?.basicDetails?.state_id {
            strStateId = "\(story)"
            DispatchQueue.main.async {
                self.getCitysListFromStates(state_id: self.strStateId)
            }
        }
        
        //BaseviewVC
        if let story = Singleton.sharedInstance.data_details?.basicDetails?.city, story != "" {
            tfCity.text = story
            strCityName = story
        }
        if let story = Singleton.sharedInstance.data_details?.basicDetails?.city_id {
            strCityId = "\(story)"
        }
        
        tfCountry.tag = 200
        tfState.tag = 201
        tfCity.tag = 202
        tfDOB.delegate = self
        tfHomeTown.delegate = self
        tfCountry.delegate = self
        tfState.delegate = self
        tfCity.delegate = self
        txtGender.delegate = self
        
        let toolBar = UIToolbar().ToolbarPikerteast(mySelect: #selector(dismissPicker), delegate: self)
        tfDOB.inputAccessoryView = toolBar
        tfDOB.addTarget(self, action: #selector(self.dateTextInputPressed(sender:)), for: .allTouchEvents)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let udefaults = UserDefaults.standard
        if udefaults.object(forKey: "countryid") != nil {
            
            if self.strCountryId != udefaults.object(forKey: "countryid") as! String {
                self.strStateId = ""
                self.strStateName = ""
                self.strCityId = ""
                self.strCityName = ""
            }
            
            self.strCountryId = udefaults.object(forKey: "countryid") as! String
            self.strCountryName = udefaults.object(forKey: "countryname") as! String
            udefaults.removeObject(forKey: "countryid")
            udefaults.removeObject(forKey: "countryname")
            self.tfCountry.text = self.strCountryName
        } else if udefaults.object(forKey: "stateid") != nil {
            self.strStateId = udefaults.object(forKey: "stateid") as! String
            self.strStateName = udefaults.object(forKey: "statename") as! String
            udefaults.removeObject(forKey: "stateid")
            udefaults.removeObject(forKey: "statename")
            self.tfState.text = self.strStateName
        } else if udefaults.object(forKey: "cityid") != nil {
            self.strCityId = udefaults.object(forKey: "cityid") as! String
            self.strCityName = udefaults.object(forKey: "cityname") as! String
            udefaults.removeObject(forKey: "cityid")
            udefaults.removeObject(forKey: "cityname")
            
            self.tfCity.text = self.strCityName
        }

        
        
    }
    
    ///=========================================================================///
    //Get Gender
    func getFullGenderString(firstWord_Gender: String) -> String {
        if firstWord_Gender == "M" {
            strSelectGender = arrGender[0]
        }
        else if firstWord_Gender == "F" {
            strSelectGender = arrGender[1]
        }
        else if firstWord_Gender == "O" {
            strSelectGender = arrGender[2]
        }
        else if firstWord_Gender == "U" {
            strSelectGender = arrGender[3]
        }
        return strSelectGender
    }
    ///=========================================================================///
    ///=========================================================================///
    
    
    
    func getStatesListFromCountry(county_id: String) {
        
        if county_id != "" {
            let dict = NSMutableDictionary()
            dict["country_code"] = (county_id)
            
            self.tfState.tag = Int(county_id)!
            self.strCountryId = county_id
            
            KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict, api: "get_states", arrFlag: false, Hendler: { (json, status) in
                
                if let list = json["data"] as? [NSDictionary]
                {
                    self.StateList.removeAll()
                    self.arrCheckState.removeAll()
                    for item in list
                    {
                        self.arrCheckState.append(item["name"] as? String ?? "")
                        self.StateList.append(StateListData.init(item))
                    }
                    self.tfState.reloadData()
                }
            })
        }
    }
    
    
    func getCitysListFromStates(state_id: String) {
        
        if state_id != "" {
            print(state_id)
            
            let dict = NSMutableDictionary()
            dict["country_id"] = strCountryId
            dict["state_id"] = state_id
            
            KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict, api: "get_cities", arrFlag: false, Hendler: { (json, status) in
                
                if let list = json["data"] as? [NSDictionary]
                {
                    self.CityList.removeAll()
                    self.arrCheckCity.removeAll()
                    for item in list
                    {
                        self.arrCheckCity.append(item["name"] as? String ?? "")
                        self.CityList.append(CityListData.init(item))
                    }
                    
                    // self.tfCity.reloadData()
                }
            })
        }
        
    }
    
    
    
    
    func selectLocationTapped(StringTag Tag:Int) {
        var isContinue:Bool = false
        if Tag == 200 {
            isContinue = true
            indexLocationSelection = 0
        } else if Tag == 201 {
            if strCountryId.count > 0 {
                isContinue = true
                indexLocationSelection = 1
            }
        } else if Tag == 202 {
            if strCountryId.count > 0 && strStateId.count > 0 {
                isContinue = true
                indexLocationSelection = 2
            }
        }
        
        if isContinue == true {
            self.performSegue(withIdentifier: "listforlocation", sender: self)
        } else {
            var strAlertMsg = ""
            if Tag == 201 {
                strAlertMsg = "Please select Country Field first before State."
            } else {
                strAlertMsg = "Please select Country and State Field first before select City."
            }
            
            let alertView = UIAlertController(title: "", message: strAlertMsg, preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertView.addAction(action)
            self.present(alertView, animated: true, completion: nil)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "listforlocation" {
            let nextScene =  segue.destination as! SearchViewController
            if indexLocationSelection == 1 {
                nextScene.strCountryId = self.strCountryId
            } else if indexLocationSelection == 2 {
                nextScene.strCountryId = self.strCountryId
                nextScene.strStateId = self.strStateId
            }
        }
    }
    
    //===============================================================================================//
    //===============================================================================================//
    //MARK: Picker View Delegate Datasource Method
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrGender.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrGender[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
        if self.arrGender.count > 0 {
            SelectedRowType = row
            strSelectGender = arrGender[row]
        }
    }
    //===============================================================================================//
    //===============================================================================================//
    
    //===============================================================================================//
    //UITextField Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        return true
//    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtGender {
            if arrGender.contains(strSelectGender) {
                txtGender.text = strSelectGender
            }
            else {
                txtGender.text = ""
                KVClass.ShowNotification("", "Please Select Proper Gender")
            }
        }
        else if textField == self.tfDOB {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            if formatter.date(from: textField.text!) != nil {
            }
            else {
                self.tfDOB.text = ""
                KVClass.ShowNotification("", "Please Select Proper Date")
                return
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtGender {
            if self.arrGender.count > 0 {
                strSelectGender = self.arrGender[SelectedRowType]
                self.settxtInputview()
            }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfHomeTown {
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
    }
    //======================================================================================================//
    //======================================================================================================//
    
    //For Gender Picker
    func settxtInputview() {
        let picker = UIPickerView.init()
        picker.delegate = self
        picker.dataSource = self
        let toobar = UIToolbar()
        
        toobar.sizeToFit()
        
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.doneTxt(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toobar.setItems([ spaceButton, doneButton], animated: false)
        toobar.isUserInteractionEnabled = true
        
        self.txtGender.inputView = picker
        self.txtGender.inputAccessoryView = toobar
        self.txtGender.clearButtonMode = .never
        picker.selectRow(SelectedRowType, inComponent: 0, animated: false)
        
    }
    
    @IBAction func doneTxt(_ sender: UIBarButtonItem) {
        self.txtGender.endEditing(true)
        self.txtGender.text = strSelectGender
        //=====Fot Get Selected Gender=====//
        for itemType in 0..<arrGender.count {
            if self.arrGender[itemType] == strSelectGender {
                SelectedRowType = itemType
            }
        }
        //=====================================///
        if arrGender.contains(strSelectGender) {
            txtGender.text = strSelectGender
        }
        else {
            txtGender.text = ""
            KVClass.ShowNotification("", "Please Select Proper Gender")
        }
    }
    //===============================================================================================//
    //===============================================================================================//
    
    func autoCompleteTextField(_ textField: MLPAutoCompleteTextField!, possibleCompletionsFor string: String!) -> [Any]! {
        if textField == self.tfCountry
        {
            return self.CountryList.map{($0.name)}
        }
        else if textField == self.tfState
        {
            return self.StateList.map{($0.name)}
        }
        else if textField == self.tfCity
        {
            return self.CityList.map{($0.name)}
        }
        return []
    }
    
    func autoCompleteTextField(_ textField: MLPAutoCompleteTextField!, didSelectAutoComplete selectedString: String!, withAutoComplete selectedObject: MLPAutoCompletionObject!, forRowAt indexPath: IndexPath!) {
        
        if textField == self.tfCountry
        {
            let idDict = self.CountryList.filter({ (data) -> Bool in
                
                return data.name.lowercased() == selectedString.lowercased()
            })
            
            if idDict.count > 0
            {
                let dict = NSMutableDictionary()
                dict["country_code"] = (idDict.last?.code ?? 0)!
                
                self.tfState.tag = (idDict.last?.code ?? 0)!
                self.strCountryId = "\((idDict.last?.code ?? 0)!)"
                
                KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict, api: "get_states", arrFlag: false, Hendler: { (json, status) in
                    
                    if let list = json["data"] as? [NSDictionary]
                    {
                        self.StateList.removeAll()
                        self.arrCheckState.removeAll()
                        for item in list
                        {
                            self.arrCheckState.append(item["name"] as? String ?? "")
                            self.StateList.append(StateListData.init(item))
                        }
                        
                        //self.tfState.reloadData()
                    }
                })
                
            }
        }
        else if textField == self.tfCity
        {
            let idDict = self.CityList.filter({ (data) -> Bool in
                
                return data.name.lowercased() == selectedString.lowercased()
            })
            
            if idDict.count > 0
            {
                self.strCityId = "\((idDict.last?.id ?? 0)!)"
            }
            
        }
            
            
        else if textField == self.tfState
        {
            let idDict = self.StateList.filter({ (data) -> Bool in
                
                return data.name.lowercased() == selectedString.lowercased()
            })
            
            if idDict.count > 0
            {
                self.strStateId = "\((idDict.last?.id ?? 0)!)"
                
                let dict = NSMutableDictionary()
                dict["country_id"] = self.tfState.tag
                dict["state_id"] = (idDict.last?.id ?? 0)!
                
                KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict, api: "get_cities", arrFlag: false, Hendler: { (json, status) in
                    
                    if let list = json["data"] as? [NSDictionary]
                    {
                        self.CityList.removeAll()
                        self.arrCheckCity.removeAll()
                        for item in list
                        {
                            self.arrCheckCity.append(item["name"] as? String ?? "")
                            self.CityList.append(CityListData.init(item))
                        }
                        
                        // self.tfCity.reloadData()
                    }
                })
                
            }
        }
        
    }
    
    func dateTextInputPressed(sender: UITextField) {
        
        //Create the view
        let datePickerView  : UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.tag = sender.tag
        //  inputView.addSubview(datePickerView) // add date picker to UIView
        
        if sender.text != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if dateFormatter.date(from: sender.text!) != nil {
                let currentDate:Date = dateFormatter.date(from: sender.text!)!
                datePickerView.setDate(currentDate, animated: false)
            }
            else {
                self.tfDOB.text = ""
                KVClass.ShowNotification("", "Please Select Proper Date")
                return
            }
            
        }
        // datePickerView.maximumDate
        //      = (Calendar.current as NSCalendar).date(byAdding: .year, value: -13, to: Date(), options: [])!
        
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.handleDatePicker(sender:)), for: UIControlEvents.valueChanged)
        
        handleDatePicker(sender: datePickerView) // Set the date on start.
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        tfDOB.text = dateFormatter.string(from: sender.date)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //===============================================================================================//
    //MARK : - UIButton Action Method
    
    @IBAction func closePress(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDoneClicked(_ sender: Any) {
        if tfDOB.text == "" {
            KVClass.ShowNotification("", "Please select Date of Birth")
            return
        }
        if txtGender.text == "" {
            KVClass.ShowNotification("", "Please select Gender")
            return
        }
        if !(arrGender.contains(txtGender.text!)) {
            KVClass.ShowNotification("", "Please select proper Gender")
            return
        }
        if tfHomeTown.text == "" {
            KVClass.ShowNotification("", "Please enter Hometown")
            return
        }
        
        if !(tfCountry.text == "")  {
            if !(self.arrCheckCountry.contains(tfCountry.text!)) {
                KVClass.ShowNotification("", "Please select proper Country")
                return
            }
        }
        
        if !(tfState.text == "")  {
            if !(self.arrCheckState.contains(tfState.text!)) {
                KVClass.ShowNotification("", "Please select proper State")
                return
            }
        }
        
        if !(tfCity.text == "")  {
            if !(self.arrCheckCity.contains(tfCity.text!)) {
                KVClass.ShowNotification("", "Please select proper City")
                return
            }
        }
        
        let strIndex = strSelectGender.index(strSelectGender.startIndex, offsetBy: 0)
        print(strSelectGender[strIndex])
        
        var myDate = ""
        if let strDate = tfDOB.text, strDate.count > 0
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let dates = dateFormatter.date(from: tfDOB.text!)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            myDate = dateFormatter.string(from: dates!)
        }
        
        let dictData:[String:String] = ["user_id": "\(KVAppDataServieces.shared.LoginUserID())","dob":myDate, "gender": strSelectGender[strIndex].description ,"hometown": tfHomeTown.text!, "country": strCountryId, "state": strStateId, "city": strCityId]
        
        let myHeader:[String:String] = ["QK_ACCESS_KEY":"AAAAmGLql2s:APA91bF-fhfWlp4p_aRjK7UL8ZUPy4VlWWnbRExecXf9yZ1B0pYvT5Jg0r2XS6DOWT1Zb4KH-NfRlG3K_WSTM2RbkP3xD7d-pZF5zlSmb", "content-Type":"application/json"]
        

        KVClass.KVShowLoader()
        Alamofire.request("\(self.getPrefixURL())user_basic_profile", method: .post, parameters: dictData, encoding: JSONEncoding.default, headers: myHeader).responseJSON { (response:DataResponse<Any>) in
            kUserDefults(true as AnyObject, key: "isprofileupdate")
            KVClass.KVHideLoader()
            switch(response.result) {
            case .success(_):
            if let data = response.result.value {
                    
                if let datadict = data as? NSDictionary
                {
                    print(datadict)
                    let dict = datadict["data"] as? NSDictionary ?? [:]
                    print(dict)
                    KVAppDataServieces.shared.updateProfile(.basic_detail, dict, 0)
                        
                    print(KVAppDataServieces.shared.UserValue(.profile_data) as? NSDictionary ?? [:])
                        
                    Timer.scheduledTimer(withTimeInterval: 0.15, repeats: false, block: { (tim) in
                        tim.invalidate()
                        self.navigationController?.popViewController(animated: true)
                    })
                    NotificationCenter.default.post(name: Notification.Name("UPDATEPROFILE"), object: nil)
                }
                else
                {
                self.navigationController?.popViewController(animated: true)
                }
            }
            break
                    
            case .failure(_):
                self.navigationController?.popViewController(animated: true)
                break
            }
        }
    }
    
}
































