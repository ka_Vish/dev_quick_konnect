//
//  SocialVC.swift
//  Quickkonnect
//
//  Created by Kavi on 21/12/17.
//  Copyright © 2017 com.kavish. All rights reserved.
//

import UIKit
import TwitterKit
import InstagramLogin
import LinkedinSwift
import RealmSwift


class SocialMediaStatusData: NSObject {
    
    var id = 0
    var status = false
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        
        self.id = dict["id"] as? Int ?? 0
        if dict["status"] as? String ?? "" == "Active"
        {
            self.status = true
        }
        else
        {
            self.status = false
        }
    }
}

class SocialVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet var continue_btn: UIButton!
    
    var instagramLogin: InstagramLoginViewController!
    
    var TitleImageArr = ["social_qk_logo", "fb_ic","link_ic","twit_ic","insta_ic","google_ic"]
    var TitleLabelArr = ["Quickkonnect", "Facebook","Linkedin","Twitter","Instagram","Google plus"]
    
    let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81pbh72vnmiudv", clientSecret: "Tdq6mikPJbxFkVOT", state: "DLKDJF46ikMMZADfdfds", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://www.example.com/"))
    
    var strComeFrom = ""
    
    var StatusList = [NSDictionary]()
    var ispresentedview = false
    
    @IBOutlet var Social_tab: UITableView!
    @IBOutlet var constraint_table_bottom: NSLayoutConstraint!
    
    var UserData: resultUserDict! = resultUserDict()
    
    
    func updateValue()
    {
        self.StatusList.removeAll()
        self.StatusList.append(KVAppDataServieces.shared.localQKData())
        self.StatusList.append(KVAppDataServieces.shared.fbData())
        self.StatusList.append(KVAppDataServieces.shared.linkedinData())
        self.StatusList.append(KVAppDataServieces.shared.twitterData())
        self.StatusList.append(KVAppDataServieces.shared.instaData())
        self.StatusList.append(KVAppDataServieces.shared.googleData())
        
        self.Social_tab.reloadData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.view.tag == 2211
        {
            self.continue_btn.isHidden = true
            self.btnSkip.isHidden = true
            self.btnClose.isHidden = false
            constraint_table_bottom.constant = -65
        }
        else
        {
            constraint_table_bottom.constant = 8
            self.continue_btn.isHidden = false
            self.btnSkip.isHidden = false
            self.btnClose.isHidden = true
        }
        
        
        self.updateValue()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateValue()
    }
    
    //===============================================================================================//
    //MARK : -  UITable View Delegate and Datasource Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.TitleLabelArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SocialCell = tableView.dequeueReusableCell(withIdentifier: "SocialCell", for: indexPath) as! SocialCell
        
        cell.Sub_lbl.text = ""
        
        if indexPath.row == 0 {
            cell.Switch.isHidden = true
            cell.lbl_Active.isHidden = false
            cell.Switch.isUserInteractionEnabled = false
            cell.Sub_lbl.text = KVAppDataServieces.shared.UserValue(KVAppDataServieces.KVAppDataEnum.fullname) as? String
        }
        else {
            cell.Switch.isHidden = false
            cell.lbl_Active.isHidden = true
            if  self.StatusList.count > 4
            {
                cell.Switch.isOn = self.StatusList[indexPath.row]["status"] as? Bool ?? false
                if cell.Switch.isOn
                {
                    if (self.StatusList[indexPath.row]["data"] as? NSDictionary ?? [:])["username"] as? String ?? "" == ""
                    {
                        switch indexPath.row{
                            
                        case 1:
                            cell.Sub_lbl.text = (self.StatusList[indexPath.row]["data"] as? NSDictionary ?? [:])["first_name"] as? String ?? ""
                        case 2:
                            cell.Sub_lbl.text = (self.StatusList[indexPath.row]["data"] as? NSDictionary ?? [:])["firstName"] as? String ?? ""
                        case 3:
                            cell.Sub_lbl.text = (self.StatusList[indexPath.row]["data"] as? NSDictionary ?? [:])["screen_name"] as? String ?? ""
                        case 4:
                            cell.Sub_lbl.text = (self.StatusList[indexPath.row]["data"] as? NSDictionary ?? [:])["username"] as? String ?? ""
                        case 5:
                            cell.Sub_lbl.text = (self.StatusList[indexPath.row]["data"] as? NSDictionary ?? [:])["first_name"] as? String ?? ""
                        default:
                            break
                        }
                    }
                    else
                    {
                        cell.Sub_lbl.text = (self.StatusList[indexPath.row]["data"] as? NSDictionary ?? [:])["username"] as? String ?? ""
                        
                    }
                }
            }
            
            cell.Switch.isUserInteractionEnabled = true
            cell.Switch.addTarget(self, action: #selector(socialSwitchpress(_:)), for: .valueChanged)
            cell.Switch.tag = indexPath.row
            if indexPath.row == 5 {
                cell.Switch.isOn = false
                cell.Switch.isUserInteractionEnabled = false
            }
            
        }
        
        cell.Title_img.image = UIImage.init(named: self.TitleImageArr[indexPath.row])
        cell.Title_lbl.text = self.TitleLabelArr[indexPath.row]
        
        return cell
    }
    //===============================================================================================//
    //===============================================================================================//
    
    
    //===============================================================================================//
    //MARK : -  UIButton Action Method
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func socialSwitchpress(_ sender: UISwitch) {
        if sender.isOn {
            if sender.tag == 0 || sender.tag == 5 {
                return
            }
            if let _ = self.StatusList[sender.tag]["data"]! as? NSDictionary
            {
                let dict = NSMutableDictionary()
                dict["user_id"] = kUserDefults_("kLoginusrid") as? Int ?? 0
                dict["social_media_id"] = sender.tag
                dict["status"] = 1
                
                KVClass.KVServiece(methodType: "POST", processView: nil, baseView: self, processLabel: "", params: dict, api: "social_network_status", arrFlag: false, Hendler: { (json, status) in
                    
                    print(json)
                    
                    KVClass.ShowNotification("", json["msg"] as? String ?? "")
                    if status == 1
                    {
                        switch sender.tag{
                        case 1:
                            let datadict = NSMutableDictionary.init(dictionary: (KVAppDataServieces.shared.UserValue(.facebook)) as! [AnyHashable : AnyHashable])
                            datadict["status"] = true
                            KVAppDataServieces.shared.update(.facebook, datadict)
                        case 2:
                            let datadict = NSMutableDictionary.init(dictionary: (KVAppDataServieces.shared.UserValue(.linkedin)) as! [AnyHashable : AnyHashable])
                            datadict["status"] = true
                            KVAppDataServieces.shared.update(.linkedin, datadict)
                        case 3:
                            let datadict = NSMutableDictionary.init(dictionary: (KVAppDataServieces.shared.UserValue(.twitter)) as! [AnyHashable : AnyHashable])
                            datadict["status"] = true
                            KVAppDataServieces.shared.update(.twitter, datadict)
                        case 4:
                            let datadict = NSMutableDictionary.init(dictionary: (KVAppDataServieces.shared.UserValue(.instagram)) as! [AnyHashable : AnyHashable])
                            datadict["status"] = true
                            KVAppDataServieces.shared.update(.instagram, datadict)
                        case 5:
                            let datadict = NSMutableDictionary.init(dictionary: (KVAppDataServieces.shared.UserValue(.google_plus)) as! [AnyHashable : AnyHashable])
                            datadict["status"] = true
                            KVAppDataServieces.shared.update(.google_plus, datadict)
                        default:
                            break
                            
                        }
                        
                        
                        self.updateValue()
                        // return
                    }
                    else if status == 2214
                    {
                    }
                })
            }
            else {
                
                //For Facebook Account Mapping......................................................................//
                
                if sender.tag == 1
                {
                    DispatchQueue.main.async {
                        
                        DLFacebookKit.loginWithPostPermisson(contoller: self) { (JSON, err) in
                            if let data = JSON as? NSDictionary
                            {
                                
                                print(data)
                                let dicFriend = data["friends"] as! [String : Any]
                                let FriendList = dicFriend["summary"] as! [String : Any]
                                let FriendsCount = FriendList["total_count"] as? Int ?? 0
                                
                                let dict = NSMutableDictionary()
                                dict["firstname"] = data["first_name"]
                                dict["lastname"] = data["last_name"]
                                
                                if data["email"] as? String ?? "" == ""
                                {
                                    dict["email"] = data["id"] as? String ?? ""
                                }
                                else
                                {
                                    dict["email"] = data["email"] as? String ?? ""
                                }
                                //dict["password"] = ""
                                dict["social_media_user_id"] = data["id"]
                                dict["social_media_id"] = "1"
                                dict["friend_list_count"] = FriendsCount.description
                                
                                let dictParameters:NSMutableDictionary
                                    = ["user_id":kUserDefults_("kLoginusrid") as? Int ?? 0,"social_media_user_id":data["id"] as Any , "social_media_id":"1","data":data]
                                
                                KVClass.KVServiece(methodType: "POST", processView: self, baseView: self, processLabel: "", params: dictParameters, api: "social_media_map", arrFlag: false, Hendler: { (json, status) in
                                    print(json)
                                    
                                    if (json["data"] as? NSDictionary) != nil
                                    {
                                        KVClass.ShowNotification("", json["msg"] as? String ?? "")
                                    }
                                    
                                    let updatedict = NSMutableDictionary()
                                    updatedict["status"] = true
                                    updatedict["data"] = data
                                    
                                    KVAppDataServieces.shared.update(.facebook, updatedict)
                                    self.updateValue()
                                })
                            }
                            
                        }
                    }
                }
                
                //..................................................................................................//
                
                //........For Lindin Account Mapping................................................................//
                
                else if sender.tag == 2
                {
                    DispatchQueue.main.async {
                        self.linkedinHelper.authorizeSuccess({ [unowned self] (lsToken) -> Void in
                            
                            KVClass.KVShowLoader()
                            
                            self.linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),positions,date-of-birth,phone-numbers,location,public-profile-url)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
                                
                                print("Request success with response: \(response.jsonObject)")
                                let strId = response.jsonObject["id"]
                                
                                print("Request success with response: \(response)")
                                
                                let dictParameters:NSMutableDictionary = ["user_id":kUserDefults_("kLoginusrid") as? Int ?? 0,"social_media_user_id":strId ?? 0 , "social_media_id":"2","data": response.jsonObject]
                                
                                KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dictParameters, api: "social_media_map", arrFlag: false, Hendler: { (json, status) in
                                    print(json)
                                    KVClass.KVHideLoader()
                                    if let user = json["data"] as? NSDictionary
                                    {
                                        print(user)
                                        KVClass.ShowNotification("Message !!", json["msg"] as? String ?? "")
                                    }
                                    
                                    let updatedict = NSMutableDictionary()
                                    updatedict["status"] = true
                                    updatedict["data"] = response.jsonObject
                                    
                                    KVAppDataServieces.shared.update(.linkedin, updatedict)
                                    self.updateValue()
                                    
                                })
                                
                                
                            }) { [unowned self] (error) -> Void in
                                
                                print("Encounter error: \(error.localizedDescription)")
                            }
                            
                            print("Login success lsToken: \(lsToken)")
                            }, error: { [unowned self] (error) -> Void in
                                
                                print("Encounter error: \(error.localizedDescription)")
                            }, cancel: { [unowned self] () -> Void in
                                
                                print("User Cancelled!")
                        })
                    }
                }
                
                //..................................................................................................//
                
                //..... For Twitter Account Mapping.................................................................//
                
                else if sender.tag == 3
                {
                    DispatchQueue.main.async {
                        TWTRTwitter.sharedInstance().logIn(completion: { (session, err) in
                            
                            if session != nil
                            {
                                let params = ["user_id": (session?.userID ?? "")!]
                                
                                let client = TWTRAPIClient.withCurrentUser()
                                
                                let Twitrequest = client.urlRequest(withMethod: "GET", urlString: "https://api.twitter.com/1.1/users/show.json", parameters: params , error: NSErrorPointer.init(nilLiteral: ()))
                                
                                client.sendTwitterRequest(Twitrequest, completion: { (response, data, err) in
                                    
                                    if data != nil
                                    {
                                        do{
                                            if  let JSON = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                                            {
                                                print(JSON)
                                                
                                                let Twitdata = NSMutableDictionary()
                                                Twitdata["profile_id"] = JSON["id"]
                                                Twitdata["name"] = JSON["name"]
                                                Twitdata["favourite_count"] = JSON["favourites_count"]
                                                Twitdata["followers_count"] = JSON["followers_count"]
                                                Twitdata["friends_count"] = JSON["friends_count"]
                                                Twitdata["screen_name"] = JSON["screen_name"]
                                                Twitdata["favourites"] = ""
                                                Twitdata["friends_list"] = ""
                                                Twitdata["followers_list"] = ""
                                                
                                                let TwitParaDict = NSMutableDictionary()
                                                TwitParaDict["user_id"] = kUserDefults_("kLoginusrid") as? Int ?? 0
                                                TwitParaDict["social_media_id"] = 3
                                                TwitParaDict["data"] = Twitdata
                                                
                                                
                                                KVClass.KVServiece(methodType: "POST", processView: self, baseView: self, processLabel: "", params: TwitParaDict, api: "social_media_map", arrFlag: false, Hendler: { (json, status) in
                                                    
                                                    let updatedict = NSMutableDictionary()
                                                    updatedict["status"] = true
                                                    updatedict["data"] = Twitdata
                                                    
                                                    KVAppDataServieces.shared.update(.twitter, updatedict)
                                                    
                                                    print(json)
                                                    
                                                    //                                        kUserDefults(true as Bool as AnyObject, key: "istwitterlogin")
                                                    KVClass.ShowNotification("", json["msg"] as? String ?? "")
                                                    
                                                    //                                        let userSocial = RealmSocialLoginData.init(kUserDefults_("kLoginusrid") as? Int ?? 0, 3, JSON["screen_name"] as? String ?? "", "Twitter", true)
                                                    //                                        RealmServieces.shared.create(userSocial)
                                                    self.updateValue()
                                                    
                                                })
                                                
                                                
                                            }
                                        }
                                        catch
                                        {
                                            
                                        }
                                    }
                                    
                                })
                            }
                        })
                    }
                }
                
                //..................................................................................................//
                
                //....... For Instagram Account Mapping.............................................................//
                else if sender.tag == 4
                {
                    DispatchQueue.main.async {
                        
                        self.instagramLogin = InstagramLoginViewController.init(clientId: "35c68cc48e2346ab83ff86be4bcafb0a", redirectUri: "http://quickkonnect.com")
                        
                        self.instagramLogin.delegate = self
                        self.instagramLogin.scopes = [.all]
                        self.instagramLogin.progressViewTintColor = .gray
                        self.instagramLogin.title = "Instagram"
                        self.instagramLogin.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.dismissLoginViewController))
                        
                        // You could also add a refresh UIBarButtonItem on the right
                        self.instagramLogin.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(self.refreshPage))
                        
                        // 4. Present it inside a UINavigationController (for example)
                        self.present(UINavigationController(rootViewController: self.instagramLogin), animated: true)
                        
                    }
                }
                //.......................................................................................................//
            }

            
        }
        else {
            
            let dict = NSMutableDictionary()
            dict["user_id"] = kUserDefults_("kLoginusrid") as? Int ?? 0
            dict["social_media_id"] = sender.tag
            dict["status"] = 0
            
            KVClass.KVServiece(methodType: "POST", processView: self, baseView: self, processLabel: "", params: dict, api: "social_network_status", arrFlag: false, Hendler: { (json, status) in
                
                print(json)
                
                if status == 1
                {
                    switch sender.tag{
                    case 1:
                        let datadict = NSMutableDictionary.init(dictionary: (KVAppDataServieces.shared.UserValue(.facebook)) as! [AnyHashable : AnyHashable])
                        datadict["status"] = false
                        KVAppDataServieces.shared.update(.facebook, datadict)
                    case 2:
                        let datadict = NSMutableDictionary.init(dictionary: (KVAppDataServieces.shared.UserValue(.linkedin)) as! [AnyHashable : AnyHashable])
                        datadict["status"] = false
                        KVAppDataServieces.shared.update(.linkedin, datadict)
                    case 3:
                        let datadict = NSMutableDictionary.init(dictionary: (KVAppDataServieces.shared.UserValue(.twitter)) as! [AnyHashable : AnyHashable])
                        datadict["status"] = false
                        KVAppDataServieces.shared.update(.twitter, datadict)
                    case 4:
                        let datadict = NSMutableDictionary.init(dictionary: (KVAppDataServieces.shared.UserValue(.instagram)) as! [AnyHashable : AnyHashable])
                        datadict["status"] = false
                        KVAppDataServieces.shared.update(.instagram, datadict)
                    case 5:
                        let datadict = NSMutableDictionary.init(dictionary: (KVAppDataServieces.shared.UserValue(.google_plus)) as! [AnyHashable : AnyHashable])
                        datadict["status"] = false
                        KVAppDataServieces.shared.update(.google_plus, datadict)
                    default:
                        break
                        
                    }
                }
                
                
                KVClass.ShowNotification("", json["msg"] as? String ?? "")
                self.updateValue()
                
            })
            
        }
        
    }
    
    @objc func dismissLoginViewController() {
        // self.Social_tab.reloadData()
        self.instagramLogin.dismiss(animated: true)
    }
    
    @objc func refreshPage() {
        self.instagramLogin.reloadPage()
    }
    
    
    func TwitGetLists(_ user_id: String,_ api: String, Hendler complition:@escaping (_ JSON:NSDictionary) -> Void)
    {
        let params = ["user_id": user_id]
        
        let client = TWTRAPIClient.init(userID: user_id)
        
        let Twitrequest = client.urlRequest(withMethod: "GET", urlString: "https://api.twitter.com/1.1/\(api)", parameters: params , error: NSErrorPointer.init(nilLiteral: ()))
        
        client.sendTwitterRequest(Twitrequest, completion: { (response, data, err) in
            
            if data != nil
            {
                do{
                    if  let JSON = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        
                        complition(JSON)
                        
                    }
                }
                catch
                {
                    
                }
            }
            
        })
    }
    
    @IBAction func skipPress(_ sender: UIButton)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        obj.isedit = true
        obj.scanuserid = KVAppDataServieces.shared.LoginUserID()
        obj.strContactProfile = ""
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

class SocialCell: UITableViewCell {
    
    @IBOutlet var Title_img: UIImageView!
    @IBOutlet var Title_lbl: UILabel!
    @IBOutlet var Sub_lbl: UILabel!
    @IBOutlet var lbl_Active: UILabel!
    @IBOutlet var Switch: UISwitch!
    
    
}


extension SocialVC: InstagramLoginViewControllerDelegate
{
    func instagramLoginDidFinish(accessToken: String?, error: InstagramError?) {
        
        self.Social_tab.reloadData()
        
        print((accessToken ?? "")!)
        
        self.instagramLogin.dismiss(animated: true) {
            
            DispatchQueue.main.async {
                
                KVClass.KVShowLoader()
                
                let urlString = "https://api.instagram.com/v1/users/self/?access_token=\((accessToken ?? "")!)"
                
                print(urlString)
                
                let url = NSURL(string: urlString)
                if url == nil {
                    
                    return
                }
                
                let request = NSMutableURLRequest(url: url! as URL)
                let config = URLSessionConfiguration.default
                let session = URLSession(configuration: config)        //session.delegate = self
                request.httpMethod = "GET"
                request.timeoutInterval = 60
                
                let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, respomse, err) in
                    
                    
                    //
                    
                    if data != nil
                    {
                        do
                        {
                            if  let JSON = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                                
                                
                                
                                if let dataDict = JSON["data"] as? NSDictionary, let MetaDict = JSON["meta"] as? NSDictionary
                                {
                                    print(dataDict)
                                    DispatchQueue.main.async {
                                        
                                        KVClass.KVHideLoader()
                                        
                                        let Instadata = NSMutableDictionary()
                                        Instadata["user_id"] = kUserDefults_("kLoginusrid") as? Int ?? 0
                                        Instadata["social_media_id"] = 4
                                        Instadata["data"] = dataDict
                                        Instadata["meta"] = MetaDict
                                        
                                        KVClass.KVServiece(methodType: "POST", processView: self, baseView: self, processLabel: "", params: Instadata, api: "social_media_map", arrFlag: false, Hendler: { (json, status) in
                                            
                                            // kUserDefults(true as Bool as AnyObject, key: "isinstalogin")
                                            KVClass.ShowNotification("", json["msg"] as? String ?? "")
                                            
                                            let updatedict = NSMutableDictionary()
                                            updatedict["status"] = true
                                            updatedict["data"] = dataDict
                                            
                                            KVAppDataServieces.shared.update(.instagram, updatedict)
                                            self.updateValue()
                                            
                                        })
                                    }
                                }
                            }
                            
                        }
                        catch
                        {
                            KVClass.ShowNotification("", "Opps!! something went wrong please try again later !")
                        }
                    }
                    
                })
                task.resume()
            }
        }
        
    }
}
