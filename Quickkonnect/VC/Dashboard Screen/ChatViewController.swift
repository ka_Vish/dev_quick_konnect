//
//  ChatViewController.swift
//  Quickkonnect
//
//  Created by Dhaval Tannarana on 02/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class ChatViewController: QKMainBaseVC {

    @IBOutlet weak var btnAllContacts: UIButton!
    var selectedIndex:Int = 1
    var arrData:[[String:Any]] = []
    var arrMainData:[[String:Any]] = []
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnHistory: UIButton!
    @IBOutlet var emptylbl: UILabel!
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tblView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.emptylbl.isHidden = false
        self.Bar.Title_lbl.text = "Chat"
        
        self.tblView.delegate = self
        self.tblView.dataSource = self
        
        //self.Bar.img_TopBG.isHidden = true
        self.Bar.Menu_btn.isHidden = true
        self.Bar.Noti_btn.isHidden = false
        self.Bar.btn_Filter.isHidden = true
        self.Bar.Back_btn.isHidden = false
        self.Bar.btn_Chat.isHidden = true
        self.Bar.Noti_btn.isHidden = true
        self.Bar.Title_lbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.Bar.Profile_Img.isHidden = true
        self.Bar.Profile_Img_BG.isHidden = true
        self.Bar.backgroundColor = UIColor.init(hex: "28AFB0")
        self.Bar.constraint_img_profile_trilling.constant = 12
        
        
        
        //=========Add Refresh Control For Pull To Refresh==============//
        self.tblView.pullTorefresh(#selector(self.callAPIForContactList), self)
        //==============================================================//
        //==============================================================//
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.arrData.removeAll()
        self.arrMainData.removeAll()
        if let valuedata = KVAppDataServieces.shared.UserValue(.contact_user_list) as? [[String:Any]]
        {
            self.arrData = valuedata
            
            self.arrMainData = self.arrData
            if self.arrMainData.count > 0
            {
                self.emptylbl.isHidden = true
            }
            else
            {
                self.emptylbl.isHidden = false
            }
            
        }
        if self.arrMainData.count > 0
        {
            self.emptylbl.isHidden = true
        }
        else
        {
            self.emptylbl.isHidden = false
        }
        self.tblView.reloadData()
    }
   
    func callAPIForContactList() {
        //scan_user_list
        KVClass.KVShowLoader()
        
        Alamofire.request(self.getPrefixURL() + "scan_user_list/\(kUserDefults_("kLoginusrid") as? Int ?? 0)", headers: self.getHeader()).responseJSON { (response: DataResponse<Any>) in
            debugPrint(response)
            KVClass.KVHideLoader()
            self.tblView.closeEndPullRefresh()
            if response.result.error == nil {
                guard let dictData = (response.result.value as? NSDictionary ?? [:])["data"] as? [NSDictionary] else {
                    KVAppDataServieces.shared.removeListData(.contact_user_list)
                    KVAppDataServieces.shared.update(.contact_all, 0)
                    KVAppDataServieces.shared.update(.contact_week, 0)
                    KVAppDataServieces.shared.update(.contact_month, 0)
                    self.viewWillAppear(false)
                    print("Ops... no car found")
                    return
                }
                self.arrData = dictData as! [[String:Any]]
                self.arrMainData = self.arrData
                
                let datadicti = NSKeyedArchiver.archivedData(withRootObject: dictData)
                
                KVAppDataServieces.shared.update(.contact_user_list, datadicti)
                
                if self.arrMainData.count > 0
                {
                    self.emptylbl.isHidden = true
                }
                else
                {
                    self.emptylbl.isHidden = false
                }
                
                if let countdatadict = (response.result.value as? NSDictionary ?? [:])["contact_count"] as? NSDictionary
                {
                    KVAppDataServieces.shared.update(.contact_all, countdatadict["contact_all"]!)
                    KVAppDataServieces.shared.update(.contact_week, countdatadict["contact_week"]!)
                    KVAppDataServieces.shared.update(.contact_month, countdatadict["contact_month"]!)
                }
                
                self.viewWillAppear(false)
            } else {
                let alertView = UIAlertController(title: "", message: response.result.error?.localizedDescription, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertView.addAction(action)
                self.present(alertView, animated: true, completion: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//================================================================================================//
//MARK :- UITableView Delegate Datasource Method
extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:contactCustomCell = tableView.dequeueReusableCell(withIdentifier: "contactCustomCell") as! contactCustomCell
       
        cell.imgView.layer.borderWidth = 3
        cell.imgView.layer.borderColor = UIColor.white.withAlphaComponent(0.16).cgColor
        cell.imgView.layer.cornerRadius = cell.imgView.frame.size.width/2
        
        let dictData:[String:Any] = arrData[indexPath.row]
        
        if let strURL = dictData["scan_user_profile_url"] as? String, strURL.count > 7  {
            cell.imgView.image = UIImage.init(named: "user_profile_pic")
            SDWebImageManager().loadImage(with: URL.init(string: dictData["scan_user_profile_url"] as! String)!, options: SDWebImageOptions.refreshCached, progress: { (p1, p2, uri) in
                
            }, completed: { (img, dta, err, typw, isfinished, uri) in
                
                cell.imgView.image = (img ?? UIImage.init(named: "user_profile_pic"))
            })
        }
        
        cell.lblTitle.text = "\((dictData["firstname"] as? String ?? "".capitalized)) \((dictData["lastname"] as? String ?? "".capitalized))"
        
        if (dictData["scan_date"] != nil) {
            let dateFormatter = DateFormatter.init()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDates = dateFormatter.date(from: (dictData["scan_date"] as! String))
            
            dateFormatter.dateFormat = "EEE. dd MMM yyyy hh:mm a"
            
            let strF_Name = (dictData["firstname"] as? String ?? "")
            let strL_Name = (dictData["lastname"] as? String ?? "")
            
            let rangestring: NSString = "\((strF_Name.capitalized)) \((strL_Name.capitalized))\nScan at :  \(dateFormatter.string(from: myDates!))" as NSString
            let rangeName = rangestring.range(of: "\((strF_Name.capitalized)) \((strL_Name.capitalized))")
            let rangeScan = rangestring.range(of: "Scan at :")
            let rangeScanTime = rangestring.range(of: "\(dateFormatter.string(from: myDates!))")
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 0
            //            style.minimumLineHeight = 6
            //            style.maximumLineHeight = 16
            
            
            
            let strAtributedTitle = NSMutableAttributedString.init(string: rangestring as String)
            strAtributedTitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Bold", size: 17.5)!, range: rangeName)
            strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkText, range: rangeName)
            
            strAtributedTitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Medium", size: 14)!, range: rangeScan)
            strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: rangeScan)
            
            strAtributedTitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Regular", size: 13)!, range: rangeScanTime)
            strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkText, range: rangeScanTime)
            
            cell.lblTitle.attributedText = strAtributedTitle
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //ContactsViewController
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        vc.scanuniquecode = arrData[indexPath.row]["scan_user_unique_code"] as? String ?? ""
        vc.scanuserid = arrData[indexPath.row]["scan_user_id"] as? Int ?? 0
        vc.isedit = false
        vc.isaddcontect = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
//================================================================================================//
//================================================================================================//
