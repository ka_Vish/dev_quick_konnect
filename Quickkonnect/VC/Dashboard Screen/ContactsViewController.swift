//
//  ContactsViewController.swift
//  Quickkonnect
//
//  Created by Dhaval Tannarana on 02/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class csv_Data: NSObject {
    var name = ""
    var phone = ""
    var email = ""
    var date = ""
    
    convenience init(_ dict: [String:Any]) {
        self.init()
        
        self.name = "\(dict["firstname"] as? String ?? "") \(dict["lastname"] as? String ?? "")"
        self.phone = dict["phone"] as? String ?? ""
        self.email = dict["email"] as? String ?? ""
        self.date = dict["scan_date"] as? String ?? ""
        
    }
}


class ContactsViewController: QKMainBaseVC {
    
    
    var str_Selection = ""
    var strTabSelection = 0
    var currentPage : Int = 1
    var arrSelectedContacts = [Int]()
    var selectedIndex:Int = 1
    var isLoadingList : Bool = false
    var arrData:[[String:Any]] = []
    var arrMainData:[[String:Any]] = []
    var arr_CSVData: [csv_Data]! = [csv_Data]()
    
    @IBOutlet var ErrorLabel: UILabel!
    @IBOutlet var lblContacts: UILabel!
    @IBOutlet var lblFollowers: UILabel!
    @IBOutlet var lblFollowing: UILabel!
    @IBOutlet var btnContacts: UIButton!
    @IBOutlet var btnFollowers: UIButton!
    @IBOutlet var btnFollowing: UIButton!
    @IBOutlet var btnExportCSV: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var constraint_top_search_bar: NSLayoutConstraint!
    var arr_FollowingList: [QKProListData]! = [QKProListData]()
    var arr_All_FollowerList: [QKProfileFollowersListData]! = [QKProfileFollowersListData]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentPage = 1
        strTabSelection = 0
        arr_CSVData.removeAll()
        lblContacts.isHidden = false
        btnContacts.setTitleColor(KVClass.themeColor(), for: .normal)
        btnFollowers.setTitleColor(UIColor.darkGray, for: .normal)
        btnFollowing.setTitleColor(UIColor.darkGray, for: .normal)
        
        self.ErrorLabel.isHidden = false
        self.Bar.Title_lbl.text = "Contacts"
        
        self.tblView.delegate = self
        self.tblView.dataSource = self
        
        self.Bar.Title_lbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.Bar.btn_Filter.isHidden = false
        self.Bar.backgroundColor = UIColor.init(hex: "28AFB0")
        self.Bar.constraint_btn_chat_trilling.constant = self.Bar.btn_Chat.frame.size.width + 18
        //self.callAPIForContactList()
        
        btnExportCSV.layer.cornerRadius = 3
        btnExportCSV.layer.borderWidth = 0.4
        btnExportCSV.layer.borderColor = UIColor.white.cgColor
        
        //=========Add Refresh Control For Pull To Refresh==============//
        self.tblView.pullTorefresh(#selector(self.callAPIForCurrentTab), self)
        //==============================================================//
        //==============================================================//
        
        self.Bar.btn_Filter.addTarget(self, action: #selector(self.filterByPress(_:)), for: .touchUpInside)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if strTabSelection == 0 {
            self.callAPIForContactList()
        }
        else if strTabSelection == 1 {
            self.callAPIForAllFollowerList()
        }
        else if strTabSelection == 2 {
            self.callAPIForContactList()
        }
        self.tblView.reloadData()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.arrData.removeAll()
        self.arrMainData.removeAll()
        
        if let valuedata = KVAppDataServieces.shared.UserValue(.contact_user_list) as? [[String:Any]] {
            self.arrData = valuedata
            self.arrMainData = self.arrData
            if self.arrMainData.count > 0 {
                self.ErrorLabel.isHidden = true
            }
            else {
                self.ErrorLabel.isHidden = false
            }
        }
        if self.arrMainData.count > 0 {
            self.ErrorLabel.isHidden = true
        }
        else {
            self.ErrorLabel.isHidden = false
        }
        
        self.tblView.reloadData()
        
    }
    
    //======================================================================================================================//
    // MARK : UIScrollView Delegate
    func loadMoreItemsForList(){
        currentPage += 1
        self.callAPIForAllFollowerList()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if strTabSelection == 1 {
            if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){
                self.isLoadingList = true
                self.loadMoreItemsForList()
            }
        }
    }
    //======================================================================================================================//
    //======================================================================================================================//
    //======================================================================================================================//
    
    func callAPIForCurrentTab() {
        if strTabSelection == 0 {
            self.callAPIForContactList()
        }
        else if strTabSelection == 1 {
            self.callAPIForAllFollowerList()
        }
        else if strTabSelection == 2 {
            self.callAPIForFollowingList()
        }
    }
    
    //=============================================================================================================//
    // Call Api for All Contacts
    func callAPIForContactList() {
        //scan_user_list
        //KVClass.KVShowLoader()
        
        Alamofire.request(self.getPrefixURL() + "scan_user_list/\(kUserDefults_("kLoginusrid") as? Int ?? 0)", headers: self.getHeader()).responseJSON { (response: DataResponse<Any>) in
            //KVClass.KVHideLoader()
            self.tblView.closeEndPullRefresh()
            if response.result.error == nil {
                print(response.result.value)
                guard let dictData = (response.result.value as? NSDictionary ?? [:])["data"] as? [NSDictionary] else {
                    print("Ops... no car found")
                    
                    KVAppDataServieces.shared.removeListData(.contact_user_list)
                    KVAppDataServieces.shared.update(.contact_all, 0)
                    KVAppDataServieces.shared.update(.contact_week, 0)
                    KVAppDataServieces.shared.update(.contact_month, 0)
                    self.viewWillAppear(false)
                    return
                }
                self.arrData = dictData as! [[String:Any]]
                self.arrMainData = self.arrData
                
                let datadicti = NSKeyedArchiver.archivedData(withRootObject: dictData)
                
                KVAppDataServieces.shared.update(.contact_user_list, datadicti)
                
                if self.arrMainData.count > 0
                {
                    self.ErrorLabel.isHidden = true
                }
                else
                {
                    self.ErrorLabel.isHidden = false
                }
                if let countdatadict = (response.result.value as? NSDictionary ?? [:])["contact_count"] as? NSDictionary
                {
                    KVAppDataServieces.shared.update(.contact_all, countdatadict["contact_all"]!)
                    KVAppDataServieces.shared.update(.contact_week, countdatadict["contact_week"]!)
                    KVAppDataServieces.shared.update(.contact_month, countdatadict["contact_month"]!)
                }
                self.viewWillAppear(false)
                
               // self.tblView.reloadData()
            } else {
                let alertView = UIAlertController(title: "", message: response.result.error?.localizedDescription, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertView.addAction(action)
                self.present(alertView, animated: true, completion: nil)
            }
        }
    }
    //=========================================================================================================//
    //=========================================================================================================//
    
    func callAPIForAllFollowerList() {
        
        let dict_param = NSMutableDictionary()
        dict_param["type"] = "follower"
        dict_param["page"] = self.currentPage
        dict_param["user_id"] = KVAppDataServieces.shared.LoginUserID()
        
        KVClass.KVServiece(methodType: "POST", processView: self.currentPage == 1 ? KVAppDataServieces.shared.getAll_FollowerList(KVAppDataServieces.shared.LoginUserID()) != nil ? nil : self.navigationController : nil, baseView: self, processLabel: "", params: dict_param, api: "profile/followers_list", arrFlag: false) { (JSON, status) in
            self.tblView.closeEndPullRefresh()
            if status == 1 {
                
                if self.currentPage == 1 {
                    self.arr_All_FollowerList.removeAll()
                }
                self.isLoadingList = true
                
                if let dataDict = JSON["data"] as? [NSDictionary] {
                    print(JSON)
                    for item in dataDict {
                        self.arr_All_FollowerList.append(QKProfileFollowersListData.init(item))
                    }
                    
                    //Set All Followers User List in Local Database
                    KVAppDataServieces.shared.setAllFollowerListFromLoginUser_ID(KVAppDataServieces.shared.LoginUserID(), JSON)
                    //================================================================================//
                    
                    if self.strTabSelection == 1 {
                        if self.arr_All_FollowerList.count > 0 {
                            self.ErrorLabel.isHidden = true
                        } else {
                            self.ErrorLabel.text = "No Followers Found!"
                            self.ErrorLabel.isHidden = false
                        }
                        self.tblView.reloadData()
                        
                        if ((self.arr_All_FollowerList.count * self.currentPage) + 20) > ((20 * self.currentPage) + 20) {
                            self.isLoadingList = false
                        }
                        else {
                            self.isLoadingList = true
                        }
                    }
                    
                }
            }
            else {
                if self.arr_All_FollowerList.count == 0 {
                    self.ErrorLabel.text = "No Followers Found!"
                    self.ErrorLabel.isHidden = false
                    self.arr_All_FollowerList.removeAll()
                }
                
            }
        }
    }
    //=========================================================================================================//
    //=========================================================================================================//
    
    func callAPIForFollowingList() {
        
        let dict_param = NSMutableDictionary()
        dict_param["user_id"] = KVAppDataServieces.shared.LoginUserID()
        
        KVClass.KVServiece(methodType: "POST", processView: KVAppDataServieces.shared.get_FollowingList(KVAppDataServieces.shared.LoginUserID()) != nil ? nil : self.navigationController, baseView: self, processLabel: "", params: dict_param, api: "profile/getAllfollowing", arrFlag: false) { (JSON, status) in
            self.tblView.closeEndPullRefresh()
            if status == 1 {
                print(JSON)
                self.arr_FollowingList.removeAll()
                
                if let dataDict = JSON["data"] as? [NSDictionary] {
                    for item in dataDict {
                        self.arr_FollowingList.append(QKProListData.init(item))
                    }
                    
                    //Set All Followers User List in Local Database
                    KVAppDataServieces.shared.set_FollowingListFromLoginUser_ID(KVAppDataServieces.shared.LoginUserID(), JSON)
                    //================================================================================//
                    
                    if self.strTabSelection == 2 {
                        if self.arr_FollowingList.count > 0 {
                            self.ErrorLabel.isHidden = true
                        } else {
                            self.ErrorLabel.text = "No Result Found!"
                            self.ErrorLabel.isHidden = false
                        }
                        self.tblView.reloadData()
                    }
                    
                }
            }
            else {
                self.ErrorLabel.text = "No Followers Found!"
                self.ErrorLabel.isHidden = false
                self.arr_FollowingList.removeAll()
            }
        }
    }
    //=========================================================================================================//
    //=========================================================================================================//
    //=========================================================================================================//
    //=========================================================================================================//
    
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK :- FILTER ACTION
    
    func filterByPress(_ sender: UIButton) {
        
        let Alert = UIAlertController.init(title: "", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let attributedTitle = NSMutableAttributedString(string: "Filter by", attributes:
            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
        
        Alert.setValue(attributedTitle, forKey: "attributedTitle")
        
        
        let action_all = UIAlertAction.init(title: "All Contact", style: .default) { (action) in
            
           
            self.arrData = (self.arrMainData as [NSDictionary]).sorted{ ($0["firstname"] as? String) ?? "" < ($1["firstname"] as? String) ?? "" } as! [[String : Any]]
            
            self.tblView.reloadData()
        }
        
        let action_history = UIAlertAction.init(title: "History", style: .default) { (action) in
            
            self.arrData = (self.arrMainData as [NSDictionary]).sorted{ ($0["scan_date"] as? String) ?? "" > ($1["scan_date"] as? String) ?? "" } as! [[String : Any]]
            
            self.tblView.reloadData()
        }
        
        let action_cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            
            self.arrData = self.arrMainData
            
            self.tblView.reloadData()
            Alert.dismiss(animated: true, completion: nil)
        }
        
        Alert.addAction(action_all)
        Alert.addAction(action_history)
        Alert.addAction(action_cancel)
        
        self.present(Alert, animated: true, completion: nil)
        
    }
    
    
    // MARK :- UIButton Action Method
    @IBAction func btnContactsTapped(_ sender: UIButton) {
        if !(strTabSelection == 0) {
            str_Selection = ""
            arr_CSVData.removeAll()
            btnExportCSV.isHidden = true
            arrSelectedContacts.removeAll()
        }
        strTabSelection = 0
        tblView.reloadData()
        lblFollowers.isHidden = true
        lblFollowing.isHidden = true
        lblContacts.isHidden = false
        btnFollowers.setTitleColor(UIColor.darkGray, for: .normal)
        btnFollowing.setTitleColor(UIColor.darkGray, for: .normal)
        btnContacts.setTitleColor(KVClass.themeColor(), for: .normal)
    }
    
    @IBAction func btnFollowersTapped(_ sender: UIButton) {
        if !(strTabSelection == 1) {
            str_Selection = ""
            arr_CSVData.removeAll()
            btnExportCSV.isHidden = true
            arrSelectedContacts.removeAll()
        }
        strTabSelection = 1
        if KVAppDataServieces.shared.getAll_FollowerList(KVAppDataServieces.shared.LoginUserID()) != nil
        {
            self.arr_All_FollowerList.removeAll()
            for item in KVAppDataServieces.shared.getAll_FollowerList(KVAppDataServieces.shared.LoginUserID())! {
                self.arr_All_FollowerList.append(QKProfileFollowersListData.init(item))
            }
            self.tblView.reloadData()
        }
        else {
            currentPage = 1
        }
        self.callAPIForAllFollowerList()

        lblFollowers.isHidden = false
        lblFollowing.isHidden = true
        lblContacts.isHidden = true
        btnContacts.setTitleColor(UIColor.darkGray, for: .normal)
        btnFollowing.setTitleColor(UIColor.darkGray, for: .normal)
        btnFollowers.setTitleColor(KVClass.themeColor(), for: .normal)
    }
    
    @IBAction func btnFollwingTapped(_ sender: UIButton) {
        strTabSelection = 2
        arr_CSVData.removeAll()
        btnExportCSV.isHidden = true
        str_Selection = "NotAuthorize"
        arrSelectedContacts.removeAll()
        
        if KVAppDataServieces.shared.get_FollowingList(KVAppDataServieces.shared.LoginUserID()) != nil
        {
            self.arr_FollowingList.removeAll()
            for item in KVAppDataServieces.shared.get_FollowingList(KVAppDataServieces.shared.LoginUserID())! {
                self.arr_FollowingList.append(QKProListData.init(item))
            }
            tblView.reloadData()
        }
        self.callAPIForFollowingList()
        lblFollowers.isHidden = true
        lblFollowing.isHidden = false
        lblContacts.isHidden = true
        btnContacts.setTitleColor(UIColor.darkGray, for: .normal)
        btnFollowers.setTitleColor(UIColor.darkGray, for: .normal)
        btnFollowing.setTitleColor(KVClass.themeColor(), for: .normal)
    }
   
    // MARK : Share CSV Data
    
    @IBAction func btnExportCSVTapped(_ sender: UIButton) {

        let CSVstring = NSMutableString()
        CSVstring.append("Index,Name,Phone,Email,Date\n\n")
            
        for i in 0..<self.arr_CSVData.count {
            CSVstring.append("\(i+1),\(self.arr_CSVData[i].name),\(self.arr_CSVData[i].phone),\(self.arr_CSVData[i].email),\(self.arr_CSVData[i].date)\n")
        }
            
        DispatchQueue.main.async() {
                
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let docdict = path[0]
            let filepath = "\(docdict)/\("Export_file.csv")"
            do {
                try CSVstring.write(toFile: filepath, atomically: true, encoding: String.Encoding.utf8.rawValue)
                let Link = NSURL(fileURLWithPath: filepath)
                    
                let objectsToShare = [Link]//[URL.init(string: filepath)!]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                    
                if UI_USER_INTERFACE_IDIOM() == .phone {
                    self.present(activityVC, animated: true) { _ in }
                }
                else {
                    // Change Rect to position Popover
                    let popup = UIPopoverController(contentViewController: activityVC)
                    popup.present(from: CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0), in: self.view, permittedArrowDirections: .any, animated: true)
                    }
                }
                catch {
                    print(error.localizedDescription)
                }
        }
    }

}


// MARK: UITableView Delegate and Datasource Method
extension ContactsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch strTabSelection {
        case 0:
            return arrData.count
            
        case 1:
            return arr_All_FollowerList.count
            
        case 2:
            return arr_FollowingList.count
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:contactCustomCell = tableView.dequeueReusableCell(withIdentifier: "contactCustomCell") as! contactCustomCell
       
        cell.imgView.layer.borderWidth = 3
        cell.imgView.layer.borderColor = UIColor.white.withAlphaComponent(0.16).cgColor
        cell.imgView.layer.cornerRadius = cell.imgView.frame.size.width/2
        
        var user_iD = 0
        var strImgURL = ""
        var strFullName = ""
        var rangeScan = NSRange()
        var rangeScan_By = NSRange()
        var rangestring: NSString = ""
        var rangeFollowedON = NSRange()
        var rangeScanBy_name = NSRange()
        var rangeScanTime_P_name = NSRange()
        
        if strTabSelection == 0 {
            
            //For Contact==============================================================================================//
            let dictData:[String:Any] = arrData[indexPath.row]
            
            strImgURL = dictData["scan_user_profile_url"] as? String ?? ""
            
            cell.lblTitle.text = "\((dictData["firstname"] as? String ?? "".capitalized)) \((dictData["lastname"] as? String ?? "".capitalized))"
            
            if (dictData["scan_date"] != nil) {
                let dateFormatter = DateFormatter.init()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let myDates = dateFormatter.date(from: (dictData["scan_date"] as! String))
                dateFormatter.dateFormat = "EEE. dd MMM yyyy hh:mm a"
                
                strFullName = "\(dictData["firstname"] as? String ?? "") \(dictData["lastname"] as? String ?? "")"
                
                let scanBy = "\(dictData["scanned_by"] as? String ?? "")"
                
                rangestring = "\(strFullName.capitalized)\nScan at :  \(dateFormatter.string(from: myDates!))\nScan by : \(scanBy.capitalized)    " as NSString
                
                rangeScan = rangestring.range(of: "Scan at :")
                rangeScan_By = rangestring.range(of: "Scan by :")
                rangeScanBy_name = rangestring.range(of: scanBy.capitalized)
                rangeScanTime_P_name = rangestring.range(of: "\(dateFormatter.string(from: myDates!))")
                
                // Get User ID
                user_iD = dictData["scan_user_id"] as? Int ?? 0
            }
        }
            
        //========================================================================================================//
        //========================================================================================================//
        else if strTabSelection == 1 {
            
            user_iD = arr_All_FollowerList[indexPath.row].user_id
            strFullName = arr_All_FollowerList[indexPath.row].name
            strImgURL = arr_All_FollowerList[indexPath.row].profile_pic
            let strProfileName = arr_All_FollowerList[indexPath.row].profile_name
            
            rangestring = "\((strFullName.capitalized))\n\(strProfileName)    " as NSString
            
            rangeScanTime_P_name = rangestring.range(of: "\(strProfileName)")
        }
        
        //========================================================================================================//
        //========================================================================================================//
        else if strTabSelection == 2 {
            
            strImgURL = arr_FollowingList[indexPath.row].logo
            strFullName = arr_FollowingList[indexPath.row].name
            
            //Change Date Format
            let dateFormatter = DateFormatter.init()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDates = dateFormatter.date(from: arr_FollowingList[indexPath.row].followed_on)
            dateFormatter.dateFormat = "EEE. dd MMM yyyy hh:mm a"
            
            if arr_FollowingList[indexPath.row].followed_at == "" {
                rangestring = "\(strFullName.capitalized)\nFollowed on :  \(dateFormatter.string(from: myDates!))    " as NSString
            }
            else {
                rangestring = "\(strFullName.capitalized)\nFollowed on :  \(dateFormatter.string(from: myDates!))\nFollowed at : \(arr_FollowingList[indexPath.row].followed_at.capitalized)    " as NSString
            }
            
            rangeScan = rangestring.range(of: "Followed on :")
            rangeScan_By = rangestring.range(of: "Followed at :")
            rangeFollowedON = rangestring.range(of: "\(dateFormatter.string(from: myDates!))")
            rangeScanBy_name = rangestring.range(of: arr_FollowingList[indexPath.row].followed_at.capitalized)
        }
        
        let rangeName = rangestring.range(of: "\(strFullName.capitalized)")
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = strTabSelection == 0 ? 0 : 5
            
        let strAtributedTitle = NSMutableAttributedString.init(string: rangestring as String)
        
        strAtributedTitle.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSRange.init(location: 0, length: strAtributedTitle.length))
        
        strAtributedTitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Bold", size: 16)!, range: rangeName)
        strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkText, range: rangeName)
            
        strAtributedTitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Medium", size: 13)!, range: rangeScan)
        strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: rangeScan)
        
        strAtributedTitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Medium", size: 13)!, range: rangeScan_By)
        strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: rangeScan_By)
            
        strAtributedTitle.addAttribute(NSFontAttributeName, value: strTabSelection == 0 ? UIFont.init(name: "lato-Regular", size: 13)!: UIFont.init(name: "lato-Medium", size: 13)!, range: rangeScanTime_P_name)
        strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: strTabSelection == 1 ? KVClass.themeColor() : UIColor.lightGray, range: rangeScanTime_P_name)
        
        strAtributedTitle.addAttribute(NSFontAttributeName, value: strTabSelection == 2 ? UIFont.init(name: "lato-Regular", size: 12)! : UIFont.init(name: "lato-Regular", size: 13)!, range: rangeScanBy_name)
        strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGray , range: rangeScanBy_name)
        
        strAtributedTitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Regular", size: 12)!, range: rangeFollowedON)
        strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGray, range: rangeFollowedON)
        
        
        cell.lblTitle.attributedText = strAtributedTitle
        
        
        if strImgURL != "" {
            cell.imgView.setImageWith(URLRequest.init(url: URL.init(string: strImgURL)!), placeholderImage: strTabSelection == 2 ? #imageLiteral(resourceName: "logo") : #imageLiteral(resourceName: "user_profile_pic"), success: { (request, response, image) in
                cell.imgView.image = image
            }, failure: { (request, response, error) in
                cell.imgView.image = self.strTabSelection == 2 ? #imageLiteral(resourceName: "logo") : #imageLiteral(resourceName: "user_profile_pic")
            })
        }
        else {
            cell.imgView.image = strTabSelection == 2 ? #imageLiteral(resourceName: "logo") : #imageLiteral(resourceName: "user_profile_pic")
        }
        
        
            
        if str_Selection == "ContactSelect" {
            if arrSelectedContacts.contains(user_iD) {
                cell.viewBG.backgroundColor = #colorLiteral(red: 0.1568627451, green: 0.6862745098, blue: 0.6901960784, alpha: 0.1)
                cell.imgSelect_UnSelect.image = #imageLiteral(resourceName: "radioselected")
                cell.imgSelect_UnSelect.backgroundColor = UIColor.white
            }
            else {
                cell.imgSelect_UnSelect.image = nil
                cell.viewBG.backgroundColor = UIColor.white
                cell.imgSelect_UnSelect.backgroundColor = UIColor.clear
            }
            cell.imgSelect_UnSelect.isHidden = false
        }
        else {
            cell.imgSelect_UnSelect.isHidden = true
            cell.viewBG.backgroundColor = UIColor.white
        }
            
            
        // LongPress Gesture
        cell.tag = indexPath.row
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress(_:)))
        longGesture.minimumPressDuration = 1
        cell.addGestureRecognizer(longGesture)

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch strTabSelection {
        case 0:
            
            if str_Selection == "ContactSelect" {
                //==============================//
                self.addAndRemoveDataForCSVFile(selectIndex: indexPath.row)
            }
            else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                vc.scanuniquecode = arrData[indexPath.row]["scan_user_unique_code"] as? String ?? ""
                vc.scanuserid = arrData[indexPath.row]["scan_user_id"] as? Int ?? 0
                vc.isedit = false
                vc.isaddcontect = false
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        case 1:
            
            if str_Selection == "ContactSelect" {
                //==============================//
                self.addAndRemoveDataForCSVFile(selectIndex: indexPath.row)
            }
            else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                vc.scanuniquecode = arr_All_FollowerList[indexPath.row].unique_code
                vc.scanuserid = arr_All_FollowerList[indexPath.row].user_id
                vc.isedit = false
                vc.isaddcontect = false
                vc.ScreenFrom = "FollowersList"
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        case 2:
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
            if arr_FollowingList[indexPath.row].role == 0 || arr_FollowingList[indexPath.row].role == 1 {
                obj.int_userID = -1
                obj.strBlockProfileID = arr_FollowingList[indexPath.row].id
                obj.str_uniquecode = arr_FollowingList[indexPath.row].unique_code
                obj.str_Screen_From = arr_FollowingList[indexPath.row].role == 1 ? "OnlyViewProfile" : ""
            }
            else {
                if arr_FollowingList[indexPath.row].role == 2 {
                    obj.str_Screen_From = "View/EditProfile"
                }
                app_Delegate.strProfileScreenFrom = "ViewProfile"
                obj.int_userID = arr_FollowingList[indexPath.row].id
            }
            self.navigationController?.pushViewController(obj, animated: true)
            
            
        default:
            break
        }
        
    
        
        
    }
    
    
    func longPress(_ sender: UILongPressGestureRecognizer) {
        print(sender.view?.tag ?? 0)
        if str_Selection == "" {
            str_Selection = "ContactSelect"
            let point: CGPoint = sender.location(in: self.tblView)
            let indexPath = self.tblView.indexPathForRow(at: point)!
            self.addAndRemoveDataForCSVFile(selectIndex: indexPath.row)
        }
        
    }
    
    func addAndRemoveDataForCSVFile(selectIndex: Int) {
        
        var user_iD = 0
        var dictData = [String:Any]()
        
        
        if strTabSelection == 0 {
            dictData = arrData[selectIndex]
            user_iD = dictData["scan_user_id"] as? Int ?? 0
        }
        else if strTabSelection == 1 {
            dictData = arr_All_FollowerList[selectIndex].selectionData
            user_iD = arr_All_FollowerList[selectIndex].user_id
        }
        
        if arrSelectedContacts.contains(user_iD) {
            if let index = arrSelectedContacts.index(of: user_iD) {
                arr_CSVData.remove(at: index)
                arrSelectedContacts.remove(at: index)
            }
        }
        else {
            arrSelectedContacts.append(user_iD)
            arr_CSVData.append(csv_Data.init(dictData))
        }
        
        if arrSelectedContacts.count == 0 {
            str_Selection = ""
            arr_CSVData.removeAll()
            btnExportCSV.isHidden = true
        }
        else {
            btnExportCSV.isHidden = false
        }
        self.tblView.reloadData()
    }

}

extension ContactsViewController:UISearchBarDelegate {
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        if searchText == "" {
            arrData = arrMainData
        } else {
            //  arrData = filter(array: arrMainData, byString: searchText)
            print(arrData)
            arrData = filter(array: arrMainData, byString: searchText)
        }
        
        tblView.reloadData()
    }
    
    func filter(array : [[String : Any]], byString filterString : String) -> [[String : Any]] {
        return array.filter{
            dictionary in
            return "\((dictionary["firstname"] as? String ?? "")) \((dictionary["lastname"] as? String ?? ""))".lowercased().range(of: filterString.lowercased()) != nil }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        arrData = arrMainData
        //  tblData.reloadData()
    }
}
//========================================================================================================//
//========================================================================================================//


// MARK : Custom Contact Cell
class contactCustomCell:UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var imgSelect_UnSelect: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imgView.image = UIImage.init(named: "user_profile_pic")
    }
}

