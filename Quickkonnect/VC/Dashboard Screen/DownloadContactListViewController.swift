//
//  DownloadContactListViewController.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 06/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import Alamofire

class DownloadContactListViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    var arrData:[[String:Any]] = []
    var dictSelectedData: [String:Any] = [:]
    var arrSelectedData:[String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(dictSelectedData)
        
        self.callAPIToGetNotificationData()
        
        self.tblView.pullTorefresh(#selector(callAPIToGetNotificationData), self)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func callAPIToGetNotificationData() {
        //http://54.70.2.200/quickkonnect/api/notification_list/234
        
       KVClass.KVShowLoader()
        
        Alamofire.request(self.getPrefixURL() + "user_contact_list/\(KVAppDataServieces.shared.LoginUserID()))", headers: self.getHeader()).responseJSON { (response: DataResponse<Any>) in
            debugPrint(response)
            KVClass.KVHideLoader()
            self.tblView.closeEndPullRefresh()
            if response.result.error == nil {
                guard let dictData = (response.result.value as! [String:Any])["data"] else {
                    print("Ops... no car found")
                    return
                }
                
                self.arrData = dictData as! [[String:Any]]
                
                if self.arrData.count > 0 {
                    self.tblView.reloadData()
                } else {
                  //  self.lblNoDataAvailable.isHidden = true
                }
            } else {
                let alertView = UIAlertController(title: "", message: response.result.error?.localizedDescription, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertView.addAction(action)
                self.present(alertView, animated: true, completion: nil)
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func btnSelectDeselectContactClicked(_ sender: UIButton) {
        
        if arrSelectedData.contains("\(sender.tag)") {
            arrSelectedData.remove(at: arrSelectedData.index(of: "\(sender.tag)")!)
            sender.setImage(UIImage.init(named: "radio_"), for: .normal)
        } else {
            arrSelectedData.append("\(sender.tag)")
            sender.setImage(UIImage.init(named: "radioselected"), for: .normal)
        }
        self.tblView.reloadData()
    }
    
    func btnApproveRejectClicked(_ sender: UIButton) {
        var SenderId = 0  //dictSelectedData["sender_id"] as? Int ?? 0
        var Noti_id = 0  //dictSelectedData["notification_id"] as? Int ?? 0
        
        if dictSelectedData["sender_id"] as? Int ?? 0 != 0
        {
            SenderId = dictSelectedData["sender_id"] as? Int ?? 0
        }
        else if dictSelectedData["sender_id"] as? String ?? "" != ""
        {
            SenderId = Int(dictSelectedData["sender_id"] as? String ?? "")!
        }
        
        if dictSelectedData["notification_id"] as? Int ?? 0 != 0
        {
            Noti_id = dictSelectedData["notification_id"] as? Int ?? 0
        }
        else if dictSelectedData["notification_id"] as? String ?? "" != ""
        {
            Noti_id = Int(dictSelectedData["notification_id"] as? String ?? "")!
        }
        
        
        if sender.tag == 121 {
            //arrSelectedData
            
            if arrSelectedData.count == 0
            {
                KVClass.ShowNotification("", "Select Contacts First !")
                return
            }
            
            let senddict = NSMutableDictionary()
            senddict["sender_id"] = SenderId
            senddict["user_id"] = kUserDefults_("kLoginusrid") as? Int ?? 0
            senddict["notification_id"] = Noti_id
           let contactarr = NSMutableArray()
            for item in arrSelectedData
            {
                 let contactdict = NSMutableDictionary()
                contactdict["contact_id"] = item
                contactarr.add(contactdict)
            }
            senddict["contact_approve"] = contactarr
            
            KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: senddict, api: "user_approved_contact", arrFlag: false, Hendler: { (JSON, status) in
                
                print(JSON)
                
                KVClass.ShowNotification("", JSON["msg"] as? String ?? "")
                
                kUserDefults(true as AnyObject, key: "isnotificationrefresh")
                
                self.navigationController?.popViewController(animated: true)
            })
        } else {
            
            let senddict = NSMutableDictionary()
            senddict["sender_id"] = SenderId
            senddict["user_id"] = kUserDefults_("kLoginusrid") as? Int ?? 0
            senddict["notification_id"] = Noti_id
            
            KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: senddict, api: "reject_contact", arrFlag: false, Hendler: { (JSON, status) in
                
                print(JSON)
                
                KVClass.ShowNotification("", JSON["msg"] as? String ?? "")
                kUserDefults(true as AnyObject, key: "isnotificationrefresh")
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension DownloadContactListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrData.count > 0 {
            return arrData.count + 1
        } else {
            return 0
        }
    }
    
    //80 50
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrData.count == indexPath.row {
            return 50.0
        } else {
            return UITableViewAutomaticDimension//80.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arrData.count == indexPath.row {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ApproveRejectButtonsCell") as! ApproveRejectButtonsCell
            cell.selectionStyle = .none
            cell.btnApproveContact.tag = 121
            cell.btnApproveContact.addTarget(self, action: #selector(self.btnApproveRejectClicked(_:)), for: .touchUpInside)
            cell.btnRejectContact.addTarget(self, action: #selector(self.btnApproveRejectClicked(_:)), for: .touchUpInside)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DownloadContactListCell") as! DownloadContactListCell
            cell.selectionStyle = .none
            let dictData = arrData[indexPath.row]
            
            cell.lblContactTypeValue.text = dictData["contact_type"] as? String ?? ""
            cell.lblEmailTypeValue.text = dictData["email"] as? String ?? ""
            cell.lblPhoneTypeValue.text = dictData["phone"] as? String ?? ""
            
            cell.btnSelectDeselect.tag = dictData["contact_id"] as? Int ?? 0
            
            if arrSelectedData.contains("\(cell.btnSelectDeselect.tag)") {
                cell.btnSelectDeselect.setImage(UIImage.init(named: "radioselected"), for: .normal)
            } else {
                cell.btnSelectDeselect.setImage(UIImage.init(named: "radio_"), for: .normal)
            }
            
            cell.btnSelectDeselect.addTarget(self, action: #selector(self.btnSelectDeselectContactClicked(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    
    
}


class DownloadContactListCell: UITableViewCell  {
    
    @IBOutlet weak var btnSelectDeselect: UIButton!
    @IBOutlet weak var lblContactTypeHeader: UILabel!
    @IBOutlet weak var lblEmailTypeHeader: UILabel!
    @IBOutlet weak var lblPhoneTypeHeader: UILabel!
    
    @IBOutlet weak var lblContactTypeValue: UILabel!
    @IBOutlet weak var lblEmailTypeValue: UILabel!
    
    @IBOutlet weak var lblPhoneTypeValue: UILabel!
}

class ApproveRejectButtonsCell: UITableViewCell {
    @IBOutlet weak var btnRejectContact: UIButton!
    
    @IBOutlet weak var btnApproveContact: UIButton!
    
}
