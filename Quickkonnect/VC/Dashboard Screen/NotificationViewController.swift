//
//  NotificationViewController.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 06/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import Alamofire
import Material
import SDWebImage

enum NotiType: String {
    case one = "User Scan"
    case two = "Download Contact Request"
    case three = "Request Approved"
}

class NotificationViewController: QKMainBaseVC {

    @IBOutlet weak var lblNoDataAvailable: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet var clear_btn: UIButton!
    @IBOutlet var viewBlurBGForPopUp: UIView!
    var strScreenFrom = ""
    var arrData:[[String:Any]] = []
    var currentPage : Int = 0
    var isLoadingList : Bool = false
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBlurBGForPopUp.isHidden = true
        self.view.bringSubview(toFront: viewBlurBGForPopUp)
        self.tblView.estimatedRowHeight = self.view.bounds.height
        self.tblView.estimatedSectionFooterHeight = 1
        self.clear_btn.setImage(UIImage.init(named: "broom")?.tint(with: UIColor.white), for: .normal)
        
        self.Bar.btn_Chat.isHidden = true
        self.Bar.Title_lbl.text = "Notification"
        self.Bar.Back_btn.isHidden = false
        self.Bar.Menu_btn.isHidden = true
        self.Bar.Noti_btn.isHidden = true
        
        self.callAPIToGetNotificationData(self.currentPage)
        self.lblNoDataAvailable.isHidden = true
        
        self.lblNoDataAvailable.font = UIFont.init(name: "lato-Medium", size: 18)
        self.lblNoDataAvailable.textAlignment = .center
        self.lblNoDataAvailable.textColor = UIColor.lightGray
        
        self.Bar.backgroundColor = UIColor.init(hex: "28AFB0")
        self.Bar.btn_Filter.isHidden = true
        self.Bar.Back_btn.isHidden = true
        self.Bar.btn_Chat.isHidden = false
        //self.Bar.img_TopBG.isHidden = false
        self.Bar.Title_lbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.Bar.constraint_btn_chat_trilling.constant = 12
        
        if strScreenFrom == "Push" {
            self.Bar.btn_Chat.isHidden = true
            self.Bar.Back_btn.isHidden = false
            //self.Bar.img_TopBG.isHidden = true
            self.Bar.constraint_img_profile_trilling.constant = 12
        }
        
        //=========Add Refresh Control For Pull To Refresh==============//
        self.tblView.pullTorefresh(#selector(self.APICall), self)
        //==============================================================//
        //==============================================================//
    }
    
    func APICall() {
        self.arrData.removeAll()
        self.tblView.reloadData()
        self.currentPage = 0
        self.callAPIToGetNotificationData(self.currentPage)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("DISPLAYBLURVIEW"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tblView.reloadData()
        if kUserDefults_("isnotificationrefresh") as? Bool ?? false == true
        {
            self.currentPage = 0
            self.callAPIToGetNotificationData(self.currentPage)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationForDisplayBlurView(notification:)), name: Notification.Name("DISPLAYBLURVIEW"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedReloadNotification(notification:)), name: Notification.Name("RELOADNOTIFICATION"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tblView.reloadData()
        if kUserDefults_("isnotificationrefresh") as? Bool ?? false == true
        {
            self.currentPage = 0
            self.callAPIToGetNotificationData(self.currentPage)
        }
    }
    
    //MARK :- Received Notification For Display Blur View
    func methodOfReceivedNotificationForDisplayBlurView(notification: NSNotification){
        //Take Action on Notification
        if app_Delegate.isDisplayBlur == true {
            viewBlurBGForPopUp.isHidden = false
        }
        else {
            viewBlurBGForPopUp.isHidden = true
            self.viewDidAppear(true)
        }
    }
    //================================================================================//
    
    //MARK :- Received Notification For Display Blur View
    func methodOfReceivedReloadNotification(notification: NSNotification){
        //Take Action on Notification
        self.currentPage = 0
        self.callAPIToGetNotificationData(self.currentPage)
    }
    //================================================================================//
    //================================================================================//
    
    @IBAction func clearnotiPress(_ sender: UIButton)
    {
        let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let clear_all = UIAlertAction.init(title: "Clear All", style: .destructive) { (action) in
            
        KVClass.KVServiece(methodType: "GET", processView: self.navigationController, baseView: self, processLabel: "", params: nil, api: "clear_notifications/\(kUserDefults_("kLoginusrid") as? Int ?? 0)", arrFlag: false) { (JSON, status) in
            
            if status == 1
            {
                self.arrData.removeAll()
                self.tblView.reloadData()
                
                KVClass.ShowNotification("", JSON["msg"] as? String ?? "")
                
                if self.arrData.count > 0
                {
                    self.lblNoDataAvailable.isHidden = true
                }
                else
                {
                    self.lblNoDataAvailable.isHidden = false
                }
            }
            
        }
        }
        
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(clear_all)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    func callAPIToGetNotificationData(_ page: Int) {
        //http://54.70.2.200/quickkonnect/api/notification_list/234
        if currentPage == 0
        {
        KVClass.KVShowLoader()
        }
        Alamofire.request(self.getPrefixURL() + "notification_list/\(kUserDefults_("kLoginusrid") as? Int ?? 0)/\(page)?timezone=\(KVClass.KVTimeZone())", headers: self.getHeader()).responseJSON { (response: DataResponse<Any>) in
            debugPrint(response)

            if self.currentPage == 0
            {
                self.arrData.removeAll()
                KVClass.KVHideLoader()
            }
            kUserDefults(false as AnyObject, key: "isnotificationrefresh")
            self.tblView.closeEndPullRefresh()
            if response.result.error == nil {
                
                guard let dictData = (response.result.value as! [String:Any])["data"] as? [[String:Any]]  else {
                    self.isLoadingList = true
                    print("Ops... no car found")
                               // self.arrData.removeAll()
                                self.tblView.reloadData()
                                if self.arrData.count > 0 {
                                    self.lblNoDataAvailable.isHidden = true
                                } else {
                                    self.lblNoDataAvailable.isHidden = false
                                }
                    return
                }
                
                for item in dictData
                {
                   self.arrData.append(item)
                }
                
                if self.arrData.count > 0 {
                    self.tblView.reloadData()
                    self.lblNoDataAvailable.isHidden = true
                } else {
                    self.tblView.reloadData()
                    self.lblNoDataAvailable.isHidden = false
                }
                
                if ((self.arrData.count * self.currentPage) + 20) > ((20 * self.currentPage) + 20)
                {
                  self.isLoadingList = false
                }
                else
                {
                    self.isLoadingList = true
                }
                
            } else {
                 self.lblNoDataAvailable.isHidden = false
                let alertView = UIAlertController(title: "", message: response.result.error?.localizedDescription, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertView.addAction(action)
                self.present(alertView, animated: true, completion: nil)
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadMoreItemsForList(){
        currentPage += 1
        callAPIToGetNotificationData(currentPage)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){
            self.isLoadingList = true
            self.loadMoreItemsForList()
        }
    }
    
    
    
    

}

//============================================================================================================//
//MARK : - UITableView Delegate and DataSource Method
extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
      return 1
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        cell.selectionStyle = .none
        let dictData:[String:Any] = arrData[indexPath.row]
        
        cell.lblDate.text = ""
    
        
        if (dictData["created_time"] != nil) {
            let dateFormatter = DateFormatter.init()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDates = dateFormatter.date(from: (dictData["created_time"] as! String))
            
            dateFormatter.dateFormat = "EEE. dd MMM yyyy h:mm a"
            //cell.lblDate.text = dateFormatter.string(from: myDates!)
            
            if dictData["message"] as? String ?? "" != ""
            {
                
                print(dictData["notification_type"] as? Int ?? 0)
                
                
                var strprofilemsg =  ""
                
                let strNotimsg = "\(dictData["message"] as? String ?? "")    \n\(dateFormatter.string(from: myDates!))   "
                
                if dictData["notification_type"] as? Int ?? 0 == 5 {
                    
                    strprofilemsg = "Your profile \(dictData["profile_name"] as? String ?? "") is Approve.   \n\(dateFormatter.string(from: myDates!))       "
                    
                }else if dictData["notification_type"] as? Int ?? 0 == 6 {
                    strprofilemsg = "Your profile \(dictData["profile_name"] as? String ?? "") is rejected.   \n\(dateFormatter.string(from: myDates!))       "
                }
                
                //let rangestring: NSString = ""
                
                
                
                
                let rangestring: NSString = (dictData["notification_type"] as? Int ?? 0 < 5) || (dictData["notification_type"] as? Int ?? 0 > 6) ? (strNotimsg) as NSString : strprofilemsg as NSString
                
                
                
                let range = (dictData["notification_type"] as? Int ?? 0 < 5) || (dictData["notification_type"] as? Int ?? 0 > 6) ? rangestring.range(of: dictData["username"] as? String ?? "") : rangestring.range(of: dictData["profile_name"] as? String ?? "")
                
                let messagerange = (dictData["notification_type"] as? Int ?? 0 < 5) || (dictData["notification_type"] as? Int ?? 0 > 6) ? rangestring.range(of: dictData["message"] as? String ?? "") : rangestring.range(of: strprofilemsg)
                
                let timerange = rangestring.range(of: "\(dateFormatter.string(from: myDates!))")
                
                let style = NSMutableParagraphStyle()
                style.lineSpacing = 1
               // style.minimumLineHeight = 6
               // style.maximumLineHeight = 16
                let attributedstring = (dictData["notification_type"] as? Int ?? 0 < 5) || (dictData["notification_type"] as? Int ?? 0 > 6) ? NSMutableAttributedString.init(string: strNotimsg) :  NSMutableAttributedString.init(string: strprofilemsg)
                
                attributedstring.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Regular", size: 16)!, range: messagerange)
                attributedstring.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: messagerange)
                
                attributedstring.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Bold", size: 17)!, range: range)
                attributedstring.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: range)
                
                attributedstring.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Regular", size: 13)!, range: timerange)
                attributedstring.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: timerange)
                
                //attributedstring.addAttribute(NSParagraphStyleAttributeName, value: style, range: messagerange)
                
                
                cell.lblName.attributedText = attributedstring// = dictData["message"] as? String ?? ""//attributedstring
                
            }
            
            
            
        }
        else
        {
            cell.lblName.text = dictData["username"] as? String ?? ""
        }
        
        if let picURI = dictData["profile_pic"] as? String, picURI.count > 0
        {
            SDWebImageManager().loadImage(with: URL.init(string: picURI)!, options: SDWebImageOptions.refreshCached, progress: { (p1, p2, url) in
                
            }, completed: { (imag, dta, err, type, isfinish, uri) in
                
                cell.img_Profile.image = (imag ?? #imageLiteral(resourceName: "user_profile_pic"))
            })
        }
        
        
        
//       if dictData["message"] as? String ?? "" != ""
//       {
//
//        print(dictData["notification_type"] as? Int ?? 0)
//
//
//        var strprofilemsg =  ""
//
//        if dictData["notification_type"] as? Int ?? 0 == 5{
//
//            strprofilemsg = "Your profile \(dictData["profile_name"] as? String ?? "") is Approve"
//
//        }else if dictData["notification_type"] as? Int ?? 0 == 6{
//            strprofilemsg = "Your profile \(dictData["profile_name"] as? String ?? "") is rejected"
//        }
//
//
//        let rangestring: NSString =  dictData["notification_type"] as? Int ?? 0 < 5 ? (dictData["message"] as? String ?? "") as NSString : strprofilemsg as NSString
//        let range = dictData["notification_type"] as? Int ?? 0 < 5 ? rangestring.range(of: dictData["username"] as? String ?? "") : rangestring.range(of: dictData["profile_name"] as? String ?? "")
//        let messagerange = dictData["notification_type"] as? Int ?? 0 < 5 ? rangestring.range(of: dictData["message"] as? String ?? "") : rangestring.range(of: strprofilemsg)
//        let style = NSMutableParagraphStyle()
//        style.lineSpacing = 1
//        style.minimumLineHeight = 6
//        style.maximumLineHeight = 16
//        let attributedstring = dictData["notification_type"] as? Int ?? 0 < 5 ? NSMutableAttributedString.init(string: "\(dictData["message"] as? String ?? "")     ") :  NSMutableAttributedString.init(string: "\(strprofilemsg)     ")
//
//
////        var rangestring: NSString = (dictData["message"] as? String ?? "") as NSString
////        var messagerange = rangestring.range(of: dictData["message"] as? String ?? "")
////        var range = rangestring.range(of: dictData["username"] as? String ?? "")
////
////        let notiType = dictData["notification_type"] as? Int ?? 0
////        if notiType == 5 {
////            var strProfileName = dictData["profile_name"] as? String ?? ""
////            strProfileName = "Your profile" + strProfileName + "is approved"
////            rangestring = "Your profile \(dictData["profile_name"] as? String ?? "") is approved" as NSString
////            messagerange = rangestring.range(of: strProfileName)
////            range = rangestring.range(of: dictData["profile_name"] as? String ?? "")
////        }
////        else if notiType == 6 {
////            var strProfileName = dictData["profile_name"] as? String ?? ""
////            strProfileName = "Your profile" + strProfileName + "is rejected"
////            rangestring = "Your profile \(dictData["profile_name"] as? String ?? "") is rejected" as NSString
////            messagerange = rangestring.range(of: strProfileName)
////            range = rangestring.range(of: dictData["profile_name"] as? String ?? "")
////        }
////
////
////        let style = NSMutableParagraphStyle()
////        style.lineSpacing = 1
////        style.minimumLineHeight = 6
////        style.maximumLineHeight = 16
////        let attributedstring = NSMutableAttributedString.init(string: "\(messagerange)     ")
//
//        attributedstring.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Regular", size: 16)!, range: messagerange)
//        attributedstring.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: messagerange)
//
//        attributedstring.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Bold", size: 17)!, range: range)
//        attributedstring.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: range)
//        attributedstring.addAttribute(NSParagraphStyleAttributeName, value: style, range: range)
//
//
//        cell.lblName.attributedText = attributedstring// = dictData["message"] as? String ?? ""//attributedstring
//
//        }
//        else
//       {
//        cell.lblName.text = dictData["username"] as? String ?? ""
//        }

        
        let view = UIView.init(frame: cell.bounds)
        view.backgroundColor = UIColor.blue
        
       // cell.editingAccessoryView. = view
       
        switch dictData["notification_type"] as? Int ?? 0 {
        case 1:
            cell.img_StatusImg.image = #imageLiteral(resourceName: "noti_match")
            cell.img_more.isHidden = true
        case 2:
            cell.img_StatusImg.image = #imageLiteral(resourceName: "notistatus_downloadcontact")
            cell.img_more.isHidden = false
        case 3:
            cell.img_StatusImg.image = #imageLiteral(resourceName: "noti_contactaprove")
            cell.img_more.isHidden = true
        case 5:
            cell.img_StatusImg.image = #imageLiteral(resourceName: "noti_contactaprove")
            cell.img_more.isHidden = true
        case 6:
            cell.img_StatusImg.image = #imageLiteral(resourceName: "noti_rejected")
            cell.img_more.isHidden = true
        case 7:
            cell.img_StatusImg.image = #imageLiteral(resourceName: "notistatus_downloadcontact")
            cell.img_more.isHidden = true
        case 8:
            cell.img_StatusImg.image = #imageLiteral(resourceName: "noti_match")
            cell.img_more.isHidden = true
        case 9:
            cell.img_StatusImg.image = #imageLiteral(resourceName: "noti_contactaprove")
            cell.img_more.isHidden = true
        case 10:
            cell.img_StatusImg.image = #imageLiteral(resourceName: "noti_rejected")
            cell.img_more.isHidden = true
        case 11:
            cell.img_StatusImg.image = #imageLiteral(resourceName: "noti_match")
            cell.img_more.isHidden = true
            
        default:
            cell.img_StatusImg.image = nil
            cell.img_more.isHidden = true
            break
        }
        
        
        
       
        if let isRead = dictData["is_read"] as? Int {
            if isRead == 1 {
               cell.backgroundColor = UIColor.white
            } else {
                cell.backgroundColor = UIColor.init(hex: "e8f9ff")
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
    
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        
        let deleteAction  = UITableViewRowAction(style: .destructive, title: "Delete") { (rowAction, index) in
           
            let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let clear_all = UIAlertAction.init(title: "Delete", style: .destructive) { (action) in
            
            let dict = NSMutableDictionary()
            dict[""] = ""
            
            KVClass.KVServiece(methodType: "GET", processView: nil, baseView: self, processLabel: "", params: nil, api: "delete_notification/\(self.arrData[index.row]["notification_id"] as? Int ?? 0)", arrFlag: false, Hendler: { (JSON, status) in
                
                
                if status == 1
                {
                self.arrData.remove(at: index.row)
                self.tblView.beginUpdates()
                self.tblView.deleteRows(at: [index], with: UITableViewRowAnimation.fade)
                self.tblView.endUpdates()
                
                if self.arrData.count > 0
                {
                    self.lblNoDataAvailable.isHidden = true
                }
                else
                {
                    self.lblNoDataAvailable.isHidden = false
                }
                }
                //self.callAPIToGetNotificationData()
            })
           
            }
            
            let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
                alert.dismiss(animated: true, completion: nil)
            }
            alert.addAction(clear_all)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
        
       // deleteAction.la
        
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
       
        self.clear_btn.isHidden = true
        
    }
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        self.clear_btn.isHidden = false
    }

    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if section == tableView.numberOfSections - 1{
            return 20
        }
        
        return 0
    }
 
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.sizeToFit()
        view.backgroundColor = .clear
        return view
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dictData:[String:Any] = arrData[indexPath.row]
        
        print(dictData)
        self.readnotification(dictData, indexPath.row)
        if dictData["notification_type"] as? Int ?? 0 != 1
        {
            if dictData["notification_type"] as? Int ?? 0 > 4 && dictData["notification_type"] as? Int ?? 0 < 11 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
                obj.int_userID = dictData["profile_id"] as? String ?? "" != "" ? Int(dictData["profile_id"] as? String ?? "")! : dictData["profile_id"] as? Int ?? 0
                
                if (dictData["notification_type"] as? Int ?? 0 == 8) {
                    obj.str_Screen_From = "AcceptReject"
                    if (dictData["status"] as? String ?? "" == "Reject") || (dictData["status"] as? String ?? "" == "Approve") {
                        obj.str_Screen_From = ""
                    }
                    obj.strScreenFrom = "AppDelegate"
                    obj.str_uniquecode = dictData["unique_code"] as? String ?? ""
                    obj.Notification_ID = dictData["notification_id"] as? Int ?? 0
                }
                if (dictData["notification_type"] as? Int ?? 0 == 9) {
                    obj.str_Screen_From = ""
                    obj.strScreenFrom = "AppDelegate"
                    obj.str_uniquecode = dictData["unique_code"] as? String ?? ""
                    obj.Notification_ID = dictData["notification_id"] as? Int ?? 0
                }
                else if (dictData["notification_type"] as? Int ?? 0 == 10) {
                    obj.str_Screen_From = ""
                    obj.strScreenFrom = "AppDelegate"
                    obj.str_uniquecode = dictData["unique_code"] as? String ?? ""
                    obj.Notification_ID = dictData["notification_id"] as? Int ?? 0
                }
                
                self.navigationController?.pushViewController(obj, animated: true)
                return
            }
            else if dictData["notification_type"] as? Int ?? 0 > 10 {
                
                if dictData["notification_type"] as? Int ?? 0  == 14 {
                    
                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
                    if dictData["role"] as? Int ?? 0 == 1 {
                        obj.int_userID = -1
                        obj.str_Screen_From = "OnlyViewProfile"
                        obj.strBlockProfileID = dictData["profile_id"] as? Int ?? 0
                        obj.str_uniquecode = dictData["unique_code"] as? String ?? ""
                    }
                    else {
                        if dictData["role"] as? Int ?? 0 == 2 {
                            obj.str_Screen_From = "View/EditProfile"
                        }
                        obj.int_userID = dictData["profile_id"] as? Int ?? -1
                        app_Delegate.strProfileScreenFrom = "ViewProfile"
                    }
                    self.navigationController?.pushViewController(obj, animated: true)
                    return

                }
                else {
                
                    let objEmp = self.storyboard?.instantiateViewController(withIdentifier: "AddMemberInProfileVC") as! AddMemberInProfileVC
                    objEmp.strScreenFrom = "Notification"
                    objEmp.NotificationID = dictData["notification_id"] as? Int ?? 0
                    self.navigationController?.pushViewController(objEmp, animated: true)
                    return
                    
                }
                
            }
            
        if let isRead = dictData["status"] as? String {
            if isRead != "Approve" {
                //DownloadContactListViewController
                
                DispatchQueue.main.async {
                    self.aproveRejectNotipress(dictData, indexPath)
                }
            }
            else if isRead == "Approve"
            {
                
        
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                vc.scanuniquecode = dictData["unique_code"] as? String ?? ""
                vc.scanuserid = dictData["sender_id"] as? Int ?? 0
                vc.isaddcontect = false
                vc.Scan_user_update = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            vc.scanuniquecode = dictData["unique_code"] as? String ?? ""
            vc.scanuserid = dictData["sender_id"] as? Int ?? 0
            vc.isaddcontect = true
            vc.Scan_user_update = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func readnotification(_ dataDict: [String:Any],_ tag: Int)
    {
        if let isread = dataDict["is_read"] as? Int
        {
            if isread == 0
            {
                let dict = NSMutableDictionary()
                dict["notification_ids"] = dataDict["notification_id"]
                dict["status"] = 1
                
                KVClass.KVServiece(methodType: "POST", processView: nil, baseView: nil, processLabel: "", params: dict, api: "read_notifications", arrFlag: false, Hendler: { (JSON, status) in
                    
                    if status == 1{
                        
                        self.arrData[tag]["is_read"] = 1
                        self.tblView.reloadData()
                    }
                    
                })
                
            }
        }
    }
    // MARK : - APPROVE / REJECT NOTIFICATION
    
    
    func aproveRejectNotipress(_ dictData: [String : Any], _ indexPath: IndexPath)
    {
        let ActionAlert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        let accept = UIAlertAction.init(title: "Approve", style: .default, handler: { (action) in
            
            let view = UIView.fromNib("QKDownloadContactView") as! QKDownloadContactView
            view.tag = indexPath.row
            view.dictSelectedData = dictData
            view.addContactView(to: self)
            
        })
        let reject = UIAlertAction.init(title: "Reject", style: .destructive, handler: { (action) in
            
            self.rejectNotification(dictData, at: indexPath)
            
        })
        
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
            ActionAlert.dismiss(animated: true, completion: nil)
        })
        
        //self.view.setValue(UIColor.black, forKey: "backgroundColor")
        
        
        accept.setValue(#imageLiteral(resourceName: "approve_noti_ic").withRenderingMode(.automatic), forKey: "image")
        reject.setValue(#imageLiteral(resourceName: "reject_noti_ic").withRenderingMode(.automatic), forKey: "image")
        accept.setValue(NSTextAlignment.left.rawValue, forKey: "titleTextAlignment")
        reject.setValue(NSTextAlignment.left.rawValue, forKey: "titleTextAlignment")
        ///  accept.setValue(UISemanticContentAttribute.forceRightToLeft.rawValue, forKey: "semanticContentAttribute")
        ///  reject.setValue(UISemanticContentAttribute.forceRightToLeft.rawValue, forKey: "semanticContentAttribute")
        //  ActionAlert.semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
        
        ActionAlert.addAction(accept)
        ActionAlert.addAction(reject)
        ActionAlert.addAction(cancel)
        
        self.present(ActionAlert, animated: true, completion: nil)
    
        ActionAlert.view.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        for tbview in ActionAlert.view.subviews
        {
            tbview.setValue(UISemanticContentAttribute.forceRightToLeft.rawValue, forKey: "semanticContentAttribute")
        }
        
    }
    
    
    // MARK : - REJECT NOTIFICATION
    
    func rejectNotification(_ dictSelectedData: [String : Any], at index: IndexPath)
    {
        var SenderId = 0  //dictSelectedData["sender_id"] as? Int ?? 0
        var Noti_id = 0  //dictSelectedData["notification_id"] as? Int ?? 0
        
        if dictSelectedData["sender_id"] as? Int ?? 0 != 0
        {
            SenderId = dictSelectedData["sender_id"] as? Int ?? 0
        }
        else if dictSelectedData["sender_id"] as? String ?? "" != ""
        {
            SenderId = Int(dictSelectedData["sender_id"] as? String ?? "")!
        }
        
        if dictSelectedData["notification_id"] as? Int ?? 0 != 0
        {
            Noti_id = dictSelectedData["notification_id"] as? Int ?? 0
        }
        else if dictSelectedData["notification_id"] as? String ?? "" != ""
        {
            Noti_id = Int(dictSelectedData["notification_id"] as? String ?? "")!
        }
        
        let senddict = NSMutableDictionary()
        senddict["sender_id"] = SenderId
        senddict["user_id"] = kUserDefults_("kLoginusrid") as? Int ?? 0
        senddict["notification_id"] = Noti_id
        
        KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: senddict, api: "reject_contact", arrFlag: false, Hendler: { (JSON, status) in
            
            KVClass.ShowNotification("", JSON["msg"] as? String ?? "")
            self.arrData.remove(at: index.row)
            self.tblView.deleteRows(at: [index], with: .fade)
        })
    }
    
}
//============================================================================================================//
//============================================================================================================//

class NotificationCell: UITableViewCell {
    
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet var img_Profile: UIImageView!
    @IBOutlet var img_StatusImg: UIImageView!
    @IBOutlet var img_more: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.img_StatusImg.image = nil
        self.img_Profile.image = #imageLiteral(resourceName: "user_profile_pic")
    }
}
