//
//  HomeViewController.swift
//  Quickkonnect
//
//  Created by Dhaval Tannarana on 02/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import AnimatedCollectionViewLayout

class QKTagImageList: NSObject {
  
    var name = ""
    var qr_code = ""
    var middle_tag = ""
    var border_color = ""
    var pattern_color = ""
    var background_color = ""
    var background_gradient = ""
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        
        self.name = dict["name"] as? String ?? ""
        
        if let dataDict = dict["qk_tag_info"] as? NSDictionary
        {
            self.qr_code = dataDict["qr_code"] as? String ?? ""
            self.middle_tag = dataDict["middle_tag"] as? String ?? ""
            self.border_color = dataDict["border_color"] as? String ?? "0xff000000"
            self.pattern_color = dataDict["pattern_color"] as? String ?? "0xff000000"
            self.background_color = dataDict["background_color"] as? String ?? "0xffFFFFFF"
            self.background_gradient = dataDict["background_gradient"] as? String ?? "1"
        }
    }
}

class HomeViewController: QKMainBaseVC {
    
    //=======================================================================================//
    @IBOutlet weak var lblUserName: UILabel!
    var UserDetailData: resultUserDict! = resultUserDict()
    @IBOutlet var collection_QKTag: UICollectionView!
    @IBOutlet var Count_collect: UICollectionView!
    @IBOutlet var collectionViewForBeges: UICollectionView!
   // @IBOutlet var page_QKTagpage: UIPageControl!
    @IBOutlet var lblNo_avaible_badges: UILabel!
    @IBOutlet var activityIndicate_CollectionView: UIActivityIndicatorView!
    
    var arr_QKImages: [QKTagImageList]! = [QKTagImageList]()
    var QKCountArr = ["CONTACTS","THIS WEEK","THIS MONTH"]
    var QKCountTitleArr = ["0","0","0"]
    var arrBeges = [#imageLiteral(resourceName: "logo"), #imageLiteral(resourceName: "logo2")]
    var arrQkEditData: [resultUserDict]! = [resultUserDict]()
    
    var visibleIndexpath = 0
    
    @IBOutlet var collection_PageControll: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNo_avaible_badges.isHidden = true
        self.Bar.img_TopBG.isHidden = true
        self.Bar.backgroundColor = UIColor.clear
        self.Bar.constraint_btn_chat_trilling.constant = 12
        ///===================================================================================///
        //For Collecetion Cell Scroll with Animation===========================================//
        let layout = AnimatedCollectionViewLayout()
        layout.animator = CubeAttributesAnimator()
        layout.scrollDirection = .horizontal
        collection_QKTag.collectionViewLayout = layout
        ///===================================================================================///
        ///===================================================================================///
        
        
        
        self.callAPIforgetQKTagViewList()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.collection_QKTag.reloadData()
        self.collectionViewForBeges.reloadData()
        self.collection_PageControll.reloadData()
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//
//        if arrBeges.count == 0 {
//            lblNo_avaible_badges.isHidden = false
//        }
//        else {
//            lblNo_avaible_badges.isHidden = true
//        }
//
//        self.QKCountTitleArr = ["\(KVAppDataServieces.shared.UserValue(.contact_all))","\(KVAppDataServieces.shared.UserValue(.contact_week))","\(KVAppDataServieces.shared.UserValue(.contact_month))"]
//
//        self.Count_collect.reloadData()
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        debugPrint(">>>>>>>>>>> viewDidAppear")
        //TagImages for User Scan user profile

        if KVAppDataServieces.shared.getUserProfiles_List(KVAppDataServieces.shared.LoginUserID()) != nil
        {
            self.setDisplayAndManageUserProfilesQKTag(dicData: KVAppDataServieces.shared.getUserProfiles_List(KVAppDataServieces.shared.LoginUserID())!)
            self.collection_QKTag.reloadData()
        }
        
        self.callAPIforgetQKTagViewList()
      
        if arrBeges.count == 0 {
            lblNo_avaible_badges.isHidden = false
        }
        else {
            lblNo_avaible_badges.isHidden = true
        }
        
        
        self.QKCountTitleArr = ["\(KVAppDataServieces.shared.UserValue(.contact_all))","\(KVAppDataServieces.shared.UserValue(.contact_week))","\(KVAppDataServieces.shared.UserValue(.contact_month))"]
        
        self.Count_collect.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        debugPrint(">>>>>>>>>>> viewWillDisappear")
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        debugPrint(">>>>>>>>>>> viewDidDisappear")

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
        
        //Get Country code from Current country
        if let countryCode = Locale.current.regionCode {
            if let currentCountry = SMCountry.shared.getinfoUsingCountryCode(code: countryCode) {
                app_Delegate.CURRENT_COUNTRY_CODE = currentCountry.phoneCode!
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedReloadQKTAG(notification:)), name: Notification.Name("RELOADNOTIFICATION"), object: nil)
    }
    
    //================================================================================//
    //MARK :- Received Notification
    func methodOfReceivedReloadQKTAG(notification: NSNotification){
        //Take Action on Notification
        self.callAPIforgetQKTagViewList()
    }
    //================================================================================//
    
    
    
    
    func callAPIforgetQKTagViewList()
    {
        let dict = NSMutableDictionary()
        dict["user_id"] = KVAppDataServieces.shared.LoginUserID()
        
        activityIndicate_CollectionView.stopAnimating()
        
        KVClass.KVServiece(methodType: "POST", processView: nil, baseView: self, processLabel: "", params: dict, api: "profile/profile_list", arrFlag: false) { (json, status) in
            
            if status == 1
            {
                print(json)
                
                if let dataDict = json["data"] as? [NSDictionary]
                {
                 
                    KVAppDataServieces.shared.setProfileListFromLoginUser_ID(KVAppDataServieces.shared.LoginUserID(), json)
                    self.setDisplayAndManageUserProfilesQKTag(dicData: dataDict)
                    self.collection_QKTag.reloadData()
                }
                
            }
            else {
                print("User not found")
                let strErrorMsg = json["msg"] as? String ?? ""
                if strErrorMsg == "User not found" {
                    AlldefaultsRemove()
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NaviVC") as! NaviVC
                    UserDefaults.standard.removeObject(forKey: "session")
                    UserDefaults.standard.removeObject(forKey: "dashboard")
                    UIApplication.shared.keyWindow?.rootViewController = vc
                }
            }
        }
        
    }
    
    
    func setDisplayAndManageUserProfilesQKTag(dicData: [NSDictionary]) {
    
        self.arrQkEditData.removeAll()
        self.arr_QKImages.removeAll()
        
        KVProfileCreate.shared.arr_Profile.removeAll()
        KVProfileCreate.shared.arr_Badges.removeAll()
        
        if dicData.count == 2 {
            KVProfileCreate.shared.arr_Profile.append(dicData.first!)
        }
        else if dicData.count > 2 {
            for item in dicData {
                if KVProfileCreate.shared.arr_Profile.count < 2 {
                    KVProfileCreate.shared.arr_Profile.append(item)
                }
            }
        }
        
        for item in dicData
        {
            let dicEdit = NSMutableDictionary.init(dictionary: item)
            if let valueDict = item["qk_tag_info"] as? NSDictionary
            {
                dicEdit["background_color"] = valueDict["background_color"]
                dicEdit["border_color"] = valueDict["border_color"]
                dicEdit["middle_tag"] = valueDict["middle_tag"]
                dicEdit["pattern_color"] = valueDict["pattern_color"]
                dicEdit["qr_code"] = valueDict["qr_code"]
                dicEdit["background_gradient"] = valueDict["background_gradient"]
            }
            
            self.arrQkEditData.append(resultUserDict.init(dicEdit))
            self.arr_QKImages.append(QKTagImageList.init(item))
            
            if let isprimary = item["is_primary"] as? String, isprimary == "Y"
            {
                if let valueDict = item["qk_tag_info"] as? NSDictionary
                {
                    KVAppDataServieces.shared.update(.background_color, valueDict["background_color"] as? String ?? "0xffFFFFFF")
                    KVAppDataServieces.shared.update(.border_color, valueDict["border_color"] as? String ?? "0xff000000")
                    KVAppDataServieces.shared.update(.middle_tag, valueDict["middle_tag"] as? String ?? "")
                    KVAppDataServieces.shared.update(.pattern_color, valueDict["pattern_color"] as? String ?? "0xff000000")
                    KVAppDataServieces.shared.update(.qr_code, valueDict["qr_code"] as? String ?? "")
                    KVAppDataServieces.shared.update(.background_gradient, valueDict["background_gradient"] as? String ?? "1")
                }
            }
        }
        
        if self.arr_QKImages.count > 1 && self.arrQkEditData.count > 1
        {
            let lastObjet = self.arr_QKImages.last
            self.arr_QKImages.insert(lastObjet!, at: 0)
            self.arr_QKImages.removeLast()
            
            let lastEditObject = self.arrQkEditData.last
            self.arrQkEditData.insert(lastEditObject!, at: 0)
            self.arrQkEditData.removeLast()
        }
        
        
        if self.arr_QKImages.count == 1 {
            self.collection_PageControll.isHidden = true
        }
        else {
            self.collection_PageControll.isHidden = false
        }
        
        self.collection_QKTag.reloadData()
        self.collectionViewForBeges.reloadData()
        self.collection_PageControll.reloadData()
        self.activityIndicate_CollectionView.stopAnimating()
    }
    //==============================================================================================================//
    //==============================================================================================================//
    //==============================================================================================================//
    
    
    
    
    func convertImageToBase64(image: UIImage) -> String {
        let imageDataa = UIImagePNGRepresentation(image)
        let base64String = imageDataa?.base64EncodedString(options: Data.Base64EncodingOptions.init(rawValue: 0))
        return base64String!
    }
    
    func QKEditPress(_ img: UIImage?, selecDic : resultUserDict, Selected_Index: Int)
    {
        var strBase64Image = ""
        let strURL = selecDic.middle_tag
        // Put Your Image URL
        if !(selecDic.middle_tag == "") {
            let url:NSURL = NSURL(string : strURL)!
            // It Will turn Into Data
            let imageData : NSData = NSData.init(contentsOf: url as URL)!
            // Data Will Encode into Base64
            strBase64Image = self.convertImageToBase64(image: UIImage(data: imageData as Data)!)
        }
        
        DispatchQueue.main.async {
        
        let QKAlert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let EditAction = UIAlertAction.init(title: "Edit", style: .default) { (action) in
            
            DispatchQueue.main.async {
//                var strBase64Image = ""
//                let strURL = selecDic.middle_tag
//                // Put Your Image URL
//                if !(selecDic.middle_tag == "") {
//                    let url:NSURL = NSURL(string : strURL)!
//                    // It Will turn Into Data
//                    let imageData : NSData = NSData.init(contentsOf: url as URL)!
//                    // Data Will Encode into Base64
//                    strBase64Image = self.convertImageToBase64(image: UIImage(data: imageData as Data)!)
//                }
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "UpdateQKTagViewController") as! UpdateQKTagViewController
                
                if selecDic.is_primary == "N"
                {
                    obj.issubProfile = true
                }
                else if selecDic.is_primary == "Y"
                {
                    obj.issubProfile = false
                }
                obj.strMiddleBase64 = strBase64Image
                obj.dict_subuser_data = selecDic
                obj.view.tag = 2214
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
        
        let ViewAction = UIAlertAction.init(title: "View", style: .default) { (action) in
            DispatchQueue.main.async {
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "ViewQKTagVC") as! ViewQKTagVC
                obj.ShowImage = img
                obj.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                obj.view.backgroundColor = UIColor.clear
                self.present(obj, animated: false, completion: nil)
            }
        }
        
        let ShareAction = UIAlertAction.init(title: "Share" , style: .default) { (action) in
            
            DispatchQueue.main.async {
                let shareVC = UIActivityViewController.init(activityItems: [img!], applicationActivities: nil)
                
                self.present(shareVC, animated: true, completion: nil)
            }
        }
        
        let ViewProfileAction = UIAlertAction.init(title: "View Profile" , style: .default) { (action) in
            
            DispatchQueue.main.async {
                
                if Selected_Index == 0 {
                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                    obj.isedit = false
                    self.navigationController?.pushViewController(obj, animated: true)
                }
                else {
                    
                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
                    if selecDic.role == 1 {
                        obj.int_userID = -1
                        obj.str_Screen_From = "OnlyViewProfile"
                        obj.strBlockProfileID = selecDic.id
                        obj.str_uniquecode = selecDic.unique_code
                    }
                    else {
                        
                        if selecDic.role == 2 {
                            obj.str_Screen_From = "View/EditProfile"
                        }
                        
                        app_Delegate.strProfileScreenFrom = "ViewProfile"
                        obj.int_userID = selecDic.id
                    }
                    self.navigationController?.pushViewController(obj, animated: true)
                    
                }
                
                
                
                
            }
        }
        
        let CancelAction = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            
            QKAlert.dismiss(animated: true, completion: nil)
        }
        
        
        QKAlert.addAction(ViewAction)
        if selecDic.role == 1 {
        }
        else if selecDic.role == 2 {
            QKAlert.addAction(EditAction)
            QKAlert.addAction(ShareAction)
        }
        else {
            QKAlert.addAction(EditAction)
            QKAlert.addAction(ShareAction)
        }
        QKAlert.addAction(ViewProfileAction)
        QKAlert.addAction(CancelAction)
        
        self.present(QKAlert, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collection_QKTag {
           // self.page_QKTagpage.numberOfPages = self.arr_QKImages.count
            return self.arr_QKImages.count
        }
        else if collectionView == collectionViewForBeges {
            return arrBeges.count
        }
        else if collectionView == self.collection_PageControll{
            return self.arr_QKImages.count
        }
        return 3
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        let visibleRect = CGRect.init(origin: self.collection_QKTag.contentOffset, size: self.collection_QKTag.bounds.size)
        let visiblePoint = CGPoint.init(x: visibleRect.midX, y: visibleRect.midY)
        let visibleindex = self.collection_QKTag.indexPathForItem(at: visiblePoint)
        
        self.visibleIndexpath = (visibleindex?.row ?? 0)
        
        
      //  self.collection_PageControll.scrollToItem(at: visibleindex!, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
      //  self.collection_PageControll.reloadItems(at: [visibleindex!])
        self.collection_PageControll.reloadData()
        
        //self.page_QKTagpage.currentPage = (visibleindex?.row)!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.Count_collect
        {
            let cell: DashCollectCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashCollectCell", for: indexPath) as! DashCollectCell
            cell.Title_lbl.text = self.QKCountArr[indexPath.row]
            cell.Count_lbl.text = self.QKCountTitleArr[indexPath.row]
            return cell
        }
       else if collectionView == self.collection_PageControll
        {
            let cell: pageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "pageCell", for: indexPath) as! pageCell
                //collectionView.dequeueReusableCell(withReuseIdentifier: "DashCollectCell", for: indexPath) as! DashCollectCell
            cell.layer.cornerRadius = cell.frame.height / 2
            cell.clipsToBounds = true
            cell.indicatorView.backgroundColor = UIColor.white
            
            if indexPath.row == self.visibleIndexpath{
              cell.indicatorView.backgroundColor = UIColor.white
            }
            else{
              cell.indicatorView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
            }
            
//            cell.Title_lbl.text = self.QKCountArr[indexPath.row]
//            cell.Count_lbl.text = self.QKCountTitleArr[indexPath.row]
            return cell
        }
        else if collectionView == collectionViewForBeges {
            let b_cell: BagesCollectCell = collectionView.dequeueReusableCell(withReuseIdentifier: "BagesCollectCell", for: indexPath) as! BagesCollectCell
            
            b_cell.viewBG.layer.masksToBounds = true
            b_cell.viewBG.layer.borderWidth = 0.8
            b_cell.viewBG.layer.borderColor = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1).cgColor
            
            b_cell.imgBeges.image = self.arrBeges[indexPath.row]
            return b_cell
        }
        let cell: cell_QKTag = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_QKTag", for: indexPath) as! cell_QKTag
        
        cell.activityView.startAnimating()
        cell.lblDesc.text = ""
        cell.lblUserName.text = self.arr_QKImages[indexPath.row].name
        
        for imgitem in self.arr_QKImages {
            let strURL = imgitem.qr_code
            if strURL != "" {
                let imgTemp = UIImageView()
                imgTemp.setImageWith(URLRequest.init(url: URL.init(string: strURL)!), placeholderImage: nil, success: { (request, response, image) in
                }, failure: { (request, response, error) in
                })
            }
        }
        
        
        let strURL = self.arr_QKImages[indexPath.row].qr_code
        if strURL != ""
        {
            cell.img_QKtag.setImageWith(URLRequest.init(url: URL.init(string: strURL)!), placeholderImage: nil, success: { (request, response, image) in
                cell.activityView.stopAnimating()
                cell.img_QKtag.image = image
            }, failure: { (request, response, error) in
                cell.activityView.stopAnimating()
            })
        }
        else
        {
            cell.activityView.stopAnimating()
        }
        
        let strQk_TagBG = self.arr_QKImages[indexPath.row].background_gradient
        cell.imgQKTagBG.image = UIImage.init(named: "gradient-" + strQk_TagBG)

        cell.img_QKtag.layer.cornerRadius = cell.img_QKtag.frame.height/2
        cell.img_TagBGForBorder.layer.cornerRadius = cell.img_TagBGForBorder.frame.height/2
        cell.img_TagBGForBorder.clipsToBounds = true
        cell.img_TagBGForBorder.layer.borderWidth = 10
        cell.img_TagBGForBorder.layer.borderColor = UIColor.black.withAlphaComponent(0.1).cgColor
        
                
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.Count_collect
        {
        let size = CGSize.init(width: self.Count_collect.frame.size.width/3, height: self.Count_collect.frame.size.height)
        return size
        }
        else if collectionView == self.collection_PageControll
        {
            let size = CGSize.init(width: ((self.collection_PageControll.frame.width)/CGFloat(self.arr_QKImages.count)), height: collectionView.frame.size.height)
            return size
        }
            
        else if collectionView == collectionViewForBeges {
            let size = CGSize.init(width: collectionViewForBeges.frame.size.height, height: collectionViewForBeges.frame.size.height)
            return size
        }
        else {
            let size = CGSize.init(width: collectionView.bounds.width, height: collectionView.bounds.height)
            return size
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collection_QKTag
        {
            
            //let dicselectedqKTagImge =  self.arrQkEditData[indexPath.row]
            print(self.arrQkEditData[indexPath.row].id)
          let cell = collectionView.cellForItem(at: indexPath) as? cell_QKTag
            self.QKEditPress(cell?.img_QKtag.image, selecDic: self.arrQkEditData[indexPath.row], Selected_Index: indexPath.row)
        }
        else if collectionView == self.collection_PageControll{
            
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == self.Count_collect {
            return UIEdgeInsetsMake(0, 0, 1, 1)
        }
//       else if collectionView == self.collection_PageControll {
//            return UIEdgeInsetsMake(0, 0, 0, 0)
//        }
        else {
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.Count_collect {
            return 1
        }
        else if collectionView == self.collection_PageControll {
            return 0
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.Count_collect {
            return 1
        }
        else if collectionView == self.collection_PageControll {
            return 0
        }
        else {
            return 0
        }
    }
    
}

class pageCell: UICollectionViewCell{
    
    @IBOutlet var indicatorView: UIView!
    
}

class DashCollectCell: UICollectionViewCell {
    
    @IBOutlet var Title_lbl: UILabel!
    @IBOutlet var Count_lbl: UILabel!
    
}

class BagesCollectCell: UICollectionViewCell {
    @IBOutlet var viewBG: UIView!
    @IBOutlet var imgBeges: UIImageView!
}

class cell_QKTag: UICollectionViewCell {
    
    var back: UIView!
    var front: UIView!
    var showingBack = true
    var initNumber:Int = 0
    
    @IBOutlet weak var imgQKTagBG: UIImageView!
    @IBOutlet var img_TagBGForBorder: UIImageView!
    @IBOutlet var img_QKtag: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet var activityView: UIActivityIndicatorView!
    @IBOutlet var lbl_userName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imgQKTagBG.image = nil
        self.img_QKtag.image = nil
    }
}
