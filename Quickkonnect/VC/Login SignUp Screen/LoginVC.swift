//
//  ViewController.swift
//  Quickkonnect
//
//  Created by Kavi on 17/12/17.
//  Copyright © 2017 com.kavish. All rights reserved.
//

import UIKit
import Material
import LNRSimpleNotifications
import LinkedinSwift
import FBSDKLoginKit

class resultUserDict: NSObject {
    
    var background_color = ""
    var border_color = ""
    var email = ""
    var firstname = ""
    var firsttime = false
    var id = 0
    var lastname = ""
    var middle_tag = ""
    var middle_tag_base64 = ""
    var name = ""
    var notification_status = false
    var pattern_color = ""
    var profile_pic = ""
    var qr_code = ""
    var status = 0
    var unique_code = ""
    var user_detail = ""
    var type = 0
    var role = 0
    var is_primary = ""
    var background_gradient = ""
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        self.id = dict["id"] as? Int ?? 0
        self.role = dict["role"] as? Int ?? 0
        self.type = dict["type"] as? Int ?? 0
        self.status = dict["status"] as? Int ?? 0
        self.name = dict["name"] as? String ?? ""
        self.email = dict["email"] as? String ?? ""
        self.qr_code = dict["qr_code"] as? String ?? ""
        self.lastname = dict["lastname"] as? String ?? ""
        self.firstname = dict["firstname"] as? String ?? ""
        self.firsttime = dict["firsttime"] as? Bool ?? false
        self.middle_tag = dict["middle_tag"] as? String ?? ""
        self.is_primary = dict["is_primary"] as? String ?? ""
        self.unique_code = dict["unique_code"] as? String ?? ""
        self.user_detail = dict["user_detail"] as? String ?? ""
        self.profile_pic = dict["profile_pic"] as? String ?? ""
        self.middle_tag_base64 = dict["middle_tag_base64"] as? String ?? ""
        self.border_color = dict["border_color"] as? String ?? "0xff000000"
        self.pattern_color = dict["pattern_color"] as? String ?? "0xff000000"
        self.notification_status = dict["notification_status"] as? Bool ?? false
        self.background_gradient = dict["background_gradient"] as? String ?? "1"
        self.background_color = dict["background_color"] as? String ?? "0xffFFFFFF"
    }
}

class LoginVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var User_txt: TextField!
    @IBOutlet var Pass_txt: TextField!
    @IBOutlet var Scrol_Vie: UIScrollView!
    @IBOutlet var Signup_btn: UILabel!
    @IBOutlet var Forget_pass: UIButton!
    
    let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81pbh72vnmiudv", clientSecret: "Tdq6mikPJbxFkVOT", state: "DLKDJF46ikMMZADfdfds", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://www.example.com/"))
    
    var UserDetailData: resultUserDict! = resultUserDict()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (FBSDKAccessToken.current()) != nil{
            
            let manager = FBSDKLoginManager()
            manager.logOut()
        }
        
        //==========Set TextField PlaceHolder and BG Color===========//
        self.User_txt.loginSetDivider()
        self.Pass_txt.loginSetDivider()
        //===========================================================//
        //===============================================================================================//
        
        
        //===============================================================================================//
        //=======Set Color And Font In Sign and Forgot Pass Button Text==================================//
        let st = NSMutableAttributedString.init(string: "Don't have an account? SIGN UP")
        st.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Regular", size: 15)!, range: NSRange.init(location: 0, length: 22))
        st.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange.init(location: 0, length: 22))
        st.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Regular", size: 16)!, range: NSRange.init(location: 23, length: 7))
        st.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(hex: "28AFB0"), range: NSRange.init(location: 23, length: 7))
        self.Signup_btn.attributedText = st
        
        //ForgotButton
        let stForget = NSMutableAttributedString.init(string: "Forgot Password?")
        stForget.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Regular", size: 14)!, range: NSRange.init(location: 0, length: 16))
        stForget.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange.init(location: 0, length: 16))
        stForget.addAttribute(NSUnderlineColorAttributeName, value: UIColor.white, range: NSRange.init(location: 0, length: 16))
        stForget.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange.init(location: 0, length: 16))
        self.Forget_pass.setAttributedTitle(stForget, for: .normal)
        //===============================================================================================//
        //===============================================================================================//
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    //===============================================================================================//
    //MARK :- UITextField Delegate Method============================================================//
    
    // MARK: UITextFileld Delegate and Datasource Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == User_txt {
            textField.resignFirstResponder()
            Pass_txt.becomeFirstResponder()
            return false
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    
    // MARK: - Validation function
    func isValidData() -> Bool{
        
        if !(self.User_txt.text?.isValidfname())! {
            KVClass.ShowNotification("", "Please enter your email!")
            return false
        }
        else if !(self.User_txt.text?.isValidEmail())! {
            KVClass.ShowNotification("", "Please enter your valid email!")
            return false
        }
        else if !(self.Pass_txt.text?.isValidfname())! {
            KVClass.ShowNotification("", "Please enter password!")
            return false
        }
        else if !(self.Pass_txt.text?.isValidPasswordLength())! {
            KVClass.ShowNotification("", "Password must contain at least 6 characters!")
            return false
        }
        
        return true
    }
    //===============================================================================================//
    //===============================================================================================//
    
    
    //===============================================================================================//
    //MARK :- UIButton Action Method=================================================================//
    
    @IBAction func signUpPress(_ sender: UIButton)
    {
        let objSignUp = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(objSignUp, animated: true)
    }
    
    
    @IBAction func loginPress(_ sender: UIButton)
    {
        if isValidData() == false {
            return
        }
        else {
            self.view.endEditing(true)
        
            let dict = NSMutableDictionary()
            dict["email"] = self.User_txt.text
            dict["password"] = self.Pass_txt.text
            dict["device_id"] = kUserDefults_("fcmtoken")
            dict["device_type"] = "iOS"
        
            KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict, api: "login", arrFlag: false) { (JSON, status) in
            
                if let user = JSON["data"] as? NSDictionary {
                    print(JSON)
                
                    kUserDefults((user["id"] as? Int ?? 0) as AnyObject, key: "kLoginusrid")
                    KVAppDataServieces.shared.initializeAppData(user)
                    KVAppDataServieces.shared.update(.loginFrom, "Normal")
                    appDelegate.updateContactList()
                    appDelegate.updateBlockList()
                    if KVAppDataServieces.shared.UserValue(.qr_code) as? String ?? "" == "" {
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SocialVC") as! SocialVC
                        obj.UserData = self.UserDetailData
                        obj.view.tag = 2214
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                    else {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "tabbar")
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                }
            }
        }
    }
    
    
    
    //===============================================================================================//
    //===============================================================================================//
    
    
    // MARK : Login with social Accounts
    //============================================================================================================//
    //============================================================================================================//
    
    //MARK : Login with Facebook
    @IBAction func facebookloginpress(_ sender: UIButton)
    {
        let action = UIAlertAction.init(title: "Accept & Continue", style: UIAlertActionStyle.destructive) { (action) in
            
            DLFacebookKit.loginWithPostPermisson(contoller: self) { (result, error) in
                
                if error == nil, let data = result as? NSDictionary {
                    
                    
                    let dict = NSMutableDictionary()
                    dict["firstname"] = data["first_name"]
                    dict["lastname"] = data["last_name"]
                    
                    if data["email"] as? String ?? "" == ""
                    {
                        dict["email"] = data["id"] as? String ?? ""
                    }
                    else
                    {
                        dict["email"] = data["email"] as? String ?? ""
                    }
                    //dict["password"] = ""
                    dict["social_media_user_id"] = data["id"]
                    dict["social_media_id"] = "1"
                    
                    var strToken:String = ""
                    if UserDefaults.standard.object(forKey: "fcmtoken") != nil {
                        strToken = UserDefaults.standard.object(forKey: "fcmtoken") as! String
                    }
                    
                    
                    
                    let dictParameters:NSMutableDictionary
                        = ["social_media_user_id":data["id"] as Any , "social_media_id":"1", "device_id":strToken, "data":data, "device_type" : "iOS"]
                    
                    
                    KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dictParameters, api: "create_user", arrFlag: false, Hendler: { (json, status) in
                        
                        if let user = json["data"] as? NSDictionary
                        {
                            kUserDefults((user["id"] as? Int ?? 0) as AnyObject, key: "kLoginusrid")
                            
                            KVAppDataServieces.shared.initializeAppData(user)
                            KVAppDataServieces.shared.update(.loginFrom, "Facebook")
                            appDelegate.updateContactList()
                            appDelegate.updateBlockList()
                            if KVAppDataServieces.shared.UserValue(.qr_code) as? String ?? "" == ""
                            {
                                let obj = self.storyboard?.instantiateViewController(withIdentifier: "SocialVC") as! SocialVC
                                obj.UserData = self.UserDetailData
                                obj.view.tag = 2214
                                self.navigationController?.pushViewController(obj, animated: true)
                            }
                            else
                            {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "tabbar")
                                self.navigationController?.pushViewController(vc!, animated: true)
                            }
                        }
                        
                    })
                }
            }
        }
        
        self.showTremsalert(action)
    }
    //============================================================================================================//
    //============================================================================================================//
    
    // MARK : Login with Linkdin
    @IBAction func btnLinkedInLoginTapped(_ sender: Any) {
        
        let action = UIAlertAction.init(title: "Accept & Continue", style: UIAlertActionStyle.destructive) { (action) in
            
            self.linkedinHelper.authorizeSuccess({ [unowned self] (lsToken) -> Void in
                
                KVClass.KVShowLoader()
                
                self.linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),positions,date-of-birth,phone-numbers,location)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
                    
                    print("Request success with response: \(response.jsonObject)")
                    let strId = response.jsonObject["id"]
                    
                    //let username = response.jsonObject["firstName"]
                    
                    print("Request success with response: \(response)")
                    
                    var strToken:String = ""
                    //UserDefaults.standard.set(fcmToken, forKey: "fcmtoken")
                    if UserDefaults.standard.object(forKey: "fcmtoken") != nil {
                        strToken = UserDefaults.standard.object(forKey: "fcmtoken") as! String
                    }
                    
                    let dictParameters:NSMutableDictionary
                        = ["social_media_user_id":strId ?? 0 , "social_media_id":"2", "device_id":strToken, "data": response.jsonObject, "device_type" : "iOS"]
                    
                    KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dictParameters, api: "create_user", arrFlag: false, Hendler: { (json, status) in
                        print(json)
                        KVClass.KVHideLoader()
                        if let user = json["data"] as? NSDictionary
                        {
                            kUserDefults((user["id"] as? Int ?? 0) as AnyObject, key: "kLoginusrid")
                            
                            KVAppDataServieces.shared.initializeAppData(user)
                            KVAppDataServieces.shared.update(.loginFrom, "Lindin")
                            appDelegate.updateContactList()
                            appDelegate.updateBlockList()
                            if KVAppDataServieces.shared.UserValue(.qr_code) as? String ?? "" == ""
                            {
                                let obj = self.storyboard?.instantiateViewController(withIdentifier: "SocialVC") as! SocialVC
                                obj.UserData = self.UserDetailData
                                obj.view.tag = 2214
                                self.navigationController?.pushViewController(obj, animated: true)
                            }
                            else
                            {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "tabbar")
                                self.navigationController?.pushViewController(vc!, animated: true)
                            }
                        }
                    })
                    
                    
                }) { [unowned self] (error) -> Void in
                    
                    print("Encounter error: \(error.localizedDescription)")
                }
                
                print("Login success lsToken: \(lsToken)")
                }, error: { [unowned self] (error) -> Void in
                    
                    print("Encounter error: \(error.localizedDescription)")
                    KVClass.ShowNotification("No Internet Connection", "Please Check your Internet Connection and try again")
                }, cancel: { [unowned self] () -> Void in
                    
                    print("User Cancelled!")
            })
        }
        
        self.showTremsalert(action)
        
    }
    //============================================================================================================//
    //============================================================================================================//
}


//===============================================================================================//
//MARK :- UITextField Extension==================================================================//


extension TextField {
    func setPhoneDivider() {
        
        self.dividerActiveColor = UIColor.init(hex: "28AFB0")
        self.placeholderVerticalOffset = 14
        //self.placeholderHorizontalOffset = 50
        let padding = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 8)
        self.placeholderRect(forBounds: UIEdgeInsetsInsetRect(self.frame, padding))
        
        //self.placeholderRect(forBounds: UIEdgeInsetsInsetRect(bounds, UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 8)))
        
        //self.setValue(UIEdgeInsetsInsetRect(self.frame, UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 8)), forKey: "frame")
        
        // self.dividerNormalColor = UIColor.init(hex: "434343")
        self.placeholderActiveColor = UIColor.init(hex: "28AFB0")
        self.font = UIFont.init(name: "lato-Regular", size: 16)
        self.clearButtonMode = .whileEditing
        
        
    }
}


extension TextField
{
    func setDivider() {
        
        self.dividerActiveColor = UIColor.init(hex: "28AFB0")
        self.placeholderVerticalOffset = 14
        
        // self.dividerNormalColor = UIColor.init(hex: "434343")
        self.placeholderActiveColor = UIColor.init(hex: "28AFB0")
        self.font = UIFont.init(name: "lato-Regular", size: 16)
        self.clearButtonMode = .whileEditing
    }
    
    func setDividerForC_Profile() {
        self.dividerActiveColor = UIColor.init(hex: "28AFB0")
        self.placeholderVerticalOffset = 5
        // self.dividerNormalColor = UIColor.init(hex: "434343")
        self.placeholderActiveColor = UIColor.init(hex: "28AFB0")
        self.font = UIFont.init(name: "lato-Regular", size: 16)
        self.clearButtonMode = .whileEditing
    }
    
    func loginSetDivider()
    {
        
        self.dividerActiveColor = UIColor.init(hex: "ffffff")
        self.dividerNormalColor = UIColor.init(hex: "ffffff")
        self.placeholderVerticalOffset = 14
        // self.placeholderActiveScale = 2
        self.placeholderActiveColor = UIColor.init(hex: "ffffff")
        self.placeholderNormalColor = UIColor.init(hex: "ffffff")
        self.font = UIFont.init(name: "lato-Regular", size: 16)
        self.textColor = UIColor.init(hex: "ffffff")
        //self.placeholderLabel.font = UIFont.init(name: "lato-Regular", size: 14)!
        self.clearButtonMode = .whileEditing
    }
}

extension TextView
{
    func setDivider() {
        
        self.isDividerHidden = true    // = UIColor.lightGray
        //self.placeholderVerticalOffset = 14
        
        // self.dividerNormalColor = UIColor.init(hex: "434343")
        //self.placeholderActiveColor = UIColor.init(hex: "28AFB0")
        self.font = UIFont.init(name: "lato-Regular", size: 16)
        //self.clearButtonMode = .whileEditing
    }
}
//===============================================================================================//
//===============================================================================================//

extension KVtxtTextField
{
    func setDivider() {
        
        self.dividerActiveColor = UIColor.init(hex: "28AFB0")
        self.placeholderVerticalOffset = 14
        // self.dividerNormalColor = UIColor.init(hex: "ffffff")
        self.placeholderActiveColor = UIColor.init(hex: "28AFB0")
        self.font = UIFont.init(name: "lato-Regular", size: 16)
        self.clearButtonMode = .whileEditing
    }
}
//===============================================================================================//
//===============================================================================================//


extension UIViewController
{
    func showTremsalert(_ action: UIAlertAction)
    {
        DispatchQueue.main.async {
            
            let obj = UIAlertController.init(title: "", message: "", preferredStyle: .actionSheet)
            
            let attributedTitle = NSMutableAttributedString(string: "Warning !", attributes:
                [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
            
            let attributedMessage = NSMutableAttributedString(string: "By accepting and continuing, Quickkonnect assumes that you willingly agree to both the Terms and Conditions & Privacy Contract. If this is not the case, you can delete your account and may leave this application.", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
            obj.setValue(attributedTitle, forKey: "attributedTitle")
            obj.setValue(attributedMessage, forKey: "attributedMessage")
            
            let readterms = UIAlertAction.init(title: "Read Terms & Conditions", style: UIAlertActionStyle.default, handler: { (action) in
                
                DispatchQueue.main.async {
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpSettingWebVC") as! HelpSettingWebVC
                obj.titlestring = "Read Terms & Conditions"
                obj.urlstring = "http://quickkonnect.com/terms-of-use.html"
                self.present(obj, animated: true, completion: nil)
                }
            })
            
            let privacy = UIAlertAction.init(title: "Privacy Contract", style: UIAlertActionStyle.default, handler: { (action) in
                DispatchQueue.main.async {
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpSettingWebVC") as! HelpSettingWebVC
                obj.titlestring = "Privacy Contract"
                obj.urlstring = "http://quickkonnect.com/privacy-contract.html"
                self.present(obj, animated: true, completion: nil)
                }
            })
            
            let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                
                obj.dismiss(animated: true, completion: nil)
            })
            
            
            
            obj.addAction(readterms)
            obj.addAction(privacy)
            obj.addAction(action)
            obj.addAction(cancel)
            
            
            self.present(obj, animated: true, completion: nil)
        }
    }
}

