//
//  ForgotpassVC.swift
//  Quickkonnect
//
//  Created by Kavi on 22/12/17.
//  Copyright © 2017 com.kavish. All rights reserved.
//

import UIKit
import Material


class ForgotpassVC: UIViewController, TextFieldDelegate {
    
    @IBOutlet var email_txt: TextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.email_txt.setDivider()
        
        let back = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 35, height: 35))
        let image = UIImageView.init(frame: CGRect.init(x: 5, y: 5, width: 25, height: 25))
        image.contentMode = .scaleAspectFit
        self.email_txt.leftViewMode = .always
        back.addSubview(image)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func forgotPasss()
    {
        if (self.email_txt.text?.isValidEmail())!
        {
            let dict = NSMutableDictionary()
            dict["email"] = self.email_txt.text
            
            KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict, api: "change_password", arrFlag: false, Hendler: { (JSON, status) in
                
                if status == 1
                {
                    KVClass.ShowNotification("", JSON["msg"] as? String ?? "")
                    
                    self.navigationController?.popViewController(animated: true)
                }
                
            })
            
            
        }
        else
        {
            KVClass.ShowNotification("", "Please enter valid Email!")
        }
    }
    
    
    //===============================================================================================//
    //MARK :- UIButton Action Event==================================================================//
    @IBAction func forgotpasPress(_ sender: UIButton)
    {
        self.forgotPasss()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (self.email_txt.text?.isValidEmail())!
        {
            self.email_txt.resignFirstResponder()
        }
        else
        {
            self.email_txt.resignFirstResponder()
            KVClass.ShowNotification("", "Please enter valid Email!")
        }
        return true
    }
    
    @IBAction func closepress(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    //===============================================================================================//
    //===============================================================================================//
    
}
