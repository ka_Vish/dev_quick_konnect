//
//  EmailedVC.swift
//  Quickkonnect
//
//  Created by Kavi on 20/12/17.
//  Copyright © 2017 com.kavish. All rights reserved.
//

import UIKit

class EmailedVC: UIViewController {
    
    @IBOutlet var Emaillabel_lbl: UILabel!
    
    var EmailString = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let newText = NSMutableAttributedString.init(string: "We have sent an email to \(self.EmailString)")
        
        let textRange = NSString(string: newText.string)
        
        let termrange = textRange.range(of: self.EmailString)
        
        newText.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(hex: "28AFB0"), range: termrange)
        
        self.Emaillabel_lbl.attributedText = newText
        
    }
    
    //===============================================================================================//
    //MARK : - UIButton Action Event=================================================================//
    @IBAction func donepress(_ sender: UIButton)
    {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
                
                topController.dismiss(animated: true, completion: nil)
            
               (appDelegate.window?.rootViewController as? UINavigationController)?.popViewController(animated: true)
                
            }
        }
    }
    //===============================================================================================//
    //===============================================================================================//
    
}
