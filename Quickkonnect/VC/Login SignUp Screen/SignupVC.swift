//
//  SignupVC.swift
//  Quickkonnect
//
//  Created by Kavi on 19/12/17.
//  Copyright © 2017 com.kavish. All rights reserved.
//

import UIKit
import Material
import DLRadioButton
import LinkedinSwift


class SignupVC: UIViewController, TextFieldDelegate {
    
    @IBOutlet var Signin_btn: UILabel!
    
    let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81pbh72vnmiudv", clientSecret: "Tdq6mikPJbxFkVOT", state: "DLKDJF46ikMMZADfdfds", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://www.example.com/"))
    
    @IBOutlet var firstname_txt: TextField!
    @IBOutlet var lastname_txt: TextField!
    @IBOutlet var email_txt: TextField!
    @IBOutlet var pass_txt: TextField!
    @IBOutlet var confirmpass_txt: TextField!
    
    @IBOutlet var Terms_btn: DLRadioButton!
    @IBOutlet var Trems_label: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.Terms_btn.isMultipleSelectionEnabled = true
        
        self.firstname_txt.loginSetDivider()
        self.lastname_txt.loginSetDivider()
        self.email_txt.loginSetDivider()
        self.pass_txt.loginSetDivider()
        self.confirmpass_txt.loginSetDivider()
        
        let st = NSMutableAttributedString.init(string: "Already have an account? SIGN IN")
        st.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Regular", size: 15)!, range: NSRange.init(location: 0, length: 24))
        st.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange.init(location: 0, length: 24))
        st.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Regular", size: 16)!, range: NSRange.init(location: 25, length: 7))
        st.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(hex: "28AFB0"), range: NSRange.init(location: 25, length: 7))
        
        self.Signin_btn.attributedText = st
        
        self.setTermsButton()
        
    }
    
    func setTermsButton()
    {
        
        let newText = NSMutableAttributedString.init(string: self.Trems_label.text!)
        newText.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange.init(location: 0, length: newText.length))
        
        let textRange = NSString(string: self.Trems_label.text!)
        
        let termrange = textRange.range(of: "Terms of Use")
        
        newText.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Regular", size: 16)!, range: termrange)
        newText.addAttribute(NSUnderlineColorAttributeName, value: UIColor.white, range: termrange)
        newText.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: termrange)
        
        let privacyrange = textRange.range(of: "Privacy Policy")
        
        newText.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Regular", size: 16)!, range: privacyrange)
        newText.addAttribute(NSUnderlineColorAttributeName, value: UIColor.white, range: privacyrange)
        newText.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: privacyrange)
        
        self.Trems_label.attributedText = newText
        self.Trems_label.font = UIFont.init(name: "lato-Regular", size: 15)!
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(TapResponce(_:)))
        
        self.Trems_label.isUserInteractionEnabled = true
        self.Trems_label.addGestureRecognizer(tapGesture)
        
        
        
    }
    
    
    @objc func TapResponce(_ sender: UITapGestureRecognizer)
    {
        let termsRange = (self.Trems_label.text! as NSString).range(of: "Terms of Use")
        
        let policyRange = (self.Trems_label.text! as NSString).range(of: "Privacy Policy")
        
        if (sender.didTapAttributedTextInLabel(label: self.Trems_label, inRange: termsRange)) {
            
            self.openURL("http://quickkonnect.com/terms-of-use.html")
        }
            
        else if (sender.didTapAttributedTextInLabel(label: self.Trems_label, inRange: policyRange))
        {
            self.openURL("http://quickkonnect.com/privacy-contract.html")
        }
        else {
            
            self.AcceptTermsConditions(self.Terms_btn)
            
        }
        
    }
    
    @IBAction func AcceptTermsConditions(_ sender: DLRadioButton)
    {
        if sender.isSelected
        {
            sender.isSelected = false
        }
        else
        {
            sender.isSelected = true
        }
    }
    
    func openURL(_ string: String)
    {
        DispatchQueue.main.async {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpSettingWebVC") as! HelpSettingWebVC
        obj.urlstring = string
        self.present(obj, animated: true, completion: nil)
        }
    }
    
    
    
    
    
    
    // MARK: UITextFileld Delegate and Datasource Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstname_txt {
            textField.resignFirstResponder()
            lastname_txt.becomeFirstResponder()
            return false
        }
        else if textField == lastname_txt {
            textField.resignFirstResponder()
            email_txt.becomeFirstResponder()
            return false
        }
        else if textField == email_txt {
            textField.resignFirstResponder()
            pass_txt.becomeFirstResponder()
            return false
        }
        else if textField == pass_txt {
            textField.resignFirstResponder()
            confirmpass_txt.becomeFirstResponder()
            return false
        }
        textField.resignFirstResponder()
        return true
    }
    
    
    // MARK: - Validation function
    func isValidData() -> Bool{
        
        if !(self.firstname_txt.text?.isValidfname())! {
            KVClass.ShowNotification("", "Please enter your first name!")
            return false
        }
        else if !(self.lastname_txt.text?.isValidfname())! {
            KVClass.ShowNotification("", "Please enter your last name!")
            return false
        }
        else if !(self.email_txt.text?.isValidfname())! {
            KVClass.ShowNotification("", "Please enter your email!")
            return false
        }
        else if !(self.email_txt.text?.isValidEmail())! {
            KVClass.ShowNotification("", "Please enter your valid email!")
            return false
        }
        else if !(self.pass_txt.text?.isValidfname())! {
            KVClass.ShowNotification("", "Please enter password!")
            return false
        }
        else if !(self.pass_txt.text?.isValidPasswordLength())! {
            KVClass.ShowNotification("", "Password must contain at least 6 characters!")
            return false
        }
        else if !(self.confirmpass_txt.text?.isValidfname())! {
            KVClass.ShowNotification("", "Please enter confirm password!")
            return false
        }
        else if !(self.pass_txt.text == self.confirmpass_txt.text) {
            KVClass.ShowNotification("", "Please enter correct confirm password!")
            return false
        }
        else if !self.Terms_btn.isSelected {
            KVClass.ShowNotification("", "Please Accept Terms of Use and Privacy Policy!")
            return false
        }
        
        return true
    }
    
    
    // MARK: UIButton Action Method
    @IBAction func signinpress(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SignupPress(_ sender: UIButton)
    {
        if isValidData() == false {
            return
        }
        else {
            self.view.endEditing(true)
            self.CallAPIforSignUp_User()
        }
    }
    

    
    func CallAPIforSignUp_User()
    {
        let dict = NSMutableDictionary()
        dict["firstname"] = self.firstname_txt.text
        dict["lastname"] = self.lastname_txt.text
        dict["email"] = self.email_txt.text
        dict["password"] = self.pass_txt.text
            
        KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict, api: "create_user", arrFlag: false, Hendler: { (JSON, status) in
                
            if status == 1
            {
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "EmailedVC") as! EmailedVC
                obj.EmailString = self.email_txt.text!
                self.navigationController?.present(obj, animated: true, completion: nil)
            }
        })
    }
    
    
    
    
    
    
    
    
    
    //================================================================================================================//
    //================================================================================================================//
    //================================================================================================================//
    //================================================================================================================//
    
    
    //MARK : Login With Facebook
    @IBAction func btnFacebookTapped(_ sender: Any) {
        
        let action = UIAlertAction.init(title: "Accept & Continue", style: UIAlertActionStyle.destructive) { (action) in
            
            DLFacebookKit.loginWithPostPermisson(contoller: self) { (JSON, err) in
                if let data = JSON as? NSDictionary
                {
                    
                    print(data)
                    
                    let dict = NSMutableDictionary()
                    dict["firstname"] = data["first_name"]
                    dict["lastname"] = data["last_name"]
                    
                    if data["email"] as? String ?? "" == ""
                    {
                        dict["email"] = data["id"] as? String ?? ""
                    }
                    else
                    {
                        dict["email"] = data["email"] as? String ?? ""
                    }
                    //dict["password"] = ""
                    dict["social_media_user_id"] = data["id"]
                    dict["social_media_id"] = "1"
                    
                    var strToken:String = ""
                    //UserDefaults.standard.set(fcmToken, forKey: "fcmtoken")
                    if UserDefaults.standard.object(forKey: "fcmtoken") != nil {
                        strToken = UserDefaults.standard.object(forKey: "fcmtoken") as! String
                    }
                    
                    let dictParameters:NSMutableDictionary
                        = ["social_media_user_id":data["id"] as Any , "social_media_id":"1", "device_type" : "iOS", "device_id":strToken, "data":["id":data["id"],"first_name":data["first_name"],"name": data["name"], "last_name":data["last_name"],"email":data["email"] as? String ?? "","friends":["data":[]]]]
                    
                    KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dictParameters, api: "create_user", arrFlag: false, Hendler: { (json, status) in
                        print(json)
                        
                        if let user = json["data"] as? NSDictionary
                        {
                            let UserDetailData = resultUserDict.init(user)
                            kUserDefults((user["id"] as? Int ?? 0) as AnyObject, key: "kLoginusrid")
                            KVAppDataServieces.shared.initializeAppData(user)
                            let myData = NSKeyedArchiver.archivedData(withRootObject: user)
                            UserDefaults.standard.set(myData, forKey: "session")
                            //  kUserDefults((user as? NSDictionary)!, key: KEYSPROJECT.resultUser)
                            let obj = self.storyboard?.instantiateViewController(withIdentifier: "SocialVC") as! SocialVC
                            obj.strComeFrom = "fb"
                            UserDefaults.standard.set("fb", forKey: "strcomefrom")
                            KVAppDataServieces.shared.update(.loginFrom, "Facebook")
                            obj.UserData = UserDetailData
                            self.navigationController?.pushViewController(obj, animated: true)
                        }
                    })
                }
            }
        }
        
        self.showTremsalert(action)
    }
    //================================================================================================================//
    //================================================================================================================//
    
    
    // MARK: Login with Linkdin
    
    @IBAction func btnLinkedInTapped(_ sender: Any) {
        
        let action = UIAlertAction.init(title: "Accept & Continue", style: UIAlertActionStyle.destructive) { (action) in
            
            self.linkedinHelper.authorizeSuccess({ [unowned self] (lsToken) -> Void in
                
                KVClass.KVShowLoader()
                
                self.linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),positions,date-of-birth,phone-numbers,location)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
                    
                    print("Request success with response: \(response.jsonObject)")
                    let strId = response.jsonObject["id"]
                    
                    print("Request success with response: \(response)")
                    
                    var strToken:String = ""
                    //UserDefaults.standard.set(fcmToken, forKey: "fcmtoken")
                    if UserDefaults.standard.object(forKey: "fcmtoken") != nil {
                        strToken = UserDefaults.standard.object(forKey: "fcmtoken") as! String
                    }
                    
                    let dictParameters:NSMutableDictionary
                        = ["social_media_user_id":strId ?? 0 , "social_media_id":"2", "device_id":strToken, "data": response.jsonObject, "device_type" : "iOS"]
                    
                    KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dictParameters, api: "create_user", arrFlag: false, Hendler: { (json, status) in
                        print(json)
                        KVClass.KVHideLoader()
                        if let user = json["data"] as? NSDictionary
                        {
                            let UserDetailData = resultUserDict.init(user)
                            kUserDefults((user["id"] as? Int ?? 0) as AnyObject, key: "kLoginusrid")
                            KVAppDataServieces.shared.initializeAppData(user)
                            let myData = NSKeyedArchiver.archivedData(withRootObject: user)
                            UserDefaults.standard.set(myData, forKey: "session")
                            //  kUserDefults((user as? NSDictionary)!, key: KEYSPROJECT.resultUser)
                            let obj = self.storyboard?.instantiateViewController(withIdentifier: "SocialVC") as! SocialVC
                            obj.strComeFrom = "linkedin"
                            UserDefaults.standard.set("linkedin", forKey: "strcomefrom")
                            KVAppDataServieces.shared.update(.loginFrom, "linkdin")
                            obj.UserData = UserDetailData
                            self.navigationController?.pushViewController(obj, animated: true)
                        }
                    })
                    
                    
                }) { [unowned self] (error) -> Void in
                    
                    print("Encounter error: \(error.localizedDescription)")
                }
                
                print("Login success lsToken: \(lsToken)")
                }, error: { [unowned self] (error) -> Void in
                    
                    print("Encounter error: \(error.localizedDescription)")
                    KVClass.ShowNotification("No Internet Connection", "Please Check your Internet Connection and try again")
                }, cancel: { [unowned self] () -> Void in
                    
                    print("User Cancelled!")
            })
        }
        
        self.showTremsalert(action)
    }
    //================================================================================================================//
    //================================================================================================================//
}
