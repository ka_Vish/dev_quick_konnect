//
//  SearchViewController.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 01/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import Alamofire


class SearchViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblData: UITableView!
    var strSearchFor = ""
    var strCountryId = ""
    var strStateId = ""
    
    var arrMainData:[[String:Any]] = []
    var arrData:[[String:Any]] = []
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tblData.delegate = self
        self.tblData.dataSource = self
        self.searchBar.delegate = self
        if strCountryId.count > 0 && strStateId.count > 0{
            lblTitle.text = "Select City"
            searchBar.placeholder = "Select City"
            getCityData()
        }else if strCountryId.count > 0 {
            lblTitle.text = "Select State"
            searchBar.placeholder = "Select State"
            getStateData()
        } else {
            lblTitle.text = "Select Country"
            searchBar.placeholder = "Select Country"
            self.getCountryData()
        }
    }
    
    func getCityData() {
        
        KVClass.KVShowLoader()
        
        Alamofire.request("\(self.getPrefixURL())get_cities", method: .post, parameters: ["country_id":strCountryId, "state_id":strStateId], encoding: JSONEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
            KVClass.KVHideLoader()
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    self.arrData = (response.result.value as! [String:Any])["data"] as! [[String : Any]]
                    self.arrMainData = self.arrData
                    
                    self.tblData.reloadData()
                } else {
                    self.showAlert(Message: "There is problem to connecting server. Please Try Again!")
                }
                break
                
            case .failure(_):
                
                self.showAlert(Message: (response.result.error?.localizedDescription)!)
                
                break
            }
        }
        
    }
    
    
    func getStateData() {
        
        KVClass.KVShowLoader()
        
        Alamofire.request("\(self.getPrefixURL())get_states", method: .post, parameters: ["country_code":strCountryId], encoding: JSONEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
            KVClass.KVHideLoader()
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    self.arrData = (response.result.value as! [String:Any])["data"] as! [[String : Any]]
                    self.arrMainData = self.arrData
                    
                    self.tblData.reloadData()
                } else {
                    self.showAlert(Message: "There is problem to connecting server. Please Try Again!")
                }
                break
                
            case .failure(_):
                self.showAlert(Message: (response.result.error?.localizedDescription)!)
                break
            }
        }
        
        
    }
    
    
    
    func getCountryData() {
        KVClass.KVShowLoader()
        
        Alamofire.request("\(self.getPrefixURL())get_countries", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
           KVClass.KVHideLoader()
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    self.arrData = (response.result.value as! [String:Any])["data"] as! [[String : Any]]
                    self.arrMainData = self.arrData
                    
                    self.tblData.reloadData()
                } else {
                    self.showAlert(Message: "There is problem to connecting server. Please Try Again!")
                }
                break
                
            case .failure(_):
                self.showAlert(Message: (response.result.error?.localizedDescription)!)
                break
            }
        }
    }
    
    func showAlert(Message strmsg:String) {
        let alertView = UIAlertController(title: "", message: strmsg, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SearchViewController: UISearchBarDelegate {
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        if searchText == "" {
            arrData = arrMainData
        } else {
            arrData = filter(array: arrMainData, byString: searchText)
            print(arrData)
        }
        
        tblData.reloadData()
    }
    
    func filter(array : [[String : Any]], byString filterString : String) -> [[String : Any]] {
        return array.filter{
            dictionary in
            return (dictionary["name"] as! String).lowercased().range(of: filterString.lowercased()) != nil }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        arrData = arrMainData
        tblData.reloadData()
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "customCell") as! customCell
        cell.lblTitle.text = self.arrData[indexPath.row]["name"] as? String ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dictData:[String:Any] = arrData[indexPath.row]
        if strCountryId.count > 0 && strStateId.count > 0{
            UserDefaults.standard.set(String("\(dictData["id"] as! Int)"), forKey: "cityid")
            UserDefaults.standard.set(String("\(dictData["name"] as! String)"), forKey: "cityname")
        }else if strCountryId.count > 0 {
            UserDefaults.standard.set(String("\(dictData["id"] as! Int)"), forKey: "stateid")
            UserDefaults.standard.set(String("\(dictData["name"] as! String)"), forKey: "statename")
        } else {
            UserDefaults.standard.set(String("\(dictData["code"] as! Int)"), forKey: "countryid")
            UserDefaults.standard.set(String("\(dictData["name"] as! String)"), forKey: "countryname")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}

class customCell:UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
}
