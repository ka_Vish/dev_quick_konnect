//
//  SelectCategoryVC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 19/04/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import RealmSwift


protocol SetSelectedCategoryDelegate {
    func closeKeyboardFromCloseCategory()
    func setCategoryValue(_ category_ID: Int, _ category_NAME: String)
}


class SelectCategoryVC: UIViewController {

    @IBOutlet var bar_Search: UISearchBar!
    @IBOutlet var tab_Category: UITableView!
    
    var delegate: SetSelectedCategoryDelegate?
    
    var arrCategory: [CategoryDataList]! = [CategoryDataList]()
    var arrFilterCategory: [CategoryDataList]! = [CategoryDataList]()
    var arr_LocalPass_Category: Results<RealmCategoryList>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        
        arrCategory.removeAll()
        arrFilterCategory.removeAll()
        for item in arr_LocalPass_Category
        {
            let dic = ["id" : item.id,
                       "name" : item.name] as NSDictionary
            arrCategory.append(CategoryDataList.init(dic))
            arrFilterCategory.append(CategoryDataList.init(dic))
        }
        
        self.tab_Category.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Button action
    
    @IBAction func closePress(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
        self.delegate?.closeKeyboardFromCloseCategory()
        
    }

   

}



//=============================================================================================================//
//=============================================================================================================//
//=============================================================================================================//
// MARK : Followers User Search
extension SelectCategoryVC:UISearchBarDelegate {
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        self.arrCategory = searchText.isEmpty ? self.arrFilterCategory : self.arrFilterCategory.filter { (item) -> Bool in
                // If dataItem matches the searchText, return true to include it
                return item.name.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tab_Category.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.arrCategory = self.arrFilterCategory
        self.view.endEditing(true)
        tab_Category.reloadData()
    }
    
    
}
//=============================================================================================================//
//=============================================================================================================//
//=============================================================================================================//


//// MARK: - Searchbar delegate
//
//extension SelectCategoryVC: UISearchBarDelegate{
//
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//
//        self.arrCategory = searchText.isEmpty ? self.arrFilterCategory : self.arrFilterCategory.filter({ (iteam) -> Bool in
//            return iteam.name.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
//        })
//
//        self.tab_Category.reloadData()
//    }
//}

// MARK: - tableview delegate

extension SelectCategoryVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CategoryCell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        cell.lbl_name.text = self.arrCategory[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
            self.delegate?.setCategoryValue(self.arrCategory[indexPath.row].id, "\(self.arrCategory[indexPath.row].name)")
        }
    }
}

// MARK: - Recipient Cell

class CategoryCell: UITableViewCell{
    
    @IBOutlet var lbl_name: UILabel!
}

