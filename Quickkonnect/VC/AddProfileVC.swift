//
//  AddProfileVC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 16/02/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import Material
import GooglePlaces
import DLRadioButton
import GooglePlacePicker
import RealmSwift
import CoreTelephony


class ContactInfoDataList: NSObject {
    var name = ""
    
    convenience init(_ dict: NSDictionary)
    {
        self.init()
        self.name = dict["name"] as? String ?? ""
    }
    
}

class KVProfileCreate {
    private init() {}
    static let shared = KVProfileCreate()
    var DataDict = NSMutableDictionary()
    var arr_OtherAddress: [NSDictionary]! = [NSDictionary]()
    var arr_ContactInfo: [NSDictionary]! = [NSDictionary]()
    var arr_EmailInfo: [NSDictionary]! = [NSDictionary]()
    var arr_WebInfo: [NSDictionary]! = [NSDictionary]()
    var arr_SocialMedia: [NSDictionary]! = [NSDictionary]()
    var arr_Founders: [NSDictionary]! = [NSDictionary]()
    var arr_Employees: [NSDictionary]! = [NSDictionary]()
    var arr_Badges: [NSDictionary]! = [NSDictionary]()
    var arr_Profile: [NSDictionary]! = [NSDictionary]()
    
    
    enum KVKey: String {
        case id = "id"
        case user_id = "user_id"
        case type = "type"
        case type_name = "type_name"
        case name = "name"
        case address = "address"
        case latitude = "latitude"
        case longitude = "longitude"
        case tag = "tag"
        case sub_tag = "sub_tag"
        case cat_name = "cat_name"
        case sub_cat_name = "sub_cat_name"
        case logo = "logo"
        case about = "about"
        case establish_date = "establish_date"
        case contact_phone = "contact_phone"
        case phone = "phone"
        case email = "email"
        case weblink = "weblink"
        case social_media = "social_media"
        case contact_email = "contact_email"
        case founders = "founders"
        case other_address = "otheraddress"
        case contact_text = "contact_text"
        case contact_weblink = "contact_weblink"
        case company_team_size = "company_team_size"
        case DOB = "dob"
        case current_ceo = "current_ceo"
        case current_cfo = "current_cfo"
    }
    
    func setValue(_ value: Any?, _ key: KVKey)
    {
        KVProfileCreate.shared.DataDict.setValue(value, forKey: key.rawValue)
    }
    
    func removeKeyValue( _ key: KVKey) {
        KVProfileCreate.shared.DataDict.removeObject(forKey: key.rawValue)
        KVProfileCreate.shared.DataDict.setValue(nil, forKey: key.rawValue)
    }
    
    func isValueAvailable(_ key: KVKey) -> Bool
    {
        let dicti = KVProfileCreate.shared.DataDict
        
        if dicti[key.rawValue] != nil {
            return true
        }
        return false
    }
    
    
    func initialize(_ dict: NSDictionary)
    {
        KVProfileCreate.shared.setValue(dict["id"] as? Int ?? 0, .id)
        KVProfileCreate.shared.setValue(dict["user_id"] as? Int ?? 0, .user_id)
        KVProfileCreate.shared.setValue(dict["name"] as? String ?? "", .name)
        KVProfileCreate.shared.setValue(dict["type"] as? Int ?? 0, .type)
        KVProfileCreate.shared.setValue(dict["tag"] as? Int ?? 0, .tag)
        KVProfileCreate.shared.setValue(dict["sub_tag"] as? Int ?? 0, .sub_tag)
        KVProfileCreate.shared.setValue(dict["type_name"] as? Int ?? 0, .type_name)
        KVProfileCreate.shared.setValue(dict["cat_name"] as? Int ?? 0, .cat_name)
        KVProfileCreate.shared.setValue(dict["sub_cat_name"] as? Int ?? 0, .sub_cat_name)
        
        if self.isvalidURL(dict["logo"] as? String ?? "")
        {
          KVProfileCreate.shared.setValue(dict["logo"] as? String ?? "", .logo)
        }
        KVProfileCreate.shared.setValue(dict["about"] as? String ?? "", .about)
        KVProfileCreate.shared.setValue(dict["address"] as? String ?? "Address*", .address)
        KVProfileCreate.shared.setValue(dict["company_team_size"] as? Int ?? 0, .company_team_size)
        KVProfileCreate.shared.setValue(dict["establish_date"] as? String ?? "", .establish_date)
        KVProfileCreate.shared.setValue(dict["phone"] as? [NSDictionary] ?? [], .phone)
        KVProfileCreate.shared.setValue(dict["email"] as? [NSDictionary] ?? [], .email)
        KVProfileCreate.shared.setValue(dict["weblink"] as? [NSDictionary] ?? [], .weblink)
        KVProfileCreate.shared.setValue(dict["social_media"] as? [NSDictionary] ?? [], .social_media)
        KVProfileCreate.shared.setValue(dict["founders"] as? [NSDictionary] ?? [], .founders)
        KVProfileCreate.shared.setValue(dict["otheraddress"] as? [NSDictionary] ?? [], .other_address)
    }
    
    func isvalidURL(_ string: String) -> Bool
    {
        let url = URL.init(string: string)
        
        return UIApplication.shared.canOpenURL(url!)
    }
    
}


class CategoryDataList: NSObject {
    var name = ""
    var id = 0
    var subname = ""
    var subid = 0
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        self.name = dict["name"] as? String ?? ""
        self.id = dict["id"] as? Int ?? 0
        self.subname = dict["name"] as? String ?? ""
        self.subid = dict["id"] as? Int ?? 0
    }
}
class SubcategoryDataList: NSObject {
    
    var subname = ""
    var subid = 0
    
    convenience init(_ dict: NSDictionary) {
        self.init()
    
        self.subname = dict["name"] as? String ?? ""
        self.subid = dict["id"] as? Int ?? 0
    }
}

class AddProfileVC: UIViewController {

    @IBOutlet var Pro_collect: UICollectionView!
    var currentPage = 0
    var cellsize: CGSize?
    var CategoryID = 0
    var CategoryList: [CategoryDataList]! = [CategoryDataList]()
    var dict_userdata: resultUserDict! = resultUserDict()
    var isedit = false
    var int_profileid = 0
    var strScreenFrom = ""
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        app_Delegate.isBackChange = false
        app_Delegate.strSelectedTermsCondition = ""
        self.cellsize = self.Pro_collect.bounds.size
        
        self.dict_userdata = resultUserDict.init([:])
        KVProfileCreate.shared.arr_ContactInfo.removeAll()
        KVProfileCreate.shared.arr_EmailInfo.removeAll()
        KVProfileCreate.shared.arr_WebInfo.removeAll()
        KVProfileCreate.shared.arr_Founders.removeAll()
        KVProfileCreate.shared.arr_SocialMedia.removeAll()
        
        
        
//        if isedit
//        {
//            let dict_param = NSMutableDictionary()
//            dict_param["profile_id"] = self.int_profileid
//            
//            KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "Please Wait..", params: dict_param, api: "profile/profile_view", arrFlag: false, Hendler: { (JSON, status) in
//                
//                if status == 1
//                {
//                   if let datadict = JSON["data"] as? NSDictionary
//                   {
//                    KVProfileCreate.shared.initialize(datadict)
//                    }
//                }
//                
//            })
//        }
        
      // self.getCategoryList()
        
        self.Pro_collect.reloadData()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("MAINTABLERELOAD"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.Pro_collect.reloadData()
    }
    
    
    
//    func getCategoryList()
//    {
//        let dict = NSMutableDictionary()
//        dict["parent_id"] = self.CategoryID
//        KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict, api: "profile/tag_list", arrFlag: false) { (json, status) in
//
//            if status == 1
//            {
//                if let datadict = json["data"] as? [NSDictionary]
//                {
//                    self.CategoryList.removeAll()
//                    for item in datadict
//                    {
//                        self.CategoryList.append(CategoryDataList.init(item))
//                    }
//                    self.Pro_collect.reloadData()
//                }
//            }
//        }
//    }
    
    
    @IBAction func backpress(_ sender: UIButton)
    {
        if self.currentPage == 0
        {
            KVProfileCreate.shared.DataDict.removeAllObjects()
            KVProfileCreate.shared.arr_ContactInfo.removeAll()
            KVProfileCreate.shared.arr_WebInfo.removeAll()
            KVProfileCreate.shared.arr_Founders.removeAll()
            KVProfileCreate.shared.arr_EmailInfo.removeAll()
            KVProfileCreate.shared.arr_SocialMedia.removeAll()
            KVProfileCreate.shared.arr_OtherAddress.removeAll()
          self.navigationController?.popViewController(animated: true)
        }
        else
        {
            let Discart = UIAlertController.init(title: "", message: "", preferredStyle: .alert)

            let attributedTitle = NSMutableAttributedString(string: "Warning !", attributes:
                [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])

            let attributedMessage = NSMutableAttributedString(string: "Do you really want Discard Changes ?", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
            Discart.setValue(attributedTitle, forKey: "attributedTitle")
            Discart.setValue(attributedMessage, forKey: "attributedMessage")

            let ok = UIAlertAction.init(title: "Yes", style: .destructive, handler: { (action) in
            
                self.currentPage = 0
//
                KVProfileCreate.shared.DataDict.removeAllObjects()
                KVProfileCreate.shared.arr_ContactInfo.removeAll()
                KVProfileCreate.shared.arr_WebInfo.removeAll()
                KVProfileCreate.shared.arr_Founders.removeAll()
                KVProfileCreate.shared.arr_EmailInfo.removeAll()
                KVProfileCreate.shared.arr_SocialMedia.removeAll()
                KVProfileCreate.shared.arr_OtherAddress.removeAll()
//                let index = IndexPath.init(item: self.currentPage, section: 0)
//                self.Pro_collect.scrollToItem(at: index, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
                self.navigationController?.popViewController(animated: true)
                
                
            })

            let no = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                Discart.dismiss(animated: true, completion: nil)
            })

            Discart.addAction(ok)
            Discart.addAction(no)

            self.present(Discart, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func nextPress(_ sender: UIButton)
    {
        self.view.endEditing(true)
        sender.setTitle("Next", for: .normal)
        
        let arrTypeofMain = ["Company","Enterprise","Public Figure/Celebrity"]
        
        if self.currentPage < self.Pro_collect.numberOfItems(inSection: 0) - 1
        {
            if self.currentPage == 1 && !KVProfileCreate.shared.isValueAvailable(.name) || KVProfileCreate.shared.DataDict["name"] as? String == "" || KVProfileCreate.shared.DataDict["name"] as? String == " " {
                KVClass.ShowNotification("", "Please enter Profile name !")
                return
            }
            else if self.currentPage == 1 && (!KVProfileCreate.shared.isValueAvailable(.type_name) || !KVProfileCreate.shared.isValueAvailable(.type)) || KVProfileCreate.shared.DataDict["type_name"] as? String == "" || KVProfileCreate.shared.DataDict["type_name"] as? String == " "
            {
                KVClass.ShowNotification("", "Please Select Profile Type !")
                return
            }
            else if self.currentPage == 1 && !(arrTypeofMain.contains(KVProfileCreate.shared.DataDict["type_name"] as? String ?? "")) {
                KVClass.ShowNotification("", "Please Select Proper Profile Type !")
                return
            }
            
            if self.currentPage == 2 && (!KVProfileCreate.shared.isValueAvailable(.tag))
            {
                KVClass.ShowNotification("", "Please Select Category !")
                return
            }
            else if self.currentPage == 2 && (!KVProfileCreate.shared.isValueAvailable(.cat_name) || KVProfileCreate.shared.DataDict["cat_name"] as? String == "" || KVProfileCreate.shared.DataDict["cat_name"] as? String == " ")
            {
                KVClass.ShowNotification("", "Please Select Category !")
                return
            }
            
            //===============================//
            var arr_LocalCat = (RealmServieces.shared.realm.objects(RealmCategoryList.self).filter(NSPredicate.init(format: "parent_id == %d", KVProfileCreate.shared.DataDict["type"] as? Int ?? 0))).flatMap{($0.name)}
            
            if KVProfileCreate.shared.DataDict["type"] as? Int ?? 0 == 1 || KVProfileCreate.shared.DataDict["type"] as? Int ?? 0 == 2 {
                
                arr_LocalCat = (RealmServieces.shared.realm.objects(RealmCategoryList.self).filter(NSPredicate.init(format: "parent_id == 1"))).flatMap{($0.name)}
                
            }
            
            
            if self.currentPage == 2 && !(arr_LocalCat.contains(KVProfileCreate.shared.DataDict["cat_name"] as? String ?? "")) {
                KVClass.ShowNotification("", "Please Select Proper Category !")
                return
            }
            
            if self.currentPage == 2 && (!KVProfileCreate.shared.isValueAvailable(.sub_tag))
            {
                KVClass.ShowNotification("", "Please Select Subcategory !")
                return
            }
            
            self.currentPage += 1
            
            let index = IndexPath.init(item: self.currentPage, section: 0)
            self.Pro_collect.scrollToItem(at: index, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
            
            print(KVProfileCreate.shared.DataDict)
        }
        else
        {
            if  !KVProfileCreate.shared.isValueAvailable(KVProfileCreate.KVKey.about) {
                
                KVClass.ShowNotification("", "Please Enter About!")
                return
                
            }
        if  !KVProfileCreate.shared.isValueAvailable(KVProfileCreate.KVKey.address) {
            
            KVClass.ShowNotification("", "Please Select Address!")
            return
            
        }
        else if KVProfileCreate.shared.arr_EmailInfo.count == 0 {
            KVClass.ShowNotification("", "Please Enter Email Address!")
            return
        }
        else if KVProfileCreate.shared.DataDict["type"] as? Int == 3 &&  !KVProfileCreate.shared.isValueAvailable(.DOB){
             KVClass.ShowNotification("", "Please enter your Date of Birth!")
        }
        
        else if KVProfileCreate.shared.DataDict["type"] as? Int == 3 {
            if app_Delegate.strSelectedTermsCondition == "false" || app_Delegate.strSelectedTermsCondition == "" {
                KVClass.ShowNotification("", "Please select representing the person whom you are signing up !")
            }
            else {
                self.CreateProfileAPICalling()
            }
        }
        else {
            self.CreateProfileAPICalling()
        }
        
        }
        
    }
    
    
    func CreateProfileAPICalling() {
        KVProfileCreate.shared.setValue(KVProfileCreate.shared.arr_ContactInfo, KVProfileCreate.KVKey.phone)
        KVProfileCreate.shared.setValue(KVProfileCreate.shared.arr_EmailInfo, KVProfileCreate.KVKey.email)
        KVProfileCreate.shared.setValue(KVProfileCreate.shared.arr_WebInfo, KVProfileCreate.KVKey.weblink)
        KVProfileCreate.shared.setValue(KVProfileCreate.shared.arr_Founders, .founders)
        KVProfileCreate.shared.setValue(KVProfileCreate.shared.arr_SocialMedia, .social_media)
        if self.dict_userdata.id != 0
        {
            KVProfileCreate.shared.setValue(self.dict_userdata.id, .id)
        }

        KVProfileCreate.shared.setValue(KVProfileCreate.shared.arr_OtherAddress, .other_address)

        KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: KVProfileCreate.shared.DataDict, api: "profile/create", arrFlag: false, Hendler: { (JSON, status) in
                
            print(JSON)
            if status == 1
            {
                if let datadict = JSON["data"] as? NSDictionary
                {
                    self.dict_userdata = resultUserDict.init(datadict)
                    
                    DispatchQueue.main.async {
                        let strBase64Image = (UIImageJPEGRepresentation(#imageLiteral(resourceName: "logo"), 0.25)?.base64EncodedString())!
                        
                        //Push Update QK Tag VC
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateQKTagViewController") as! UpdateQKTagViewController
                        vc.issubProfile = true
                        
                        if self.strScreenFrom == "Slider" {
                            vc.strScreenFrom = "Slider"
                        }
                        else {
                            vc.strScreenFrom = "CreateProfile"
                        }
                        vc.strMiddleBase64 = strBase64Image
                        vc.dict_subuser_data = self.dict_userdata
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        })
    }

    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        var scrollViewFrame: CGRect = self.Pro_collect.frame
        // Begin animation
        
        scrollViewFrame.size.height += (keyboardViewEndFrame.height)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
           
           // self.Pro_collect.contentInset = UIEdgeInsets.zero
           // self.Pro_collect.contentOffset = .zero
        } else {
          //  self.Pro_collect.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
//
//       // self.Pro_collect.scrollIndicatorInsets = self.Pro_collect.contentInset
//
//        let selectedRange = CGRect.init(center: CGPoint.init(x: self.Pro_collect.contentInset.top, y: self.Pro_collect.contentInset.left), size: self.Pro_collect.contentSize)
       // self.Pro_collect.scrollRectToVisible(scrollViewFrame, animated: true)
    }
}


//=================================================================================================//
//====================CollectionView Delegate and Datasource Method================================//

extension AddProfileVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {

            //cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
            //UIView.animate(withDuration: 0.3, animations: {
                let mycell = collectionView.cellForItem(at: indexPath) as! FirstProfileCell
                mycell.NumbersOfSection = indexPath.row
                // cell.Main_tab.endUpdates()
                mycell.Main_tab.reloadData()
                if indexPath.row == 3
                {
                    mycell.configureCell()
                }
               // cell.transform = CGAffineTransform.identity
            //}) { (finished) in
           // }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.Pro_collect.frame.size.width, height: self.Pro_collect.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: FirstProfileCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FirstProfileCell", for: indexPath) as! FirstProfileCell
        
        return cell
    }
    
}
//=================================================================================================//
//=================================================================================================//



//=================================================================================================//
//============Custom First Profile Collection Cell ================================================//

class FirstProfileCell: UICollectionViewCell {
    //For Country Code
    var str_email = ""
    var strF_name = ""
    var strL_name = ""
    var str_designation = ""
    
    var strPhone_ADD = ""
    var strTypedPhone = ""
    var strSelectCountryCode = ""
    var PhoneSenderTag: Int = 0
    
    @IBOutlet var Main_tab: UITableView!
    var NumbersOfSection = 0
    var SocialAlert: UIAlertController?
    var arrType = ["Facebook","Linkedin","Twitter","Instagram"]
    var arr_HeaderTitle = [String]()
    var CategoryList: [CategoryDataList]! = [CategoryDataList]()
    var arr_SectionNumber = [[String : Any]]()
    var socialMediaType = 1
    var isUserProfile = KVProfileCreate.shared.DataDict["type"] as? Int == 3 ? true : false
    
    
    enum deleteInfoKey: String {
        case phone = "phone"
        case email = "email"
        case weblink = "weblink"
        case social = "social"
        case founders = "founders"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.Main_tab.delegate = self
        self.Main_tab.dataSource = self
        
        self.Main_tab.estimatedRowHeight = self.bounds.height
        self.Main_tab.estimatedSectionFooterHeight = 0
        self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("MAINTABLERELOAD"), object: nil)
        
        //Get Default Country Code
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print(countryCode)
            self.strSelectCountryCode = app_Delegate.CURRENT_COUNTRY_CODE
        }
        
    }
    
    
    func configureCell(){
       
        self.isUserProfile = KVProfileCreate.shared.DataDict["type"] as? Int == 3 ? true : false
        
        self.arr_HeaderTitle = self.isUserProfile ? ["SOCIAL MEDIA","PHONE","EMAIL","WEB LINK"] : ["OTHER ADDRESS","FOUNDERS", "SOCIAL MEDIA","PHONE","EMAIL","WEB LINK", "ACCEPT TERMS CONDITION"]
        
        self.arr_SectionNumber.append(["sec_id":"profil_info","title":"", "row":[1]])
//        if (KVProfileCreate.shared.DataDict["type"] as? Int) != 3  {
            self.arr_SectionNumber.append(["sec_id":"other_address","title":"OTHER ADDRESS", "row":NSArray()])
        //}
        
        if !isUserProfile{
            self.arr_SectionNumber.append(["sec_id":"founder","title":"FOUNDERS", "row":NSArray()])
            //self.arr_SectionNumber.append(["sec_id":"employees","title":"EMPLOYEES", "row":NSArray()])
        }
        self.arr_SectionNumber.append(["sec_id":"social_media","title":"SOCIAL MEDIA", "row":NSArray()])
        self.arr_SectionNumber.append(["sec_id":"phone","title":"PHONE", "row":NSArray()])
        self.arr_SectionNumber.append(["sec_id":"email","title":"EMAIL", "row":NSArray()])
        self.arr_SectionNumber.append(["sec_id":"web_link","title":"WEB LINK", "row":NSArray()])
        
        if KVProfileCreate.shared.DataDict["type"] as? Int == 3 {
            self.arr_SectionNumber.append(["sec_id":"accept_terms","title":"accept_terms", "row":NSArray()])
        }
        
        self.Main_tab.reloadData()
        
    }
    
    func methodOfReceivedNotification(notification: NSNotification){
        //Take Action on Notification
        self.Main_tab.reloadData()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.isUserProfile = KVProfileCreate.shared.DataDict["type"] as? Int == 3 ? true : false
    }
}
//=================================================================================================//
//=================================================================================================//


//=================================================================================================//
//===========Table View Delegate and Datasource Method For First Profile Cell======================//

extension FirstProfileCell: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource
{
    //===========================================================================================================//
    // MARK : UITextField Delegate Method
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.accessibilityHint == "name" || textField.accessibilityHint == "nameEmail" {
            if textField == self.SocialAlert?.textFields![0] {
                textField.resignFirstResponder()
                self.SocialAlert?.textFields![1].becomeFirstResponder()
                return false
            }
            else if textField == self.SocialAlert?.textFields![1] {
                textField.resignFirstResponder()
                self.SocialAlert?.textFields![2].becomeFirstResponder()
                return false
            }
            else if textField == self.SocialAlert?.textFields![2] {
                textField.resignFirstResponder()
                self.SocialAlert?.textFields![3].becomeFirstResponder()
                return false
            }
            textField.resignFirstResponder()
            return true
        }
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if (self.SocialAlert?.textFields!.count ?? 0)! > 0
        {
            if textField == self.SocialAlert?.textFields![0] && textField.tag == 1
            {
                if textField.text == nil || textField.text == ""
                {
                    textField.text = self.arrType[0]
                }
                self.settxtInputview(textField)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.accessibilityHint == "name" {
            let maxLength = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        
        else if textField.accessibilityHint == "phone" {
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_NUMBERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
    }
    
    func didChangeTextPhone() {
        let strPhone_Text = self.SocialAlert?.textFields![0].text ?? ""
        if !(strPhone_Text == "") {
            let strTest = strPhone_Text
            let index = strTest.index(strTest.startIndex, offsetBy: 0)
            let firstCharcter = strTest[index]
            if firstCharcter == "0" {
                self.SocialAlert?.textFields![0].text = ""
                return
            }
        }
    }
    //===========================================================================================================//
    //===========================================================================================================//
    //===========================================================================================================//
    
    
    @objc func editpress(_ sender: UIButton)
    {
        if sender.tag == 1
        {
        self.SocialAlert?.textFields![0].becomeFirstResponder()
        }
    }
    
    func settxtInputview(_ txtfield: UITextField)
    {
        let picker = UIPickerView.init()
        picker.delegate = self
        picker.dataSource = self
        picker.tag = txtfield.tag
        let toobar = UIToolbar()
        
        toobar.sizeToFit()
        
        if txtfield.tag != 0
        {
           // picker.selectRow(self.socialMediaType - 1, inComponent: 0, animated: true)
        }
        
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.doneTxt(_:)))
        doneButton.tag = txtfield.tag
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toobar.setItems([ spaceButton, doneButton], animated: false)
        toobar.isUserInteractionEnabled = true
        
        //  toobar.items = [UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil), UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.doneTxt(_:)))]
        if txtfield.tag != 0
        {
        txtfield.inputView = picker
        txtfield.inputAccessoryView = toobar
        txtfield.clearButtonMode = .never
        }
        else
        {
            txtfield.inputAccessoryView = toobar
        }
    }
    
    @IBAction func doneTxt(_ sender: UIBarButtonItem)
    {
        if sender.tag == 1
        {
            self.SocialAlert?.textFields![0].resignFirstResponder()
            return
        }
        self.SocialAlert?.textFields![0].resignFirstResponder()
        print("hy")
    }
    
    
    //===========================================================================================================//
    // MARK : UIPickerView Delegate datasource method
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrType.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrType[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
        if self.arrType.count > 0
        {
            if pickerView.tag == 1
            {
                self.SocialAlert?.textFields![0].text = arrType[row]
                self.socialMediaType = row + 1
            }
        }
    }
    //===========================================================================================================//
    //===========================================================================================================//
    //===========================================================================================================//
    
    
    func AddOtherAddress(_ sender: UIButton){
      
        DispatchQueue.main.async {
            
            let section = sender.tag / 100
            var row = sender.tag % 100
            row = row - 1
            print(section)
            print(row)
        
            var latitude = 0.0
            var longitude = 0.0
            if row < 0{
                latitude = Double(KVClass.shareinstance().Lattitude)!
                longitude = Double(KVClass.shareinstance().Longitude)!
            }
            else{
                latitude = Double(KVProfileCreate.shared.arr_OtherAddress[row]["lati"] as? CGFloat ?? 0)
                longitude = Double(KVProfileCreate.shared.arr_OtherAddress[row]["longi"] as? CGFloat ?? 0)
            }
            
            
            let center = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
            let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            let config = GMSPlacePickerConfig(viewport: viewport)
            let placePicker = GMSPlacePicker(config: config)
            
            placePicker.pickPlace(callback: {(place, error) -> Void in
                if let error = error {
                    print("Pick Place error: \(error.localizedDescription)")
                    return
                }
                
                if let place = place {
                
                    print(place.coordinate.latitude)
                    print(place.coordinate.longitude)
                    //print(place.
                    
                    if (place.formattedAddress ?? "") != ""
                    {
                        let dataDict = NSMutableDictionary()
                        dataDict["lati"] =  CGFloat(place.coordinate.latitude)
                        dataDict["longi"] = CGFloat(place.coordinate.longitude)
                        dataDict["name"] = place.formattedAddress
                        
                        if row < 0{
                            KVProfileCreate.shared.arr_OtherAddress.append(dataDict)
                        }
                        else{
                            KVProfileCreate.shared.arr_OtherAddress.remove(at: row)
                            KVProfileCreate.shared.arr_OtherAddress.insert(dataDict, at: row)
                        }
                        
                       self.Main_tab.reloadSections(IndexSet.init(integer: section), with: .fade)
                    }
                    
                    
                } else {
                    
                }
            })
            
        }
    }
    
    
    func AddFounderpress(_ sender: UIButton)
    {
        DispatchQueue.main.async {
            
        let section = sender.tag / 100
        var row = sender.tag % 100
        row = row - 1
        print(section)
        print(row)
        
        var str_action_title = ""
        if row < 0
        {
            str_action_title = "Add Founders"
        }
        else
        {
            str_action_title = "Edit Founders"
        }
        
        self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
        
        let attributedTitle = NSMutableAttributedString(string: str_action_title, attributes:
            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
        
        self.SocialAlert?.setValue(attributedTitle, forKey: "attributedTitle")
        self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.tag = 2
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.tintColor = UIColor.lightGray
            textfield.clearButtonMode = .whileEditing
            textfield.autocorrectionType = .no
            textfield.autocapitalizationType = .words
            textfield.returnKeyType = .next
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Firstname*"
            textfield.accessibilityHint = "name"
            if row > -1
            {
                textfield.text = KVProfileCreate.shared.arr_Founders[row]["firstname"] as? String ?? ""
            }
            if self.strPhone_ADD == "Exists" {
                textfield.text = self.strF_name
            }
            
        })
        self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.tintColor = UIColor.lightGray
            textfield.clearButtonMode = .whileEditing
            textfield.autocorrectionType = .no
            textfield.autocapitalizationType = .words
            textfield.returnKeyType = .next
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Lastname*"
            textfield.accessibilityHint = "name"
            if row > -1
            {
                textfield.text = KVProfileCreate.shared.arr_Founders[row]["lastname"] as? String ?? ""
            }
            if self.strPhone_ADD == "Exists" {
                textfield.text = self.strL_name
            }
        })
        self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.tintColor = UIColor.lightGray
            textfield.clearButtonMode = .whileEditing
            textfield.autocorrectionType = .no
            textfield.autocapitalizationType = .sentences
            textfield.returnKeyType = .next
            textfield.keyboardType = .emailAddress
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Email"
            textfield.accessibilityHint = "nameEmail"
            
            if row > -1
            {
                textfield.text = KVProfileCreate.shared.arr_Founders[row]["email"] as? String ?? ""
            }
            if self.strPhone_ADD == "Exists" {
                textfield.text = self.str_email
            }
        })
            self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
                
                //textfield.tag = 2
                textfield.borderStyle = .roundedRect
                textfield.delegate = self
                textfield.tintColor = UIColor.lightGray
                textfield.clearButtonMode = .whileEditing
                textfield.autocorrectionType = .no
                textfield.autocapitalizationType = .words
                textfield.returnKeyType = .done
                textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
                textfield.placeholder = "Designation*"
                textfield.accessibilityHint = "name"
                if row > -1
                {
                    textfield.text = KVProfileCreate.shared.arr_Founders[row]["designation"] as? String ?? ""
                }
                if self.strPhone_ADD == "Exists" {
                    textfield.text = self.str_designation
                }
                
            })
            
            
        let addaction = UIAlertAction.init(title: str_action_title, style: .destructive) { (action) in
            
            if self.SocialAlert?.textFields![0].text == "" || self.SocialAlert?.textFields![1].text == "" || self.SocialAlert?.textFields![3].text == ""
            {
                KVClass.ShowNotification("", "Please enter first name, last name and designation !")
                
                self.strPhone_ADD = "Exists"
                self.str_email = (self.SocialAlert?.textFields![2].text!)!
                self.strF_name = (self.SocialAlert?.textFields![0].text!)!
                self.strL_name = (self.SocialAlert?.textFields![1].text!)!
                self.str_designation = (self.SocialAlert?.textFields![3].text!)!
                self.AddFounderpress(sender)
                
                return
            }
            else if !(self.SocialAlert?.textFields![2].text == "") {
                if !(self.SocialAlert?.textFields![2].text?.isValidEmail())!{
                    
                    KVClass.ShowNotification("", "Enter valid Email !")
                    self.strPhone_ADD = "Exists"
                    self.str_email = (self.SocialAlert?.textFields![2].text!)!
                    self.strF_name = (self.SocialAlert?.textFields![0].text!)!
                    self.strL_name = (self.SocialAlert?.textFields![1].text!)!
                    self.str_designation = (self.SocialAlert?.textFields![3].text!)!
                    self.AddFounderpress(sender)
                    
                    return
                }
            }
            else {
                let dict = NSMutableDictionary()
                dict["firstname"] = self.SocialAlert?.textFields![0].text
                dict["lastname"] = self.SocialAlert?.textFields![1].text
                dict["designation"] = self.SocialAlert?.textFields![3].text
                dict["email"] = self.SocialAlert?.textFields![2].text
                if row < 0
                {
                    KVProfileCreate.shared.arr_Founders.append(dict)
                }
                else
                {
                    KVProfileCreate.shared.arr_Founders.remove(at: row)
                    KVProfileCreate.shared.arr_Founders.insert(dict, at: row)
                }
                self.Main_tab.reloadSections(IndexSet.init(integer: section), with: .fade)
            }
            
            
        }
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            self.SocialAlert?.dismiss(animated: true, completion: nil)
        }
        
        self.SocialAlert?.addAction(addaction)
        self.SocialAlert?.addAction(cancel)
        
            if self.strPhone_ADD == "Exists" {
                appDelegate.window?.rootViewController?.present(self.SocialAlert!, animated: false, completion: nil)
            }
            else {
                appDelegate.window?.rootViewController?.present(self.SocialAlert!, animated: true, completion: nil)
            }
        
        
        for textfield: UIView in (self.SocialAlert?.textFields!)! {
            let container: UIView = textfield.superview!
            
            let effectView: UIView = container.superview!.subviews[0]
            container.backgroundColor = UIColor.clear
            effectView.removeFromSuperview()
        }
      }
    }
    
    
    
    
    func AddEmployeesPress(_ sender: UIButton) {
        KVClass.ShowNotification("", "Coming Soon !")
    }
    
    func AddEmailpress(_ sender: UIButton)
    {
        DispatchQueue.main.async {
            
            let section = sender.tag / 100
            var row = sender.tag % 100
            row = row - 1
            print(section)
            print(row)
            
            var str_action_title = ""
        
        if row < 0
        {
            str_action_title = "Add Email"
        }
        else
        {
            str_action_title = "Edit Email"
        }
        self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
        let attributedTitle = NSMutableAttributedString(string: str_action_title, attributes:
            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 16.5)!])
        
        self.SocialAlert?.setValue(attributedTitle, forKey: "attributedTitle")
       
        self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.tag = 0
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.tintColor = UIColor.lightGray
            textfield.clearButtonMode = .whileEditing
            textfield.autocorrectionType = .no
           // textfield.keyboardType = .numberPad
            textfield.returnKeyType = .done
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Email"
            if row > -1
            {
                textfield.text = KVProfileCreate.shared.arr_EmailInfo[row]["name"] as? String ?? ""
            }
        
            
            
        })
        let addaction = UIAlertAction.init(title: str_action_title, style: .destructive) { (action) in
            
            if !(self.SocialAlert?.textFields![0].text?.isValidEmail())!
            {
                KVClass.ShowNotification("", "Please enter valid Email!")
                return
            }
            if self.SocialAlert?.textFields![0].text == "" || self.SocialAlert?.textFields![0].text == nil
            {
                KVClass.ShowNotification("", "Please enter valid Email!")
                return
            }
            let dict = NSMutableDictionary()
            dict["name"] = self.SocialAlert?.textFields![0].text
            if row < 0
            {
                KVProfileCreate.shared.arr_EmailInfo.append(dict)
            }
            else
            {
                KVProfileCreate.shared.arr_EmailInfo.remove(at: row)
                KVProfileCreate.shared.arr_EmailInfo.insert(dict, at: row)
            }
            self.Main_tab.reloadSections(IndexSet.init(integer: section), with: .fade)
            
        }
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            self.SocialAlert?.dismiss(animated: true, completion: nil)
        }
        
        self.SocialAlert?.addAction(addaction)
        self.SocialAlert?.addAction(cancel)
        
        appDelegate.window?.rootViewController?.present(self.SocialAlert!, animated: true, completion: nil)
        
        for textfield: UIView in (self.SocialAlert?.textFields!)! {
            let container: UIView = textfield.superview!
            
            let effectView: UIView = container.superview!.subviews[0]
            container.backgroundColor = UIColor.clear
            effectView.removeFromSuperview()
        }
      }
    }
    func AddWebLinkpress(_ sender: UIButton)
    {
        DispatchQueue.main.async {
            
            let section = sender.tag / 100
            var row = sender.tag % 100
            row = row - 1
            print(section)
            print(row)
            
            var str_action_title = ""
            
        if row < 0
        {
            str_action_title = "Add Web Link"
        }
        else
        {
            str_action_title = "Edit Web Link"
        }
        self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
        let attributedTitle = NSMutableAttributedString(string: str_action_title, attributes:
            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 16.5)!])
        
        self.SocialAlert?.setValue(attributedTitle, forKey: "attributedTitle")
        self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.tag = 0
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.tintColor = UIColor.lightGray
            textfield.clearButtonMode = .whileEditing
            textfield.autocorrectionType = .no
            // textfield.keyboardType = .numberPad
            textfield.returnKeyType = .done
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Web Link"
            if row > -1
            {
                textfield.text = KVProfileCreate.shared.arr_WebInfo[row]["name"] as? String ?? ""
            }
            
        })
        let addaction = UIAlertAction.init(title: str_action_title, style: .destructive) { (action) in
            
            if self.SocialAlert?.textFields![0].text == "" || self.SocialAlert?.textFields![0].text == nil
            {
                KVClass.ShowNotification("", "Please enter valid Web Link!")
                return
            }
            let strUrl = self.SocialAlert?.textFields![0].text!
            if (self.SocialAlert?.textFields![0].text?.isValidateUrl(urlString: strUrl!))! {
            }
            else {
                KVClass.ShowNotification("", "Please Enter Proper URL")
                return
            }
            
            
            
            
            let dict = NSMutableDictionary()
            dict["name"] = self.SocialAlert?.textFields![0].text
            if row < 0
            {
                KVProfileCreate.shared.arr_WebInfo.append(dict)
            }
            else
            {
                KVProfileCreate.shared.arr_WebInfo.remove(at: row)
                KVProfileCreate.shared.arr_WebInfo.insert(dict, at: row)
            }
            self.Main_tab.reloadSections(IndexSet.init(integer: section), with: .fade)
            
        }
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            self.SocialAlert?.dismiss(animated: true, completion: nil)
        }
        
        self.SocialAlert?.addAction(addaction)
        self.SocialAlert?.addAction(cancel)
        
        appDelegate.window?.rootViewController?.present(self.SocialAlert!, animated: true, completion: nil)
        
        for textfield: UIView in (self.SocialAlert?.textFields!)! {
            let container: UIView = textfield.superview!

            let effectView: UIView = container.superview!.subviews[0]
            container.backgroundColor = UIColor.clear
            effectView.removeFromSuperview()
        }
      }
    }
    
    
    
    
   
    
    
    
    func AddPhonepress(_ sender: UIButton)
    {
        DispatchQueue.main.async {
            
            if self.strPhone_ADD == "NewAdd" {
                self.strSelectCountryCode = app_Delegate.CURRENT_COUNTRY_CODE
                self.PhoneSenderTag = sender.tag
            }
            
            let section = self.PhoneSenderTag / 100
            var row = self.PhoneSenderTag % 100
            row = row - 1
            print(section)
            print(row)
            
            var str_action_title = ""
        
            if row < 0
            {
                str_action_title = "Add Phone"
            }
            else
            {
                str_action_title = "Edit Phone"
            }
            
            self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
            let attributedTitle = NSMutableAttributedString(string: str_action_title, attributes:
                [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 16.5)!])
            
            self.SocialAlert?.setValue(attributedTitle, forKey: "attributedTitle")
            
            self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
                textfield.tag = 0
                textfield.borderStyle = .roundedRect
                textfield.delegate = self
                textfield.tintColor = UIColor.lightGray
                textfield.clearButtonMode = .whileEditing
                textfield.autocorrectionType = .no
                textfield.keyboardType = .numberPad
                textfield.returnKeyType = .next
                textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
                textfield.placeholder = "Phone"
                textfield.accessibilityHint = "phone"
                textfield.addTarget(self, action: #selector(self.didChangeTextPhone), for: UIControlEvents.editingChanged)
                
                let viewLeft = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 45, height: 30))
                viewLeft.backgroundColor = UIColor.clear
                let btnPick = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 45, height: 30))
                btnPick.setTitle(self.strSelectCountryCode, for: .normal)
                btnPick.setTitleColor(UIColor.black, for: .normal)
                btnPick.titleLabel?.font =  UIFont(name: "lato-Regular", size: 13)
                btnPick.addTarget(self, action: #selector(self.openCountryPicker), for: .touchUpInside)
                viewLeft.addSubview(btnPick)
            
                    textfield.leftView = viewLeft
                    textfield.leftViewMode = .always

                    if row > -1
                    {
                        self.strTypedPhone = KVProfileCreate.shared.arr_ContactInfo[row]["name"] as? String ?? ""
                        if self.strTypedPhone != "" {
                            let arrP = self.strTypedPhone.components(separatedBy: " ")
                            if arrP.count == 2 {
                                self.strSelectCountryCode = arrP.first!
                                self.strTypedPhone = arrP.last!
                                btnPick.setTitle(self.strSelectCountryCode, for: .normal)
                            }
                            else {
                                self.strTypedPhone = arrP.first!
                            }
                        }
                        textfield.text = self.strTypedPhone
                    
                }
                if self.strPhone_ADD == "AddCountry" {
                    textfield.text = self.strTypedPhone
                }

                self.settxtInputview(textfield)
        
            })
            let addaction = UIAlertAction.init(title: str_action_title, style: .destructive) { (action) in
                
                if !(self.SocialAlert?.textFields![0].text?.isPhoneValid())!
                {
                    KVClass.ShowNotification("", "Please enter valid Phone!")
                    self.strTypedPhone = self.SocialAlert?.textFields![0].text ?? ""
                    self.strPhone_ADD = "AddCountry"
                    self.AddPhonepress(sender)
                    return
                }
                if self.SocialAlert?.textFields![0].text == "" || self.SocialAlert?.textFields![0].text == nil
                {
                    return
                }
                let dict = NSMutableDictionary()

                dict["name"] = self.strSelectCountryCode + " " + (self.SocialAlert?.textFields![0].text)!
                if row < 0
                {
                KVProfileCreate.shared.arr_ContactInfo.append(dict)
                }
                else
                {
                    KVProfileCreate.shared.arr_ContactInfo.remove(at: row)
                    KVProfileCreate.shared.arr_ContactInfo.insert(dict, at: row)
                }
                self.Main_tab.reloadSections(IndexSet.init(integer: section), with: .fade)
                
            }
            let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
                self.SocialAlert?.dismiss(animated: true, completion: nil)
            }
        
            self.SocialAlert?.addAction(addaction)
            self.SocialAlert?.addAction(cancel)
        
            appDelegate.window?.rootViewController?.present(self.SocialAlert!, animated: true, completion: nil)
        
            for textfield: UIView in (self.SocialAlert?.textFields!)! {
                let container: UIView = textfield.superview!
            
                let effectView: UIView = container.superview!.subviews[0]
                container.backgroundColor = UIColor.clear
                effectView.removeFromSuperview()
            }
        }
    }
    
    func openCountryPicker(_ sender: UIButton) {
        self.strTypedPhone = self.SocialAlert?.textFields![0].text ?? ""
        print(self.strTypedPhone)
        
        self.SocialAlert?.dismiss(animated: false, completion: {
            app_Delegate.app_CountryPickpopup { (objCountry, isSuccess) in
                print(objCountry?.phoneCode ?? "")
                print(objCountry?.code ?? "")
                print(objCountry?.name ?? "")
                self.strPhone_ADD = "AddCountry"
                self.strSelectCountryCode = objCountry?.phoneCode ?? ""
                debugPrint("check this out!! [\(objCountry?.name! ?? "0")]")
                self.AddPhonepress(sender)
            }
        })
    }
    
    func AddSocialMediapress(_ sender: UIButton)
    {
        DispatchQueue.main.async {
            
            let section = sender.tag / 100
            var row = sender.tag % 100
            row = row - 1
            print(section)
            print(row)

        var str_action_title = ""
            
        if row < 0
        {
            str_action_title = "Add Social Media"
        }
        else
        {
            str_action_title = "Edit Social Media"
        }
        
        self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
        let attributedTitle = NSMutableAttributedString(string: str_action_title, attributes:
            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
        
        // let attributedMessage = NSMutableAttributedString(string: "Do you really want Discard Changes ?", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
        self.SocialAlert?.setValue(attributedTitle, forKey: "attributedTitle")
        // Discart.setValue(attributedMessage, forKey: "attributedMessage")
        self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.tag = 1
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.tintColor = .clear
            textfield.clearButtonMode = .never
            textfield.autocorrectionType = .no
            textfield.returnKeyType = .next
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Social Media Type"
            textfield.rightViewMode = .always
            if row > -1
            {
                textfield.text = self.arrType[(KVProfileCreate.shared.arr_SocialMedia[row]["type"] as? Int ?? 1) - 1]
            }
            
            let rightView = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 30, height: 30))
            rightView.setImage(UIImage.init(named: "downar_ic")!, for: .normal)
            rightView.tag = textfield.tag
            rightView.addTarget(self, action: #selector(self.editpress(_:)), for: .touchUpInside)
            textfield.rightView = rightView
            
        })
        self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.isHidden = true
        })
        self.SocialAlert?.addTextField(configurationHandler: { (textfield) in
            
            textfield.borderStyle = .roundedRect
            textfield.delegate = self
            textfield.clearButtonMode = .whileEditing
            textfield.autocorrectionType = .no
            textfield.tintColor = UIColor.lightGray
            
            textfield.returnKeyType = .done
            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
            textfield.placeholder = "Social Media URL"
            if row > -1
            {
                textfield.text = KVProfileCreate.shared.arr_SocialMedia[row]["name"] as? String ?? ""
            }
            
        })
        
        let addaction = UIAlertAction.init(title: str_action_title, style: .destructive) { (action) in
            
            if self.SocialAlert?.textFields![2].text == "" || self.SocialAlert?.textFields![2].text == nil
            {
                KVClass.ShowNotification("", "Please enter valid Media URL !")
                return
            }
            
            let strUrl = self.SocialAlert?.textFields![2].text!
            if (self.SocialAlert?.textFields![2].text?.isValidateUrl(urlString: strUrl!))! {
            }
            else {
                KVClass.ShowNotification("", "Please Enter Proper Media URL")
                return
            }
            
            
            let dict = NSMutableDictionary()
            dict["name"] = self.SocialAlert?.textFields![2].text
            dict["type"] = self.socialMediaType//self.SocialAlert?.textFields![0].text
            if row < 0
            {
                for item in KVProfileCreate.shared.arr_SocialMedia
                {
                    if item["type"] as? Int ?? 1 == self.socialMediaType
                    {
                        KVClass.ShowNotification("", "This Social Media is already Added !")
                        return
                    }
                }
                
                KVProfileCreate.shared.arr_SocialMedia.append(dict)
            }
            else
            {
               KVProfileCreate.shared.arr_SocialMedia.remove(at: row)
               KVProfileCreate.shared.arr_SocialMedia.insert(dict, at: row)
            }
            self.Main_tab.reloadSections(IndexSet.init(integer: section), with: .fade)
        
        }
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            self.SocialAlert?.dismiss(animated: true, completion: nil)
        }
        
        self.SocialAlert?.addAction(addaction)
        self.SocialAlert?.addAction(cancel)
        
        appDelegate.window?.rootViewController?.present(self.SocialAlert!, animated: true, completion: nil)
        
        for textfield: UIView in (self.SocialAlert?.textFields!)! {
            let container: UIView = textfield.superview!
            
            let effectView: UIView = container.superview!.subviews[0]
            container.backgroundColor = UIColor.clear
            effectView.removeFromSuperview()
        }
      }
    }
    
    //MARK:- TABLVIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.NumbersOfSection == 3
        {
            return self.arr_SectionNumber.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.NumbersOfSection == 3
        {
            let dict = self.arr_SectionNumber[section]
            let strID = dict["sec_id"] as? String ?? ""
            
            switch strID{
            case "profil_info":
                return 1
            case "phone":
                    if KVProfileCreate.shared.arr_ContactInfo.count == 0
                    {
                        return 1
                    }
                    return KVProfileCreate.shared.arr_ContactInfo.count + 1
            case "email":
                    if KVProfileCreate.shared.arr_EmailInfo.count == 0
                    {
                        return 1
                    }
                    return KVProfileCreate.shared.arr_EmailInfo.count + 1
            case "web_link":
                    if KVProfileCreate.shared.arr_WebInfo.count == 0
                    {
                        return 1
                    }
                    return KVProfileCreate.shared.arr_WebInfo.count + 1
            case "social_media":
                
                    if KVProfileCreate.shared.arr_SocialMedia.count == 0
                    {
                        return 1
                    }
                    return KVProfileCreate.shared.arr_SocialMedia.count + 1
            case "founder":
                    if KVProfileCreate.shared.arr_Founders.count == 0
                    {
                        return 1
                    }
                    return KVProfileCreate.shared.arr_Founders.count + 1
            case "employees":
                if KVProfileCreate.shared.arr_Employees.count == 0
                {
                    return 1
                }
                return KVProfileCreate.shared.arr_Employees.count + 1
            case "other_address":
                if KVProfileCreate.shared.arr_OtherAddress.count == 0
                {
                    return 1
                }
                return KVProfileCreate.shared.arr_OtherAddress.count + 1
            case "accept_terms":
                return 1
            default:
                return 0
            }
        }
        return 1
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        if self.NumbersOfSection == 3
//        {
//
//
//            switch indexPath.section{
//            case 0:
//                return UITableViewAutomaticDimension
//            case 3:
//                if KVProfileCreate.shared.arr_ContactInfo.count != 0
//                {
//                    return 50
//                }
//                return 120
//            case 4:
//                if KVProfileCreate.shared.arr_EmailInfo.count != 0
//                {
//                    return 50
//                }
//                return 120
//            case 5:
//                if KVProfileCreate.shared.arr_WebInfo.count != 0
//                {
//                    return 50
//                }
//                return 120
//            case 6:
//                if KVProfileCreate.shared.arr_OtherAddress.count != 0
//                {
//                    return 50
//                }
//                return 120
//            case 2:
//                if KVProfileCreate.shared.arr_SocialMedia.count != 0
//                {
//                    if indexPath.row == 0
//                    {
//                    return 50
//                    }
//                    return 83
//                }
//                return 120
//            case 1:
//                if KVProfileCreate.shared.arr_Founders.count != 0
//                {
//                    if indexPath.row == 0
//                    {
//                        return 50
//                    }
//                    return 83
//                }
//                return 120
//            default:
//                return 0
//            }
//        }
//        return self.bounds.height
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.NumbersOfSection == 3
        {
            let dict = self.arr_SectionNumber[indexPath.section]
            let strID = dict["sec_id"] as? String ?? ""
            
            switch strID{
            case "profil_info":
                return UITableViewAutomaticDimension
            case "phone":
                if KVProfileCreate.shared.arr_ContactInfo.count != 0
                {
                 return UITableViewAutomaticDimension
                }
                return 120
            case "email":
                if KVProfileCreate.shared.arr_EmailInfo.count != 0
                {
                return UITableViewAutomaticDimension
                }
                return 120
            case "web_link":
                if KVProfileCreate.shared.arr_WebInfo.count != 0
                 {
                    return UITableViewAutomaticDimension
                 }
                 return 120
            case "social_media":
                
                if KVProfileCreate.shared.arr_SocialMedia.count != 0
                {
                    if indexPath.row == 0
                    {
                        return 50
                    }
                    return UITableViewAutomaticDimension
                }
                return 120

            case "founder":
                if KVProfileCreate.shared.arr_Founders.count != 0
                {
                    if indexPath.row == 0
                    {
                        return 50
                    }
                    return UITableViewAutomaticDimension
                }
                return 120
                
            case "employees":
                if KVProfileCreate.shared.arr_Employees.count != 0
                {
                    if indexPath.row == 0
                    {
                        return 50
                    }
                    return UITableViewAutomaticDimension
                }
                return 120

            case "other_address":
                if KVProfileCreate.shared.arr_OtherAddress.count != 0
                {
                    if indexPath.row == 0
                    {
                        return 50
                    }
                    return UITableViewAutomaticDimension
                }
                return 120

            case "accept_terms":
                return 100
            default:
                return 0
            }
        }
        return self.bounds.height
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.sizeToFit()
        view.backgroundColor = UIColor.clear
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       if self.NumbersOfSection == 3
       {
        if section != 0
        {
            return 6
        }
        return 0
        }
        return 0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.NumbersOfSection == 3
        {
            
            let dict = self.arr_SectionNumber[indexPath.section]
            let strID = dict["sec_id"] as? String ?? ""
            
            switch strID{
            case "phone":
                if KVProfileCreate.shared.arr_ContactInfo.count > 0 && indexPath.row != 0
                {
                    let btn = UIButton.init(type: UIButtonType.contactAdd)
                    btn.tag = (indexPath.section*100)+indexPath.row
                    self.strPhone_ADD = "NewAdd"
                    self.AddPhonepress(btn)
                }
            case "email":
                if KVProfileCreate.shared.arr_EmailInfo.count > 0 && indexPath.row != 0
                {
                    let btn = UIButton.init(type: UIButtonType.contactAdd)
                    btn.tag = (indexPath.section*100)+indexPath.row
                    self.AddEmailpress(btn)
                }
            case "web_link":
                if KVProfileCreate.shared.arr_WebInfo.count > 0 && indexPath.row != 0
                {
                    let btn = UIButton.init(type: UIButtonType.contactAdd)
                    btn.tag = (indexPath.section*100)+indexPath.row
                    self.AddWebLinkpress(btn)
                }
            case "social_media":
                if KVProfileCreate.shared.arr_SocialMedia.count > 0 && indexPath.row != 0
                {
                    let btn = UIButton.init(type: UIButtonType.contactAdd)
                    btn.tag = (indexPath.section*100)+indexPath.row
                    self.AddSocialMediapress(btn)
                }
            case "founder":
                if KVProfileCreate.shared.arr_Founders.count > 0 && indexPath.row != 0
                {
                    let btn = UIButton.init(type: UIButtonType.contactAdd)
                    btn.tag = (indexPath.section*100)+indexPath.row
                    self.AddFounderpress(btn)
                }
                
            case "employees":
//                if KVProfileCreate.shared.arr_Employees.count > 0 && indexPath.row != 0
//                {
//                    let btn = UIButton.init(type: UIButtonType.contactAdd)
//                    btn.tag = (indexPath.section*100)+indexPath.row
                    KVClass.ShowNotification("", "Coming Soon !")
//                }
                
            case "other_address":
                if KVProfileCreate.shared.arr_OtherAddress.count > 0 && indexPath.row != 0
                {
                    let btn = UIButton.init(type: UIButtonType.contactAdd)
                    btn.tag = (indexPath.section*100)+indexPath.row
                    self.AddOtherAddress(btn)
                }
                
            default:
                break
            }
        }
    }
    
    @IBAction func deleteInfopress(_ sender: UIButton)
    {
        let section = sender.tag / 100
        var row = sender.tag % 100
        row = row - 1
        print(section)
        print(row)
        
        let dict = self.arr_SectionNumber[section]
        let strID = dict["sec_id"] as? String ?? ""
        
        switch strID{
        case "phone":
            KVProfileCreate.shared.arr_ContactInfo.remove(at: row)
            self.Main_tab.reloadSections(IndexSet.init(integer: section), with: .fade)
        case "email":
            KVProfileCreate.shared.arr_EmailInfo.remove(at: row)
            self.Main_tab.reloadSections(IndexSet.init(integer: section), with: .fade)
        case "web_link":
            KVProfileCreate.shared.arr_WebInfo.remove(at: row)
            self.Main_tab.reloadSections(IndexSet.init(integer: section), with: .fade)
        case "social_media":
            KVProfileCreate.shared.arr_SocialMedia.remove(at: row)
           self.Main_tab.reloadSections(IndexSet.init(integer: section), with: .fade)
        case "other_address":
            KVProfileCreate.shared.arr_OtherAddress.remove(at: row)
            self.Main_tab.reloadSections(IndexSet.init(integer: section), with: .fade)
        case "founder":
            KVProfileCreate.shared.arr_Founders.remove(at: row)
            self.Main_tab.reloadSections(IndexSet.init(integer: section), with: .fade)
        default:
            break
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch self.NumbersOfSection{
        case 0:
            self.Main_tab.isScrollEnabled = false
            let cell: FirstProTableCell = tableView.dequeueReusableCell(withIdentifier: "FirstProTableCell", for: indexPath) as! FirstProTableCell
            KVProfileCreate.shared.setValue(KVAppDataServieces.shared.LoginUserID(), .user_id)
            
            
            return cell
        case 1:
            self.Main_tab.isScrollEnabled = false
            let cell: SecProTableCell = tableView.dequeueReusableCell(withIdentifier: "SecProTableCell", for: indexPath) as! SecProTableCell
            
            return cell
        case 2:
            self.Main_tab.isScrollEnabled = false
            let cell: ThirdProTableCell = tableView.dequeueReusableCell(withIdentifier: "ThirdProTableCell", for: indexPath) as! ThirdProTableCell
            print(self.CategoryList.map{($0.name)})
            cell.arrType = self.CategoryList.map{($0.name)}
            
            return cell
        case 3:
            self.Main_tab.isScrollEnabled = true
            let dict = self.arr_SectionNumber[indexPath.section]
            let strID = dict["sec_id"] as? String
            
            if strID == "profil_info" {
                
                let cell: FourthProTableCell = tableView.dequeueReusableCell(withIdentifier: "FourthProTableCell", for: indexPath) as! FourthProTableCell
                
                if cell.lbl_adres.text == "Address*"
                {
                    cell.lbl_adres.textColor = UIColor.lightGray
                }
                else
                {
                    cell.lbl_adres.textColor = UIColor.black
                }
                if self.isUserProfile {
                    cell.Upload_pic_btn.setTitle("    Upload Profile Image    ", for: .normal)
                    cell.teamsizeTextfieldHeight.constant = 0
                    cell.teamsizeTextfieldTop.constant = 0
                    cell.teamsizeTextfieldBottom.constant = 30
                    
                    cell.ceotextfieldTop.constant = 0
                    cell.ceotextfieldBottom.constant = 0
                    cell.ceotextfieldHeight.constant = 0
                    
                    cell.cfotextfieldTop.constant = 0
                    cell.cfotextfieldHeight.constant = 0
                    
                    cell.txt_estadate.placeholder = "Date of Birth"
                    cell.Pcompanysize.isHidden = true
                }
                else{
                    cell.Upload_pic_btn.setTitle("    Upload Logo    ", for: .normal)
                    cell.teamsizeTextfieldHeight.constant = 35
                    cell.teamsizeTextfieldTop.constant = 30
                    cell.teamsizeTextfieldBottom.constant = 30
                    
                    cell.ceotextfieldTop.constant = 30
                    cell.ceotextfieldBottom.constant = 30
                    cell.ceotextfieldHeight.constant = 35
                    
                    cell.cfotextfieldTop.constant = 0
                    cell.cfotextfieldHeight.constant = 0
                    
                    cell.txt_estadate.placeholder = "Establish Date"
                    cell.Pcompanysize.isHidden = false
                }
                cell.layoutIfNeeded()
                
                
                return cell
            }
            else if strID == "other_address" {
              
                if KVProfileCreate.shared.arr_OtherAddress.count == 0
                {
                    let cell: AddSocialCell = tableView.dequeueReusableCell(withIdentifier: "AddSocialCell", for: indexPath) as! AddSocialCell
                    cell.Add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.lbl_title.text = "other address".uppercased()
                    cell.Add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.Add_btn.addTarget(self, action: #selector(self.AddOtherAddress(_:)), for: .touchUpInside)
                    return cell
                }
                switch indexPath.row{
                case 0:
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    cell.title_lbl.text = "other address".uppercased()
                    cell.add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.add_btn.addTarget(self, action: #selector(self.AddOtherAddress(_:)), for: .touchUpInside)
                    
                    return cell
                default:
                    let cell: ProfileInfoCell = tableView.dequeueReusableCell(withIdentifier: "ProfileInfoCell", for: indexPath) as! ProfileInfoCell
                    
                    let style = NSMutableParagraphStyle()
                    style.lineSpacing = 6
                    
                    let attributedText = NSMutableAttributedString.init(string: KVProfileCreate.shared.arr_OtherAddress[indexPath.row - 1]["name"] as? String ?? "")
                    attributedText.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSRange.init(location: 0, length: attributedText.length))
                    
                    cell.lbl_info.attributedText = attributedText
                    cell.btn_delete.tag = (indexPath.section*100)+indexPath.row
                    cell.btn_delete.addTarget(self, action: #selector(self.deleteInfopress(_:)), for: .touchUpInside)
                    return cell
                }
                
            }
            else if strID == "founder" {
                
                if KVProfileCreate.shared.arr_Founders.count == 0
                {
                    let cell: AddSocialCell = tableView.dequeueReusableCell(withIdentifier: "AddSocialCell", for: indexPath) as! AddSocialCell
                    cell.Add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.lbl_title.text = "founder/key members".uppercased()
                    cell.Add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.Add_btn.addTarget(self, action: #selector(self.AddFounderpress(_:)), for: .touchUpInside)
                    return cell
                }
                switch indexPath.row{
                case 0:
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    cell.title_lbl.text = "founder/key member".uppercased()
                    cell.add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.add_btn.addTarget(self, action: #selector(self.AddFounderpress(_:)), for: .touchUpInside)
                    
                    return cell
                default:
//                    let cell: MediaInfoCell = tableView.dequeueReusableCell(withIdentifier: "MediaInfoCell", for: indexPath) as! MediaInfoCell
//
//                    let valuedicti = KVProfileCreate.shared.arr_Founders[indexPath.row - 1]
//                    cell.lbl_type.text = "Designation :"
//                    cell.lbl_urlname.text = "\(valuedicti["firstname"] as? String ?? "") \(valuedicti["lastname"] as? String ?? "")"
//                    cell.lbl_detail.text = valuedicti["designation"] as? String ?? ""
//                    cell.btn_delete.tag = (indexPath.section*100)+indexPath.row
//                    cell.btn_delete.addTarget(self, action: #selector(self.deleteInfopress(_:)), for: .touchUpInside)
                    
                    
                    let cell: QKProfileFounderCell = tableView.dequeueReusableCell(withIdentifier: "QKProfileFounderCell", for: indexPath) as! QKProfileFounderCell
                    
                    let dataDict = KVProfileCreate.shared.arr_Founders[indexPath.row - 1]
                    
                    cell.lbl_name.text = "\(dataDict["firstname"] as? String ?? "") \(dataDict["lastname"] as? String ?? "")"
                    
                    let strEmail = dataDict["email"] as? String ?? ""
                    if strEmail == "" {
                        cell.lbl_email.text = ""
                        cell.lbl_email_Title.text = ""
                    }
                    else {
                        cell.lbl_email_Title.text = "Email :"
                        cell.lbl_email.text = dataDict["email"] as? String ?? ""
                    }
                    cell.lbl_designation.text = dataDict["designation"] as? String ?? ""
                    cell.btn_edit.isHidden = false
                    cell.btn_edit.tag = (indexPath.section*100)+indexPath.row
                    cell.btn_edit.addTarget(self, action: #selector(self.deleteInfopress(_:)), for: .touchUpInside)
                    
                    
                    
                    
                    //  cell.lbl_info.text = KVProfileCreate.shared.arr_WebInfo[indexPath.row - 1].name
                    
                    return cell
                }
                
            }
                
            ////////
            else if strID == "employees" {
                
                if KVProfileCreate.shared.arr_Employees.count == 0
                {
                    let cell: AddSocialCell = tableView.dequeueReusableCell(withIdentifier: "AddSocialCell", for: indexPath) as! AddSocialCell
                    cell.Add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.lbl_title.text = "employees".uppercased()
                    cell.Add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.Add_btn.addTarget(self, action: #selector(self.AddEmployeesPress(_:)), for: .touchUpInside)
                    return cell
                }
            }
            //////////////
             
            else if strID == "accept_terms" {
                let cell: TermsConditionsCell = tableView.dequeueReusableCell(withIdentifier: "TermsConditionsCell", for: indexPath) as! TermsConditionsCell
                
//                cell.Terms_btn.isSelected = false
//                if app_Delegate.strSelectedTermsCondition == "false" || app_Delegate.strSelectedTermsCondition == "" {
//                    cell.Terms_btn.isSelected = true
//                    app_Delegate.strSelectedTermsCondition = "true"
//                }
//                else {
//                    cell.Terms_btn.isSelected = false
//                    app_Delegate.strSelectedTermsCondition = "false"
//                }
                
                return cell
            }
                
                
            else if strID == "social_media" {
              
                if KVProfileCreate.shared.arr_SocialMedia.count == 0
                {
                    let cell: AddSocialCell = tableView.dequeueReusableCell(withIdentifier: "AddSocialCell", for: indexPath) as! AddSocialCell
                    cell.Add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.lbl_title.text = "social media".uppercased()
                    cell.Add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.Add_btn.addTarget(self, action: #selector(self.AddSocialMediapress(_:)), for: .touchUpInside)
                    return cell
                }
                switch indexPath.row{
                case 0:
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    cell.title_lbl.text = "social media".uppercased()
                    cell.add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.add_btn.addTarget(self, action: #selector(self.AddSocialMediapress(_:)), for: .touchUpInside)
                    
                    return cell
                default:
                    let cell: MediaInfoCell = tableView.dequeueReusableCell(withIdentifier: "MediaInfoCell", for: indexPath) as! MediaInfoCell
                    
                    let valuedicti = KVProfileCreate.shared.arr_SocialMedia[indexPath.row - 1]
                    cell.lbl_type.text = "Media Type :"
                    cell.lbl_urlname.text = valuedicti["name"] as? String ?? ""
                    cell.lbl_detail.text = self.arrType[(valuedicti["type"] as? Int ?? 1) - 1]//valuedicti["type"] as? Int ?? 1
                    cell.btn_delete.tag = (indexPath.section*100)+indexPath.row
                    cell.btn_delete.addTarget(self, action: #selector(self.deleteInfopress(_:)), for: .touchUpInside)
                    
                    return cell
                }
                
            }
            else if strID == "phone" {
               
                if KVProfileCreate.shared.arr_ContactInfo.count == 0
                {
                    let cell: AddSocialCell = tableView.dequeueReusableCell(withIdentifier: "AddSocialCell", for: indexPath) as! AddSocialCell
                    cell.Add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.lbl_title.text = "phone".uppercased()
                    cell.Add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    self.strPhone_ADD = "NewAdd"
                    cell.Add_btn.addTarget(self, action: #selector(self.AddPhonepress(_:)), for: .touchUpInside)
                    return cell
                }
                switch indexPath.row{
                case 0:
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    cell.title_lbl.text = "phone".uppercased()
                    cell.add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.add_btn.tag = (indexPath.section*100)+indexPath.row
                    self.strPhone_ADD = "NewAdd"
                    cell.add_btn.addTarget(self, action: #selector(self.AddPhonepress(_:)), for: .touchUpInside)
                    if KVProfileCreate.shared.arr_ContactInfo.count > 4
                    {
                        cell.add_btn.isHidden = true
                    }
                    else
                    {
                        cell.add_btn.isHidden = false
                    }
                    return cell
                default:
                    let cell: ProfileInfoCell = tableView.dequeueReusableCell(withIdentifier: "ProfileInfoCell", for: indexPath) as! ProfileInfoCell
                    let strPhoneContact = KVProfileCreate.shared.arr_ContactInfo[indexPath.row - 1]["name"] as? String ?? ""
                    cell.lbl_info.text = strPhoneContact
                    cell.btn_delete.tag = (indexPath.section*100)+indexPath.row
                    cell.btn_delete.addTarget(self, action: #selector(self.deleteInfopress(_:)), for: .touchUpInside)
                    return cell
                }
                
            }
            else if strID == "email" {
                
                
                if KVProfileCreate.shared.arr_EmailInfo.count == 0
                {
                    let cell: AddSocialCell = tableView.dequeueReusableCell(withIdentifier: "AddSocialCell", for: indexPath) as! AddSocialCell
                    cell.Add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.lbl_title.text = "email*".uppercased()
                    cell.Add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.Add_btn.addTarget(self, action: #selector(self.AddEmailpress(_:)), for: .touchUpInside)
                    return cell
                }
                switch indexPath.row{
                case 0:
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    cell.title_lbl.text = "email*".uppercased()
                    cell.add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.add_btn.addTarget(self, action: #selector(self.AddEmailpress(_:)), for: .touchUpInside)
                    if KVProfileCreate.shared.arr_EmailInfo.count > 4
                    {
                        cell.add_btn.isHidden = true
                    }
                    else
                    {
                        cell.add_btn.isHidden = false
                    }
                    return cell
                default:
                    let cell: ProfileInfoCell = tableView.dequeueReusableCell(withIdentifier: "ProfileInfoCell", for: indexPath) as! ProfileInfoCell
                    cell.lbl_info.text = KVProfileCreate.shared.arr_EmailInfo[indexPath.row - 1]["name"] as? String ?? ""
                    cell.btn_delete.tag = (indexPath.section*100)+indexPath.row
                    cell.btn_delete.addTarget(self, action: #selector(self.deleteInfopress(_:)), for: .touchUpInside)
                    return cell
                }
                
                
            }
            else if strID == "web_link" {
                
                
                if KVProfileCreate.shared.arr_WebInfo.count == 0
                {
                    let cell: AddSocialCell = tableView.dequeueReusableCell(withIdentifier: "AddSocialCell", for: indexPath) as! AddSocialCell
                    cell.Add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.lbl_title.text = "web link".uppercased()
                    cell.Add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.Add_btn.addTarget(self, action: #selector(self.AddWebLinkpress(_:)), for: .touchUpInside)
                    return cell
                }
                switch indexPath.row{
                case 0:
                    let cell: TitleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell", for: indexPath) as! TitleHeaderCell
                    cell.title_lbl.text = "web link".uppercased()
                    cell.add_btn.removeTarget(nil, action: nil, for: .allEvents)
                    cell.add_btn.tag = (indexPath.section*100)+indexPath.row
                    cell.add_btn.addTarget(self, action: #selector(self.AddWebLinkpress(_:)), for: .touchUpInside)
                    if KVProfileCreate.shared.arr_WebInfo.count > 4
                    {
                        cell.add_btn.isHidden = true
                    }
                    else
                    {
                        cell.add_btn.isHidden = false
                    }
                    return cell
                default:
                    let cell: ProfileInfoCell = tableView.dequeueReusableCell(withIdentifier: "ProfileInfoCell", for: indexPath) as! ProfileInfoCell
                    cell.lbl_info.text = KVProfileCreate.shared.arr_WebInfo[indexPath.row - 1]["name"] as? String ?? ""
                    cell.btn_delete.tag = (indexPath.section*100)+indexPath.row
                    cell.btn_delete.addTarget(self, action: #selector(self.deleteInfopress(_:)), for: .touchUpInside)
                    
                    return cell
                }
            }
        default:
             return UITableViewCell()
        }
        return UITableViewCell()
    }
//        default:
//            return UITableViewCell()
//        }
}
//=================================================================================================//
//=================================================================================================//

class ProfileInfoCell: UITableViewCell{
    
    @IBOutlet var lbl_info: UILabel!
    @IBOutlet var btn_delete: UIButton!
    
    override func awakeFromNib() {
        self.btn_delete.setImage(#imageLiteral(resourceName: "dlt_ic").tint(with: UIColor.lightGray), for: .normal)
    }
}

class MediaInfoCell: UITableViewCell {
   
    @IBOutlet var lbl_urlname: UILabel!
    @IBOutlet var lbl_type: UILabel!
    @IBOutlet var lbl_detail: UILabel!
    
    @IBOutlet var btn_delete: UIButton!
    
    override func awakeFromNib() {
        self.btn_delete.setImage(#imageLiteral(resourceName: "dlt_ic").tint(with: UIColor.lightGray), for: .normal)
    }
}

class AddSocialCell: UITableViewCell {
    @IBOutlet var Add_btn: UIButton!
    @IBOutlet var lbl_title: UILabel!
    var arrType = ["Facebook","Linkedin","Twitter","Instagram"]
    var SocialAlert: UIAlertController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
        self.Add_btn.setAttributedTitle(KVClass.KVattributeIconString(inputImage: "add", stringName: "  Add Information"), for: .normal)
       // self.Add_btn.addTarget(self, action: #selector(self.addpress(_:)), for: .touchUpInside)
    }
    
    @IBAction func addpress(_ sender: UIButton)
    {
        if sender.tag == 1
        {
//        self.AddSocialMediapress()
        }
        else if sender.tag == 2
        {
//        self.AddFounderpress()
        }
    }
}

class FirstProTableCell: UITableViewCell
{
   
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}


class TermsConditionsCell: UITableViewCell {
    @IBOutlet var lbl_title: UILabel!
    @IBOutlet var Terms_btn: DLRadioButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.Terms_btn.isMultipleSelectionEnabled = true
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(TapResponce(_:)))
        self.lbl_title.isUserInteractionEnabled = true
        self.lbl_title.addGestureRecognizer(tapGesture)
        self.Terms_btn.addGestureRecognizer(tapGesture)
       // self.Terms_btn.addTarget(self, action: #selector(TapResponce(_:)), for: .touchUpInside)
    }
    
    
    @objc func TapResponce(_ sender: UITapGestureRecognizer) {
        self.AcceptTermsConditions(self.Terms_btn)
    }
    
    @IBAction func AcceptTermsConditions(_ sender: DLRadioButton) {
        if app_Delegate.strSelectedTermsCondition == "false" || app_Delegate.strSelectedTermsCondition == "" {
            self.Terms_btn.isSelected = true
            app_Delegate.strSelectedTermsCondition = "true"
        }
        else {
            self.Terms_btn.isSelected = false
            app_Delegate.strSelectedTermsCondition = "false"
        }
    }
}



class SecProTableCell: UITableViewCell, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource
{
    @IBOutlet var txtProfileName: UITextField!
    @IBOutlet var txtProfileType: UITextField!
    
    var arrType = ["Company","Enterprise","Public Figure/Celebrity"]
    var Category = 1
    var SelectedRowType = 0
    var StrselectedProfileType = ""
    
    var isPasteEnabled: Bool = true
    
    var isSelectEnabled: Bool = true
    
   var isSelectAllEnabled: Bool = true
    
    var isCopyEnabled: Bool = true
    
    var isCutEnabled: Bool = true
    
    var isDeleteEnabled: Bool = true
    
    var isReadonly = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //........Set Delegate.............//
        self.txtProfileName.delegate = self
        self.txtProfileType.delegate = self
        
        //.....................TextField PlaceHolder......................................//
        let strP_name = NSAttributedString(string: "Profile Name*", attributes: [NSForegroundColorAttributeName: UIColor.lightGray])
        self.txtProfileName.attributedPlaceholder! = strP_name
        
        let strType = NSAttributedString(string: "SELECT TYPE*", attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        self.txtProfileType.attributedPlaceholder! = strType
        //............................................................................................//
        
        self.txtProfileType.accessibilityHint = "disable_past"
    
        self.txtProfileName.layer.borderWidth = 1;
        self.txtProfileName.layer.borderColor = UIColor.lightGray.cgColor
        
        self.txtProfileType.layer.borderWidth = 1;
        self.txtProfileType.layer.borderColor = UIColor.lightGray.cgColor
        
        self.txtProfileType.clearButtonMode = .never
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        KVProfileCreate.shared.setValue(self.txtProfileName.text, .name)
        KVProfileCreate.shared.setValue(self.txtProfileType.text, .type_name)
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        super.canPerformAction(action, withSender: sender)
        return false
    }
    
    open override func target(forAction action: Selector, withSender sender: Any?) -> Any? {
        guard isReadonly else {
            return super.target(forAction: action, withSender: sender)
        }
        
        if #available(iOS 10, *) {
            if action == #selector(UIResponderStandardEditActions.paste(_:)) {
                return nil
            }
        } else {
            if action == #selector(paste(_:)) {
                return nil
            }
        }
        
        return super.target(forAction: action, withSender: sender)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
    
            return true
        }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.isReadonly = true
        if textField == self.txtProfileType {
            self.isReadonly = false
            if self.arrType.count > 0 {
                StrselectedProfileType = self.arrType[SelectedRowType]
                self.settxtInputview()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtProfileName {
            let maxLength = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
            
        }
       
        return true
    }
    

    @IBAction func editBegin(_ sender: UIButton) {
        self.txtProfileType.becomeFirstResponder()
    }
    
    func settxtInputview() {
        let picker = UIPickerView.init()
        picker.delegate = self
        picker.dataSource = self
        let toobar = UIToolbar()
        toobar.sizeToFit()
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.doneTxt(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
    
        toobar.setItems([ spaceButton, doneButton], animated: false)
        toobar.isUserInteractionEnabled = true
    
        //  toobar.items = [UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil), UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.doneTxt(_:)))]
        self.txtProfileType.inputView = picker
        self.txtProfileType.inputAccessoryView = toobar
        self.txtProfileType.clearButtonMode = .never
        picker.selectRow(SelectedRowType, inComponent: 0, animated: false)
    }
    
    @IBAction func doneTxt(_ sender: UIBarButtonItem) {
        self.txtProfileType.endEditing(true)
        self.txtProfileType.text = StrselectedProfileType
        //=====Fot Get Selected Category ID=====//
        for itemType in 0..<arrType.count {
            if self.arrType[itemType] == StrselectedProfileType {
                self.Category = itemType + 1
                SelectedRowType = itemType
                if self.Category == 2 {
                }
            }
            
        }
        //=====================================///
        KVProfileCreate.shared.setValue(self.Category, .type)
        KVProfileCreate.shared.setValue(self.txtProfileType.text, .type_name)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrType.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrType[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
        if self.arrType.count > 0
        {
        StrselectedProfileType = arrType[row]
        }
    }
}

//extension Ptypetxt{
//
//
//}

//class NMTextField: TextField {
//    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
//            return false
//        }
//        return super.canPerformAction(action, withSender: sender)
//    }
//}


class ThirdProTableCell: UITableViewCell, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource,  MLPAutoCompleteTextFieldDelegate, MLPAutoCompleteTextFieldDataSource, SetSelectedCategoryDelegate
{
    func setCategoryValue(_ category_ID: Int, _ category_NAME: String) {
        self.CategoryID = category_ID
        strSelectedCat = category_NAME
        self.txtCategory.text = category_NAME
        
        self.txtCategory.endEditing(true)
        //=====Fot Get Selected Category ID=====//
        for itemCat in 0..<arr_LocalCategory.count {
            if self.arr_LocalCategory[itemCat].name == category_NAME {
                CategoryID = self.arr_LocalCategory[itemCat].id
                selectedRowCat = itemCat
            }
        }
        selectedRowSub_Cat = 0
        self.txtSubCategory.text = ""
        KVProfileCreate.shared.removeKeyValue(.sub_tag)
        ///=============================================///
        self.getCategoryList(2)
        KVProfileCreate.shared.setValue(self.CategoryID, .tag)
        KVProfileCreate.shared.setValue(self.txtCategory.text, .cat_name)
    }
    
    func closeKeyboardFromCloseCategory() {
        self.txtCategory.resignFirstResponder()
    }
    
    @IBOutlet var txtCategory: UITextField!
    @IBOutlet var txtSubCategory: MLPAutoCompleteTextField!
    
    //var CategoryList: [CategoryDataList]! = [CategoryDataList]()
    var SubcatList: [SubcategoryDataList]! = [SubcategoryDataList]()
    var arrType: [String]! = [String]()
    var CategoryID = 0
    var Sub_CategoryID = 0
    var selectedRowCat = 0
    var selectedRowSub_Cat = 0
    var picker = UIPickerView()
    var strSelectedCat = ""
    var strSelectedSub_Cat = ""
    @IBOutlet var cat_btn: UIButton!
    @IBOutlet var subcat_btn: UIButton!
    
    var arr_LocalCategory: Results<RealmCategoryList>!
    var arr_LocalSubCategory: Results<RealmSubCategoryList>!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //........Set Delegate.............//
        self.txtCategory.delegate = self
        self.txtSubCategory.delegate = self
        
        //.....................TextField PlaceHolder......................................//
        let strcar_name = NSAttributedString(string: "CATEGORY*", attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        self.txtCategory.attributedPlaceholder! = strcar_name

        let strsubcatname = NSAttributedString(string: "SUB CATEGORY*", attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        self.txtSubCategory.attributedPlaceholder! = strsubcatname
        
        self.txtCategory.clearButtonMode = .never
        self.txtSubCategory.clearButtonMode = .whileEditing
        
        self.txtCategory.layer.borderWidth = 1;
        self.txtCategory.layer.borderColor = UIColor.lightGray.cgColor
        
        self.txtSubCategory.layer.borderWidth = 1;
        self.txtSubCategory.layer.borderColor = UIColor.lightGray.cgColor
        //............................................................................................//

        
        
        
        self.txtSubCategory.autoCompleteDelegate = self
        self.txtSubCategory.autoCompleteDataSource = self
        self.txtSubCategory.autoCompleteTableView.backgroundColor = UIColor.white
        self.txtSubCategory.autoCompleteTableBorderColor = #colorLiteral(red: 0, green: 0.6777210236, blue: 0.7377243042, alpha: 1)
        self.txtSubCategory.autoCompleteTableBorderWidth = 0.5
        self.txtSubCategory.showAutoCompleteTableWhenEditingBegins = true
        self.txtSubCategory.autoCompleteTableView.separatorInset.left = 0
        self.txtSubCategory.autoCompleteTableView.separatorInset.right = 0
      //  self.txtSubCategory.isPlaceholderAnimated = false
        
        
        self.cat_btn.tag = 1
        self.cat_btn.addTarget(self, action: #selector(self.editBegin(_:)), for: .touchUpInside)
        self.subcat_btn.tag = 2
        self.subcat_btn.addTarget(self, action: #selector(self.editBegin(_:)), for: .touchUpInside)
        
        var P_DataType = 0
        if KVProfileCreate.shared.DataDict["type"] as? Int ?? 0 == 1 || KVProfileCreate.shared.DataDict["type"] as? Int ?? 0 == 2 {
            P_DataType = 1
        }
        else {
            P_DataType = 3
        }
        
        self.arr_LocalCategory = RealmServieces.shared.realm.objects(RealmCategoryList.self).filter(NSPredicate.init(format: "parent_id == %d", P_DataType))
        self.getCategoryList(1)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtSubCategory {
            let maxLength = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
    }
    
    func getCategoryList(_ tag: Int)
    {
        let dict = NSMutableDictionary()
        dict["parent_id"] = self.CategoryID
//        if self.CategoryList.count < 1
//        {
        dict["type"] = KVProfileCreate.shared.DataDict["type"] as? Int == 2 ? 1 : KVProfileCreate.shared.DataDict["type"] as? Int ?? 1
       // }
        
        KVClass.KVServiece(methodType: "POST", processView: self.arr_LocalCategory.count > 0 ? nil : (appDelegate.window?.rootViewController)!, baseView: nil, processLabel: "", params: dict, api: "profile/tag_list", arrFlag: false) { (json, status) in
            
            if status == 1
            {
                
                if let datadict = json["data"] as? [NSDictionary]
                {
                    if tag == 1
                    {
                  //  self.CategoryList.removeAll()
                    for item in datadict
                    {
                       // self.CategoryList.append(CategoryDataList.init(item))
                        print(self.arr_LocalCategory)
                       let obj = RealmCategoryList.init(item)
                        if KVProfileCreate.shared.DataDict["type"] as? Int ?? 1 == 1 || KVProfileCreate.shared.DataDict["type"] as? Int ?? 1 == 2 {
                            obj.parent_id = 1
                        }
                        else {
                            obj.parent_id = 3
                        }
                        RealmServieces.shared.create(obj)
                    }
                        
                    print(self.arr_LocalCategory)
                    self.picker.reloadAllComponents()
                    }
                    else if tag == 2
                    {
                        self.SubcatList.removeAll()
                        for item in datadict
                        {
                            self.SubcatList.append(SubcategoryDataList.init(item))
//                            self.backgroundColor = //#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                        }
                        self.txtSubCategory.reloadData()
                    }
                }
            }
        }
    }
    
    func autoCompleteTextField(_ textField: MLPAutoCompleteTextField!, possibleCompletionsFor string: String!) -> [Any]! {
        if textField == self.txtSubCategory
        {
            return self.SubcatList.map{($0.subname)}
        }
        return []
    }
    
    func autoCompleteTextField(_ textField: MLPAutoCompleteTextField!, didSelectAutoComplete selectedString: String!, withAutoComplete selectedObject: MLPAutoCompletionObject!, forRowAt indexPath: IndexPath!) {
        
        if textField == self.txtSubCategory
        {
            textField.text = selectedString
            
            for itemSubCat in 0..<SubcatList.count {
                if self.SubcatList[itemSubCat].subname == selectedString {
                    Sub_CategoryID = self.SubcatList[itemSubCat].subid
                    selectedRowSub_Cat = itemSubCat
                }
            }
            //=====================================///
            KVProfileCreate.shared.setValue(self.Sub_CategoryID, .sub_tag)
            
        }
        
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        KVProfileCreate.shared.setValue(self.txtCategory.text, .cat_name)
        if textField == self.txtSubCategory && textField.text != "" && textField.text != " " {
            let arr = self.SubcatList.map{($0.subname)}
            
            if self.CategoryID == 0 {
                KVClass.ShowNotification("", "Please Select Category !")
                return
            }
            
            let filtered = arr.filter({ (text) -> Bool in
                let tmp: NSString = text as NSString
                let range = tmp.range(of: textField.text!, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
            
            if filtered.count == 0
            {
                let subcatdict = NSMutableDictionary()
                subcatdict["parent_id"] = self.CategoryID
                subcatdict["type"] = KVProfileCreate.shared.DataDict["type"] as? Int == 2 ? 1 : KVProfileCreate.shared.DataDict["type"] as? Int ?? 1
                subcatdict["name"] = textField.text
                
                KVClass.KVServiece(methodType: "POST", processView: (appDelegate.window?.rootViewController)!, baseView: nil, processLabel: "", params: subcatdict, api: "profile/add_sub_tag", arrFlag: false, Hendler: { (JSON, status) in
                    
                    if status == 1{
                        self.Sub_CategoryID = (JSON["data"] as? NSDictionary ?? [:])["sub_tag_id"] as? Int ?? 0
                      KVProfileCreate.shared.setValue(self.Sub_CategoryID, .sub_tag)
                    }
                })
            }
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.txtCategory {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let objCat = storyboard.instantiateViewController(withIdentifier: "SelectCategoryVC") as! SelectCategoryVC
            objCat.delegate = self
            objCat.arr_LocalPass_Category = arr_LocalCategory
            objCat.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            objCat.modalTransitionStyle = .crossDissolve
            objCat.view.backgroundColor = UIColor.clear
            app_Delegate.window?.rootViewController?.present(objCat, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //self.endEditing(true)
        
        if textField == self.txtCategory {
            //let arr = self.arr_LocalCategory!
            if self.arr_LocalCategory.count > 0 {
                //strSelectedCat = self.arr_LocalCategory[selectedRowCat].name
               // self.CategoryID = self.arr_LocalCategory[selectedRowCat].id
                self.txtCategory.resignFirstResponder()
                self.txtSubCategory.resignFirstResponder()
                self.endEditing(true)
                
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let objCat = storyboard.instantiateViewController(withIdentifier: "SelectCategoryVC") as! SelectCategoryVC
                objCat.delegate = self
                objCat.arr_LocalPass_Category = arr_LocalCategory
                objCat.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                objCat.modalTransitionStyle = .crossDissolve
                objCat.view.backgroundColor = UIColor.clear
                app_Delegate.window?.rootViewController?.present(objCat, animated: true, completion: nil)
                
                
                //self.settxtInputview(1)
            }
        }
        else if textField == self.txtSubCategory {
            let arr = self.SubcatList.map{($0.subname)}
                if arr.count > 0 {
                    strSelectedSub_Cat = arr[selectedRowSub_Cat]
                    self.Sub_CategoryID = self.SubcatList[selectedRowSub_Cat].subid
                    //self.settxtInputview(2)
                }
//                else {
//                    KVClass.ShowNotification("", "Please Select Category")
//                }
        }
    }
    
    @IBAction func editBegin(_ sender: UIButton)
    {
        switch sender.tag {
        case 1:
            self.txtCategory.resignFirstResponder()
            self.txtSubCategory.resignFirstResponder()
        case 2:
            self.txtSubCategory.becomeFirstResponder()
        default:
            break
        }
    }
    
    func settxtInputview(_ tag: Int)
    {
        picker = UIPickerView.init()
        picker.delegate = self
        picker.dataSource = self
        picker.tag = tag
        let toobar = UIToolbar()
        if tag == 1 {
            picker.selectRow(selectedRowCat, inComponent: 0, animated: true)
        }
        else {
            picker.selectRow(selectedRowSub_Cat, inComponent: 0, animated: true)
        }
        
        
        toobar.sizeToFit()
        
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.doneTxt(_:)))
        doneButton.tag = tag
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toobar.setItems([ spaceButton, doneButton], animated: false)
        toobar.isUserInteractionEnabled = true
        
        //  toobar.items = [UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil), UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.doneTxt(_:)))]
        if tag == 1
        {
            self.txtCategory.inputView = picker
            self.txtCategory.inputAccessoryView = toobar
            self.txtCategory.clearButtonMode = .never
            self.txtCategory.tintColor = .clear
            picker.selectRow(selectedRowCat, inComponent: 0, animated: true)
        }
//        else
//        {
//            self.Psubcattxt.inputView = picker
//            self.Psubcattxt.inputAccessoryView = toobar
//            self.Psubcattxt.clearButtonMode = .never
//            self.Psubcattxt.tintColor = .clear
//            picker.selectRow(selectedRowSub_Cat, inComponent: 0, animated: true)
//        }
    }
    
    @IBAction func doneTxt(_ sender: UIBarButtonItem)
    {
        switch sender.tag {
        case 1:
            self.txtCategory.endEditing(true)
            self.txtCategory.text = strSelectedCat
            //=====Fot Get Selected Category ID=====//
            for itemCat in 0..<arr_LocalCategory.count {
                if self.arr_LocalCategory[itemCat].name == strSelectedCat {
                    CategoryID = self.arr_LocalCategory[itemCat].id
                    selectedRowCat = itemCat
                }
            }
            selectedRowSub_Cat = 0
            self.txtSubCategory.text = ""
            KVProfileCreate.shared.removeKeyValue(.sub_tag)
            ///=============================================///
            self.getCategoryList(2)
            KVProfileCreate.shared.setValue(self.CategoryID, .tag)
            KVProfileCreate.shared.setValue(self.txtCategory.text, .cat_name)
        case 2:
            self.txtSubCategory.endEditing(true)
            self.txtSubCategory.text = strSelectedSub_Cat
            //=====Fot Get Selected Category ID=====//
            for itemSubCat in 0..<SubcatList.count {
                if self.SubcatList[itemSubCat].subname == strSelectedSub_Cat {
                    Sub_CategoryID = self.SubcatList[itemSubCat].subid
                    selectedRowSub_Cat = itemSubCat
                }
            }
            //=====================================///
            KVProfileCreate.shared.setValue(self.Sub_CategoryID, .sub_tag)
        default:
            break
        }
        
        print("hy")
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag{
        case 1:
            return self.arr_LocalCategory.count//self.CategoryList.map{($0.name)}.count
        case 2:
            return self.SubcatList.map{($0.subname)}.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag{
        case 1:
            return self.arr_LocalCategory[row].name//self.CategoryList.map{($0.name)}[row]
        case 2:
            return self.SubcatList.map{($0.subname)}[row]
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
        
        switch pickerView.tag{
        case 1:
           // let arr = self.CategoryList.map{($0.name)}
           if self.arr_LocalCategory.count > 0 {
                strSelectedCat = self.arr_LocalCategory[row].name//arr[row]
                //self.CategoryID = self.CategoryList[row].id
                //KVProfileCreate.shared.setValue(self.CategoryList[row].id, .tag)
            }
        case 2:
            let arr = self.SubcatList.map{($0.subname)}
            if arr.count > 0 {
                strSelectedSub_Cat = arr[row]
                //self.Sub_CategoryID = self.SubcatList[row].subid
                //KVProfileCreate.shared.setValue(self.SubcatList[row].subid, .sub_tag)
            }
        default:
            break
        }
    }
}


//=================================================================================================//
//===========Fourth Pro Table View Cell============================================================//
class FourthProTableCell: UITableViewCell
{
    @IBOutlet weak var txtAbout: TextView!
    @IBOutlet weak var lblTxtChar: UILabel!
    @IBOutlet weak var lblStorTitle: UILabel!
    @IBOutlet weak var lblDeviderStory: UILabel!
    
    @IBOutlet var txt_estadate: TextField!
    @IBOutlet var Pphonetxt: TextField!
    @IBOutlet var lbl_adresTitle: UILabel!
    @IBOutlet var lbl_adres: UILabel!
    @IBOutlet var Pemailtxt: TextField!
    @IBOutlet var Pweblinktxt: TextField!
    @IBOutlet var Pcompanysize: TextField!
    @IBOutlet var Pimgview: UIImageView!
    
    @IBOutlet var txt_CEO: TextField!
    @IBOutlet var txt_CFO: TextField!
    
    
    var ImagePicker = UIImagePickerController()
    @IBOutlet var Upload_pic_btn: UIButton!
    @IBOutlet var Add_social_btn: UIButton!
    var placesClient: GMSPlacesClient!
    var arrType = ["Facebook","Linkedin","Twitter","Instagram"]
    var SocialAlert: UIAlertController?
    
    @IBOutlet var teamsizeTextfieldHeight: NSLayoutConstraint!
    @IBOutlet var teamsizeTextfieldTop: NSLayoutConstraint!
    @IBOutlet var teamsizeTextfieldBottom: NSLayoutConstraint!
    
    @IBOutlet var ceotextfieldHeight: NSLayoutConstraint!
    @IBOutlet var ceotextfieldTop: NSLayoutConstraint!
    @IBOutlet var ceotextfieldBottom: NSLayoutConstraint!
    
    @IBOutlet var cfotextfieldHeight: NSLayoutConstraint!
    @IBOutlet var cfotextfieldTop: NSLayoutConstraint!
    @IBOutlet var cfotextfieldBottom: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblStorTitle.text = ""
        self.txtAbout.setDivider()
        self.txt_estadate.setDivider()
        self.txt_CEO.setDivider()
        self.txt_CFO.setDivider()
        self.txt_estadate.text = ""
        
        self.lbl_adres.isUserInteractionEnabled = true
        self.lbl_adres.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(addresspicPress(_:))))
        self.Pcompanysize.setDivider()
        self.Pcompanysize.tag = 2
        self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
        
        //==============================================================================================//
        //==============================================================================================//
        //let toolBar = UIToolbar().ToolbarPikerteast(mySelect: #selector(dismissPicker), delegate: self)
        //txtAbout.inputAccessoryView = toolBar
        //==============================================================================================//
        //==============================================================================================//
    }
    
    
    
    
    @IBAction func addresspicPress(_ sender: UITapGestureRecognizer) {
        
        DispatchQueue.main.async {
            
       // let lattitude = CLLoca
        self.endEditing(true)
            
        let center = CLLocationCoordinate2D(latitude: Double(KVClass.shareinstance().Lattitude)!, longitude: Double(KVClass.shareinstance().Longitude)!)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePicker(config: config)
        
        placePicker.pickPlace(callback: {(place, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let place = place {
               // self.lbl_adres.text = place.name
                print(place.coordinate.latitude)
                print(place.coordinate.longitude)
                //print(place.)
                self.lbl_adresTitle.isHidden = false
                
                self.lbl_adres.text = (place.formattedAddress ?? "")
                
                if (place.formattedAddress ?? "") == ""
                {
                    self.lbl_adres.text = "Address*"
                    self.lbl_adresTitle.isHidden = true
                }
                
                KVProfileCreate.shared.setValue(place.formattedAddress, .address)
                KVProfileCreate.shared.setValue(place.coordinate.latitude, .latitude)
                KVProfileCreate.shared.setValue(place.coordinate.longitude, .longitude)
                
                NotificationCenter.default.post(name: Notification.Name("MAINTABLERELOAD"), object: nil)
                
            } else {
                //self.lbl_adres.text = "No place selected"
                //self.addressLabel.text = ""
            }
        })
        }
    }
    
    @IBAction func resignNumberpad(_ sender: UIBarButtonItem)
    {
        switch sender.tag {
        case 1:
            self.Pphonetxt.resignFirstResponder()
        case 2:
            self.Pcompanysize.resignFirstResponder()
        default:
            self.txt_estadate.resignFirstResponder()
            break
        }
    }
    
    
    func setDoneToolBarInAboutTextView(_ txtfield: UITextField) {
        let toobar = UIToolbar()
        toobar.sizeToFit()
        
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.resignAboutTextViewpad(_:)))
        doneButton.tag = txtfield.tag
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toobar.setItems([ spaceButton, doneButton], animated: false)
        toobar.isUserInteractionEnabled = true
        
        txtfield.inputAccessoryView = toobar
        txtfield.clearButtonMode = .never
    }
    
    @IBAction func resignAboutTextViewpad(_ sender: UIBarButtonItem) {
        self.txtAbout.resignFirstResponder()
    }
    
    
    func setDatepickerView(_ txtfield: UITextField)
    {
        let toobar = UIToolbar()
        
        toobar.sizeToFit()
        
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.resignNumberpad(_:)))
        doneButton.tag = txtfield.tag
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toobar.setItems([ spaceButton, doneButton], animated: false)
        toobar.isUserInteractionEnabled = true
        
        txtfield.inputAccessoryView = toobar
        txtfield.clearButtonMode = .never
        
        
        let datepicker = UIDatePicker()
        datepicker.date = Date()
        datepicker.datePickerMode = .date
        datepicker.maximumDate = Date()
        
        
        let formatter = DateFormatter()
        
        if txtfield.text == "" || txtfield.text == nil
        {
            formatter.dateFormat = "dd/MM/yyyy"
            
            txtfield.text = formatter.string(from: datepicker.date)
            
        }
        else
        {
            formatter.dateFormat = "dd/MM/yyyy"
            
            datepicker.date = formatter.date(from: txtfield.text!)!
           
        }
        
        datepicker.addTarget(self, action: #selector(self.handleDatePicker(_:)), for: .valueChanged)
        txtfield.inputView = datepicker
        
       
        
        
    }
    
    @IBAction func handleDatePicker(_ sender: UIDatePicker)
    {
       let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        self.txt_estadate.text = formatter.string(from: sender.date)
    }
    
    func setToolbarView(_ txtfield: UITextField)
    {
        let toobar = UIToolbar()
        
        toobar.sizeToFit()
        
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.resignNumberpad(_:)))
        doneButton.tag = txtfield.tag
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toobar.setItems([ spaceButton, doneButton], animated: false)
        toobar.isUserInteractionEnabled = true
        
        txtfield.inputAccessoryView = toobar
        txtfield.clearButtonMode = .whileEditing
        
    }
    
    @IBAction func uploadPicpress(_ sender: UIButton)
    {
        self.ImagePicker.delegate = self
        
        let imageAlert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
            imageAlert.dismiss(animated: true, completion: nil)
        })
        
        let Capture = UIAlertAction.init(title: "Take Photo", style: .destructive, handler: { (action) in
            
            self.ImagePicker.sourceType = .camera
            self.ImagePicker.showsCameraControls = true
            self.ImagePicker.allowsEditing = false
            
            appDelegate.window?.rootViewController?.present(self.ImagePicker, animated: true, completion: nil)
        })
        
        let chosefromlib = UIAlertAction.init(title: "Choose Photo", style: .default, handler: { (action) in
            
            self.ImagePicker.sourceType = .photoLibrary
            self.ImagePicker.allowsEditing = false
            
            appDelegate.window?.rootViewController?.present(self.ImagePicker, animated: true, completion: nil)
        })
        
        imageAlert.addAction(Capture)
        imageAlert.addAction(chosefromlib)
        imageAlert.addAction(cancel)
        
        appDelegate.window?.rootViewController?.present(imageAlert, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let PickedImage: UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        picker.dismiss(animated: true, completion: nil)
        //================Image Crop=====================//
        let cropController:TOCropViewController = TOCropViewController(image: PickedImage)
        cropController.delegate = self
        appDelegate.window?.rootViewController?.present(cropController, animated: true, completion: nil)
        //===============================================//
        
    }
    
    // MARK: - Cropper Delegate
    @objc(cropViewController:didCropToImage:withRect:angle:) func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        
        cropViewController.presentingViewController?.dismiss(animated: true, completion: { _ in })
        print("\(image)")
        
        self.Pimgview.image = image
        KVProfileCreate.shared.setValue(UIImageJPEGRepresentation(image, 0.25)?.base64EncodedString(), .logo)
    }
    //=================================================================================================//
    //=================================================================================================//
        
        

    
    
    @IBAction func addsocialPress(_ sender: UIButton)
    {
       // self.AddSocialMediapress()
    }
    
    
}
//=================================================================================================//
//=================================================================================================//


//=================================================================================================//
//==========Fourth Pro Table Cell==================================================================//
extension FourthProTableCell: UITextFieldDelegate, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate, TOCropViewControllerDelegate
{
    
    @objc func editpress(_ sender: UIButton)
    {
        self.SocialAlert?.textFields![0].becomeFirstResponder()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrType.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrType[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
        self.SocialAlert?.textFields![0].text = self.arrType[row]
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == self.Pabouttxt {
//            let maxLength = 500
//            let currentString: NSString = textField.text! as NSString
//            let newString: NSString =
//                currentString.replacingCharacters(in: range, with: string) as NSString
//            if newString.length > maxLength {
//                return false
//            }
//            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
//            let filtered = string.components(separatedBy: cs).joined(separator: "")
//
//            return (string == filtered)
//        }
        if textField == self.txt_CEO || textField == self.txt_CFO {
            let maxLength = 40
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        else if textField == self.Pcompanysize {
            let maxLength = 7
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_NUMBERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
//            if textField == self.txtAbout
//            {
//                KVProfileCreate.shared.setValue(textField.text, .about)
//            }
            if textField == self.Pphonetxt
            {
                if !(textField.text?.isPhoneValid())! {
                    return
                }
                KVProfileCreate.shared.setValue(textField.text, .contact_phone)
            }
            else if textField == self.Pemailtxt {
                if !(textField.text?.isValidEmail())! {
                    return
                }
                KVProfileCreate.shared.setValue(textField.text, .contact_email)
            }
            else if textField == self.Pcompanysize
            {
                KVProfileCreate.shared.setValue(textField.text, .company_team_size)
            }
            else if textField == self.txt_estadate
            {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                var date = Date()
                if formatter.date(from: textField.text!) != nil {
                    date = formatter.date(from: textField.text!)!
                    formatter.dateFormat = "yyyy-MM-dd"
                }
                else {
                    self.txt_estadate.text = ""
                    KVClass.ShowNotification("", "Please Select Proper Date")
                    return
                }
                
                if KVProfileCreate.shared.DataDict["type"] as? Int != 3{
                KVProfileCreate.shared.setValue((formatter.string(from: date)), .establish_date)
                }else{
                  KVProfileCreate.shared.setValue((formatter.string(from: date)), .DOB)
                }
            }
            else if textField == self.txt_CEO
            {
                KVProfileCreate.shared.setValue(textField.text, .current_ceo)
            }
            else if textField == self.txt_CFO
            {
                KVProfileCreate.shared.setValue(textField.text, .current_cfo)
        }
        
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if (self.SocialAlert?.textFields!.count ?? 0)! > 0
        {
        if textField == self.SocialAlert?.textFields![0]
        {
            self.settxtInputview(textField)
            if textField.text == nil || textField.text == ""
            {
                textField.text = self.arrType[0]
            }
        }
        }
        else if textField == self.Pphonetxt
        {
            self.setToolbarView(textField)
        }
        else if textField == self.Pcompanysize
        {
            self.setToolbarView(textField)
        }
        else if textField == self.txt_estadate
        {
            self.setDatepickerView(textField)
        }
        else if textField == self.txtAbout
        {
            
            self.setDoneToolBarInAboutTextView(textField)
        }
    }
    
    //============================================================================================//
    //MARK :- UITextView Delegate Method
    func textViewDidBeginEditing(_ textView: UITextView) {
        lblStorTitle.text = "About*"
        lblStorTitle.textColor = UIColor.init(hex: "28AFB0")
        lblDeviderStory.backgroundColor = UIColor.init(hex: "28AFB0")
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            lblStorTitle.text = ""
        }
        lblStorTitle.textColor = UIColor.lightGray
        lblDeviderStory.backgroundColor = UIColor.lightGray
        KVProfileCreate.shared.setValue(textView.text, .about)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == txtAbout {
            let maxLength = 400
            let currentString: NSString = textView.text! as NSString
            let new_Str: NSString = currentString.replacingCharacters(in: range, with: text) as NSString
            if new_Str.length > maxLength {
                return false
            }
            let newChar = maxLength - new_Str.length
            lblTxtChar.text = newChar.description
            return true
        }
        return true
    }
    //============================================================================================//
    //============================================================================================//
    
    @IBAction func doneTxt(_ sender: UIBarButtonItem)
    {
        print("hy")
        self.SocialAlert?.textFields![0].endEditing(true)
    }
    func settxtInputview(_ txtfield: UITextField)
    {
        let picker = UIPickerView.init()
        picker.delegate = self
        picker.dataSource = self
        let toobar = UIToolbar()
        
        toobar.sizeToFit()
        
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.doneTxt(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toobar.setItems([ spaceButton, doneButton], animated: false)
        toobar.isUserInteractionEnabled = true
        
        txtfield.inputView = picker
        txtfield.inputAccessoryView = toobar
        txtfield.clearButtonMode = .never
        
    }
    
//    func AddSocialMediapress()
//    {
//        self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
//
//        let attributedTitle = NSMutableAttributedString(string: "Add Social Media", attributes:
//            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
//
//        SocialAlert?.setValue(attributedTitle, forKey: "attributedTitle")
//        SocialAlert?.addTextField(configurationHandler: { (textfield) in
//
//            textfield.borderStyle = .roundedRect
//            textfield.delegate = self
//            textfield.tintColor = .clear
//            textfield.clearButtonMode = .never
//            textfield.autocorrectionType = .no
//            textfield.returnKeyType = .next
//            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
//            textfield.placeholder = "Social Media Type"
//            textfield.rightViewMode = .always
//
//            let rightView = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 30, height: 30))
//            rightView.setImage(UIImage.init(named: "downar_ic")!, for: .normal)
//            rightView.addTarget(self, action: #selector(self.editpress(_:)), for: .touchUpInside)
//            textfield.rightView = rightView
//
//        })
//        SocialAlert?.addTextField(configurationHandler: { (textfield) in
//
//            textfield.isHidden = true
//        })
//        SocialAlert?.addTextField(configurationHandler: { (textfield) in
//
//            textfield.borderStyle = .roundedRect
//            textfield.delegate = self
//            textfield.clearButtonMode = .whileEditing
//            textfield.autocorrectionType = .no
//            textfield.returnKeyType = .done
//            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
//            textfield.placeholder = "Social Media URL"
//
//        })
//
//        let addaction = UIAlertAction.init(title: "Add Media", style: .destructive) { (action) in
//            self.SocialAlert?.dismiss(animated: true, completion: nil)
//        }
//        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
//            self.SocialAlert?.dismiss(animated: true, completion: nil)
//        }
//
//        SocialAlert?.addAction(addaction)
//        SocialAlert?.addAction(cancel)
//
//        appDelegate.window?.rootViewController?.present(SocialAlert!, animated: true, completion: nil)
//
//        for textfield: UIView in (SocialAlert?.textFields!)! {
//            let container: UIView = textfield.superview!
//
//            let effectView: UIView = container.superview!.subviews[0]
//            container.backgroundColor = UIColor.clear
//            effectView.removeFromSuperview()
//        }
//    }
//
//    func AddFounderpress()
//    {
//        self.SocialAlert = UIAlertController.init(title: nil, message: "", preferredStyle: .alert)
//
//        let attributedTitle = NSMutableAttributedString(string: "Add Founders", attributes:
//            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
//
//        SocialAlert?.setValue(attributedTitle, forKey: "attributedTitle")
//        SocialAlert?.addTextField(configurationHandler: { (textfield) in
//
//            textfield.borderStyle = .roundedRect
//            textfield.delegate = self
//            textfield.tintColor = .clear
//            textfield.clearButtonMode = .never
//            textfield.autocorrectionType = .no
//            textfield.returnKeyType = .next
//            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
//            textfield.placeholder = "Firstname"
//
//
//        })
//        SocialAlert?.addTextField(configurationHandler: { (textfield) in
//
//            textfield.borderStyle = .roundedRect
//            textfield.delegate = self
//            textfield.tintColor = .clear
//            textfield.clearButtonMode = .never
//            textfield.autocorrectionType = .no
//            textfield.returnKeyType = .next
//            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
//            textfield.placeholder = "Lastname"
//
//
//        })
//        SocialAlert?.addTextField(configurationHandler: { (textfield) in
//
//            textfield.borderStyle = .roundedRect
//            textfield.delegate = self
//            textfield.clearButtonMode = .whileEditing
//            textfield.autocorrectionType = .no
//            textfield.returnKeyType = .done
//            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
//            textfield.placeholder = "Designation"
//
//        })
//
//        SocialAlert?.addTextField(configurationHandler: { (textfield) in
//
//            textfield.borderStyle = .roundedRect
//            textfield.delegate = self
//            textfield.clearButtonMode = .whileEditing
//            textfield.autocorrectionType = .no
//            textfield.returnKeyType = .done
//            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
//            textfield.placeholder = "Establish Date"
//
//        })
//        SocialAlert?.addTextField(configurationHandler: { (textfield) in
//
//            textfield.borderStyle = .roundedRect
//            textfield.delegate = self
//            textfield.clearButtonMode = .whileEditing
//            textfield.autocorrectionType = .no
//            textfield.returnKeyType = .done
//            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
//            textfield.placeholder = "Total Employees"
//
//        })
//
//        let addaction = UIAlertAction.init(title: "Add Media", style: .destructive) { (action) in
//            self.SocialAlert?.dismiss(animated: true, completion: nil)
//        }
//        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
//            self.SocialAlert?.dismiss(animated: true, completion: nil)
//        }
//
//        SocialAlert?.addAction(addaction)
//        SocialAlert?.addAction(cancel)
//
//        appDelegate.window?.rootViewController?.present(SocialAlert!, animated: true, completion: nil)
//
//        for textfield: UIView in (SocialAlert?.textFields!)! {
//            let container: UIView = textfield.superview!
//
//            let effectView: UIView = container.superview!.subviews[0]
//            container.backgroundColor = UIColor.clear
//            effectView.removeFromSuperview()
//        }
//    }
    
}
//=================================================================================================//
//=================================================================================================//


//=================================================================================================//
//================Add Social Table Cell In Table View==============================================//

extension AddSocialCell: UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    
    
    
    @objc func editpress(_ sender: UIButton)
    {
        self.SocialAlert?.textFields![0].becomeFirstResponder()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrType.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrType[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
        self.SocialAlert?.textFields![0].text = self.arrType[row]
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.SocialAlert?.textFields![0] && textField.tag != 2
        {
            self.settxtInputview(textField)
            if textField.text == nil || textField.text == ""
            {
                textField.text = self.arrType[0]
            }
        }
    }
    
    @IBAction func doneTxt(_ sender: UIBarButtonItem)
    {
        print("hy")
        self.SocialAlert?.textFields![0].endEditing(true)
    }
    func settxtInputview(_ txtfield: UITextField)
    {
        let picker = UIPickerView.init()
        picker.delegate = self
        picker.dataSource = self
        let toobar = UIToolbar()
        
        toobar.sizeToFit()
        
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.doneTxt(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toobar.setItems([spaceButton, doneButton], animated: false)
        toobar.isUserInteractionEnabled = true
        
        txtfield.inputView = picker
        txtfield.inputAccessoryView = toobar
        txtfield.clearButtonMode = .never
   }
    
}
//=================================================================================================//
//=================================================================================================//



//=================================================================================================//
//=================================================================================================//
//=================================================================================================//
