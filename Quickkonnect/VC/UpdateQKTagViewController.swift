//
//  UpdateQKTagViewController.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 30/12/17.
//  Copyright © 2017 com.kavish. All rights reserved.
//

import UIKit
import Quikkly
import SDWebImage
import Quikkly

class UpdateQKTagViewController: UIViewController, PickerSetQkTag {
    
    
    
    //<<==========IBOutlets==========>>
    var isreload = false
    var isremovepic = false
    var issubProfile = false
    
    var str_type = ""
    var str_user_id = ""
    var strScreenFrom = ""
    var QKPatterncolor = ""
    var strSelectedImage = ""
    var QKgeneratedBase64 = ""
    var QKBordercolorString = ""
    var QKBackgroundcolorString = ""
    var QKMiddleTagURI = String()
    
    var ColorPickerColor = [String]()
    var ColorPickerTitle = ["Background","Border","Pattern"]
    
    var QKTagView: ScannableView!
    var QKSkin = ScannableSkin()
    var QKScanable: Scannable!
    var strMiddleBase64 = ""
    @IBOutlet var testbtn: UIButton!
    @IBOutlet var testtxt: UITextField!
    @IBOutlet var QKMainView: UIView!
    @IBOutlet var QKBaseView: UIImageView!
    @IBOutlet var QKMainBackImg: UIImageView!
    @IBOutlet var QKMainBackFullImg: UIImageView!
    @IBOutlet var contraint_QKtagY: NSLayoutConstraint!
    @IBOutlet var contraint_QKTagHeight: NSLayoutConstraint!
    @IBOutlet var SelectProfileBtn: UIButton!
    @IBOutlet var Titlelbl: UILabel!
    @IBOutlet var Activity_view: UIActivityIndicatorView!
    @IBOutlet var QKGenerateview: UIView!
    @IBOutlet var Removeimage_btn: UIButton!
    @IBOutlet var QKGenXAxis: NSLayoutConstraint!
    var UserDetal: resultUserDict! = resultUserDict()
    var dict_subuser_data: resultUserDict! = resultUserDict()
    @IBOutlet var btn_editImage: UIButton!
    let QkColorPickView = UIView.fromNib("QKColorView") as! QKColorView
    
    @IBOutlet var collect_color: UICollectionView!
    @IBOutlet var lbl_ColorError: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //QKMainBackImg.layer.cornerRadius = 8
        if strMiddleBase64 == "" {
            strMiddleBase64 = (UIImageJPEGRepresentation(#imageLiteral(resourceName: "logo"), 0.25)?.base64EncodedString())!
        }
        
        
        self.QKTagView = ScannableView.init(frame: self.QKBaseView.bounds)
        self.view.addSubview(self.QKTagView)
        self.QKTagView.layoutSubviews()
        
        self.QKGenXAxis.constant = self.view.bounds.width
        
        if !self.issubProfile
        {
            self.ColorPickerColor = [self.convertHexcodetoString(KVAppDataServieces.shared.UserValue(.background_color) as! String),self.convertHexcodetoString(KVAppDataServieces.shared.UserValue(.border_color) as! String),self.convertHexcodetoString(KVAppDataServieces.shared.UserValue(.pattern_color) as! String)]
            
            self.QKBackgroundcolorString = KVAppDataServieces.shared.UserValue(.background_color) as! String
            self.QKBordercolorString = KVAppDataServieces.shared.UserValue(.border_color) as! String
            self.QKPatterncolor = KVAppDataServieces.shared.UserValue(.pattern_color) as! String
            self.strSelectedImage = KVAppDataServieces.shared.UserValue(.background_gradient) as! String
        }
        else
        {
            self.ColorPickerColor = [self.convertHexcodetoString(self.dict_subuser_data.background_color),self.convertHexcodetoString(self.dict_subuser_data.border_color),self.convertHexcodetoString(self.dict_subuser_data.pattern_color)]
            
            self.QKBackgroundcolorString = self.dict_subuser_data.background_color
            self.QKBordercolorString = self.dict_subuser_data.border_color
            self.QKPatterncolor = self.dict_subuser_data.pattern_color
            self.strSelectedImage = "\(self.dict_subuser_data.background_gradient)"
        }
        
        
        self.setProfilebtnTitle()
        self.QKMainBackFullImg.image = UIImage.init(named: "gradient-" + self.strSelectedImage)
        self.Removeimage_btn.addTarget(self, action: #selector(removeTagImage(_:)), for: .touchUpInside)
        
        let skin = ScannableSkin()
        skin.backgroundColor = "#\(self.convertHexcodetoString(self.QKBackgroundcolorString))"
        skin.dataColors = ["#\(self.convertHexcodetoString(self.QKPatterncolor))"]
        skin.borderColor = "#\(self.convertHexcodetoString(self.QKBordercolorString))"
        skin.imageUri = strMiddleBase64 // KVAppDataServieces.shared.UserValue(.middle_tag) as? String ?? ""
        skin.overlayColor = "#\(self.convertHexcodetoString(self.QKBordercolorString))"
        let scan = Scannable.init(withValue: UInt64(Int(KVAppDataServieces.shared.UserValue(.unique_code) as! String)!), template: "template0015style2", skin: skin)
        self.QKTagView.scannable = scan
        
        self.isreload = true
        
        self.btn_editImage.setImage(#imageLiteral(resourceName: "edit").tint(with: UIColor.init(hex: "ffffff")), for: .normal)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIView.animate(withDuration: 2, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }) { (loaded) in
        }
        
        if self.view.tag == 2214
        {
            self.Titlelbl.text = "Update QK Tag"
        }
        else
        {
            self.Titlelbl.text = "Generate QK Tag"
        }
        
        if self.isreload {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.generateQK), object: nil)
            self.perform(#selector(self.generateQK), with: nil, afterDelay: 0.1)
            self.isreload = false
        }
    }
    
    
    func generateQK() {
        
            let skin = ScannableSkin()
            skin.backgroundColor = "#\(self.convertHexcodetoString(self.QKBackgroundcolorString))"
            skin.dataColors = ["#\(self.convertHexcodetoString(self.QKPatterncolor))"]
            skin.borderColor = "#\(self.convertHexcodetoString(self.QKBordercolorString))"
            
            if self.isremovepic
            {
                self.Removeimage_btn.isHidden = false
            }
            else
            {
                self.Removeimage_btn.isHidden = true
            }
        
        //let image = UIImage.init(named: "logo")
        
        //let urist = (UIImagePNGRepresentation(image!)?.base64EncodedString())!
        skin.imageUri = "data:image/png;base64,\(strMiddleBase64)"
        var middletag = ""
        if self.issubProfile
        {
            middletag = self.dict_subuser_data.middle_tag
        }
        else
        {
           middletag = KVAppDataServieces.shared.UserValue(.middle_tag) as? String ?? ""
        }
        
            if self.QKMiddleTagURI != ""
            {
                skin.imageUri = "data:image/png;base64,\(strMiddleBase64)"
            }
            else if middletag != ""
            {
                //skin.imageUri = middletag
//
                
                SDWebImageManager.shared().loadImage(with: URL.init(string: middletag)!, options: SDWebImageOptions.refreshCached, progress: { (st, st2, st3) in

                }, completed: { (img, dta, err, type, finish, url) in

                    if err == nil
                    {
                        self.QKMiddleTagURI = (UIImageJPEGRepresentation(img ?? #imageLiteral(resourceName: "logo"), 0.25)?.base64EncodedString())!//(UIImagePNGRepresentation(img!)?.base64EncodedString())!
                        self.isremovepic = false
                       // self.generateQK()
                    }
                })
            }
                
            else if middletag == "" && self.QKMiddleTagURI == ""
            {
                
                self.isremovepic = false
                let image = UIImage.init(named: "logo")
                
                let image2 = image?.resized(withPercentage: 0.8)
                
                self.QKMiddleTagURI = (UIImagePNGRepresentation(image2!)?.base64EncodedString())!
                
                  skin.imageUri = "data:image/png;base64,\(strMiddleBase64)"
            }
        
         skin.overlayColor = "#\(self.convertHexcodetoString(self.QKBordercolorString))"
        
        var scan = Scannable.init(withValue: 9898909909, template: "template0015style2", skin: skin)
        
        if self.issubProfile
        {
           scan = Scannable.init(withValue: UInt64(Int(self.dict_subuser_data.unique_code)!), template: "template0015style2", skin: skin)
        }
        else
        {
           scan = Scannable.init(withValue: UInt64(Int(KVAppDataServieces.shared.UserValue(.unique_code) as! String)!), template: "template0015style2", skin: skin)
        }

        self.QKTagView.scannable = scan
    }
    
    @objc func QKprocessing()
    {
        UIGraphicsBeginImageContextWithOptions(self.QKTagView.bounds.size, false, 0.0)
        let context = UIGraphicsGetCurrentContext()
        self.QKTagView.layer.render(in: context!)
        
        let imAge = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        var img = imAge!
        
        var base64 = (UIImagePNGRepresentation(img)?.base64EncodedString())!
        
        img = UIImage.init(data: Data.init(base64Encoded: base64)!)!
        
        let imageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: (img.size.width), height: (img.size.height)))
        imageView.image = img
        
        let crp = img.cgImage?.cropping(to: CGRect.init(x: 0, y: 0, width: (img.size.width), height: (img.size.width)))
        
        base64 = (UIImagePNGRepresentation(UIImage.init(cgImage: crp!))?.base64EncodedString(options: Data.Base64EncodingOptions.endLineWithCarriageReturn))!
        
        self.QKgeneratedBase64 = base64
        
        self.QKBaseView.image = UIImage.init(data: Data.init(base64Encoded: base64)!)!
        
        if self.UserDetal.middle_tag == ""
        {
            self.Activity_view.stopAnimating()
        }
    }
    
    
    
    
    //===========================================================================================//
    //MARK :- UIButton Action Method
    @IBAction func btnCancelClick(_ sender: Any) {
        if self.strScreenFrom == "CreateProfile" {
            
            let Discart = UIAlertController.init(title: "", message: "", preferredStyle: .alert)

            let attributedTitle = NSMutableAttributedString(string: "Warning !", attributes:
                [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
            
            let attributedMessage = NSMutableAttributedString(string: "Do you really want Discard Changes ?", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
            Discart.setValue(attributedTitle, forKey: "attributedTitle")
            Discart.setValue(attributedMessage, forKey: "attributedMessage")
            
            let ok = UIAlertAction.init(title: "Yes", style: UIAlertActionStyle.destructive, handler: { (action) in
                
                KVProfileCreate.shared.DataDict = NSMutableDictionary()
                KVProfileCreate.shared.arr_ContactInfo.removeAll()
                KVProfileCreate.shared.arr_WebInfo.removeAll()
                KVProfileCreate.shared.arr_Founders.removeAll()
                KVProfileCreate.shared.arr_EmailInfo.removeAll()
                KVProfileCreate.shared.arr_SocialMedia.removeAll()
                KVProfileCreate.shared.arr_OtherAddress.removeAll()
                //=====================Specific View Controll PoP====================//
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: QKProListVC.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
                //====================================================================//
            })
            let no = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                Discart.dismiss(animated: true, completion: nil)
            })
            
            Discart.addAction(ok)
            Discart.addAction(no)
            
            self.present(Discart, animated: true, completion: nil)
            
        }
        else if self.strScreenFrom == "Slider" {
            
            let Discart = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
            
            let attributedTitle = NSMutableAttributedString(string: "Warning !", attributes:
                [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
            
            let attributedMessage = NSMutableAttributedString(string: "Do you really want Discard Changes ?", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
            Discart.setValue(attributedTitle, forKey: "attributedTitle")
            Discart.setValue(attributedMessage, forKey: "attributedMessage")
            
            let ok = UIAlertAction.init(title: "Yes", style: UIAlertActionStyle.destructive, handler: { (action) in
                
                KVProfileCreate.shared.DataDict = NSMutableDictionary()
                KVProfileCreate.shared.arr_ContactInfo.removeAll()
                KVProfileCreate.shared.arr_WebInfo.removeAll()
                KVProfileCreate.shared.arr_Founders.removeAll()
                KVProfileCreate.shared.arr_EmailInfo.removeAll()
                KVProfileCreate.shared.arr_SocialMedia.removeAll()
                KVProfileCreate.shared.arr_OtherAddress.removeAll()
                //=====================Specific View Controll PoP====================//
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "tabbar")
                let navi = UINavigationController.init(rootViewController: vc!)
                navi.setNavigationBarHidden(true, animated: false)
                app_Delegate.window?.rootViewController = navi
                //====================================================================//
            })
            let no = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                Discart.dismiss(animated: true, completion: nil)
            })
            
            Discart.addAction(ok)
            Discart.addAction(no)
            
            self.present(Discart, animated: true, completion: nil)
            
            
            
            
        }
        else {
            
//            let transition = CATransition()
//            transition.duration = 0.5
//            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//            transition.type = kCATransitionPush
//            transition.subtype = kCATransitionFromLeft
//            self.navigationController!.view.layer.add(transition, forKey: nil)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnDoneClick(_ sender: Any) {
        
        
       KVClass.KVShowLoader()
        self.getQKImageBase64 { (basest64) in
            
            KVClass.KVHideLoader()
            
            if !self.issubProfile
            {
                let dict = NSMutableDictionary()
                dict["middle_tag"] = self.strMiddleBase64
                dict["qr_code"] = basest64
                dict["user_id"] = KVAppDataServieces.shared.LoginUserID()
                dict["type"] = 1
                dict["border_color"] = self.QKBordercolorString
                dict["background_color"] = self.QKBackgroundcolorString
                dict["pattern_color"] = self.QKPatterncolor
                dict["background_gradient"] = self.strSelectedImage
                
                KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict, api: "user_qr", arrFlag: false) { (JSON, status) in
                
                    if status == 1 {
                        if let dataDict = JSON["data"] as? NSDictionary {
                            if !self.issubProfile {
                                KVAppDataServieces.shared.update(.background_color, dataDict["background_color"] as? String ?? "0xffFFFFFF")
                                KVAppDataServieces.shared.update(.border_color, dataDict["border_color"] as? String ?? "0xff000000")
                                KVAppDataServieces.shared.update(.pattern_color, dataDict["pattern_color"] as? String ?? "0xff000000")
                                KVAppDataServieces.shared.update(.middle_tag, dataDict["middle_tag"] as? String ?? "")
                                KVAppDataServieces.shared.update(.qr_code, dataDict["url"] as? String ?? "")
                                KVAppDataServieces.shared.update(.background_gradient, dataDict["background_gradient"] as? String ?? "1")
                            }
                        }
                        if !self.issubProfile {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "tabbar")
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        else {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "QKProListVC") as! QKProListVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
            else {
                if self.dict_subuser_data.is_primary == "N" || self.dict_subuser_data.is_primary == "" {
                    let dict_param = NSMutableDictionary()
                    dict_param["middle_tag"] = self.strMiddleBase64
                    dict_param["qr_code"] = basest64
                    dict_param["user_id"] = self.dict_subuser_data.id
                    dict_param["type"] = 2
                    dict_param["border_color"] = self.QKBordercolorString
                    dict_param["background_color"] = self.QKBackgroundcolorString
                    dict_param["pattern_color"] = self.QKPatterncolor
                    dict_param["background_gradient"] = self.strSelectedImage
                    
                    KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict_param, api: "user_qr", arrFlag: false, Hendler: { (jSON, state) in
                        
                        if state == 1 {
                            if self.strScreenFrom == "CreateProfile" {
                                app_Delegate.isBackChange = true
                                //=====================Specific View Controll PoP====================//
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: QKProListVC.self) {
                                        self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                            //====================================================================//
                            }
                            else if self.strScreenFrom == "Slider" {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "tabbar")
                                let navi = UINavigationController.init(rootViewController: vc!)
                                navi.setNavigationBarHidden(true, animated: false)
                                app_Delegate.window?.rootViewController = navi
                                
                                
//                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "tabbar") as! TabBarViewController
//                                vc.selectedIndex = 0
//                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            else if self.strScreenFrom == "Dashboard" {
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                            else {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    })
                }
                else {
                    let dict_param = NSMutableDictionary()
                    dict_param["middle_tag"] = self.strMiddleBase64
                    dict_param["qr_code"] = basest64
                    dict_param["user_id"] = self.dict_subuser_data.id
                    dict_param["type"] = 2
                    dict_param["border_color"] = self.QKBordercolorString
                    dict_param["background_color"] = self.QKBackgroundcolorString
                    dict_param["pattern_color"] = self.QKPatterncolor
                
                    KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dict_param, api: "user_qr", arrFlag: false, Hendler: { (jSON, state) in
                    
                        if state == 1 {
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                }
            }
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let screenwidth = UIScreen.main.bounds.size.width
        let scale = screenwidth/320
        self.contraint_QKTagHeight.constant = self.contraint_QKTagHeight.constant * scale
        self.view.updateConstraints()
        var rect = CGRect()
        let originY = self.QKMainView.frame.origin.y + ((self.QKMainView.frame.height / 2) - self.QKBaseView.frame.height/2)
        rect = CGRect.init(x: 0, y:originY, width: screenwidth, height: self.QKBaseView.frame.height)
        self.QKTagView.frame = rect
        self.generateQK()
    }
    
    func convertHexcodetoString(_ hexcode: String) -> String
    {
        
        
        if hexcode.characters.count > 5
        {
            let st = hexcode.substring(from: hexcode.index(hexcode.endIndex, offsetBy: -6))
            
            return st
        }
        return "FFFFFF"
    }
    
    
    func setQkTabBackImage(_ image: UIImage?, _ selectedImgIndex: String) {
        print(selectedImgIndex)
        self.QKMainBackFullImg.image = image
        strSelectedImage = selectedImgIndex
        
    }
    func setSelectedColorQK(_ colorString: String, _ tag: Int)
    {
        DispatchQueue.main.async {
            
            if tag == 0
            {
                if UIColor.init(hex: colorString).isEqualToColor(color: UIColor.init(hex: self.convertHexcodetoString(self.QKPatterncolor)), withTolerance: 0.4){
                    
                    KVClass.ShowNotification("", "Please select other Background Color!")
                    self.ColorPickerColor[tag] = self.QKBackgroundcolorString
                    self.lbl_ColorError.text = "The color of the QK-Tag should differ (in-contrast) from the background color.If failed to do so,the scanner may not recognize your QK-Tag and will lead to errors.Thank you for your understanding."
                    return
                }
                self.QKSkin.backgroundColor = "#\(colorString)"
                self.QKBackgroundcolorString = "0xff\(colorString)"
                self.ColorPickerColor[tag] = colorString
                self.lbl_ColorError.text = ""
            }
            else if tag == 1
            {
                
                if UIColor.init(hex: self.convertHexcodetoString(self.QKBackgroundcolorString)).isEqualToColor(color: UIColor.init(hex: colorString), withTolerance: 0.4) {
                    
                    KVClass.ShowNotification("", "Please select other Border Color!")
                    self.ColorPickerColor[tag] = self.QKBordercolorString
                    self.lbl_ColorError.text = "The color of the QK-Tag should differ (in-contrast) from the background color.If failed to do so,the scanner may not recognize your QK-Tag and will lead to errors.Thank you for your understanding."
                    return
                    
                }
                
                self.QKSkin.borderColor = "#\(colorString)"
                self.QKSkin.overlayColor = "#\(colorString)"
                self.QKBordercolorString = "0xff\(colorString)"
                self.ColorPickerColor[tag] = colorString
                self.lbl_ColorError.text = ""
            }
            else if tag == 2
            {
                if UIColor.init(hex: self.convertHexcodetoString(self.QKBackgroundcolorString)).isEqualToColor(color: UIColor.init(hex: colorString), withTolerance: 0.4) {
                    
                    KVClass.ShowNotification("", "Please select other Pattern Color!")
                    self.ColorPickerColor[tag] = self.QKPatterncolor
                    self.lbl_ColorError.text = "The color of the QK-Tag should differ (in-contrast) from the background color.If failed to do so,the scanner may not recognize your QK-Tag and will lead to errors.Thank you for your understanding."
                    return
                    
                }
                self.QKSkin.dataColors = ["#\(colorString)"]
                self.QKPatterncolor = "0xff\(colorString)"
                self.ColorPickerColor[tag] = colorString
                self.lbl_ColorError.text = ""
            }
            
            self.collect_color.reloadData()
            
            self.generateQK()
            
        }
    }
    
    @IBAction func selectProfilePress(_ sender: UIButton)
    {
        DispatchQueue.main.async {
            
            let Imagepicker = UIImagePickerController()
            Imagepicker.delegate = self
            
            let ImageAlt = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
                
                ImageAlt.dismiss(animated: true, completion: nil)
            }
            
            let capture = UIAlertAction.init(title: "Take Photo", style: .destructive, handler: { (action) in
                
                Imagepicker.allowsEditing = false
                Imagepicker.sourceType = .camera
                Imagepicker.showsCameraControls = true
                self.present(Imagepicker, animated: true, completion: nil)
            })
            
            let photoLib = UIAlertAction.init(title: "Choose Photo", style: .default) { (acton) in
                
                Imagepicker.allowsEditing = false
                Imagepicker.sourceType = .photoLibrary
                
                // ImageAlt.dismiss(animated: true, completion: {
                
                self.present(Imagepicker, animated: true, completion: nil)
                // })
            }
            
            
            ImageAlt.addAction(capture)
            ImageAlt.addAction(photoLib)
            ImageAlt.addAction(cancel)
            
            self.present(ImageAlt, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func removeTagImage(_ sender: UIButton)
    {
        self.isremovepic = false
        let image = UIImage.init(named: "logo")
        
        let image2 = image?.resized(withPercentage: 0.8)
        
        self.strMiddleBase64 = (UIImagePNGRepresentation(image2!)?.base64EncodedString())!
        
        
        self.generateQK()
    }
    
    
    
    
    func getQKImageBase64(Hendler complition:@escaping (_ base64:String) -> Void)
    {
        
        DispatchQueue.main.async {
            
            
            UIGraphicsBeginImageContextWithOptions(self.QKTagView.bounds.size, false, 0.0)
            let context = UIGraphicsGetCurrentContext()
            self.QKTagView.layer.render(in: context!)
            let imAge = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            var imagecomp = imAge
            
            var base64 = (UIImagePNGRepresentation(imagecomp!)?.base64EncodedString())!
            
            imagecomp = UIImage.init(data: Data.init(base64Encoded: base64)!)!
            
            let imageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: (imagecomp?.size.width)!, height: (imagecomp?.size.height)!))
            imageView.image = imagecomp
            
            let crp = imagecomp?.cgImage?.cropping(to: CGRect.init(x: ((imagecomp?.size.width)!/2 - (imagecomp?.size.height)!/2), y: 0, width: (imagecomp?.size.height)!, height: (imagecomp?.size.height)!))
            
            
            base64 = (UIImagePNGRepresentation(UIImage.init(cgImage: crp!))?.base64EncodedString())!
            
            self.QKgeneratedBase64 = base64
            
            complition(self.QKgeneratedBase64)
            
        }
    }
}

extension UIImage {
    
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func resizedTo1MB() -> UIImage? {
        guard let imageData = UIImagePNGRepresentation(self) else { return nil }
        
        var resizingImage = self
        var imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB
        
        while imageSizeKB > 1000 { // ! Or use 1024 if you need KB but not kB
            guard let resizedImage = resizingImage.resized(withPercentage: 0.9),
                let imageData = UIImagePNGRepresentation(resizedImage)
                else { return nil }
            
            resizingImage = resizedImage
            imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB
        }
        
        return resizingImage
    }
}

extension UIImage {
    func imageByMakingWhiteBackgroundTransparent() -> UIImage? {
        
        let image = UIImage(data: UIImageJPEGRepresentation(self, 0.25)!)!
        let rawImageRef: CGImage = image.cgImage!
        
        let colorMasking: [CGFloat] = [222, 255, 222, 255, 222, 255]
        UIGraphicsBeginImageContext(image.size);
        
        let maskedImageRef = rawImageRef.copy(maskingColorComponents: colorMasking)
        UIGraphicsGetCurrentContext()?.translateBy(x: 0.0,y: image.size.height)
        UIGraphicsGetCurrentContext()?.scaleBy(x: 1.0, y: -1.0)
        UIGraphicsGetCurrentContext()?.draw(maskedImageRef!, in: CGRect.init(x: 0, y: 0, width: image.size.width, height: image.size.height))
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
        
    }
}


extension UIImageView{
    func imageFrame()->CGRect{
        let imageViewSize = self.frame.size
        guard let imageSize = self.image?.size else{return CGRect.zero}
        let imageRatio = imageSize.width / imageSize.height
        let imageViewRatio = imageViewSize.width / imageViewSize.height
        
        if imageRatio < imageViewRatio {
            let scaleFactor = imageViewSize.height / imageSize.height
            let width = imageSize.width * scaleFactor
            let topLeftX = (imageViewSize.width - width) * 0.5
            return CGRect(x: topLeftX, y: 0, width: width, height: imageViewSize.height)
        }else{
            let scalFactor = imageViewSize.width / imageSize.width
            let height = imageSize.height * scalFactor
            let topLeftY = (imageViewSize.height - height) * 0.5
            return CGRect(x: 0, y: topLeftY, width: imageViewSize.width, height: height)
        }
    }
}



extension UpdateQKTagViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    
    // MARK:- EDIT QKIMAGE
    
    @IBAction func editQKbackImagepress(_ sender: UIButton){
        
        DispatchQueue.main.async {
            print(sender.tag)
            self.QkColorPickView.tag = 3
            self.QkColorPickView.isImageMode = true
            self.QkColorPickView.datadelegate = self
            self.QkColorPickView.selectImage = self.strSelectedImage
            self.view.addSubview(self.QkColorPickView)
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.ColorPickerTitle.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
       // let sizeST = NSMutableAttributedString.init(string: self.ColorPickerTitle[0])
        
        let cellSize = CGSize.init(width: collectionView.bounds.width/3, height: collectionView.bounds.height)
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: QKColorCell = collectionView.dequeueReusableCell(withReuseIdentifier: "QKColorCell", for: indexPath) as! QKColorCell
        
        cell.Color_view.layer.borderColor = UIColor.lightGray.cgColor
        cell.Color_view.layer.borderWidth = 0.8
        
        cell.Title_lbl.text = self.ColorPickerTitle[indexPath.row]
        if indexPath.row != 3{
        cell.Color_view.backgroundColor = UIColor.init(hex: self.ColorPickerColor[indexPath.row])
        }
        
        
        cell.SelectCell = {(sender) in
            
            UIView.transition(with: self.view, duration: 0.2, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
                
            }, completion: { (finished) in
                
                DispatchQueue.main.async {
                    cell.QkColorPickView.tag = indexPath.row
                    cell.QkColorPickView.isImageMode = indexPath.row == 3 ? true : false
                    cell.QkColorPickView.datadelegate = self
                    print(self.hexStringFromColor(cell.Color_view.backgroundColor!))
                    cell.QkColorPickView.selectedColor = cell.Color_view.backgroundColor!
                    self.view.addSubview(cell.QkColorPickView)
                }
            })
            
        }
        
        return cell
    }
    
    func setProfilebtnTitle()
    {
        let tst = NSMutableAttributedString.init(string: "  Choose QKTag Image")
        tst.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange.init(location: 0, length: tst.length))
        let title = NSMutableAttributedString.init(string: "   ")
        let imageatach = NSTextAttachment()
        imageatach.image = UIImage.init(named: "uploadqk_ic")
        imageatach.bounds = CGRect.init(x: 0, y: -2.0, width: tst.size().height, height: tst.size().height)
        let attachst = NSAttributedString(attachment: imageatach)
        
        title.append(attachst)
        title.append(tst)
        
        self.SelectProfileBtn.setAttributedTitle(title, for: .normal)
        
    }
}

class QKColorCell: UICollectionViewCell, PickColorDeleagte
{
    
    
    @IBOutlet var Title_lbl: UILabel!
    @IBOutlet var Color_view: UIView!
    var SelectCell: ((UITapGestureRecognizer) -> Void)? = nil
    var TapGesture = UITapGestureRecognizer()
    let QkColorPickView = UIView.fromNib("QKColorView") as! QKColorView
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        TapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapPress(_:)))
        self.addGestureRecognizer(TapGesture)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
//        self.Color_view.layer.cornerRadius = self.Color_view.frame.height / 2
//        self.Color_view.clipsToBounds = true
        
    }
    
    
    @objc func tapPress(_ sender: UITapGestureRecognizer)
    {
        if SelectCell != nil
        {
            self.QkColorPickView.delegate = self
            self.SelectCell!(sender)
        }
    }
    
    func setSelectedColor(_ colorString: String) {
        
       // self.Color_view.backgroundColor = UIColor.init(hex: colorString)
    }
    
}

extension UpdateQKTagViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, TOCropViewControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true) {
            
            DispatchQueue.main.async {
                
                let PickedImage: UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
                //================Image Crop=====================//
                let cropController:TOCropViewController = TOCropViewController(image: PickedImage)
                cropController.delegate = self
                appDelegate.window?.rootViewController?.present(cropController, animated: true, completion: nil)
                //===============================================//
            }
            
        }
    }
    
    // MARK: - Cropper Delegate
    @objc(cropViewController:didCropToImage:withRect:angle:) func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        
        cropViewController.presentingViewController?.dismiss(animated: true, completion: { _ in })
        print("\(image)")
        
        let compimg = image.resized(withPercentage: 0.2)
        
        self.UserDetal.middle_tag = ""
        //self.QKMiddleTagURI = (UIImageJPEGRepresentation(image, 0.25)?.base64EncodedString())!
        self.strMiddleBase64 = (UIImageJPEGRepresentation(image, 0.25)?.base64EncodedString())!
        self.isremovepic = true
        self.generateQK()
    }
    //=================================================================================================//
    //=================================================================================================//
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func hexStringFromColor(_ color: UIColor) -> String
    {
        let comp = color.cgColor.components
        
        return String.init(format: "%02lX%02lX%02lX", lroundf(Float(comp![0]*255)),lroundf(Float(comp![1]*255)),lroundf(Float(comp![2]*255)))
    }
}
