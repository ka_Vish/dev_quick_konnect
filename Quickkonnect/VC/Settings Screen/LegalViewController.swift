//
//  LegalViewController.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 02/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit

class LegalViewController: QKMainBaseVC {

    var title_arr = ["Read Terms & Conditions    ","Privacy Contract","Quickkonnect UG","Press"]
    var arr_Images = [#imageLiteral(resourceName: "read-terms-&-Conditions"), #imageLiteral(resourceName: "privacy-contract"), #imageLiteral(resourceName: "quickkonnect-UG"), #imageLiteral(resourceName: "press")]
    var url_arr = ["http://quickkonnect.com/terms-of-use.html","http://quickkonnect.com/privacy-contract.html","http://quickkonnect.com/copyright-quickkonnect.html","http://quickkonnect.com/press.html"]
    
    
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.Bar.btn_Chat.isHidden = true
        self.Bar.Title_lbl.text = "Legal"
        self.Bar.Back_btn.isHidden = false
        self.Bar.Menu_btn.isHidden = true
        self.Bar.Noti_btn.isHidden = true
        //self.Bar.img_TopBG.isHidden = true
        self.Bar.Title_lbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.Bar.Profile_Img.isHidden = true
        self.Bar.Profile_Img_BG.isHidden = true
        self.Bar.backgroundColor = UIColor.init(hex: "28AFB0")
        self.Bar.constraint_img_profile_trilling.constant = 12
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //=====================================================================================================//
    //MARK :- UIButton Action Method
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        kUserDefults(false as AnyObject, key: "ishomeupdate")
        self.dismiss(animated: true, completion: nil)
    }
    //=====================================================================================================//
    //=====================================================================================================//

}

//=====================================================================================================//
//MARK :- UITable View Delegate DataSource Method
extension LegalViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 65
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: LegalCell = tableView.dequeueReusableCell(withIdentifier: "LegalCell", for: indexPath) as! LegalCell
        cell.title_label.text = self.title_arr[indexPath.row]
        cell.img_Options.image = self.arr_Images[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpSettingWebVC") as! HelpSettingWebVC
            obj.titlestring = self.title_arr[indexPath.row]
            obj.urlstring = self.url_arr[indexPath.row]
            self.present(obj, animated: true, completion: nil)
        }
    }
}
//=====================================================================================================//
//=====================================================================================================//

class LegalCell: UITableViewCell {
    
    @IBOutlet var title_label: UILabel!
    @IBOutlet var img_Options: UIImageView!
}
