//
//  RepoertProblemVC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 21/03/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import Material


class RepoertProblemVC: UIViewController {

    
    @IBOutlet var txt_Name: TextField!
    @IBOutlet var txt_Email: TextField!
    @IBOutlet var txt_Comment: TextField!
    
    @IBOutlet var btn_UploadAttachmnet: UIButton!
    @IBOutlet var collection_Images: UICollectionView!
    var arr_Image = [String]()
    var ImagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.arr_Image.removeAll()
        self.collection_Images.reloadData()
        self.txt_Comment.setDivider()
        self.txt_Email.setDivider()
        self.txt_Name.setDivider()
        
        self.txt_Name.text = KVAppDataServieces.shared.UserValue(.fullname) as? String ?? ""
        self.txt_Email.text = KVAppDataServieces.shared.UserValue(.email) as? String ?? ""
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.collection_Images.reloadData()
    }
    
    // MARK: Back Action
    
    
    @IBAction func backpress(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    // MARK: Pick Image
    
    @IBAction func pickAttachMent(_ sender: UIButton) {
        
        DispatchQueue.main.async {
        
        self.ImagePicker.delegate = self
            
        let imageAlert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
            imageAlert.dismiss(animated: true, completion: nil)
        })
        
        let Capture = UIAlertAction.init(title: "Take Photo", style: .destructive, handler: { (action) in
            
            self.ImagePicker.sourceType = .camera
            self.ImagePicker.showsCameraControls = true
            self.ImagePicker.allowsEditing = false
            
            self.present(self.ImagePicker, animated: true, completion: nil)
        })
        
        let chosefromlib = UIAlertAction.init(title: "Choose Photo", style: .default, handler: { (action) in
            
            self.ImagePicker.sourceType = .photoLibrary
            self.ImagePicker.allowsEditing = false
            
            self.present(self.ImagePicker, animated: true, completion: nil)
        })
        
        imageAlert.addAction(Capture)
        imageAlert.addAction(chosefromlib)
        imageAlert.addAction(cancel)
        
        self.present(imageAlert, animated: true, completion: nil)
        }
    }
    
    // MARK:- Submit Problem
    
    @IBAction func submitProblempress(_ sender: UIButton){
        
        if self.validateTextfieldValue() {
            
            let dictParam = NSMutableDictionary()
            dictParam["user_id"] = KVAppDataServieces.shared.LoginUserID()
            dictParam["msg"] = self.txt_Comment.text
            dictParam["name"] = self.txt_Name.text
            dictParam["email"] = self.txt_Email.text
            dictParam["file"] = self.arr_Image.count > 0 ? self.arr_Image[0] : ""
            
            KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self, processLabel: "", params: dictParam, api: "user_feedback", arrFlag: false, Hendler: { (JSON, status) in
                
                if status == 1{
                    
                    KVClass.ShowNotification("", JSON["msg"] as? String ?? "")
                    self.navigationController?.popViewController(animated: true)
                }
                
            })
            
        }
        
    }
    
    
}

// MARK:- Extension for TextField Delefgate

extension RepoertProblemVC: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.txt_Name && (self.txt_Name.text == "" || self.txt_Name.text == nil) {
            KVClass.ShowNotification("", "Please enter valid name!")
            return
        }
        else if textField == self.txt_Email && (!(self.txt_Email.text?.isValidEmail())!) {
            KVClass.ShowNotification("", "Please enter valid email!")
            return
        }
        else if textField == self.txt_Comment && (self.txt_Comment.text == "" || self.txt_Comment.text == nil) {
            KVClass.ShowNotification("", "Please enter your comment!")
            return
        }
    }
    
    
    // MARK :- Validate Text Value
    
    func validateTextfieldValue() -> Bool{
        
        if self.txt_Name.text == "" || self.txt_Name.text == nil {
            KVClass.ShowNotification("", "Please enter valid name!")
            return false
        }
        else if !(self.txt_Email.text?.isValidEmail())! {
            KVClass.ShowNotification("", "Please enter valid email!")
            return false
        }
        else if self.txt_Comment.text == "" || self.txt_Comment.text == nil {
            KVClass.ShowNotification("", "Please enter your comment!")
            return false
        }
        else {
            return true
        }
    }
    
}



// MARK:- Extension for Collection

extension RepoertProblemVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    
   // MARK:- CollectionView Delegate
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.showHideButton()
        return self.arr_Image.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: AttachmentCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCell", for: indexPath) as! AttachmentCell
        cell.img_Image.image = UIImage.init(data: Data.init(base64Encoded: self.arr_Image[indexPath.row])!)
        cell.btn_Delete.tag = indexPath.row
        cell.btn_Delete.addTarget(self, action: #selector(self.deleteAttachment(_:)), for: UIControlEvents.touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.collection_Images.frame.size.height, height: self.collection_Images.frame.size.height)
    }
    
    
    
    // MARK:- delete Image action
    
    func deleteAttachment(_ sender: UIButton){
        
        self.arr_Image.remove(at: sender.tag)
        self.collection_Images.reloadData()
        self.showHideButton()
    }
    
    
    // MARK:- Show/Hide Button
    
    func showHideButton(){
        
        self.btn_UploadAttachmnet.isHidden = self.arr_Image.count > 0 ? true : false
    }
    
}


// MARK:- ImagePicker Extention

extension RepoertProblemVC:  UIImagePickerControllerDelegate, UINavigationControllerDelegate, TOCropViewControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let PickedImage: UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        picker.dismiss(animated: true, completion: nil)
        //================Image Crop=====================//
        let cropController:TOCropViewController = TOCropViewController(image: PickedImage)
        cropController.delegate = self
        appDelegate.window?.rootViewController?.present(cropController, animated: true, completion: nil)
        //===============================================//
    }
    
    // MARK: - Cropper Delegate
    @objc(cropViewController:didCropToImage:withRect:angle:) func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        
        cropViewController.presentingViewController?.dismiss(animated: true, completion: { _ in })
        print("\(image)")
        
        self.arr_Image.append((UIImageJPEGRepresentation(image, 0.25)?.base64EncodedString())!)
        self.collection_Images.reloadData()
    }
    //=================================================================================================//
    //=================================================================================================//
    
}


// MARK:- CollectionView Cell

class AttachmentCell: UICollectionViewCell{
    
    @IBOutlet var btn_Delete: UIButton!
    @IBOutlet var img_Image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       // self.btn_Delete.setImage(#imageLiteral(resourceName: "close_ic").tint(with: UIColor.red), for: .normal)
        
    }
    
}

