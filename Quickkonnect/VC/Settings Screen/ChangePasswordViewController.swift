//
//  ChangePasswordViewController.swift
//  Quickkonnect
//
//  Created by Dhaval Tannarana on 02/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import Material
import Alamofire

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var btnChangePassword: UIButton!
    
    @IBOutlet weak var tfOldPassword: TextField!
    
    @IBOutlet weak var tfNewPassword: TextField!
    
    @IBOutlet weak var tfConfirmPassword: TextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnChangePassword.layer.cornerRadius = 3.0
        btnChangePassword.layer.layoutIfNeeded()
        self.tfOldPassword.setDivider()
        self.tfNewPassword.setDivider()
        self.tfConfirmPassword.setDivider()
        self.tfNewPassword.isSecureTextEntry = true
        self.tfOldPassword.isSecureTextEntry = true
        self.tfConfirmPassword.isSecureTextEntry = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //===============================================================================================//
    //MARK :- UITextField Delegate Method============================================================//
    
    // MARK: UITextFileld Delegate and Datasource Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfOldPassword {
            textField.resignFirstResponder()
            tfNewPassword.becomeFirstResponder()
            return false
        }
        else if textField == tfNewPassword {
            textField.resignFirstResponder()
            tfConfirmPassword.becomeFirstResponder()
            return false
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    
    // MARK: - Validation function
    func isValidData() -> Bool{
        
        if !(self.tfOldPassword.text?.isValidfname())! {
            KVClass.ShowNotification("", "Please enter old password!")
            return false
        }
        if !(self.tfNewPassword.text?.isValidfname())! {
            KVClass.ShowNotification("", "Please enter new password!")
            return false
        }
        else if !(self.tfNewPassword.text?.isValidPasswordLength())! {
            KVClass.ShowNotification("", "Password must contain at least 6 characters!")
            return false
        }
        else if !(self.tfConfirmPassword.text?.isValidfname())! {
            KVClass.ShowNotification("", "Please enter confirm password!")
            return false
        }
        else if !(self.tfConfirmPassword.text?.isValidPasswordLength())! {
            KVClass.ShowNotification("", "Password must contain at least 6 characters!")
            return false
        }
        else if !(self.tfNewPassword.text == self.tfConfirmPassword.text) {
            KVClass.ShowNotification("", "Please enter correct confirm password!")
            return false
        }
        
        return true
    }
    //===============================================================================================//
    //===============================================================================================//
    
    
    //===================================================================================================//
    //MARK : - UIButton Action Method
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnChangePasswordTapped(_ sender: Any) {
        if isValidData() == false {
            return
        }
        else {
            self.view.endEditing(true)
            self.changePasswordAPICall()
        }
    }
    
    func changePasswordAPICall() {
        
        self.tfNewPassword.resignFirstResponder()
        self.tfOldPassword.resignFirstResponder()
        self.tfConfirmPassword.resignFirstResponder()
        
        KVClass.KVShowLoader()
        
        Alamofire.request("\(self.getPrefixURL())reset_password", method: .post, parameters: ["user_id":KVAppDataServieces.shared.LoginUserID(), "old_password":tfOldPassword.text ?? "","new_password":tfNewPassword.text ?? ""], encoding: JSONEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
            KVClass.KVHideLoader()
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if ((response.result.value as! [String:Any])["status"] as! Int) == 400 {
                        
                        KVClass.ShowNotification("", ((response.result.value as! [String:Any])["msg"] as! String))
                        
                        //self.showAlert(Message: ((response.result.value as! [String:Any])["msg"] as! String))
                    } else {
                        
                        KVClass.ShowNotification("", "Password has been changed Successfully!")
                        
                       self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    KVClass.ShowNotification("", "There is problem to connecting server. Please Try Again!")
                    //self.showAlert(Message: "There is problem to connecting server. Please Try Again!")
                }
                break
                
            case .failure(_):
                print(response.result.error as Any)
                KVClass.ShowNotification("", (response.result.error?.localizedDescription)!)
               // self.showAlert(Message: (response.result.error?.localizedDescription)!)
                break
            }
        }
    }
    
    func showAlert(Message strmsg:String) {
        let alertView = UIAlertController(title: "", message: strmsg, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
