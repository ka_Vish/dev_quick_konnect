//
//  SettingsViewController.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 02/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import Material
import Alamofire

class SettingsViewController: QKMainBaseVC {

    var strTitle = ""
    @IBOutlet var Setting_tab: UITableView!
    var title_arr = ["Change Password","Notification","Help Center","Report A Problem","Blocked Contacts","App Activation"]
    var icon_arr = [#imageLiteral(resourceName: "iconLock"),nil,#imageLiteral(resourceName: "iconDanger"),#imageLiteral(resourceName: "report_ic"),#imageLiteral(resourceName: "blockuser"),#imageLiteral(resourceName: "iconAppActivation")]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.Setting_tab.reloadData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.Bar.btn_Chat.isHidden = true
        self.Bar.Title_lbl.text = strTitle
        self.Bar.Back_btn.isHidden = false
        self.Bar.Menu_btn.isHidden = true
        self.Bar.Noti_btn.isHidden = true
        self.Bar.btn_Chat.isHidden = true
        //self.Bar.img_TopBG.isHidden = true
        self.Bar.Title_lbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.Bar.Profile_Img.isHidden = true
        self.Bar.Profile_Img_BG.isHidden = true
        self.Bar.backgroundColor = UIColor.init(hex: "28AFB0")
        self.Bar.constraint_img_profile_trilling.constant = 12
        
        if self.view.tag == 1011 {
            //Help and Setting
            title_arr = ["Notification","Help Center","Report A Problem"]
            icon_arr = [nil,#imageLiteral(resourceName: "iconDanger"),#imageLiteral(resourceName: "report_ic")]
        }
        else {
            if KVAppDataServieces.shared.UserValue(.loginFrom) as? String == "Normal" {
                title_arr = ["Change Password","Blocked Contacts", "Blocked Profiles", "App Activation"]
                icon_arr = [#imageLiteral(resourceName: "iconLock"),#imageLiteral(resourceName: "blockuser"),#imageLiteral(resourceName: "blockuser"),#imageLiteral(resourceName: "iconAppActivation")]
            }
            else {
                title_arr = ["Blocked Contacts", "Blocked Profiles", "App Activation"]
                icon_arr = [#imageLiteral(resourceName: "blockuser"),#imageLiteral(resourceName: "blockuser"),#imageLiteral(resourceName: "iconAppActivation")]
            }
        }
        
        self.Setting_tab.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func accountStatusChanged(DeleteType strType:String) {
        KVClass.KVShowLoader()

        Alamofire.request("\(self.getPrefixURL())user_account_delete", method: .post, parameters: ["user_id": kUserDefults_("kLoginusrid") as? Int ?? 0, "delete_type": strType], encoding: JSONEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
            KVClass.KVHideLoader()
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    
                    if strType == "P"
                    {
                        KVAppDataServieces.shared.deleteAccount()
                    }
                
                    AlldefaultsRemove()
                    print(response.result.value as Any)
                    if ((response.result.value as! [String:Any])["status"] as! Int) == 400 {
                        self.showAlert(Message: ((response.result.value as! [String:Any])["msg"] as! String))
                    } else {
                        //NaviVC
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "NaviVC") as! NaviVC
                        UserDefaults.standard.removeObject(forKey: "session")
                        UserDefaults.standard.removeObject(forKey: "dashboard")
                        KVClass.ShowNotification("", ((response.result.value as! [String:Any])["msg"] as? String ?? "Failed !"))
                        UIApplication.shared.keyWindow?.rootViewController = vc
                    }
                } else {
                    self.showAlert(Message: "There is problem to connecting server. Please Try Again!")
                }
                break

            case .failure(_):
                print(response.result.error as Any)
                self.showAlert(Message: (response.result.error?.localizedDescription)!)
                break
            }
        }
    }

    func btnAppActiviationClicked() {

        let alertsheet = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)

        let deactivate = UIAlertAction.init(title: "Deactivate Account", style: .default) { (action) in

            let actionSheetController: UIAlertController = UIAlertController(title: "", message: "Are you sure, you want to Deactivate your Account?", preferredStyle: .alert)
            let cancelAction: UIAlertAction = UIAlertAction(title: "No", style: .cancel) { action -> Void in

                actionSheetController.dismiss(animated: true, completion: nil)
            }
            let YesAction: UIAlertAction = UIAlertAction(title: "Yes", style: .default) { action -> Void in
                //Just dismiss the action sheet
                actionSheetController.dismiss(animated: true, completion: nil)
                self.accountStatusChanged(DeleteType: "T")
            }
            actionSheetController.addAction(cancelAction)
            actionSheetController.addAction(YesAction)
            self.present(actionSheetController, animated: true, completion: nil)


        }

        let delete = UIAlertAction.init(title: "Delete Account", style: .destructive) { (action) in

            let actionSheetController: UIAlertController = UIAlertController(title: "", message: "Are you sure, you want to Delete your Account?", preferredStyle: .alert)
            let cancelAction: UIAlertAction = UIAlertAction(title: "No", style: .cancel) { action -> Void in
                //Just dismiss the action sheet
                actionSheetController.dismiss(animated: true, completion: nil)
            }
            let YesAction: UIAlertAction = UIAlertAction(title: "Yes", style: .default) { action -> Void in
                //Just dismiss the action sheet
                actionSheetController.dismiss(animated: true, completion: nil)
                self.accountStatusChanged(DeleteType: "P")
            }
            actionSheetController.addAction(cancelAction)
            actionSheetController.addAction(YesAction)
            self.present(actionSheetController, animated: true, completion: nil)

        }

        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in

            alertsheet.dismiss(animated: true, completion: nil)
        }

        alertsheet.addAction(deactivate)
        alertsheet.addAction(delete)
        alertsheet.addAction(cancel)

       self.present(alertsheet, animated: true, completion: nil)

    }

    func showAlert(Message strmsg:String) {
        let alertView = UIAlertController(title: "", message: strmsg, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }

    func clkToReportProblem() {
        
        DispatchQueue.main.async {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "RepoertProblemVC") as! RepoertProblemVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
        
//        let obj = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
//
//        let attributedTitle = NSMutableAttributedString(string: "Report A Problem", attributes:
//            [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 16)!])
//
//        let attributedMessage = NSMutableAttributedString(string: "Type your Comment here.", attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 13)!])
//        obj.setValue(attributedTitle, forKey: "attributedTitle")
//        obj.setValue(attributedMessage, forKey: "attributedMessage")
//        obj.addTextField(configurationHandler: { (textfield) in
//
//            textfield.borderStyle = .roundedRect
//            textfield.delegate = self
//            textfield.clearButtonMode = .whileEditing
//            textfield.autocorrectionType = .no
//            textfield.returnKeyType = .done
//            textfield.font = UIFont.init(name: "lato-Regular", size: 14)!
//            textfield.placeholder = "Comment.."
//        })
//
//        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
//
//            obj.dismiss(animated: true, completion: nil)
//        })
//
//        let report = UIAlertAction.init(title: "Send Report", style: .default, handler: { (action) in
//            if obj.textFields![0].text != "" {
//
//                KVClass.KVShowLoader()
//
//                Alamofire.request("\(self.getPrefixURL())user_feedback", method: .post, parameters: ["user_id": KVAppDataServieces.shared.LoginUserID(), "msg": obj.textFields![0].text!], encoding: JSONEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
//                    KVClass.KVHideLoader()
//                    switch(response.result) {
//                    case .success(_):
//                        if response.result.value != nil{
//                            print(response.result.value as Any)
//                            if ((response.result.value as! [String:Any])["status"] as! Int) == 400 {
//
//                                KVClass.ShowNotification("", ((response.result.value as! [String:Any])["msg"] as! String))
//                            } else {
//                                // self.showAlert(Message: "Sent Successfully!")
//                                KVClass.ShowNotification("", "Sent Successfully!")
//                            }
//                        } else {
//                            KVClass.ShowNotification("", "There is problem to connecting server. Please Try Again!")
//                        }
//                        break
//
//                    case .failure(_):
//                        print(response.result.error as Any)
//                        KVClass.ShowNotification("", (response.result.error?.localizedDescription)!)
//
//                        break
//                    }
//                }
//            } else {
//                //self.showAlert(Message: "Please Enter Your Comment.")
//                KVClass.ShowNotification("", "Please Enter Your Comment.")
//            }
//        })
//
//        obj.addAction(cancel)
//        obj.addAction(report)
//
//
//
//        self.present(obj, animated: true, completion: nil)
//        for textfield: UIView in obj.textFields! {
//            let container: UIView = textfield.superview!
//
//            print(container.bounds)
//
//
//
//            let mytxt = TextField.init(frame: CGRect.init(x: 0, y: 13, width: 235, height: 25))
//            mytxt.placeholder = "Comment.."
//            mytxt.placeholderVerticalOffset = 5
//            mytxt.font = UIFont.init(name: "lato-Regular", size: 14)!
//            // mytxt.isAnimating = true
//            mytxt.setDivider()
//            mytxt.backgroundColor = UIColor.clear
//            //  container.addSubview(mytxt)
//            //mytxt.sizeToFit()
//            let effectView: UIView = container.superview!.subviews[0]
//            container.backgroundColor = UIColor.clear
//            effectView.removeFromSuperview()
//        }
    }
    //=====================================================================================================//
    //MARK: - UIButton Action Method
    @IBAction func menupress(_ sender: UIButton)
    {
        kUserDefults(false as AnyObject, key: "ishomeupdate")
        self.dismiss(animated: true) {
        }
    }
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func switchNotificationOnOff(_ sender: UISwitch) {
        //notification_toggle
        var strSwitchValue = "0"

        if sender.isOn == true {
            strSwitchValue = "1"
        }

        KVClass.KVShowLoader()

        Alamofire.request("\(self.getPrefixURL())notification_toggle", method: .post, parameters: ["user_id":KVAppDataServieces.shared.LoginUserID(), "notification_status": strSwitchValue], encoding: JSONEncoding.default, headers: self.getHeader()).responseJSON { (response:DataResponse<Any>) in
            KVClass.KVHideLoader()
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    
                    KVClass.ShowNotification("", (response.result.value as? NSDictionary ?? [:])["msg"] as? String ?? "")
                    
                    if strSwitchValue == "0"
                    {
                       KVAppDataServieces.shared.update(.notification_status, false)
                    }
                    else
                    {
                        KVAppDataServieces.shared.update(.notification_status, true)
                    }
                    self.Setting_tab.reloadData()
                    print(response.result.value as Any)
                    if ((response.result.value as! [String:Any])["status"] as! Int) == 400 {
                        self.showAlert(Message: ((response.result.value as! [String:Any])["msg"] as! String))
                    }
                } else {
                    self.showAlert(Message: "There is problem to connecting server. Please Try Again!")
                }
                break

            case .failure(_):
                print(response.result.error as Any)
                self.showAlert(Message: (response.result.error?.localizedDescription)!)
                break
            }
        }
    }
    //=====================================================================================================//
    //=====================================================================================================//
}


//=====================================================================================================//
//MARK : - UITableView Delegate Datasource Method
extension SettingsViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.title_arr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 65
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SettingsCell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell", for: indexPath) as! SettingsCell
        
        cell.noti_switch.isHidden = true
        
        if self.view.tag == 1011 {
            //Help and Setting
            if indexPath.row == 0
            {
                cell.noti_switch.isHidden = false
                cell.noti_switch.setOn(KVAppDataServieces.shared.UserValue(.notification_status) as? Bool ?? false, animated: true)
                cell.image_icon.image = nil
                cell.noti_switch.addTarget(self, action: #selector(switchNotificationOnOff(_:)), for: .valueChanged)
            }
        }
       
        cell.image_icon.image = self.icon_arr[indexPath.row]
        cell.label_title.text = self.title_arr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let currentCell = tableView.cellForRow(at: indexPath) as! SettingsCell
        let strTitle = currentCell.label_title.text as? String ?? ""
        
        switch strTitle {
            
        case "Notification":
            break
    
        case "Help Center":
            
            DispatchQueue.main.async {
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpSettingWebVC") as! HelpSettingWebVC
                obj.urlstring = "http://quickkonnect.com/index.html#faq"
                obj.titlestring = "Help Center"
                self.present(obj, animated: true, completion: nil)
            }

        case "Report A Problem":
            
            DispatchQueue.main.async {
                self.clkToReportProblem()
            }

        case "Change Password":
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            self.present(obj, animated: true, completion: nil)

        case "Blocked Contacts":
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "BlockListVC") as! BlockListVC
            obj.strScreenFrom = strTitle
            self.navigationController?.pushViewController(obj, animated: true)
            
        case "Blocked Profiles":
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "BlockListVC") as! BlockListVC
            obj.strScreenFrom = strTitle
            self.navigationController?.pushViewController(obj, animated: true)
            
        case "App Activation":
        
            DispatchQueue.main.async {
                self.btnAppActiviationClicked()
            }
        
        default:
            break
        }
        
        
        
//        if indexPath.row == 0 {
//            if self.view.tag == 1011 {
//                return
//            }
//            else {
//                if KVAppDataServieces.shared.UserValue(.loginFrom) as? String == "Normal" {
//                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
//                    self.present(obj, animated: true, completion: nil)
//                }
//                else {
//                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "BlockListVC") as! BlockListVC
//                    self.navigationController?.pushViewController(obj, animated: true)
//                }
//            }
//        }
//
//        else if indexPath.row == 1 {
//            if self.view.tag == 1011 {
//                DispatchQueue.main.async {
//                let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpSettingWebVC") as! HelpSettingWebVC
//                obj.urlstring = "http://quickkonnect.com/index.html#faq"
//                obj.titlestring = "Help Center"
//                self.present(obj, animated: true, completion: nil)
//                }
//            }
//            else {
//                if KVAppDataServieces.shared.UserValue(.loginFrom) as? String == "Normal" {
//                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "BlockListVC") as! BlockListVC
//                    self.navigationController?.pushViewController(obj, animated: true)
//                }
//                else {
//                    DispatchQueue.main.async {
//                        self.btnAppActiviationClicked()
//                    }
//                }
//            }
//        }
//
//        else if indexPath.row == 2 {
//            if self.view.tag == 1011 {
//                DispatchQueue.main.async {
//                    self.clkToReportProblem()
//                }
//            }
//            else {
//                DispatchQueue.main.async {
//                    self.btnAppActiviationClicked()
//                }
//            }
//        }
    }

    
}

extension SettingsViewController: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}

class SettingsCell: UITableViewCell {
    
    @IBOutlet var image_icon: UIImageView!
    @IBOutlet var noti_switch: UISwitch!
    @IBOutlet var label_title: UILabel!
    
    override func awakeFromNib() {
        noti_switch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
    }
}

extension CALayer {
    
    func dropShadow() {
        
        self.masksToBounds = false
        self.shadowColor = UIColor.black.cgColor
        self.shadowOpacity = 0.5
        self.shadowOffset = CGSize(width: -1, height: 1)
        self.shadowRadius = 1
        
        self.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.shouldRasterize = true
        
        self.rasterizationScale = UIScreen.main.scale
        
    }
}
