//
//  HelpSettingWebVC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 16/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import WebKit


class HelpSettingWebVC: UIViewController, UIWebViewDelegate {
    
    var urlstring = ""
    var titlestring = ""
    @IBOutlet var Webview: UIWebView!
    
    @IBOutlet var Done: UIButton!
    @IBOutlet var refresh: UIButton!
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var Loader: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let TopViewStatus = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: 20))
        if (Device.IS_IPHONE_X){
            //iPhone X
            TopViewStatus.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44)
        }
        
        TopViewStatus.backgroundColor = UIColor.init(hex: "28AFB0")
        self.view.addSubview(TopViewStatus)
        
        
        self.title_lbl.text = self.titlestring
        
        var url: URL?
        
        if self.verifyUrl(urlString: self.urlstring.lowercased())
        {
            url = URL.init(string: self.urlstring.lowercased())!
        }
        else
        {
            if self.urlstring.lowercased().contains("www.")
            {
                url = URL.init(string: "http://\(self.urlstring.lowercased())")
            }
            else
            {
                url = URL.init(string: "http://www.\(self.urlstring.lowercased())")
            }
        }
        var req = URLRequest.init(url: url!)
       // req.timeoutInterval = 300
        self.Webview.delegate = self
        
        DispatchQueue.main.async {
           
           self.Webview.loadRequest(req)
        }
        
    }
    
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
        self.Loader.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        self.Loader.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.Loader.stopAnimating()
        KVClass.ShowNotification("", "Opps!! Something went wrong, Please try again later !")
        self.Webview.stopLoading()
    }
    
    @IBAction func reloadPress(_ sender: UIButton)
    {
        self.Webview.reload()
    }
    
    @IBAction func Donepress(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
