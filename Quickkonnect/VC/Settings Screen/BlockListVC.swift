//
//  BlockListVC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 14/02/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import SDWebImage


class BlockListVC: QKMainBaseVC {

    var strScreenFrom = ""
    @IBOutlet var Block_tab: UITableView!
    var arrData:[[String:Any]] = []
    var arrMainData:[[String:Any]] = []
    @IBOutlet var emptylbl: UILabel!
    var arr_BlockedProfileList: [EmployeesListInProfileData]! = [EmployeesListInProfileData]()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       // self.viewWillAppear(false)
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
        if self.strScreenFrom == "Blocked Contacts" {
            self.arrMainData.removeAll()
            self.arrData.removeAll()
            self.Block_tab.reloadData()
            emptylbl.text = "Your List of Blocked Users is Empty!"
            if let valuedata = KVAppDataServieces.shared.UserValue(.block_user_list) as? [[String:Any]]
            {
                self.arrData = valuedata
                
                self.arrMainData = self.arrData
                self.Block_tab.reloadData()
            }
            if self.arrMainData.count > 0
            {
                self.emptylbl.isHidden = true
            }
            else
            {
                self.emptylbl.isHidden = false
            }
            
            if kUserDefults_("updateblocklist") as? Bool ?? false == true
            {
                self.callAPIForContactList()
            }
            kUserDefults(false as AnyObject, key: "updateblocklist")
        }
        else {
            emptylbl.text = "Your List of Blocked Profiles is Empty!"
            self.callAPIForBlockedProfilesist()
            self.Block_tab.reloadData()
        }
        
        
        self.Block_tab.reloadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Bar.btn_Chat.isHidden = true
        self.Bar.Title_lbl.text = strScreenFrom
        self.Bar.Back_btn.isHidden = false
        self.Bar.Menu_btn.isHidden = true
        self.Bar.Noti_btn.isHidden = true
        self.Bar.btn_Chat.isHidden = true
        //self.Bar.img_TopBG.isHidden = true
        self.Bar.Title_lbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.Bar.Profile_Img.isHidden = true
        self.Bar.Profile_Img_BG.isHidden = true
        self.Bar.backgroundColor = UIColor.init(hex: "28AFB0")
        self.Bar.constraint_img_profile_trilling.constant = 12
        
        
        //=========Add Refresh Control For Pull To Refresh==============//
        if self.strScreenFrom == "Blocked Contacts" {
            self.Block_tab.pullTorefresh(#selector(self.callAPIForContactList), self)
        }
        else {
            self.Block_tab.pullTorefresh(#selector(self.callAPIForBlockedProfilesist), self)
        }
        //==============================================================//
        //==============================================================//
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func callAPIForContactList() {
        
        DispatchQueue.main.async {
            
            KVClass.KVServiece(methodType: "GET", processView: self.navigationController, baseView: self, processLabel: "", params: nil, api: "user_contact_block_list/\(KVAppDataServieces.shared.LoginUserID())?timezone=\(KVClass.KVTimeZone())", arrFlag: false, Hendler: { (json, status) in
                self.Block_tab.closeEndPullRefresh()
                if status == 1
                {
                    KVAppDataServieces.shared.removeListData(.block_user_list)
                    kUserDefults(false as AnyObject, key: "updateblocklist")
                    
                    if let dictData = json["data"] as? [NSDictionary]
                    {
                        self.arrData = dictData as! [[String:Any]]
                        self.arrMainData = self.arrData
                        
                        let datadicti = NSKeyedArchiver.archivedData(withRootObject: dictData)
                        
                        KVAppDataServieces.shared.update(.block_user_list, datadicti)
                        
                        if self.arrMainData.count > 0
                        {
                            self.emptylbl.isHidden = true
                        }
                        else
                        {
                            self.emptylbl.isHidden = false
                        }
                    }
                    if let countdatadict = json["contact_count"] as? NSDictionary
                    {
                        KVAppDataServieces.shared.update(.contact_all, countdatadict["contact_all"]!)
                        KVAppDataServieces.shared.update(.contact_week, countdatadict["contact_week"]!)
                        KVAppDataServieces.shared.update(.contact_month, countdatadict["contact_month"]!)
                    }
                    
                    self.viewWillAppear(false)
                
                }
                
                if status == 2214
                {
                   KVAppDataServieces.shared.removeListData(.block_user_list)
                    self.viewWillAppear(false)
                }
                
            })
            
            
        }
        
        
    }
    
    func callAPIForBlockedProfilesist() {
    
        DispatchQueue.main.async {
            
            let dict_param = NSMutableDictionary()
            dict_param["user_id"] = KVAppDataServieces.shared.LoginUserID()

            KVClass.KVServiece(methodType: "POST", processView: self.navigationController, baseView: self.navigationController, processLabel: "", params: dict_param, api: "member/block_profile_list", arrFlag: false) { (JSON, status) in
                self.Block_tab.closeEndPullRefresh()
                self.arr_BlockedProfileList.removeAll()
                self.Block_tab.reloadData()
                if self.arr_BlockedProfileList.count > 0 {
                    self.emptylbl.isHidden = true
                }
                else {
                    self.emptylbl.isHidden = false
                }
                if status == 1 {
                    if let dataDict = JSON["data"] as? [NSDictionary]
                    {
                        self.arr_BlockedProfileList.removeAll()
                        print(JSON)
                    
                        for item in dataDict {
                            self.arr_BlockedProfileList.append(EmployeesListInProfileData.init(item))
                        }
                        
                        if self.arr_BlockedProfileList.count > 0 {
                            self.emptylbl.isHidden = true
                        }
                        else {
                            self.emptylbl.isHidden = false
                        }
                        self.Block_tab.reloadData()
                    }
                }
                else {
                    self.arr_BlockedProfileList.removeAll()
                }
            }
        }
    }
    
    
    
    @IBAction func menupress(_ sender: UIButton)
    {
        kUserDefults(false as AnyObject, key: "ishomeupdate")
        self.dismiss(animated: true) {
            
        }
        
    }
    
}

extension BlockListVC: UITableViewDelegate, UITableViewDataSource
{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.strScreenFrom == "Blocked Contacts" {
            return arrData.count
        }
        else {
            return arr_BlockedProfileList.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell: BlockListCell = tableView.dequeueReusableCell(withIdentifier: "BlockListCell", for: indexPath) as! BlockListCell
        
        cell.Pro_img.layer.borderWidth = 3
        cell.Pro_img.layer.borderColor = UIColor.white.withAlphaComponent(0.16).cgColor
        cell.Pro_img.layer.cornerRadius = cell.Pro_img.frame.size.width/2
        
        if self.strScreenFrom == "Blocked Contacts" {
            
            //For Blocked Contact
            
            let dictData:[String:Any] = arrData[indexPath.row]
            
            //For Profile Pic
            if let strURL = dictData["scan_user_profile_url"] as? String, strURL.count > 7  {
                cell.Pro_img.image = UIImage.init(named: "user_profile_pic")
                SDWebImageManager().loadImage(with: URL.init(string: dictData["scan_user_profile_url"] as! String)!, options: SDWebImageOptions.refreshCached, progress: { (p1, p2, uri) in
                    
                }, completed: { (img, dta, err, typw, isfinished, uri) in
                    
                    cell.Pro_img.image = (img ?? UIImage.init(named: "user_profile_pic"))
                })
                
            }
            //==========================================================================//
            
            //For Text, Dete, Name, Bloacked at
            
            if (dictData["scan_date"] != nil) {
                let dateFormatter = DateFormatter.init()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let myDates = dateFormatter.date(from: (dictData["scan_date"] as! String))
                
                dateFormatter.dateFormat = "EEE. dd MMM yyyy HH:mm"
                
                let strDateTime = dateFormatter.string(from: myDates!)
                
                let strFName = dictData["firstname"] as? String ?? ""
                let strLName = dictData["lastname"] as? String ?? ""
                let fullName = strFName.capitalized + " " + strLName.capitalized
                
                let rangestring: NSString = "\((fullName))    \nBlocked at :  \(strDateTime)" as NSString
                let rangeName = rangestring.range(of: "\((fullName))")
                let rangeBlock = rangestring.range(of: "Blocked at :")
                let rangeScanTime = rangestring.range(of: "\(strDateTime)")
                let style = NSMutableParagraphStyle()
                style.lineSpacing = 0
                
                let strAtributedTitle = NSMutableAttributedString.init(string: rangestring as String)
                strAtributedTitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Bold", size: 17.5)!, range: rangeName)
                strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkText, range: rangeName)
                
                strAtributedTitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Medium", size: 14)!, range: rangeBlock)
                strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: rangeBlock)
                
                strAtributedTitle.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Regular", size: 13)!, range: rangeScanTime)
                strAtributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkText, range: rangeScanTime)
                
                cell.User_name.attributedText = strAtributedTitle
                
            }
            
            //======================================================================================//
            //======================//=========================//================//=================//
            
        }
        else {
            
            //For Blocked Profiles
            
            
            
            //For Profile logo
            cell.Pro_img.image = #imageLiteral(resourceName: "logo")
            let strImgURL = self.arr_BlockedProfileList[indexPath.row].logo
            if strImgURL != "" {
                cell.Pro_img.setImageWith(URLRequest.init(url: URL.init(string: strImgURL)!), placeholderImage: nil, success: { (request, response, image) in
                    cell.Pro_img.image = image
                }, failure: { (request, response, error) in
                    cell.Pro_img.image = #imageLiteral(resourceName: "logo")
                })
            }
            
            cell.User_name.text = arr_BlockedProfileList[indexPath.row].profilename
            //======================================================================================//
            //======================//=========================//================//=================//
        }
        
            
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.strScreenFrom == "Blocked Contacts" {
            let dictData:[String:Any] = arrData[indexPath.row]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            vc.strContactProfile = ""//dictData["scan_user_unique_code"] as? String ?? ""
            vc.scanuserid = dictData["scan_user_id"] as? Int ?? 0
            vc.scanuniquecode = dictData["scan_user_unique_code"] as? String ?? ""
            // vc.Scan_user_update = true
            vc.isaddcontect = false
            vc.ScreenFrom = "BlockList"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
            obj.int_userID = -1
            obj.str_Screen_From = "BlockProfile"
            obj.str_uniquecode = arr_BlockedProfileList[indexPath.row].UniqueCode
            obj.strBlockProfileID = arr_BlockedProfileList[indexPath.row].profile_id
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
        
        
    }
    
}

class BlockListCell: UITableViewCell {
   
    @IBOutlet var Pro_img: UIImageView!
    @IBOutlet var User_name: UILabel!
    @IBOutlet var Email: UILabel!
}
