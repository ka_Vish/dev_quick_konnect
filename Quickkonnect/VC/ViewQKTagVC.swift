//
//  ViewQKTagVC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 12/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit

class ViewQKTagVC: UIViewController {
    
    @IBOutlet var ImageView: UIImageView!
    
    var ShowImage: UIImage?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Display Generated QK Tag
        self.ImageView.image = self.ShowImage
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0)
        self.ImageView.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    
        UIView.animate(withDuration: 0.4, animations: {
            self.ImageView.transform = CGAffineTransform.identity
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        }) { (sucess) in
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismisPress(_ sender: UIButton)
    {
        self.ImageView.transform = CGAffineTransform.identity
        UIView.animate(withDuration: 0.35, animations: {
            self.ImageView.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0)

        }) { (sucess) in
            self.dismiss(animated: false, completion: nil)
        }
    
    }
    
    
}
