//
//  QKCompanyInfoVC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 01/03/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import SDWebImage

protocol saveEditedChangesforQKProfile {
    func protocallSavechangesProfile(_ imgstr: String, _ profilename: String,_ about: String)
    
}

class QKCompanyInfoVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate, TOCropViewControllerDelegate {

    var isFirstTime = true
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtName: TextField!

    @IBOutlet weak var txtAbout: TextView!
    @IBOutlet weak var lblTxtChar: UILabel!
    @IBOutlet weak var lblStorTitle: UILabel!
    @IBOutlet weak var lblDeviderStory: UILabel!
    
    @IBOutlet weak var constraint_Poup_Top: NSLayoutConstraint!
    var ImagePicker : UIImagePickerController!
    var QKProfileData: QKProfileDataList! = QKProfileDataList()
    @IBOutlet weak var tblview: UITableView!
    var delegate: saveEditedChangesforQKProfile?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view
        lblStorTitle.text = ""
        //=============================//
        
        self.view.backgroundColor = .clear
        tblview.transform = CGAffineTransform.init(scaleX: 0.000, y: 0.000)

        
        
        self.txtName.text = self.QKProfileData.name
        self.txtAbout.text = self.QKProfileData.about
        if !(self.QKProfileData.about == "") {
            lblStorTitle.text = "About*"
            let currentString: NSString = self.QKProfileData.about as NSString
            let newChar = 400 - currentString.length
            lblTxtChar.text = newChar.description
            txtAbout.setContentOffset(CGPoint.zero, animated: false)
        }
        
        
        //==============================================================================================//
        //==============================================================================================//
        let toolBar = UIToolbar().ToolbarPikerteast(mySelect: #selector(dismissPicker), delegate: self)
        txtAbout.inputAccessoryView = toolBar
        //==============================================================================================//
        //==============================================================================================//
        // Do any additional setup after loading the view.
        
        
        
        var height = UIScreen.main.bounds.height
        height = height/4
        height = height - 90
        self.constraint_Poup_Top.constant = height
        
        
        //Set TextFielw Datelegate and BGColor and PlaceHolder
        self.txtName.setDivider()
        self.txtAbout.setDivider()
        
        self.txtName.delegate = self
        self.txtAbout.delegate = self
        
        self.ImagePicker = UIImagePickerController.init()
        self.ImagePicker.delegate = self
        
        
        if self.QKProfileData.logobase64 != "" {
            self.imgProfile.image = UIImage.init(data: Data.init(base64Encoded: self.QKProfileData.logobase64)!)
            return
        }
        else if self.QKProfileData.logo != "" {
            SDWebImageManager.shared().loadImage(with: URL.init(string: self.QKProfileData.logo)!, options: SDWebImageOptions.refreshCached, progress: { (pro1, pro2, url) in
            }, completed: { (img, data, err, type, finish, url) in
                if err == nil {
                    self.imgProfile.image = (img ?? UIImage.init(named: "logo"))
                    return
                }
            })
        }
        else {
           self.imgProfile.image = UIImage.init(named: "logo")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFirstTime == true {
            isFirstTime = false
            
            tblview.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
            UIView.animate(withDuration: 0.3, animations: {
                self.tblview.transform = CGAffineTransform.identity
                self.view.backgroundColor = UIColor.black.withAlphaComponent(0.45)
            }) { (sucess) in
            }
        }
        
    }
    
    
    //==================================================================================================//
    //MARK :- UITextField Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtName {
            let maxLength = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > maxLength {
                return false
            }
            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
//        else if textField == txtAbout {
//            let maxLength = 500
//            let currentString: NSString = textField.text! as NSString
//            let newString: NSString =
//                currentString.replacingCharacters(in: range, with: string) as NSString
//            if newString.length > maxLength {
//                return false
//            }
//            let cs = NSCharacterSet(charactersIn: KVClass.ACCEPTABLE_CHARACTERS).inverted
//            let filtered = string.components(separatedBy: cs).joined(separator: "")
//
//            return (string == filtered)
//        }
        return true
    }
    //==================================================================================================//
    //==================================================================================================//
    
    
    //============================================================================================//
    //MARK :- UITextView Delegate Method
    func textViewDidBeginEditing(_ textView: UITextView) {
        lblStorTitle.text = "About*"
        lblStorTitle.textColor = UIColor.init(hex: "28AFB0")
        lblDeviderStory.backgroundColor = UIColor.init(hex: "28AFB0")
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            lblStorTitle.text = ""
        }
        lblStorTitle.textColor = UIColor.lightGray
        lblDeviderStory.backgroundColor = UIColor.lightGray
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == txtAbout {
            let maxLength = 400
            let currentString: NSString = textView.text! as NSString
            let new_Str: NSString = currentString.replacingCharacters(in: range, with: text) as NSString
            if new_Str.length > maxLength {
                return false
            }
            let newChar = maxLength - new_Str.length
            lblTxtChar.text = newChar.description
            return true
        }
        return true
    }
    //============================================================================================//
    //============================================================================================//
    
    
    //==================================================================================================//
    //MARK :- UIButon Action Method
    @IBAction func btnCloseTapped(_ sender: Any) {
        
        self.tblview.transform = CGAffineTransform.identity
        UIView.animate(withDuration: 0.3, animations: {
            self.tblview.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0)
            
        }) { (sucess) in
            self.dismiss(animated: false, completion: nil)
        }
        
    }
    
    @IBAction func btnDoneTapped(_ sender: Any) {
        if self.txtName.text == "" || self.txtAbout.text == ""  || self.txtName.text == nil || self.txtAbout.text == nil {
            KVClass.ShowNotification("", "All fields are required!")
            return
        }
        else {
            self.delegate?.protocallSavechangesProfile((UIImageJPEGRepresentation(self.imgProfile.image!, 0.25)?.base64EncodedString())!, self.txtName.text!, self.txtAbout.text!)
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func btnGetImageTapped(_ sender: Any) {
        
        let imageAlert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
            imageAlert.dismiss(animated: true, completion: nil)
        })
        
        let Capture = UIAlertAction.init(title: "Take Photo", style: .destructive, handler: { (action) in
            
            self.ImagePicker.sourceType = .camera
            self.ImagePicker.showsCameraControls = true
            self.ImagePicker.allowsEditing = false
            
            self.present(self.ImagePicker, animated: true, completion: nil)
        })
        
        let chosefromlib = UIAlertAction.init(title: "Choose Photo", style: .default, handler: { (action) in
            
            self.ImagePicker.sourceType = .photoLibrary
            self.ImagePicker.allowsEditing = false
            
            self.present(self.ImagePicker, animated: true, completion: nil)
        })
        
        imageAlert.addAction(Capture)
        imageAlert.addAction(chosefromlib)
        imageAlert.addAction(cancel)
        
        self.present(imageAlert, animated: true, completion: nil)
        
    }
    //==================================================================================================//
    //==================================================================================================//
    
    
    //==================================================================================================//
    //MARK :- Image Picker Controller delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let PickedImage: UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        picker.dismiss(animated: true, completion: nil)
        //================Image Crop=====================//
        let cropController:TOCropViewController = TOCropViewController(image: PickedImage)
        cropController.delegate = self
        self.present(cropController, animated: true, completion: nil)
        //===============================================//
    }
    
    // MARK: - Cropper Delegate
    @objc(cropViewController:didCropToImage:withRect:angle:) func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        
        cropViewController.presentingViewController?.dismiss(animated: true, completion: { _ in })
        print("\(image)")
        
        self.imgProfile.image = image
    }
    //=================================================================================================//
    //=================================================================================================//
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
































   
    
    

    
    
    
    
    
  
//
//extension UIViewController {
//    @objc func dismissPicker() {
//        self.view.endEditing(true)
//    }
//}
//
//extension UIToolbar {
//
//    func ToolbarPiker(mySelect : Selector) -> UIToolbar {
//
//        let toolBar = UIToolbar()
//
//        toolBar.barStyle = UIBarStyle.default
//        toolBar.isTranslucent = true
//        // toolBar.tintColor = UIColor.black
//        toolBar.sizeToFit()
//
//        let doneButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: mySelect)
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
//
//        toolBar.setItems([ spaceButton, doneButton], animated: false)
//        toolBar.isUserInteractionEnabled = true
//
//        return toolBar
//    }
//
//
//    func ToolbarPikerteast(mySelect : Selector, delegate:UIViewController) -> UIToolbar {
//
//        let toolBar = UIToolbar()
//
//        toolBar.barStyle = UIBarStyle.default
//        toolBar.isTranslucent = true
//        // toolBar.tintColor = UIColor.black
//        toolBar.sizeToFit()
//
//        let doneButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.done, target: delegate, action: mySelect)
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
//
//        toolBar.setItems([ spaceButton, doneButton], animated: false)
//        toolBar.isUserInteractionEnabled = true
//
//        return toolBar
//    }
//}

