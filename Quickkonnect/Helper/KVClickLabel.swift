//
//  KVClickLabel.swift
//  demotuto
//
//  Created by Kavi on 07/12/17.
//  Copyright © 2017 chetan. All rights reserved.
//

import UIKit

//protocol KVTapDelegate:NSObjectProtocol {
//    func callBack(_ string: String)
//}

class KVClickLabel: UILabel {

    var tapGesture: UITapGestureRecognizer?
    
    //Make weak refernece for SSGastureDelegate
   // weak var delegate:KVTapDelegate?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.initialization()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialization()
    }
    
    func initialization(){
        let newsString: NSMutableAttributedString = NSMutableAttributedString(string: self.text!)
        let textRange = NSString(string: self.text!)
        
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: self.text!, options: [], range: NSRange(location: 0, length: self.text!.utf16.count))
        
        for match in matches {
            guard let range = Range(match.range, in: self.text!) else { continue }
            let url = self.text![range]
            
            let substringRange = textRange.range(of: url) // You can add here for own specific under line substring
            newsString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: substringRange)
            newsString.addAttribute(NSLinkAttributeName, value: url, range: substringRange)
            newsString.addAttribute(NSForegroundColorAttributeName, value: UIColor.red, range: substringRange)
            // self.attributedText = newsString.copy() as? NSAttributedString
            self.attributedText = newsString
            self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapResponse))
            self.isUserInteractionEnabled =  true
            self.addGestureRecognizer(tapGesture!)
            
        }
    }
    
    override func reloadInputViews() {
        super.reloadInputViews()
        self.initialization()
    }

    
    func tapResponse(recognizer: UITapGestureRecognizer) {
        
        let text = (self.text)!
        
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: self.text!, options: [], range: NSRange(location: 0, length: self.text!.utf16.count))
        
        for match in matches {
            guard let range = Range(match.range, in: self.text!) else { continue }
            let url = self.text![range]
            
        let termsRange = (text as NSString).range(of: url)
        if (tapGesture?.didTapAttributedTextInLabel(label: self, inRange: termsRange))! {
            print("Tapped terms conditions")
            self.openURL(url)
        }
        else {
            print("Tapped none ")
        }
        }
    }
    
    func openURL(_ string: String)
    {
       UIApplication.shared.openURL(URL.init(string: string)!)
    }
    
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,y:(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        let locationOfTouchInTextContainer = CGPoint(x:locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y:locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

