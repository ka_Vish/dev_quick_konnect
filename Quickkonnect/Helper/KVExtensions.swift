//
//  KVExtensions.swift
//  demoappp
//
//  Created by chetan on 25/10/17.
//  Copyright © 2017 chetan. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    public func getPrefixURL() -> String {
        return KEYSPROJECT.BaseUrl //"http://54.70.2.200/quickkonnect/" //"http://quickkonnect.com/app/api/"//
    }
    
    public func getHeader() -> [String:String] {
      //  let headers:[String:String] = ["QK_ACCESS_KEY":"AAAAmGLql2s:APA91bF-fhfWlp4p_aRjK7UL8ZUPy4VlWWnbRExecXf9yZ1B0pYvT5Jg0r2XS6DOWT1Zb4KH-NfRlG3K_WSTM2RbkP3xD7d-pZF5zlSmb"]
        return KEYSPROJECT.headers
    }
}

public extension UIView {
    public class func fromNib(_ nibNameOrNil:String) ->  UIView {
        return  Bundle.main.loadNibNamed(nibNameOrNil, owner: self, options: nil)!.first as! UIView
    }
}

extension UIView {
    
    func ClearColorShaddow(_ alpha:Float){
        
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = (self.frame.size.height)/2
        self.layer.shadowPath = CGPath(rect: CGRect(x: 0,y: 0, width: self.frame.width,height: self.frame.height), transform: nil)
        self.layer.shadowOpacity = alpha
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        
        
    }
    
    @IBInspectable var KVCorneredius:CGFloat{
        
        get{
            return layer.cornerRadius
        }
        set{
            self.layer.cornerRadius = newValue
            self.layer.masksToBounds = newValue > 0
        }
    }
    @IBInspectable var KVBorderWidth:CGFloat{
        
        get{
            return layer.borderWidth
        }
        set{
            self.layer.borderWidth = newValue
            self.layer.masksToBounds = newValue > 0
        }
    }
    
    
    @IBInspectable var KVBorderColor:UIColor{
        
        get{
            return self.KVBorderColor
        }
        set{
            self.layer.borderColor = newValue.cgColor
            
        }
    }
    @IBInspectable var KVRoundDynamic:Bool{
        
        get{
            return false
        }
        set{
            if newValue == true {
                
                self.perform(#selector(UIView.AfterDelay), with: nil, afterDelay: 0.0)
            }
            
        }
        
    }
    @objc func AfterDelay(){
        
        let  Height =  self.frame.size.height
        self.layer.cornerRadius = Height/2;
        self.layer.masksToBounds = true;
        
        
    }
    @IBInspectable var KVRound:Bool{
        get{
            return false
        }
        set{
            if newValue == true {
                self.layer.cornerRadius = self.frame.size.height/2;
                self.layer.masksToBounds = true;
            }
            
        }
    }
    @IBInspectable var KVShadow:Bool{
        get{
            return false
        }
        set{
            if newValue == true {
                self.layer.masksToBounds = false
                self.layer.shadowColor = UIColor.black.cgColor
                self.layer.shadowOffset = CGSize(width: 1.0, height: 0.0)
                self.layer.shadowOpacity = 0.5;
                
            }
            
        }
        
    }
    @IBInspectable var KVheightAutomatic: Bool
    {
        get {
            return false
        }
        set
        {
            let size = UIScreen.main.bounds.width
            
            if newValue == true
            {
                if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
                {
                    if self.tag == 1122
                    {
                     let heightConstraint = self.heightAnchor.constraint(equalToConstant: size*0.22)
                        
                        NSLayoutConstraint.activate([heightConstraint])
                        self.setNeedsLayout()
                    }
                    else
                    {
                    let heightConstraint = self.heightAnchor.constraint(equalToConstant: size*0.32)
                   
                    NSLayoutConstraint.activate([heightConstraint])
                    self.setNeedsLayout()
                    }
                    
                }
                else
                {
                    if self.tag == 1122
                    {
                        let heightConstraint = self.heightAnchor.constraint(equalToConstant: size*0.125)
                        
                        NSLayoutConstraint.activate([heightConstraint])
                        self.setNeedsLayout()
                    }
                    else
                    {
                    let heightConstraint = self.heightAnchor.constraint(equalToConstant: size*0.18)
        
                    NSLayoutConstraint.activate([heightConstraint])
                    self.setNeedsLayout()
                    }
                }
            }
        }
        
    }
    @IBInspectable var KVclipsToBounds:Bool{
        get{
            return false
        }
        set{
            if newValue == true {
                
                self.clipsToBounds = true;
            }else{
                self.clipsToBounds = false
            }
            
        }
        
    }
    
    
    func roundMake() {
        self.layer.cornerRadius = self.frame.size.height/2;
        self.layer.masksToBounds = true;
    }
    
}

extension UICollectionView
{
    @IBInspectable var KVheightAuto:Bool
    {
        get{
            
            return false
            
        }
        set{
            
            if newValue == true{
                
                let size = UIScreen.main.bounds.height
                
                print(size)
                
                if self.tag == 2211
                {
                    if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
                    {
                        
                        // self.widthAnchor.constraint(equalToConstant: size*0.01)
                        
                        let heightConstraint = self.heightAnchor.constraint(equalToConstant: size*0.12)
                        
                        NSLayoutConstraint.activate([heightConstraint])
                        self.setNeedsLayout()
                        
                    }
                    else
                    {
                        let heightConstraint = self.heightAnchor.constraint(equalToConstant: size*0.08)
                        
                        NSLayoutConstraint.activate([heightConstraint])
                        self.setNeedsLayout()
                    }
                }
                else
                {
                    if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
                    {
                        
                        // self.widthAnchor.constraint(equalToConstant: size*0.01)
                        
                        let heightConstraint = self.heightAnchor.constraint(equalToConstant: size*0.70)
                        
                        NSLayoutConstraint.activate([heightConstraint])
                        self.setNeedsLayout()
                        
                    }
                    else
                    {
                        let heightConstraint = self.heightAnchor.constraint(equalToConstant: size*0.52)
                        
                        NSLayoutConstraint.activate([heightConstraint])
                        self.setNeedsLayout()
                    }
                }
            }
        }
    }
}


extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}

extension UILabel{
   
    @IBInspectable var KVFontSize:CGFloat{
        
        
        get{
           
            let size = self.font!.pointSize
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
            {
                return size
                
            }
           
            
            return size*1.38
           
        }
        set{
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
            {
               self.font = UIFont.init(name: self.font!.fontName, size: newValue)
                
            }
            else
            {
                if self.tag == 11
                {
                    self.font = UIFont.init(name: self.font!.fontName, size: newValue*1.52)
                }
                else
                {
              self.font = UIFont.init(name: self.font!.fontName, size: newValue*1.38)
                }
            }
            
        }
        
    }
}
extension UIButton{
    
    @IBInspectable var KVFontSize:CGFloat{
        
        
        get{
            
            let size = self.titleLabel!.font!.pointSize
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
            {
                return size
                
            }
            return size*1.11
            
        }
        set{
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
            {
                self.titleLabel!.font = UIFont.init(name: self.titleLabel!.font!.fontName, size: newValue)!
                
            }
            else
            {
                self.titleLabel!.font = UIFont.init(name: self.titleLabel!.font!.fontName, size: newValue*1.11)!
            }
            
        }
        
    }
}
extension UITextField
{
    @IBInspectable var KVtxtWidth:Bool
    {
        
        
        get{

            return true
            
        }
        set{
            
            if newValue == true{
                
            let size = UIScreen.main.bounds.width
                
             print(size)
            
            if self.tag == 2211
            {
                if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
                {
                    
                    // self.widthAnchor.constraint(equalToConstant: size*0.01)
                    
                    let heightConstraint = self.widthAnchor.constraint(equalToConstant: size*0.9)
                    let widthConstarint = self.heightAnchor.constraint(equalToConstant: 32)
                    NSLayoutConstraint.activate([heightConstraint,widthConstarint])
                    self.setNeedsLayout()
                    
                }
                else
                {
                    let heightConstraint = self.widthAnchor.constraint(equalToConstant: size*0.62)
                    let widthConstarint = self.heightAnchor.constraint(equalToConstant: 40)
                    NSLayoutConstraint.activate([heightConstraint,widthConstarint])
                    self.setNeedsLayout()
                }
            }
           else
            {
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
            {
                
               // self.widthAnchor.constraint(equalToConstant: size*0.01)
                
                let heightConstraint = self.widthAnchor.constraint(equalToConstant: size*0.70)
                let widthConstarint = self.heightAnchor.constraint(equalToConstant: 32)
                NSLayoutConstraint.activate([heightConstraint,widthConstarint])
                self.setNeedsLayout()
                
            }
            else
            {
                let heightConstraint = self.widthAnchor.constraint(equalToConstant: size*0.52)
                let widthConstarint = self.heightAnchor.constraint(equalToConstant: 40)
                NSLayoutConstraint.activate([heightConstraint,widthConstarint])
                self.setNeedsLayout()
            }
            }
            }
        }
        
    }
    
    
}
extension UIImageView
{
    @IBInspectable var KVimgWidth:Bool
        {
        
        
        get{
            
            return true
            
        }
        set{
            
            if newValue == true{
                
                let size = UIScreen.main.bounds.width
                
                print(size)
                
                
                    
                    if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
                    {
                        
                        // self.widthAnchor.constraint(equalToConstant: size*0.01)
                        
                        let heightConstraint = self.widthAnchor.constraint(equalToConstant: size*0.45)
                       // let widthConstarint = self.heightAnchor.constraint(equalToConstant: 30)
                        NSLayoutConstraint.activate([heightConstraint])
                        self.setNeedsLayout()
                        
                    }
                    else
                    {
                        let heightConstraint = self.widthAnchor.constraint(equalToConstant: size*0.35)
                       // let widthConstarint = self.heightAnchor.constraint(equalToConstant: 40)
                        NSLayoutConstraint.activate([heightConstraint])
                        self.setNeedsLayout()
                    }
                
            }
        }
        
    }
    
}
extension UICollectionView
{
    @IBInspectable var KVcollectionHeight:Bool
        {
        
        
        get{
            
            return false
            
        }
        set{
            
            if newValue == true{
                
                let size = UIScreen.main.bounds.width
                
                print(size)
                
                
                
                if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
                {
                    
                    // self.widthAnchor.constraint(equalToConstant: size*0.01)
                    
                    let heightConstraint = self.heightAnchor.constraint(equalToConstant: size/4.5+16)
                    // let widthConstarint = self.heightAnchor.constraint(equalToConstant: 30)
                    NSLayoutConstraint.activate([heightConstraint])
                    self.setNeedsLayout()
                    
                }
                else
                {
                    let heightConstraint = self.heightAnchor.constraint(equalToConstant: size/7.8+16)
                    // let widthConstarint = self.heightAnchor.constraint(equalToConstant: 40)
                    NSLayoutConstraint.activate([heightConstraint])
                    self.setNeedsLayout()
                }
                
            }
        }
        
    }
    
}

class PhoneTextF: TextField {
    
    let padding = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 8)
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
}


class kvTextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 8)
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
}


class DJTextField: MLPAutoCompleteTextField {
    
    let padding = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 8)
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
}


extension UIImageView
{
    func KVSetImage(urlstrng: String, baseImage: String)
    {
        self.image = UIImage.init(named: baseImage)
        
        let url = URL.init(string: urlstrng)
        
        URLSession.shared.dataTask(with: url!) { data, response, error in
            
            guard let data = data, error == nil else { return }
            
            DispatchQueue.main.async() {
                self.image = UIImage(data: data)
            }
            
            }.resume()
        
    }
    
    func KVBase64Image(base64: String, baseImage: String)
    {
        self.image = UIImage.init(named: baseImage)
        
        if let decodedData = Data(base64Encoded: base64, options: .ignoreUnknownCharacters) {
            let imag = UIImage(data: decodedData)
            self.image = imag!
        }
    }
    
    
    
}

extension String{
    
    func isValidEmail() -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let PHONE_REGEX = "[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: self)
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result2 = emailTest.evaluate(with: self)
        
        if result == false
        {
            return result2
        }
        return result
        
     }
    func isPasswordValid() -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: self)
    }
    func isPhoneValid() -> Bool
    {
        let PHONE_REGEX = "[0-9]{6,15}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return phoneTest.evaluate(with: self)
    }
    func isValidfname() -> Bool {
        if self.count > 0 {
            return true
        } else {
            return false
        }
    }
    func isValidPasswordLength() -> Bool {
        if self.count > 5 {
            return true
        } else {
            return false
        }
    }
    func isValidlname() -> Bool {
        
        let emailRegEx = "^[A-Za-z]{3,14}$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
        
    }
    
    func isValidateUrl (urlString: String) -> Bool {
        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: urlString)
    }
}
class KVCardView: UIView {
    
    @IBInspectable var KVcornerRadius: CGFloat = 4
    
    @IBInspectable var shadowOffsetWidth: CGFloat = 1
    @IBInspectable var shadowOffsetHeight: CGFloat = 1//0.6
    // @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var KVshadowOpacity: Float = 0.7
    
    override func layoutSubviews() {
        layer.cornerRadius = KVcornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: KVcornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = KVshadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}





//@IBDesignable
class KVCardBaseView: UIView {
    
    var contentView: UIView?
    
    @IBInspectable var corner: CGFloat = 2.0{
        didSet {
            self.layer.cornerRadius = corner
            self.layer.masksToBounds = true
            self.layer.masksToBounds = true
        }
        
    }
    
    @IBInspectable var shadow: Bool = false{
        didSet{
            if shadow
            {
                self.layer.masksToBounds = false
                self.layer.masksToBounds = false
                self.layer.shadowColor = UIColor.black.cgColor
                self.layer.shadowOpacity = 0.1
                self.layer.shadowOffset = CGSize(width: 3, height: 3)
                self.layer.shadowRadius = 2
                self.layer.shouldRasterize = true
                self.layer.rasterizationScale = UIScreen.main.scale
                self.layer.cornerRadius = self.corner
                self.layer.shadowPath = UIBezierPath.init(roundedRect: self.bounds, cornerRadius: self.corner).cgPath
                
                //                [layer setShadowOffset:CGSizeMake(0, 3)];
                //                [layer setShadowOpacity:0.4];
                //                [layer setShadowRadius:3.0f];
                //                [layer setShouldRasterize:YES];
                //                
                //                [layer setCornerRadius:12.0f];
                //                [layer setShadowPath:
                //                    [[UIBezierPath bezierPathWithRoundedRect:[self bounds]
                //                    cornerRadius:12.0f] CGPath]];
                
                
            }
        }
    }
    
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    convenience init() {
        self.init(frame: .zero)
    }
    
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        
        
    }
    
    
    
}

 //For PullTo Refresh TableView
extension UITableView{
    
    func pullTorefresh(_ target: Selector, _ toView: UIViewController?)
    {
        let refrshControll = UIRefreshControl()
        refrshControll.tintColor = UIColor.init(hex: "28AFB0")
        refrshControll.removeTarget(nil, action: nil, for: UIControlEvents.allEvents)
        refrshControll.addTarget(toView!, action: target, for: UIControlEvents.valueChanged)
        self.refreshControl = refrshControll
    }
    
    func closeEndPullRefresh()
    {
        self.refreshControl?.endRefreshing()
    }
    
}






