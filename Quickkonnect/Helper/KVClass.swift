//
//  KVClass.swift
//  demoappp
//
//  Created by chetan on 25/10/17.
//  Copyright © 2017 chetan. All rights reserved.
//

import Foundation
import UIKit
import AFNetworking
import CoreLocation
import LNRSimpleNotifications
import SVProgressHUD



private var _shareinstance : KVClass? = nil

class KVClass: NSObject, CLLocationManagerDelegate {
    
    
    struct Device {
        // iDevice detection code
        static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
        static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
        static let IS_RETINA           = UIScreen.main.scale >= 2.0
        
        static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
        static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
        static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
        static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
        
        static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
        static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
        static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
        static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
        static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
    }
    
    

    var isrefreshPresentView = false
    
    var Longitude = "0"
    var Lattitude = "0"
    var locationManager = CLLocationManager()
    //var  delegate : KVLocationDelegate?
    static let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 _-&+=().,?'"
    static let ACCEPTABLE_NUMBERS = "0123456789"
    static let ACCEPTABLE_NUMBERSANDCHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    
    //Reject Reason
    static let strReason1 = "My details are not correct."
    static let strReason2 = "I am not the concerned person."
    static let strReason3 = "I am not interested."
    static let strReasonFourth = "Block "
    //========================================================//
    
    
    //All Alert messages
    
    //==========================================================//
    
    var NotiManager = LNRNotificationManager()
    
    var KVUserOfflineMode: Bool = kUserDefults_(KEYSPROJECT.kUserOfflineMode) as? Bool ?? false
    
    var KVBarcodeDisableMode: Bool = kUserDefults_(KEYSPROJECT.kBarcodeDisableMode) as? Bool ?? false
    

    class func shareinstance()-> KVClass {
        if _shareinstance == nil {
            _shareinstance = KVClass()
        }
        return _shareinstance!
    }
    
    func CurrentlocationPermittion() {
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let lovcloc = manager.location
        {
            self.Longitude = String.init(format: "%.8f", lovcloc.coordinate.longitude)
            self.Lattitude = String.init(format: "%.8f", lovcloc.coordinate.latitude)
        }
    }
    
    struct AppUtility {
        
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        }
        
        /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
            
            self.lockOrientation(orientation)
            
            UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        }
    }
    
    
    class func themeColor()->UIColor{
        return UIColor(red:40/255.0, green:175/255.0, blue:176/255.0, alpha: 1.0)
    }
    
    
    class open func ShowNotification(_ Title: String, _ Message: String) {
        KVClass.shareinstance().NotiManager.notificationsBackgroundColor = UIColor.init(hex: "4C5C6B")
        KVClass.shareinstance().NotiManager.notificationsPosition = .top
        KVClass.shareinstance().NotiManager.notificationsTitleTextColor = .white
        KVClass.shareinstance().NotiManager.notificationsTitleFont = UIFont.init(name: "lato-Medium", size: 15)!
        KVClass.shareinstance().NotiManager.notificationsBodyTextColor = .white
        KVClass.shareinstance().NotiManager.notificationsBodyFont = UIFont.init(name: "lato-Regular", size: 16)!
        KVClass.shareinstance().NotiManager.showNotification(notification: LNRNotification.init(title: Title, body: Message))
    }
    
    
    class open func KVSplitString(inputString: String) -> String
    {
        let doub = Double(inputString)!
        let str = String(format: "%g",doub)
        return str
        
    }
    
    
    
    class open func KVDiscount(price: String, salseprice: String) -> String
    {
        let value = Float(price)!
        let value2 = Float(salseprice)!
        let value3 = value-value2
        
        let dis = Float(value3/value*100)
        
        let st = String(format: "%.2f",dis)
        
        
        
        return "\(self.KVSplitString(inputString: st))% off"
    }
    class open func KVattributeIconString(inputImage: String, stringName: String) -> NSMutableAttributedString
    {
        
        let sizest = NSAttributedString.init(string: stringName)
        
        let sizeV = sizest.size()
        
        let Mainimg = UIImage.init(named: inputImage)
        
        let MainImgView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: sizeV.height, height: sizeV.height))
        MainImgView.image = Mainimg
        MainImgView.contentMode = .scaleAspectFit
        
        var base64 = ""
        
        UIGraphicsBeginImageContextWithOptions(CGSize.init(width: sizeV.height, height: sizeV.height)
            , false, 0.0)
        
        let context = UIGraphicsGetCurrentContext()
        MainImgView.layer.render(in: context!)
        let imAge = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        var img = imAge!

        base64 = (UIImagePNGRepresentation(img)?.base64EncodedString(options: Data.Base64EncodingOptions.endLineWithCarriageReturn))!

        print(base64)
        
        img = UIImage.init(data: Data.init(base64Encoded: base64)!)!
        
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = UIImage(named: inputImage)?.tint(with: UIColor.darkGray)
       // imageAttachment.image?.tintWithColor(color: UIColor.darkGray)
        
        let imageOffsetY:CGFloat = -3.5
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: sizeV.height + 5, height: sizeV.height + 5)
        
        
        
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        //Add image to mutable string
        completeText.append(attachmentString)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: stringName)
        textAfterIcon.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange.init(location: 0, length: textAfterIcon.length))
        textAfterIcon.addAttribute(NSFontAttributeName, value: UIFont.init(name: "lato-Medium", size: 15)!, range: NSRange.init(location: 0, length: textAfterIcon.length))
        completeText.append(textAfterIcon)
        
        return completeText
    }
    
    class open func KVAlert(withView:UIViewController?, withTitle: String, withMessage: String, withAction: NSArray)
    {
        let alert = UIAlertController.init(title: withTitle, message: withMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        for i in 0..<withAction.count
        {
            alert.addAction(withAction.object(at: i) as! UIAlertAction)
        }
        
        withView?.present(alert, animated: true, completion: nil)
    }
    
    class open func KVDefaultAction(withTitle: String, withStyle: UIAlertActionStyle, view: UIViewController) -> UIAlertAction
    {
        let action = UIAlertAction.init(title: withTitle, style: withStyle) { (action) in
            
            
            view.dismiss(animated: true, completion: nil)
        }
        return action
    }
    class func getDayOfWeek(today:String)-> String {
        
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let todayDate = formatter.date(from: today)
        
        formatter.dateFormat = "yyyy-MM-dd (EEEE)"
        
        return formatter.string(from: todayDate!)
    }
    
    class func getMonthofYear(today:String)-> String {
        
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let todayDate = formatter.date(from: today)
        
        formatter.dateFormat = "MMMM yyyy"

        return formatter.string(from: todayDate!)
    }
    
    class func getAllYearDay(today: String) -> String
    {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let todayDate = formatter.date(from: today)
        
        formatter.dateFormat = "EE dd, MMM yyyy"
        
        return formatter.string(from: todayDate!)
    }
    
    class open func KVTimeZone() -> String
    {
        let timezone = TimeZone.current
        
        return timezone.identifier
    }
    
    class open func KVShowLoader()
    {
        appDelegate.window?.isUserInteractionEnabled = false
        SVProgressHUD.show()
    }
    class open func KVHideLoader()
    {
        appDelegate.window?.isUserInteractionEnabled = true
        SVProgressHUD.dismiss()
    }
    
    
    class open func KVServiece(methodType: String, processView: UIViewController?,baseView: UIViewController?, processLabel: String, params: NSMutableDictionary?, api: String,arrFlag: Bool, Hendler complition:@escaping (_ JSON:NSDictionary,_ status:Int) -> Void) {
        
       if !KVClass.shareinstance().KVUserOfflineMode {
            AFNetworkReachabilityManager.shared().startMonitoring()
        
            if processView != nil {
                KVShowLoader()
            }
      
            let url = NSURL(string:"\(KEYSPROJECT.BaseUrl)"  + api)
            if url == nil {
                KVAlert(withView: processView, withTitle: "Alert", withMessage: "URL is not valid", withAction: [KVDefaultAction(withTitle: "OK", withStyle: .default, view: processView!)])
                return
            }
            let request = NSMutableURLRequest(url: url! as URL)
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)        //session.delegate = self
            request.httpMethod = methodType
            //request.timeoutInterval = 60
        
            var apibody:NSMutableDictionary!
            if params == nil {
                apibody = NSMutableDictionary()
            }
            else {
                apibody = params!.mutableCopy() as! NSMutableDictionary
            }

            var jsonstring = String()
            if params != nil {
                if arrFlag {
                    let arr1arrr = NSMutableArray()
                    arr1arrr.add(params!)
                
                    if JSONSerialization.isValidJSONObject(arr1arrr) {
                        let jsondata = try! JSONSerialization.data(withJSONObject: arr1arrr, options: JSONSerialization.WritingOptions.prettyPrinted)
            
                        jsonstring = String.init(data: jsondata, encoding: String.Encoding.utf8)!
            
                        print(jsonstring)
                    }
                }
                else {
                    if JSONSerialization.isValidJSONObject(params!) {
                        let jsondata = try! JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                        jsonstring = String.init(data: jsondata, encoding: String.Encoding.utf8)!
                    
                        print(jsonstring)
                    }
                }
                request.httpBody = jsonstring.data(using: .ascii, allowLossyConversion: true)
                request.addValue("AAAAmGLql2s:APA91bF-fhfWlp4p_aRjK7UL8ZUPy4VlWWnbRExecXf9yZ1B0pYvT5Jg0r2XS6DOWT1Zb4KH-NfRlG3K_WSTM2RbkP3xD7d-pZF5zlSmb", forHTTPHeaderField: "QK_ACCESS_KEY")
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            }

        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            
            DispatchQueue.main.async {
                
                if processView != nil
                {
                    KVHideLoader()
                }
                if data != nil {
                    
                    let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("method : \(methodType)")
                    print("Url: \(KEYSPROJECT.BaseUrl)" + api)
                    print("Body: \(apibody!)" )
                    print("Responcse : \(strData!)")
                    
                    do {
                        if let returnData = String(data: data!, encoding: .utf8) {
                            print(returnData)
                        }
                        
                        if  let JSON = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            
                            if let errorCode = JSON["status"] as? Int {
                                if errorCode == 200 {
                                    complition(JSON,1)
                                    return
                                }else {

                                    // complition(JSON,1)

//                                    if AFNetworkReachabilityManager().isReachable == false
//                                    {
//                                        KVClass.ShowNotification("Error !!", "Internet Connection is missing!")
//                                    }
//                                    else
//                                    {
                                    complition(JSON,2214)
                                    if (api != "scan_user_list/\(KVAppDataServieces.shared.LoginUserID())?timezone=\(KVClass.KVTimeZone())") && (api != "member/block_profile_list") && (api != "member/check_member") && (api != "member/member_list") && (api != "scan_user_store") && (api != "profile/followers_list") && (api != "user_contact_block_list/\(KVAppDataServieces.shared.LoginUserID())?timezone=\(KVClass.KVTimeZone())")
                                    {
                                      KVClass.ShowNotification("", JSON["msg"] as? String ?? "")
                                    }

                                }
                            }
                        }
                    } catch {
                        
                        if processView != nil
                        {
                            KVClass.ShowNotification("", "Opps! Something went wrong, Please try again later!")
                           // processView?.view.makeToast("Opps! Something went wrong, Please try again later!", duration: 2.0, position: .bottom)
                        }
                        else
                        {
                            KVClass.ShowNotification("", "Opps! Something went wrong, Please try again later!")
                      //  baseView?.view.makeToast("Opps! Something went wrong, Please try again later!", duration: 2.0, position: .bottom)
                        }
                    }
                }else{
                    if AFNetworkReachabilityManager().isReachable == false {
                        KVClass.ShowNotification("No Internet Connection", "Please Check your Internet Connection and try again")
                    }
                    
                }
                
                
            }
            
        })
        task.resume()
//        DispatchQueue.main.async {
//            task.suspend()
//        }
//
        
        }
        
        else
       {
        
        }
        
        
    }
    
    class open func KVSyncServiece(methodType: String, params: NSMutableDictionary?,paramAsarr: NSArray?, api: String,arrFlag: Bool, Hendler complition:@escaping (_ JSON:NSDictionary,_ status:Int) -> Void)
    {
        
        AFNetworkReachabilityManager.shared().startMonitoring()
        
        
        let url = NSURL(string:"\(KEYSPROJECT.BaseUrl)"  + api)
        if url == nil {
           
            return
        }
        
        
        let request = NSMutableURLRequest(url: url! as URL)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)        //session.delegate = self
        //session.delegate = self
        request.httpMethod = methodType
        //request.timeoutInterval = 60
        
        var apibody:NSMutableDictionary!
        if params == nil {
            apibody = NSMutableDictionary()
        }else{
            apibody = params!.mutableCopy() as! NSMutableDictionary
        }
        
        
        
        
        var jsonstring = String()
        
        if paramAsarr?.count == 0 
        
        {
        if params != nil
        {
            
            if arrFlag
            {
                
                let arr1arrr = NSMutableArray()
                arr1arrr.add(params!)
                
                if JSONSerialization.isValidJSONObject(arr1arrr)
                {
                    let jsondata = try! JSONSerialization.data(withJSONObject: arr1arrr, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    jsonstring = String.init(data: jsondata, encoding: String.Encoding.utf8)!
                    
                    print(jsonstring)
                }
            }
            else
            {
                if JSONSerialization.isValidJSONObject(params!)
                {
                    let jsondata = try! JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    jsonstring = String.init(data: jsondata, encoding: String.Encoding.utf8)!
                    
                    print(jsonstring)
                }
            }
            
            
            request.httpBody = jsonstring.data(using: .ascii, allowLossyConversion: true)
            
            
            // [req setValue:postLength forHTTPHeaderField:@"Content-Length"];
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        }
        else
        {
            if JSONSerialization.isValidJSONObject(paramAsarr!)
            {
                let jsondata = try! JSONSerialization.data(withJSONObject: paramAsarr!, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                jsonstring = String.init(data: jsondata, encoding: String.Encoding.utf8)!
                
                print(jsonstring)
            }
            
            request.httpBody = jsonstring.data(using: .ascii, allowLossyConversion: true)
            
            
            // [req setValue:postLength forHTTPHeaderField:@"Content-Length"];
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
        }
        // request.addValue("\(jsonstring.data(using: .ascii, allowLossyConversion: true)?.count)", forHTTPHeaderField: "Content-Length")
        
        
        // let task = session.uploadTask(with: request as URLRequest, from: nil) { (data, response, error) in
        
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            
            DispatchQueue.main.async {
                
                
                
                if data != nil {
                    
                    //let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    //print("method : \(methodType)")
                    //print("Url: \(KEYSPROJECT.BaseUrl)" + api)
                   // print("Body: \(apibody!)" )
                  //  print("Responcse : \(strData!)")
                    
                    
                    do {
                        if  let JSON = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            
                            
                            
                            if let errorCode = JSON["ResponseStatus"] as? Bool {
                                if errorCode == true {
                                    complition(JSON,1)
                                    return
                                }else{
                                    
                                    // complition(JSON,1)
                                    
//                                    if AFNetworkReachabilityManager().isReachable == false
//                                    {
//                                        self.KVAlert(withView: baseView, withTitle: "Message !", withMessage: "Internet Connection is missing..", withAction: [self.KVDefaultAction(withTitle: "OK", withStyle: .cancel, view: baseView!)])
//                                    }
//                                    else
//                                    {
//                                        processView?.makeToast(JSON["Message"] as? String ?? "Failed!", duration: 2.0, position: .bottom)
//                                    }
                                    
                                }
                            }
                        }
                    } catch {
                        
//                        processView?.makeToast("Opps! Something went wrong, Please try again later!", duration: 2.0, position: .bottom)
                    }
                }else{
                    
//                    if AFNetworkReachabilityManager().isReachable == false
//                    {
//                        self.KVAlert(withView: baseView, withTitle: "Message !", withMessage: "Internet Connection is missing..", withAction: [self.KVDefaultAction(withTitle: "OK", withStyle: .cancel, view: baseView!)])
//                    }
//                    else
//                    {
//                        processView?.makeToast("Opps! Something went wrong, Please try again later!", duration: 2.0, position: .bottom)
//                    }
                    
                }
                
                
            }
            
        })
        task.resume()
        
    }
    
    
    class open func KVPhotoUPService(methodType: String, processView: UIView?,baseView: UIViewController?, processLabel: String, params: NSMutableDictionary?, api: String,arrFlag: Bool, Hendler complition:@escaping (_ JSON:NSDictionary,_ status:Int) -> Void)
    {
        
        
        
        
        AFNetworkReachabilityManager.shared().startMonitoring()
        KVClass.KVShowLoader()
        //process.mode = .determinate
       // process.label.text = processLabel
        
        
        let url = NSURL(string:"\(KEYSPROJECT.BaseUrl)"  + api)
        if url == nil {
            KVAlert(withView: baseView, withTitle: "Alert", withMessage: "URL is not valid", withAction: [KVDefaultAction(withTitle: "OK", withStyle: .default, view: baseView!)])
            return
        }
        
        
        let request = NSMutableURLRequest(url: url! as URL)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)        //session.delegate = self
        //session.delegate = self
        request.httpMethod = methodType
        //request.timeoutInterval = 60
        
        var apibody:NSMutableDictionary!
        if params == nil {
            apibody = NSMutableDictionary()
        }else{
            apibody = params!.mutableCopy() as! NSMutableDictionary
        }
        
        
        
        
        var jsonstring = String()
        
        if params != nil
        {
            
            if arrFlag
            {
                
                let arr1arrr = NSMutableArray()
                arr1arrr.add(params!)
                
                if JSONSerialization.isValidJSONObject(arr1arrr)
                {
                    let jsondata = try! JSONSerialization.data(withJSONObject: arr1arrr, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    jsonstring = String.init(data: jsondata, encoding: String.Encoding.utf8)!
                    
                    print(jsonstring)
                }
            }
            else
            {
                if JSONSerialization.isValidJSONObject(params!)
                {
                    let jsondata = try! JSONSerialization.data(withJSONObject: params!, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    jsonstring = String.init(data: jsondata, encoding: String.Encoding.utf8)!
                    
                    print(jsonstring)
                }
            }
            
            
            request.httpBody = jsonstring.data(using: .ascii, allowLossyConversion: true)
            
            
            // [req setValue:postLength forHTTPHeaderField:@"Content-Length"];
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        // request.addValue("\(jsonstring.data(using: .ascii, allowLossyConversion: true)?.count)", forHTTPHeaderField: "Content-Length")
        
        
       // let task = session.uploadTask(with: request as URLRequest, from: nil) { (data, response, error) in
            
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            
            DispatchQueue.main.async {
                
                KVClass.KVHideLoader()
                
                if data != nil {
                    
                  //  let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("method : \(methodType)")
                    print("Url: \(KEYSPROJECT.BaseUrl)" + api)
                   // print("Body: \(apibody!)" )
                   // print("Responcse : \(strData!)")
                    
                    
                    do {
                        if  let JSON = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            
                            
                            
                            if let errorCode = JSON["ResponseStatus"] as? Bool {
                                if errorCode == true {
                                    complition(JSON,1)
                                    return
                                }else{
                                    
                                    // complition(JSON,1)
                                    
                                    if AFNetworkReachabilityManager().isReachable == false
                                    {
                                        self.KVAlert(withView: baseView, withTitle: "Message !", withMessage: "Internet Connection is missing..", withAction: [self.KVDefaultAction(withTitle: "OK", withStyle: .cancel, view: baseView!)])
                                    }
                                    else
                                    {
                                        processView?.makeToast(JSON["Message"] as? String ?? "Failed!", duration: 2.0, position: .bottom)
                                    }
                                    
                                }
                            }
                        }
                    } catch {
                        
                        processView?.makeToast("Opps! Something went wrong, Please try again later!", duration: 2.0, position: .bottom)
                    }
                }else{
                    
                    if AFNetworkReachabilityManager().isReachable == false
                    {
                        self.KVAlert(withView: baseView, withTitle: "Message !", withMessage: "Internet Connection is missing..", withAction: [self.KVDefaultAction(withTitle: "OK", withStyle: .cancel, view: baseView!)])
                    }
                    else
                    {
                        processView?.makeToast("Opps! Something went wrong, Please try again later!", duration: 2.0, position: .bottom)
                    }
                    
                }
                
                
            }
            
        })
        task.resume()
        
        
    }
    


    
    class func currentDateTimeString() -> String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let strDate = dateFormat.string(from: NSDate() as Date)
        return strDate
    }
    
}

