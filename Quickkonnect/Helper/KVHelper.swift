//
//  KVHelper.swift
//  demoappp
//
//  Created by chetan on 26/10/17.
//  Copyright © 2017 chetan. All rights reserved.
//

import Foundation
import UIKit


let appDelegate = UIApplication.shared.delegate as! AppDelegate

let baseViewController = UIApplication.shared.windows.first

 func AlldefaultsRemove() {
    
    let defs = UserDefaults.standard
    let dict = defs.dictionaryRepresentation() as NSDictionary
    for key in dict.allKeys
    {
        if key as! String != "fcmtoken"  {
            defs.removeObject(forKey: key as! String)
        }
    }
    defs.synchronize()
    
}

func kUserDefults(_ value : AnyObject, key : String ){
    let defults = UserDefaults.standard
    defults .setValue(value, forKey: key )
    defults.synchronize()
}
func kUserDefults_( _ key : String) -> AnyObject? {
    let defults = UserDefaults.standard
    return defults.value(forKey: key ) as AnyObject?
    
}
func kUserDefultrRemove( _ removeKey : String) {
    let defults = UserDefaults.standard
    defults.removeObject(forKey: removeKey)
    defults.synchronize()
    
}
func gerResutUser() -> [String:String] {
    return kUserDefults_(KEYSPROJECT.resultUser) as! [String:String]
    
}





func formatDate(date: NSDate) -> String {
    let format = "yyyy-MM-dd"
    let formatter = DateFormatter()
    formatter.dateFormat = format
    return formatter.string(from: date as Date)
    
}

func formattedDaysInThisWeek() -> [String] {
   
    let formatter  = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    
    
    
    let today = Date()
    let gregorian = Calendar.current
    
    let weekdayComponetns = gregorian.dateComponents([.weekday], from: today)
    
    var compontentsToSubstract = DateComponents()
    
   // let compontentsToSubstract = NSDateComponents.new()
    // Sunday -> -(weekdayComponetns.weekday - gregorian.firstWeekday)
    // Monday -> -(weekdayComponetns.weekday - gregorian.firstWeekday - 1)
    
    compontentsToSubstract.day = -(weekdayComponetns.weekday! - gregorian.firstWeekday - 1)
    
    var beignweek = gregorian.date(byAdding: compontentsToSubstract, to: today)
    
    let compi = gregorian.dateComponents([.year,.month,.day], from: beignweek!)
    
    beignweek = gregorian.date(from: compi)
    
    let mondat = formatter.string(from: beignweek!)
    
    print(beignweek!)
    
    var WeekDat = [String]()
    
    WeekDat.removeAll()
    
    WeekDat.append(mondat)
    
    for i in 1..<7
    {
        let tomorrow = Calendar.current.date(byAdding: .day, value: i, to: beignweek!)
        let somedateString = formatter.string(from: tomorrow!)
        
        WeekDat.append(somedateString)
    }
    
    
    return WeekDat
    
}


//struct COLOR{
//
//    static let PurpleColor = UIColor(red:144/255,green:19/255,blue:254/255,alpha: 1.0)
//    static let borderColor = UIColor(red:215/255,green:215/255,blue:215/255,alpha: 1.0)
//    static let theameColor = #colorLiteral(red: 0.4412799478, green: 0.1903065443, blue: 0.6293438077, alpha: 1)
//
//
//}
//
//
//struct FONT{
//
//    static let ProxiRegular = "ProximaNova-Regular"
//    static  let ProxiBold = "ProximaNova-Bold"
//    static let UltraRegular = "ProximaNova-RegularIt"
//    static let UltraBold = "ProximaNova-SemiboldIt"
//}
//
//
//struct VALIDATION {
//
//
//}

//struct userType {
//
//
//}
struct KEYSPROJECT {
    
    static let is_Login = "is_Login"
    static let ishypaconfirmed = "ishypaconfirmed"
    static let resultUser = "resultUser"
    static let Facility = "Facility"
    static let FaciID = "FaciID"
    static let ExFaciId = "ExFaciId"
    static let ContentCount = "ContentCountUser"
    static let FaciName = "FaciName"
    static let isfacilityselected = "isfacilityselected"
    static let navigationdata = "navigation"
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    static let kDeviceToken = "DeviceToken"
    static let scopes = "https://www.googleapis.com/auth/drive.file"
    static let keychainItemName = "My App";
    static let HostStaus = "HostStaus";
    static let BaseUrl = "http://54.70.2.200/quickkonnect/api/" //demourl
    //static let BaseUrl = "https://quickkonnect.com/app/api/"
    static let ImageUrl = "http://ci.metizcloud.com/welledeals/uploads/images/full/"
    static let ImageUrl2 = "http://ci.metizcloud.com/welledeals/uploads/"
    static let kUserID = kUserDefults_("kLoginusrid") as? Int ?? 0
    static let headers:[String:String] = ["QK_ACCESS_KEY":"AAAAmGLql2s:APA91bF-fhfWlp4p_aRjK7UL8ZUPy4VlWWnbRExecXf9yZ1B0pYvT5Jg0r2XS6DOWT1Zb4KH-NfRlG3K_WSTM2RbkP3xD7d-pZF5zlSmb"]
    
    
    
    static let kUserOfflineMode = "kUserOfflineMode"
    
    static let kBarcodeDisableMode = "kBarcodeDisableMode"
}

struct KEYDEFULTS {
    
    static let kDeviceToken = kUserDefults_(KEYSPROJECT.kDeviceToken)
    static let kResultUser = kUserDefults_(KEYSPROJECT.resultUser)
    
    
    static let kUserID = kUserDefults_("kLoginusrid") as? Int ?? 0
    
    static var UserOfflineMode = kUserDefults_(KEYSPROJECT.kUserOfflineMode) as? Bool ?? false
    
}

