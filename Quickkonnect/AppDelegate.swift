//
//  AppDelegate.swift
//  Quickkonnect
//
//  Created by Kavi on 17/12/17.
//  Copyright © 2017 com.kavish. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import LinkedinSwift
import Firebase
import UserNotifications
import Quikkly
import TwitterKit
import MobileCoreServices
import SafariServices
import Contacts
import ContactsUI
import GoogleMaps
import GooglePlaces
import SDWebImage
import Fabric
import Crashlytics
import RealmSwift


let app_Delegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
//extension UIApplication {
//    var statusBarView: UIView? {
//        return value(forKey: "statusBar") as? UIView
//    }
//}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var isBackChange = false
    var isDisplayBlur = false
    let build =  UInt64(2)
    var roleID = 0
    var roleNAME = ""
    var strProfileScreenFrom = ""
    var strSelectedTermsCondition = ""
    var strStartSessionTIME = ""
    var strEndSessionTIME = ""
    var strOpenView = ""
    var strAssignRole = ""
    var arrSocialData = NSMutableArray()
    var dictAddMemberInProfile : NSDictionary = [:]
    var CURRENT_COUNTRY_CODE = ""
    
    let config = Realm.Configuration(
        // Set the new schema version. This must be greater than the previously used
        // version (if you've never set a schema version before, the version is 0).
        
       
        
        schemaVersion: 10,
        
        // Set the block which will be called automatically when opening a Realm with
        // a schema version lower than the one set above
        migrationBlock: { migration, oldSchemaVersion in
            // We haven’t migrated anything yet, so oldSchemaVersion == 0
            if (oldSchemaVersion < 10) {
                // Nothing to do!
                // Realm will automatically detect new properties and removed properties
                // And will update the schema on disk automatically
            }
    })
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        
        self.updateContactList()
        self.updateBlockList()
        self.updateLocalProfile()
        KVClass.shareinstance().CurrentlocationPermittion()
        
        //sleep(2)
        application.statusBarStyle = .lightContent
        
        TWTRTwitter.sharedInstance().start(withConsumerKey: "PocREYlNfCQkd5rFmzUruTSri", consumerSecret: "WSpilhbkETbtxqR9THDKZPjDFyy4MQMsvHzHizQy3AnvRUzsQP")
        
        Quikkly.apiKey = "x4vBEOnCkhbPJdD2ZBRYQRGPLY8JjJxaCzFecwf49IITPgC4kvoPsGDHayu6qRhef4"
        Quikkly.blueprintFilename = "custom_blueprint.json"
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        CNContactStore().requestAccess(for: CNEntityType.contacts) { (isesses, err) in
            
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { (isgranted, err) in
               self.configNotifications()
            })
            
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
         let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        
        if KVAppDataServieces.shared.LoginUserID() != 0
        {
        if KVAppDataServieces.shared.UserValue(.qr_code) as? String ?? "" == ""
        {
            let obj = storyboard.instantiateViewController(withIdentifier: "SocialVC") as! SocialVC
                            obj.view.tag = 2214
                            let navi = UINavigationController.init(rootViewController: obj)
                            navi.setNavigationBarHidden(true, animated: false)
                            self.window?.rootViewController = navi
                           // self.navigationController?.pushViewController(obj, animated: true)
        }
        else
        {
            let vc = storyboard.instantiateViewController(withIdentifier: "tabbar")
                            let navi = UINavigationController.init(rootViewController: vc)
                            navi.setNavigationBarHidden(true, animated: false)
                            self.window?.rootViewController = navi
        }
        }
        else
        {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                        kUserDefults(true as AnyObject, key: "notification_status")
                        let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC")
                        let navi = UINavigationController.init(rootViewController: vc)
                        navi.setNavigationBarHidden(true, animated: false)
                        self.window?.rootViewController = navi
        }
        
//        if kUserDefults_("session") != nil
//        {
//        if let userdict = (NSKeyedUnarchiver.unarchiveObject(with: kUserDefults_("session") as! Data)) as? NSDictionary
//        {
//           let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//
//            if let state = userdict["notification_status"] as? Int
//            {
//                if state == 0
//                {
//                  kUserDefults(false as AnyObject, key: "notification_status")
//                }
//                else
//                {
//                    kUserDefults(true as AnyObject, key: "notification_status")
//                }
//            }
//
//
//
//            if (userdict["qr_code"] as? String ?? "") == ""
//            {
//                let obj = storyboard.instantiateViewController(withIdentifier: "SocialVC") as! SocialVC
//                obj.view.tag = 2214
//                let navi = UINavigationController.init(rootViewController: obj)
//                navi.setNavigationBarHidden(true, animated: false)
//                self.window?.rootViewController = navi
//               // self.navigationController?.pushViewController(obj, animated: true)
//            }
//            else
//            {
//                let vc = storyboard.instantiateViewController(withIdentifier: "host")
//                let navi = UINavigationController.init(rootViewController: vc)
//                navi.setNavigationBarHidden(true, animated: false)
//                self.window?.rootViewController = navi
//            }
//        }
//        }
//        else
//        {
//            kUserDefults(false as AnyObject, key: "ishomeupdate")
//            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//            kUserDefults(true as AnyObject, key: "notification_status")
//            let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC")
//            let navi = UINavigationController.init(rootViewController: vc)
//            navi.setNavigationBarHidden(true, animated: false)
//            self.window?.rootViewController = navi
//        }
        

        
        //For Fabric Crashlytics
        Fabric.with([Crashlytics.self])
        //========================================================================//
        //========================================================================//
        
        //For Google Map Key
        GMSServices.provideAPIKey("AIzaSyDW6HJPpZVkdFVjS27foM4yWCLlEtUrsdM")
        GMSPlacesClient.provideAPIKey("AIzaSyDW6HJPpZVkdFVjS27foM4yWCLlEtUrsdM")
        //========================================================================//
        //========================================================================//
        
        
        //START SESSION TIME
        app_Delegate.strStartSessionTIME = KVClass.currentDateTimeString()
        //================================================================//
        
        return true
    }

    func configNotifications()
    {
        let actin = UNNotificationAction.init(identifier: "id", title: "Show Notification", options: [.foreground, .destructive])
        let cat = UNNotificationCategory.init(identifier: "id.cat", actions: [actin], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([cat])
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    func updateLocalProfile()
    {
        DispatchQueue.main.async {
           
            if KVAppDataServieces.shared.LoginUserID() != 0
            {
            let urlapi = "get_user_detail/\(KVAppDataServieces.shared.LoginUserID())?timezone=\(KVClass.KVTimeZone())"
            
            KVClass.KVServiece(methodType: "GET", processView: nil, baseView: nil, processLabel: "", params: nil, api: urlapi, arrFlag: false) { (JSON, status) in
                
                if let dataDict = JSON["data"] as? NSDictionary
                {
                    KVAppDataServieces.shared.updateFullProfile(dataDict)
                }
            }
            }
        }
       
    }
    
    
    func updateBlockList()
    {
        DispatchQueue.main.async {
            
            if KVAppDataServieces.shared.LoginUserID() != 0
            {
                
                KVClass.KVServiece(methodType: "GET", processView: nil, baseView: nil, processLabel: "", params: nil, api: "user_contact_block_list/\(KVAppDataServieces.shared.LoginUserID())?timezone=\(KVClass.KVTimeZone())", arrFlag: false, Hendler: { (json, status) in
                    
                    if status == 1
                    {
                        if let value = json["data"] as? [NSDictionary]
                        {
                            let datavalue = NSKeyedArchiver.archivedData(withRootObject: value)
                            
                            KVAppDataServieces.shared.update(.block_user_list, datavalue)
                            
                        }
                    }
                    if status == 2214
                    {
                         KVAppDataServieces.shared.removeListData(.block_user_list)
                    }
                })
                
                
            }
         }
    }
    func updateContactList()
    {
        DispatchQueue.main.async {
            
        if KVAppDataServieces.shared.LoginUserID() != 0
        {
            KVClass.KVServiece(methodType: "GET", processView: nil, baseView: nil, processLabel: "", params: nil, api: "scan_user_list/\(KVAppDataServieces.shared.LoginUserID())?timezone=\(KVClass.KVTimeZone())", arrFlag: false, Hendler: { (json, status) in
                print(json)
                
                
                    
                    if status == 1
                    {
                        
                        if let value = json["data"] as? [NSDictionary]
                        {
                          //  DispatchQueue.main.async(execute: {
                            
                            let datavalue = NSKeyedArchiver.archivedData(withRootObject: value)
                            
                            KVAppDataServieces.shared.update(.contact_user_list, datavalue)
                          // })
                        }
                        if let countdatadict = json["contact_count"] as? NSDictionary
                        {
                            KVAppDataServieces.shared.update(.contact_all, countdatadict["contact_all"]!)
                            KVAppDataServieces.shared.update(.contact_week, countdatadict["contact_week"]!)
                            KVAppDataServieces.shared.update(.contact_month, countdatadict["contact_month"]!)
                        }
                    }
                    if status == 2214
                    {
                        KVAppDataServieces.shared.removeListData(.contact_user_list)
                    }
                    
                
                    
//                if status == 1
//                {
//
//                    if let value = json["data"] as? [NSDictionary]
//                    {
//                        let datavalue = NSKeyedArchiver.archivedData(withRootObject: value)
//
//                        KVAppDataServieces.shared.update(.contact_user_list, datavalue)
//
//                    }
//                }
//                if status == 2214
//                {
//                   KVAppDataServieces.shared.removeListData(.contact_user_list)
//                }
            })
            
      }
    }
   }
    
    

    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {

        
        if LinkedinSwiftHelper.shouldHandle(url) {
            return LinkedinSwiftHelper.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        } else {
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
    }
    
    
//    func application(_ application: UIApplication, willChangeStatusBarFrame newStatusBarFrame: CGRect) {
//        UIView.animate(withDuration: 0.35, animations: {() -> Void in
//            var windowFrame: CGRect? = ((self.window?.rootViewController as? UITabBarController)?.viewControllers![0] as? UINavigationController)?.view?.frame
//            if newStatusBarFrame.size.height > 20 {
//                windowFrame?.origin.y = newStatusBarFrame.size.height - 20
//                // old status bar frame is 20
//            }
//            else {
//                windowFrame?.origin.y = 0.0
//            }
//            ((self.window?.rootViewController as? UITabBarController)?.viewControllers![0] as? UINavigationController)?.view?.frame = windowFrame!
//        })
//    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        //START SESSION TIME
        app_Delegate.strEndSessionTIME = KVClass.currentDateTimeString()
        //Call API Session Analytics
        self.callAPIforSessionAnalytics()
        //================================================================//
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        //START SESSION TIME
        app_Delegate.strStartSessionTIME = KVClass.currentDateTimeString()
        //================================================================//
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
        
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        UserDefaults.standard.set(fcmToken, forKey: "fcmtoken")
        UserDefaults.standard.synchronize()
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    private func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        self.updateContactList()
        self.updateBlockList()
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
  
        print(userInfo)
        self.updateContactList()
        self.updateBlockList()
        self.openNotificationViewController(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void)
    {
       // self.openNotificationViewController()
        self.updateContactList()
        self.updateBlockList()
        NotificationCenter.default.post(name: Notification.Name("RELOADNOTIFICATION"), object: nil)
        
        if let dictidata = notification.request.content.userInfo as NSDictionary? as! [String:Any]? {
            print(dictidata)
            
            if dictidata["notification_type"] as? String == "4"{
               
                self.updateLocalProfile()
                completionHandler([.sound])
            }
            else{
                completionHandler([.alert,.sound,.badge])
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        self.updateContactList()
        self.updateBlockList()
        self.openNotificationViewController(response.notification.request.content.userInfo)
      
        
    }
    
    func openNotificationViewController(_ dict: [AnyHashable: Any])
    {
       let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//       let obj = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
//        
//        let vc = storyboard.instantiateViewController(withIdentifier: "host")
//        let navi = UINavigationController.init(rootViewController: vc)
//        navi.setNavigationBarHidden(true, animated: false)
//        self.window?.rootViewController = navi
//        (self.window?.rootViewController as? UINavigationController)?.pushViewController(obj, animated: true)
        
        if let dictidata = dict as NSDictionary? as! [String:Any]? {
            print(dictidata)
        
        var naviobj: UIViewController? = nil
        
        switch dictidata["notification_type"] as? String ?? "" {
        case "1":
            naviobj = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            (naviobj as? ProfileVC)?.strContactProfile = ""
            (naviobj as? ProfileVC)?.scanuniquecode = dictidata["unique_code"] as? String ?? ""
            
            if dictidata["sender_id"] as? String ?? "" != ""
            {
             (naviobj as? ProfileVC)?.scanuserid = Int(dictidata["sender_id"] as? String ?? "0")!
            }
            else if dictidata["sender_id"] as? Int ?? 0 != 0
            {
              (naviobj as? ProfileVC)?.scanuserid = dictidata["sender_id"] as? Int ?? 0
            }
            (naviobj as? ProfileVC)?.isaddcontect = false
            (naviobj as? ProfileVC)?.Scan_user_update = true
        case "3":
            naviobj = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            (naviobj as? ProfileVC)?.strContactProfile = ""
            (naviobj as? ProfileVC)?.scanuniquecode = dictidata["unique_code"] as? String ?? ""
            if dictidata["sender_id"] as? String ?? "" != ""
            {
                (naviobj as? ProfileVC)?.scanuserid = Int(dictidata["sender_id"] as? String ?? "0")!
            }
            else if dictidata["sender_id"] as? Int ?? 0 != 0
            {
                (naviobj as? ProfileVC)?.scanuserid = dictidata["sender_id"] as? Int ?? 0
            }
            (naviobj as? ProfileVC)?.isaddcontect = false
            (naviobj as? ProfileVC)?.Scan_user_update = true
        case "2":
            
            naviobj = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            (naviobj as? NotificationViewController)?.strScreenFrom = "Push"
//            if let dictidata = dict as NSDictionary? as! [String:Any]? {
//                (naviobj as? DownloadContactListViewController)?.dictSelectedData = dictidata
//            }
        case "5":
            
             app_Delegate.isBackChange = true
            naviobj = storyboard.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
             (naviobj as? QKProfileVC)?.int_userID = dictidata["profile_id"] as? String ?? "" != "" ? Int(dictidata["profile_id"] as? String ?? "")! : dictidata["profile_id"] as? Int ?? 0
           
            
        case "6":
            app_Delegate.isBackChange = true
            naviobj = storyboard.instantiateViewController(withIdentifier: "QKProListVC") as! QKProListVC
            
            
        case "7":
            
            app_Delegate.isBackChange = true
            naviobj = storyboard.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
            (naviobj as? QKProfileVC)?.int_userID = dictidata["profile_id"] as? String ?? "" != "" ? Int(dictidata["profile_id"] as? String ?? "")! : dictidata["profile_id"] as? Int ?? 0
            
            
        case "8":
            
            app_Delegate.isBackChange = true
            naviobj = storyboard.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
            (naviobj as? QKProfileVC)?.int_userID = dictidata["profile_id"] as? String ?? "" != "" ? Int(dictidata["profile_id"] as? String ?? "")! : dictidata["profile_id"] as? Int ?? 0
            (naviobj as? QKProfileVC)?.str_Screen_From = "AcceptReject"
            if (dictidata["status"] as? String ?? "" == "Reject") || (dictidata["status"] as? String ?? "" == "Approve") {
                (naviobj as? QKProfileVC)?.str_Screen_From = ""
            }
            (naviobj as? QKProfileVC)?.strScreenFrom = "AppDelegate"
            (naviobj as? QKProfileVC)?.str_uniquecode = dictidata["unique_code"] as? String ?? ""
            (naviobj as? QKProfileVC)?.Notification_ID = dictidata["notification_id"] as? String ?? "" != "" ? Int(dictidata["notification_id"] as? String ?? "")! : dictidata["notification_id"] as? Int ?? 0
            
        case "9":
            
            app_Delegate.isBackChange = true
            naviobj = storyboard.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
            (naviobj as? QKProfileVC)?.int_userID = dictidata["profile_id"] as? String ?? "" != "" ? Int(dictidata["profile_id"] as? String ?? "")! : dictidata["profile_id"] as? Int ?? 0
            (naviobj as? QKProfileVC)?.str_Screen_From = ""
            (naviobj as? QKProfileVC)?.strScreenFrom = "AppDelegate"
            (naviobj as? QKProfileVC)?.str_uniquecode = dictidata["unique_code"] as? String ?? ""
            (naviobj as? QKProfileVC)?.Notification_ID = dictidata["notification_id"] as? String ?? "" != "" ? Int(dictidata["notification_id"] as? String ?? "")! : dictidata["notification_id"] as? Int ?? 0
            
        case "10":
            
            app_Delegate.isBackChange = true
            naviobj = storyboard.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
            (naviobj as? QKProfileVC)?.int_userID = dictidata["profile_id"] as? String ?? "" != "" ? Int(dictidata["profile_id"] as? String ?? "")! : dictidata["profile_id"] as? Int ?? 0
            (naviobj as? QKProfileVC)?.str_Screen_From = ""
            (naviobj as? QKProfileVC)?.strScreenFrom = "AppDelegate"
            (naviobj as? QKProfileVC)?.str_uniquecode = dictidata["unique_code"] as? String ?? ""
            (naviobj as? QKProfileVC)?.Notification_ID = dictidata["notification_id"] as? String ?? "" != "" ? Int(dictidata["notification_id"] as? String ?? "")! : dictidata["notification_id"] as? Int ?? 0
            
            
        case "11":
            
            app_Delegate.isBackChange = true
            naviobj = storyboard.instantiateViewController(withIdentifier: "AddMemberInProfileVC") as! AddMemberInProfileVC
            (naviobj as? AddMemberInProfileVC)?.strScreenFrom = "Notification"
            (naviobj as? AddMemberInProfileVC)?.NotificationID = dictidata["notification_id"] as? String ?? "" != "" ? Int(dictidata["notification_id"] as? String ?? "")! : dictidata["notification_id"] as? Int ?? 0
            
        case "12":
            
            app_Delegate.isBackChange = true
            naviobj = storyboard.instantiateViewController(withIdentifier: "AddMemberInProfileVC") as! AddMemberInProfileVC
            (naviobj as? AddMemberInProfileVC)?.strScreenFrom = "Notification"
            (naviobj as? AddMemberInProfileVC)?.NotificationID = dictidata["notification_id"] as? String ?? "" != "" ? Int(dictidata["notification_id"] as? String ?? "")! : dictidata["notification_id"] as? Int ?? 0
            
        case "13":
            
            app_Delegate.isBackChange = true
            naviobj = storyboard.instantiateViewController(withIdentifier: "AddMemberInProfileVC") as! AddMemberInProfileVC
            (naviobj as? AddMemberInProfileVC)?.strScreenFrom = "Notification"
            (naviobj as? AddMemberInProfileVC)?.NotificationID = dictidata["notification_id"] as? String ?? "" != "" ? Int(dictidata["notification_id"] as? String ?? "")! : dictidata["notification_id"] as? Int ?? 0
            
        case "14":
            
            app_Delegate.isBackChange = true
            naviobj = storyboard.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
            
            if dictidata["role"] as? Int ?? 0 == 1 || dictidata["role"] as? String ?? "0" == "1" {
                (naviobj as? QKProfileVC)?.int_userID = -1
                (naviobj as? QKProfileVC)?.str_Screen_From = "OnlyViewProfile"
                (naviobj as? QKProfileVC)?.strBlockProfileID = dictidata["profile_id"] as? String ?? "" != "" ? Int(dictidata["profile_id"] as? String ?? "")! : dictidata["profile_id"] as? Int ?? 0
                (naviobj as? QKProfileVC)?.str_uniquecode = dictidata["unique_code"] as? String ?? ""
            }
            else {
                if dictidata["role"] as? Int ?? 0 == 2 || dictidata["role"] as? String ?? "0" == "2"  {
                    (naviobj as? QKProfileVC)?.str_Screen_From = "View/EditProfile"
                }
                (naviobj as? QKProfileVC)?.int_userID = dictidata["profile_id"] as? String ?? "" != "" ? Int(dictidata["profile_id"] as? String ?? "")! : dictidata["profile_id"] as? Int ?? 0
                app_Delegate.strProfileScreenFrom = "ViewProfile"
            }
            
        default:
            break
        }
        
        
        if naviobj != nil
        {
    if var topController = UIApplication.shared.keyWindow?.rootViewController {
        while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                    
                    topController.dismiss(animated: true, completion: nil)
                    
                }
            
        switch self.window?.rootViewController {
            
        case is UINavigationController:
            
            if (self.window?.rootViewController as? UINavigationController)?.visibleViewController is QKProListVC && naviobj is QKProListVC{
                
              ((self.window?.rootViewController as? UINavigationController)?.visibleViewController as? QKProListVC)?.callAPIForGetUserProfileListing()
            }
            else
            {
            (self.window?.rootViewController as? UINavigationController)?.pushViewController(naviobj!, animated: true)
            }
        case is UITabBarController:
            
            if (self.window?.rootViewController as? UITabBarController)?.selectedViewController?.navigationController?.visibleViewController is QKProListVC && naviobj is QKProListVC{
                
                ((self.window?.rootViewController as? UITabBarController)?.selectedViewController?.navigationController?.visibleViewController as? QKProListVC)?.callAPIForGetUserProfileListing()
            }
            else{
            (self.window?.rootViewController as? UITabBarController)?.selectedViewController?.navigationController?.pushViewController(naviobj!, animated: true)
            }
        case is UIViewController:
            
            if (self.window?.rootViewController as? UIViewController)?.navigationController?.visibleViewController is QKProListVC && naviobj is QKProListVC{
                
                ((self.window?.rootViewController as? UIViewController)?.navigationController?.visibleViewController as? QKProListVC)?.callAPIForGetUserProfileListing()
            }
            else{
            (self.window?.rootViewController as? UIViewController)?.navigationController?.pushViewController(naviobj!, animated: true)
            }
        case is QKMainBaseVC:
            (self.window?.rootViewController as? QKMainBaseVC)?.navigationController?.pushViewController(naviobj!, animated: true)
        default:
            break
//            if var topController = UIApplication.shared.keyWindow?.rootViewController {
//                while let presentedViewController = topController.presentedViewController {
//                    topController = presentedViewController
//
//                    topController.dismiss(animated: true, completion: {
//
//
//                    })
//                    self.window?.rootViewController?.navigationController?.pushViewController(naviobj!, animated: true)
//                }
            
            }
        }
        }
//
        }
    }
    
    
    
    func app_CountryPickpopup(callback:@escaping ((Country?,Bool)->Void)) {
        if let parent = self.window?.rootViewController {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let objcountry = storyboard.instantiateViewController(withIdentifier: "CountryPickerVC") as? CountryPickerVC
            parent.addChildViewController(objcountry!)
            parent.view.addSubview((objcountry?.view)!)
            objcountry?.didMove(toParentViewController:  parent)
            objcountry?.completionPickCountry = callback
        }
    }

    
    //===============================================================================================//
    //===========API Call For Session Analytics===========================================================//
    // MARK : - Session Analytics API CALL
    func callAPIforSessionAnalytics()
    {
        let dict_param = NSMutableDictionary()
        dict_param["starttime"] = app_Delegate.strStartSessionTIME
        dict_param["endtime"] = app_Delegate.strEndSessionTIME
        KVClass.KVServiece(methodType: "POST", processView: nil, baseView: nil, processLabel: "", params: dict_param, api: "analytics/user_activity", arrFlag: false) { (JSON, status) in
        print(JSON)
        }
    }
    //===============================================================================================//
    //===============================================================================================//
    //===============================================================================================//
    //===============================================================================================//
}

