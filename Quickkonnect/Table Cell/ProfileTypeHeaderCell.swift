//
//  ProfileTypeHeaderCell.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 26/02/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit

class ProfileTypeHeaderCell: UITableViewCell {

    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBg.layer.cornerRadius = 12
        //viewBg.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
