//
//  SettingProfileTableCell.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 28/03/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit

class SettingProfileTableCell: UITableViewCell {

    @IBOutlet var lbl_SubTitle: UILabel!
    @IBOutlet var lbl_Options_Title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
