//
//  QKAllUserView.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 29/03/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import Alamofire

class QKUserListDATA: NSObject {
    
    var id = 0
    var is_added = 0
    
    var f_name = ""
    var l_name = ""
    var email = ""
    var profile_pic = ""
    
    
    private let dict: NSDictionary = [:]
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        
        self.id = dict["user_id"] as? Int ?? 0
        self.is_added = dict["is_added"] as? Int ?? 0
        self.f_name = dict["firstname"] as? String ?? ""
        self.l_name = dict["lastname"] as? String ?? ""
        self.email = dict["email"] as? String ?? ""
        self.profile_pic = dict["profile_pic"] as? String ?? ""
    }
}


class QKAllUserView: UIView, UITextFieldDelegate {
    
    @IBOutlet var viewBG: UIView!
    @IBOutlet var viewBg_Main: UIView!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtEmail_Main: kvTextField!
    @IBOutlet var lblNoResult: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblTextTitle: UILabel!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnCancel_Main: UIButton!
    @IBOutlet var btnConfirm: UIButton!
    @IBOutlet var btnSearchUser: UIButton!
    @IBOutlet var imgAddEmpInfo: UIImageView!
    @IBOutlet var txtSearchBar: UISearchBar!
    @IBOutlet var tbl_ViewForUser: UITableView!
    @IBOutlet var constraint_centr_vertical: NSLayoutConstraint!
    var strEmail = ""
    var Profile_ID = 0
    var strViewFrom = ""
    var strTitle = ""
    var strProfileType = ""
    var getFromView = UIViewController()
    var dic_QKUserDetailData: QKUserListDATA! = QKUserListDATA()
    var arrSelectedData:[Int] = []
    var baseViewController = UIViewController()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.frame = UIScreen.main.bounds
        lblTitle.text = strTitle
        
        txtEmail.delegate = self
        txtEmail_Main.delegate = self
        
        viewBg_Main.isHidden = true
        btnCancel_Main.isHidden = true
        viewBG.layer.cornerRadius = 8
        viewBg_Main.layer.cornerRadius = 8
        
        txtEmail.layer.cornerRadius = 8
        txtEmail.layer.borderWidth = 0.8
        txtEmail.layer.borderColor = UIColor.lightGray.cgColor
        
        //==========================================================================================================//
        
        self.backgroundColor = .clear
        self.viewBG.transform = CGAffineTransform.init(scaleX: 0.000, y: 0.000)
        
        
        //============Keyboard Open and close==========================================================================//
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        //=============================================================================================================//
        
    }
    
    
    func keyboardWillShow(notification:NSNotification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        self.constraint_centr_vertical.constant = -keyboardHeight
        
        UIView.animate(withDuration: 0.1) {
            self.layoutIfNeeded()
        }
    }
    
    func keyboardWillHide(notification:NSNotification) {
        self.constraint_centr_vertical.constant = 0
        UIView.animate(withDuration: 0.1) {
            self.layoutIfNeeded()
        }
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
    }
    
    
    // MARK :- UITextField Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if app_Delegate.strOpenView == "Main" {
            self.getSearchPressResult(strText: txtEmail_Main.text!)
        }
        else {
            self.getSearchPressResult(strText: txtEmail.text!)
        }
        textField.resignFirstResponder()
        return true
    }
    
    
    
    
    // MARK :- ButtonAction
    @IBAction func cancelPress(_ sender: UIBarButtonItem) {
        if app_Delegate.strOpenView == "Main" {
            app_Delegate.strOpenView = "Basic"
            self.viewBg_Main.transform = CGAffineTransform.identity
            UIView.animate(withDuration: 0.3, animations: {
                self.viewBg_Main.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
                self.backgroundColor = UIColor.black.withAlphaComponent(0)
            }) { (sucess) in
                self.removeFromSuperview()
                self.getQuickKonnectUserView(to: self.getFromView)
                self.txtEmail.text = self.strEmail
            }
        }
        else {
            self.viewBG.transform = CGAffineTransform.identity
            UIView.animate(withDuration: 0.3, animations: {
                if app_Delegate.strOpenView == "Main" {
                    self.viewBg_Main.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
                }
                else {
                    self.viewBG.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
                }
                self.backgroundColor = UIColor.black.withAlphaComponent(0)
                
            }) { (sucess) in
                self.removeFromSuperview()
            }
        }
    }
    
    
    
    
    
    @IBAction func approvePress(_ sender: UIBarButtonItem) {
        
        if self.btnConfirm.titleLabel?.text == "Confirm" {
            if KVAppDataServieces.shared.LoginUserID() == self.dic_QKUserDetailData.id {
                KVClass.ShowNotification("", "This Profile is already yours")
                return
            }
            else {
                self.ConfirmProfileTransfer()
            }
        }
        else {
            DispatchQueue.main.async {
                KVClass.ShowNotification("", "Coming Soon !")
//                let strShareLink = "https://itunes.apple.com/us/app/quickkonnect/id1331432824?ls=1&mt=8"
//                let shareItems: [Any] = [strShareLink]
//                let shareVC = UIActivityViewController.init(activityItems: [shareItems], applicationActivities: nil)
//                app_Delegate.window?.rootViewController?.present(shareVC, animated: true, completion: nil)
            }
            self.viewBg_Main.transform = CGAffineTransform.identity
            UIView.animate(withDuration: 0.3, animations: {
                self.viewBg_Main.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
                self.backgroundColor = UIColor.black.withAlphaComponent(0)
            }) { (sucess) in
                self.removeFromSuperview()
            }
        }
    }
    
    func ConfirmProfileTransfer() {
        
        DispatchQueue.main.async {
            self.txtSearchBar.endEditing(true)
            self.arrSelectedData.removeAll()
            self.arrSelectedData.append(self.dic_QKUserDetailData.id)
            
            let FullName = self.dic_QKUserDetailData.f_name + " " + self.dic_QKUserDetailData.l_name
            let strMsg = "you want to transfer this profile to " + FullName.capitalized + " ?"
                
            let Alert = UIAlertController.init(title: "Are you sure?", message: strMsg, preferredStyle: .alert)
                
            let attributedTitle = NSMutableAttributedString(string: "Warning !", attributes:
                    [NSFontAttributeName: UIFont.init(name: "lato-Medium", size: 15)!])
                
            let attributedMessage = NSMutableAttributedString(string: strMsg, attributes: [NSFontAttributeName: UIFont.init(name: "lato-Regular", size: 12)!])
            Alert.setValue(attributedTitle, forKey: "attributedTitle")
            Alert.setValue(attributedMessage, forKey: "attributedMessage")
                
            let ok = UIAlertAction.init(title: "Transfer", style: .destructive, handler: { (action) in
                
                //Transfer Profile
                let dict_param = NSMutableDictionary()
                dict_param["profile_id"] = self.Profile_ID
                dict_param["user_id"] = self.dic_QKUserDetailData.id
                KVClass.KVServiece(methodType: "POST", processView: self.baseViewController, baseView: self.baseViewController, processLabel: "", params: dict_param, api: "profile/transfer_request", arrFlag: false) { (JSON, status) in
                    if status == 1
                    {
                        self.viewBg_Main.transform = CGAffineTransform.identity
                        UIView.animate(withDuration: 0.3, animations: {
                            self.viewBg_Main.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
                            self.backgroundColor = UIColor.black.withAlphaComponent(0)
                        }) { (sucess) in
                            self.removeFromSuperview()
                        }
                        NotificationCenter.default.post(name: Notification.Name("TRANSFERPROFILESUCCESS"), object: nil)
                    }
                }
            })
                
            let no = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                Alert.dismiss(animated: true, completion: nil)
            })
            Alert.addAction(ok)
            Alert.addAction(no)
            self.baseViewController.present(Alert, animated: true, completion: nil)
        }
    }

    
    
    @IBAction func btnSearchPressAction(_ sender: UIBarButtonItem) {
        if app_Delegate.strOpenView == "Main" {
            self.getSearchPressResult(strText: txtEmail_Main.text!)
        }
        else {
            self.getSearchPressResult(strText: txtEmail.text!)
        }
    }
    
    func getSearchPressResult(strText: String) {
        strEmail = strText
        if strText == "" {
            return
        }
        if !(strText.isValidEmail()) {
            self.setViewWhenNoQKUSERFound(strTest: "Please Enter Valid Email", isCancelShow: false)
            //KVClass.ShowNotification("", "Please Enter Valid Email")
            return
        }
        
        //MARk : API Call For Check Memer
        let dict_param = NSMutableDictionary()
        
        if self.strViewFrom == "CreateEmployee" {
            dict_param["profile_id"] = Profile_ID
        }
        dict_param["email"] = strText
        dict_param["user_id"] = KVAppDataServieces.shared.LoginUserID()
        
        KVClass.KVServiece(methodType: "POST", processView: self.baseViewController, baseView: self.baseViewController, processLabel: "", params: dict_param, api: "member/check_member", arrFlag: false) { (JSON, status) in
            if status == 1 {
                
                if let dataDict = JSON["data"] as? NSDictionary
                {
                    let dict_BasicDetail = dataDict["basic_detail"] as? NSDictionary ?? [:]
                    print(JSON)
                    self.dic_QKUserDetailData = QKUserListDATA.init(dict_BasicDetail)
                    
                    if self.strViewFrom == "CreateEmployee" {
                        
                        self.dic_QKUserDetailData.is_added = JSON["is_added"] as? Int ?? 0
                        
                        if KVAppDataServieces.shared.LoginUserID() == self.dic_QKUserDetailData.id {
                            self.setViewWhenNoQKUSERFound(strTest: "You are an owner of this Profile", isCancelShow: false)
                            //KVClass.ShowNotification("", "You are an owner of this Profile")
                            return
                        }
                        let strUsenANme = self.dic_QKUserDetailData.f_name + " " + self.dic_QKUserDetailData.l_name
                        if self.dic_QKUserDetailData.is_added == 1 {
                            self.setViewWhenNoQKUSERFound(strTest: strUsenANme + " is already added in your profile !", isCancelShow: false)
                            //KVClass.ShowNotification("", strUsenANme + " is already added in your profile !")
                            return
                        }
                        
                        if app_Delegate.strOpenView == "Basic" {
                            self.viewBG.transform = CGAffineTransform.identity
                            UIView.animate(withDuration: 0.3, animations: {
                                self.viewBG.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
                                self.backgroundColor = UIColor.black.withAlphaComponent(0)
                            }) { (sucess) in
                                self.removeFromSuperview()
                                
                                let objAddEmp = self.getFromView.storyboard?.instantiateViewController(withIdentifier: "AddMemberInProfileVC") as! AddMemberInProfileVC
                                objAddEmp.ProfileID = self.Profile_ID
                                objAddEmp.P_Type = self.strProfileType
                                objAddEmp.dic_UserDetail = self.dic_QKUserDetailData
                                self.getFromView.navigationController?.pushViewController(objAddEmp, animated: true)
                            }
                        }
                        else {
                            self.viewBg_Main.transform = CGAffineTransform.identity
                            UIView.animate(withDuration: 0.3, animations: {
                                self.viewBg_Main.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
                                self.backgroundColor = UIColor.black.withAlphaComponent(0)
                            }) { (sucess) in
                                self.removeFromSuperview()
                                let objAddEmp = self.getFromView.storyboard?.instantiateViewController(withIdentifier: "AddMemberInProfileVC") as! AddMemberInProfileVC
                                objAddEmp.ProfileID = self.Profile_ID
                                objAddEmp.P_Type = self.strProfileType
                                objAddEmp.dic_UserDetail = self.dic_QKUserDetailData
                                self.getFromView.navigationController?.pushViewController(objAddEmp, animated: true)
                            }
                        }
                    }
                    else {
                        self.DataInsertInCurrentMainPopUP()
                        self.setViewWhen_QKUSERFound()
                    }
                }
                
            }
            else
            {
                self.txtEmail_Main.text = ""
                self.setViewWhenNoQKUSERFound(strTest: JSON["msg"] as? String ?? "This Email is No Registered with QuickKonnect", isCancelShow: true)
            }
        }
    }
    
    //.................................................................................................................//
    //.................................................................................................................//
    //.................................................................................................................//
    //For QK USER Found
    func setViewWhen_QKUSERFound()
    {
        if app_Delegate.strOpenView == "Basic" {
            app_Delegate.strOpenView = "Main"
            self.viewBG.transform = CGAffineTransform.identity
            UIView.animate(withDuration: 0.3, animations: {
                self.viewBG.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
                self.backgroundColor = UIColor.black.withAlphaComponent(0)
            }) { (sucess) in
                self.removeFromSuperview()
                self.viewBG.isHidden = false
                self.baseViewController = self.getFromView
                self.getFromView.view.addSubview(self)
                self.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.viewBg_Main.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
                UIView.animate(withDuration: 0.3, animations: {
                    self.viewBg_Main.isHidden = false
                    self.imgUser.isHidden = false
                    self.lblUserName.isHidden = false
                    self.lblNoResult.isHidden = true
                    self.btnConfirm.setTitle("Confirm", for: .normal)
                    self.viewBg_Main.transform = CGAffineTransform.identity
                    self.backgroundColor = UIColor.black.withAlphaComponent(0.65)
                }) { (sucess) in
                    self.getFromView.view.bringSubview(toFront: self)
                }
            }
        }
        else {
            self.imgUser.isHidden = false
            self.lblUserName.isHidden = false
            self.lblNoResult.isHidden = true
            self.btnConfirm.setTitle("Confirm", for: .normal)
        }
    }
    
    //.................................................................................................................//
    //For No QK USER Found
    func setViewWhenNoQKUSERFound(strTest: String, isCancelShow: Bool)
    {
        if app_Delegate.strOpenView == "Basic" {
            app_Delegate.strOpenView = "Main"
            
            self.viewBG.transform = CGAffineTransform.identity
            UIView.animate(withDuration: 0.3, animations: {
                self.viewBG.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
                self.backgroundColor = UIColor.black.withAlphaComponent(0)
                
            }) { (sucess) in
                self.removeFromSuperview()
                self.viewBG.isHidden = false
                self.baseViewController = self.getFromView
                self.getFromView.view.addSubview(self)
                self.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.viewBg_Main.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
                UIView.animate(withDuration: 0.3, animations: {
                    self.viewBg_Main.isHidden = false
                    self.imgUser.isHidden = true
                    self.lblUserName.isHidden = true
                    self.lblNoResult.isHidden = false
                    self.lblNoResult.text = strTest
                    self.btnCancel_Main.isHidden = isCancelShow
                    self.btnConfirm.setTitle("Invite", for: .normal)
                    self.viewBg_Main.transform = CGAffineTransform.identity
                    self.backgroundColor = UIColor.black.withAlphaComponent(0.65)
                }) { (sucess) in
                    self.getFromView.view.bringSubview(toFront: self)
                }
            }
        }
        else {
            self.imgUser.isHidden = true
            self.lblUserName.isHidden = true
            self.lblNoResult.isHidden = false
            self.btnConfirm.setTitle("Invite", for: .normal)
        }
    }
    //.................................................................................................................//
    //.................................................................................................................//
    //.................................................................................................................//
    
    
    // MARK :- AddViewAction
    
    func getQuickKonnectUserView(to view: UIViewController?){
        getFromView = view!
        self.baseViewController = view!
        view?.view.addSubview(self)
        self.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.viewBG.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
        UIView.animate(withDuration: 0.3, animations: {
            self.viewBg_Main.isHidden = true
            self.viewBG.transform = CGAffineTransform.identity
            self.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        }) { (sucess) in
            view?.view.bringSubview(toFront: self)
            self.txtEmail.text = self.strEmail
        }
        lblTitle.text = strTitle
        if self.strTitle == "Transfer Profile" {
            lblTextTitle.text = "Enter the email of user, you would like to transfer profile."
        }
        else {
            lblTextTitle.text = "Enter the email of user, you would like to add as an employee."
        }
        
    }
    
    
    func DataInsertInCurrentMainPopUP() {
        
        imgUser.image = #imageLiteral(resourceName: "user_profile_pic")
        let strImgURL = self.dic_QKUserDetailData.profile_pic
        if strImgURL != "" {
            imgUser.setImageWith(URLRequest.init(url: URL.init(string: strImgURL)!), placeholderImage: nil, success: { (request, response, image) in
                self.imgUser.image = image
            }, failure: { (request, response, error) in
                self.imgUser.image = #imageLiteral(resourceName: "user_profile_pic")
            })
        }
        txtEmail_Main.text = ""
        lblUserName.text = self.dic_QKUserDetailData.f_name.capitalized + " " + self.dic_QKUserDetailData.l_name.capitalized
    }
    
    
    
    
    
    // Header URl
    public func getPrefixURL() -> String {
        return KEYSPROJECT.BaseUrl
    }
    
    public func getHeader() -> [String:String] {
        return KEYSPROJECT.headers
    }
    
    
}

//=============================================================================================================//
//=============================================================================================================//
//=============================================================================================================//
//=============================================================================================================//
//=============================================================================================================//
//=============================================================================================================//

