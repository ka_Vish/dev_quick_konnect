//
//  FollowersTableCell.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 28/03/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit

class FollowersTableCell: UITableViewCell {

    @IBOutlet var imgUser_Image: UIImageView!
    @IBOutlet var lbl_UserName: UILabel!
    @IBOutlet var lbl_ResultNotFound: UILabel!
    @IBOutlet var lbl_Underline: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lbl_ResultNotFound.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
