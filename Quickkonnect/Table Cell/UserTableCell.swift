//
//  UserTableCell.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 31/03/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import DLRadioButton

class UserTableCell: UITableViewCell {

    @IBOutlet var img_User_image: UIImageView!
    @IBOutlet var lbl_User_name: UILabel!
    @IBOutlet var btnRadioSelected: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.btnRadioSelected.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
