//
//  ManageRoleTableCell.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 17/04/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit

class ManageRoleTableCell: UITableViewCell {

    @IBOutlet var viewOuterBG: UIView!
    @IBOutlet var viewInnerBG: UIView!
    @IBOutlet var btnSelectUnselect: UIButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //viewOuterBG.layer.cornerRadius = 8
        //viewOuterBG.backgroundColor = KVClass.themeColor().withAlphaComponent(0.2)
        
        //viewInnerBG.layer.cornerRadius = 8
        viewInnerBG.clipsToBounds = true
        viewInnerBG.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
