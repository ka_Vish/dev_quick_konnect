//
//  EmployeesTableCell.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 09/04/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit

class EmployeesTableCell: UITableViewCell {

    @IBOutlet var imgUser_Image: UIImageView!
    @IBOutlet var lbl_UserName: UILabel!
    @IBOutlet var lbl_ResultNotFound: UILabel!
    @IBOutlet var lbl_Underline: UILabel!
    @IBOutlet var btnOptions: UIButton!
    @IBOutlet var imgApproveReject: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnOptions.setImage(#imageLiteral(resourceName: "more").tint(with: UIColor.darkGray), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
