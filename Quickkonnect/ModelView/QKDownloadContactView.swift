//
//  QKDownloadContactView.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 09/03/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import Alamofire


class QKDownloadContactView: UIView {

    @IBOutlet var tab_Contact: UITableView!
    var baseViewController = UIViewController()
    var arrData:[[String:Any]] = []
    var dictSelectedData: [String:Any] = [:]
    var arrSelectedData:[String] = []
    @IBOutlet var btn_approve: UIBarButtonItem!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.frame = UIScreen.main.bounds
        
        self.tab_Contact.register(UINib.init(nibName: "QKDownloadContactCell", bundle: nil), forCellReuseIdentifier: "QKDownloadContactCell")
        
        //Check Enable Disable
        self.isApproveEnabled()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
    }
    
    // Mark :- View Height
    func updateHeight() -> CGRect {
        var heightScale: CGFloat = 0
        
        if (Device.IS_IPHONE_X){
            
            heightScale = 44 + 110
        }
        else
        {
           heightScale = 20 + 110
        }
        
        let height: CGFloat = (CGFloat(self.arrData.count * 100)) + heightScale <= UIScreen.main.bounds.height * 0.85 ? (CGFloat(self.arrData.count) * 100) + heightScale : UIScreen.main.bounds.height * 0.85
        
        return CGRect.init(x: 0, y: UIScreen.main.bounds.height - height + 16 , width: UIScreen.main.bounds.width, height: height)
    }
    
    // MARK :- ENABLED/DISABLED ACCEPT BUTTON
    func isApproveEnabled() {
        self.btn_approve.isEnabled = self.arrSelectedData.count > 0 ? true : false
    }
    
    
    // MARK :- ButtonAction
    @IBAction func cancelPress(_ sender: UIBarButtonItem) {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
            self.frame = CGRect.init(x: 0, y:  UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: self.updateHeight().height)
        }, completion: { (isfinished) in
            self.removeFromSuperview()
            app_Delegate.isDisplayBlur = false
            NotificationCenter.default.post(name: Notification.Name("DISPLAYBLURVIEW"), object: nil)
        })
    }
    
    
    @IBAction func approvePress(_ sender: UIBarButtonItem) {
        var SenderId = 0  //dictSelectedData["sender_id"] as? Int ?? 0
        var Noti_id = 0  //dictSelectedData["notification_id"] as? Int ?? 0
        
        if dictSelectedData["sender_id"] as? Int ?? 0 != 0
        {
            SenderId = dictSelectedData["sender_id"] as? Int ?? 0
        }
        else if dictSelectedData["sender_id"] as? String ?? "" != ""
        {
            SenderId = Int(dictSelectedData["sender_id"] as? String ?? "")!
        }
        
        if dictSelectedData["notification_id"] as? Int ?? 0 != 0
        {
            Noti_id = dictSelectedData["notification_id"] as? Int ?? 0
        }
        else if dictSelectedData["notification_id"] as? String ?? "" != ""
        {
            Noti_id = Int(dictSelectedData["notification_id"] as? String ?? "")!
        }
        
        
        if arrSelectedData.count == 0
        {
            KVClass.ShowNotification("", "Select Contacts First !")
            return
        }
        
        let senddict = NSMutableDictionary()
        senddict["sender_id"] = SenderId
        senddict["user_id"] = kUserDefults_("kLoginusrid") as? Int ?? 0
        senddict["notification_id"] = Noti_id
        let contactarr = NSMutableArray()
        for item in arrSelectedData
        {
            let contactdict = NSMutableDictionary()
            contactdict["contact_id"] = item
            contactarr.add(contactdict)
        }
        senddict["contact_approve"] = contactarr
        
        KVClass.KVServiece(methodType: "POST", processView: self.baseViewController, baseView: self.baseViewController, processLabel: "", params: senddict, api: "user_approved_contact", arrFlag: false, Hendler: { (JSON, status) in
            
            KVClass.ShowNotification("", JSON["msg"] as? String ?? "")
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
                self.frame = CGRect.init(x: 0, y:  UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: self.updateHeight().height)
            }, completion: { (isfinished) in
                self.removeFromSuperview()
                kUserDefults(true as AnyObject, key: "isnotificationrefresh")
                app_Delegate.isDisplayBlur = false
                NotificationCenter.default.post(name: Notification.Name("DISPLAYBLURVIEW"), object: nil)
                
            })
        })
    }
    
    
    // MARK :- AddViewAction
    
    func addContactView(to view: UIViewController?){
        
        KVClass.KVShowLoader()
        
        Alamofire.request(self.getPrefixURL() + "user_contact_list/\(KVAppDataServieces.shared.LoginUserID()))", headers: self.getHeader()).responseJSON { (response: DataResponse<Any>) in
            debugPrint(response)
            KVClass.KVHideLoader()
           // self.tblView.closeEndPullRefresh()
            if response.result.error == nil {
                guard let dictData = (response.result.value as! [String:Any])["data"] else {
                    print("Ops... no car found")
                    KVClass.ShowNotification("", "No contacts available!")
                    return
                }
                
                self.baseViewController = view!
                self.arrData = dictData as! [[String:Any]]
                view?.view.addSubview(self)
                self.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: self.updateHeight().height)
                UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
                    self.frame = self.updateHeight()
                    
                }, completion: { (isfinished) in
                    app_Delegate.isDisplayBlur = true
                    view?.view.bringSubview(toFront: self)
                    NotificationCenter.default.post(name: Notification.Name("DISPLAYBLURVIEW"), object: nil)
                })
                
                
                if self.arrData.count > 0 {
                  //  self.tblView.reloadData()
                    self.tab_Contact.reloadData()
                } else {
                    //  self.lblNoDataAvailable.isHidden = true
                }
            } else {
                KVClass.ShowNotification("", (response.result.error?.localizedDescription)!)
                
               // self.present(alertView, animated: true, completion: nil)
            }
        }
        
        
        
    }
    
    func btnSelectDeselectContactClicked(_ sender: UIButton) {
    
        if arrSelectedData.contains("\(sender.tag)") {
            arrSelectedData.remove(at: arrSelectedData.index(of: "\(sender.tag)")!)
            sender.setImage(UIImage.init(named: "radio_"), for: .normal)
        } else {
            arrSelectedData.append("\(sender.tag)")
            sender.setImage(UIImage.init(named: "radioselected"), for: .normal)
        }
        self.isApproveEnabled()
        self.tab_Contact.reloadData()
    }
    
    // Header URl
    
    public func getPrefixURL() -> String {
        return KEYSPROJECT.BaseUrl//"http://54.70.2.200/quickkonnect/" //"http://quickkonnect.com/app/api/"//
    }
    
    public func getHeader() -> [String:String] {
       // let headers:[String:String] = ["QK_ACCESS_KEY":"AAAAmGLql2s:APA91bF-fhfWlp4p_aRjK7UL8ZUPy4VlWWnbRExecXf9yZ1B0pYvT5Jg0r2XS6DOWT1Zb4KH-NfRlG3K_WSTM2RbkP3xD7d-pZF5zlSmb"]
        return KEYSPROJECT.headers
    }
    

}

extension QKDownloadContactView: UITableViewDataSource, UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.sizeToFit()
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if arrData.count == indexPath.row {
//            return 50.0
//        } else {
            return UITableViewAutomaticDimension//80.0
       // }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrData.count > 0 {
            return arrData.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QKDownloadContactCell") as! QKDownloadContactCell
        cell.selectionStyle = .none
        let dictData = arrData[indexPath.row]
        
        let strContactType = dictData["contact_type"] as? String ?? ""
        cell.lblContactTypeValue.text = "(" + strContactType + ")"
        cell.lblEmailTypeValue.text = dictData["email"] as? String ?? ""
        cell.lblPhoneTypeValue.text = dictData["phone"] as? String ?? ""

        cell.btnSelectDeselect.tag = dictData["contact_id"] as? Int ?? 0

        if arrSelectedData.contains("\(cell.btnSelectDeselect.tag)") {
            cell.btnSelectDeselect.setImage(UIImage.init(named: "radioselected"), for: .normal)
        } else {
            cell.btnSelectDeselect.setImage(UIImage.init(named: "radio_"), for: .normal)
        }

        cell.btnSelectDeselect.addTarget(self, action: #selector(self.btnSelectDeselectContactClicked(_:)), for: .touchUpInside)
        return cell
    }
    
    
    
    
}
