//
//  QKColorPickerCollectionCell.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 08/03/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit

class QKColorPickerCollectionCell: UICollectionViewCell {

    @IBOutlet var img_Tickmark: UIImageView!
    @IBOutlet var img_Backimg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
