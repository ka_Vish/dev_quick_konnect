//
//  QKColorView.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 09/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit

protocol PickColorDeleagte {
    
    func setSelectedColor(_ colorString: String)
}

protocol PickerSetQkTag {
    
    func setSelectedColorQK(_ colorString: String, _ tag: Int)
    
    func setQkTabBackImage(_ image: UIImage?, _ selectedImgIndex: String)
    
}

class QKColorView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet var lbl_title: UILabel!
    @IBOutlet var BaseView: UIView!
    @IBOutlet var CollectColorView: UICollectionView!
    var lastContentOffset: CGFloat = 0
    //var ColorPicker: ColorPickerView!
    var delegate: PickColorDeleagte?
    var datadelegate: PickerSetQkTag?
    var selectedColor = UIColor()
    var selectImage = ""
    var selectIndex = IndexPath()
    var scrollop = false
    var isImageMode = false
    /// Array of UIColor you want to show in the color picker
    var arr_light_colors: [UIColor] = [UIColor(hex : "FFFFFF"),
                                       UIColor(hex : "F1A9A0"),
                                       UIColor(hex : "ff91b8"),
                                       UIColor(hex : "E08283"),
                                       UIColor(hex : "E26A6A"),
                                       UIColor(hex : "947CB0"),
                                       UIColor(hex : "DCC6E0"),
                                       UIColor(hex : "AEA8D3"),
                                       UIColor(hex : "d882fc"),
                                       UIColor(hex : "BE90D4"),
                                       UIColor(hex : "7ec4f5"),
                                       UIColor(hex : "cf7ef1"),
                                       UIColor(hex : "E4F1FE"),
                                       UIColor(hex : "6ea9f5"),
                                       UIColor(hex : "59ABE3"),
                                       UIColor(hex : "81CFE0"),
                                       UIColor(hex : "52B3D9"),
                                       UIColor(hex : "C5EFF7"),
                                       UIColor(hex : "71f997"),
                                       UIColor(hex : "aacfaf"),
                                       UIColor(hex : "4ECDC4"),
                                       UIColor(hex : "A2DED0"),
                                       UIColor(hex : "87D37C"),
                                       UIColor(hex : "90C695"),
                                       UIColor(hex : "38fddd"),
                                       UIColor(hex : "83efc9"),
                                       UIColor(hex : "65C6BB"),
                                       UIColor(hex : "82f4bb"),
                                       UIColor(hex : "36D7B7"),
                                       UIColor(hex : "C8F7C5"),
                                       UIColor(hex : "86E2D5"),
                                       UIColor(hex : "2ECC71"),
                                       UIColor(hex : "E9D460"),
                                       UIColor(hex : "FDE3A7"),
                                       UIColor(hex : "f9c377"),
                                       UIColor(hex : "fcbc73"),
                                       UIColor(hex : "f9bd78"),
                                       UIColor(hex : "ffab8c"),
                                       UIColor(hex : "f9ab68"),
                                       UIColor(hex : "f9b64c"),
                                       UIColor(hex : "ECECEC"),
                                       UIColor(hex : "BDC3C7"),
                                       UIColor(hex : "ecf0f1"),
                                       UIColor(hex : "DADFE1"),
                                       UIColor(hex : "ABB7B7"),
                                       UIColor(hex : "F2F1EF"),
                                       UIColor(hex : "95A586"),
                                       UIColor(hex : "95BFBF"),
                                       UIColor(hex : "95A586"),
                                       UIColor(hex : "95BFBF")]
    
    var arr_DarkColors: [UIColor] = [UIColor(hex : "83132f"),
                                     UIColor(hex : "67307e"),
                                     UIColor(hex : "812a20"),
                                     UIColor(hex : "000000"),
                                     UIColor(hex : "8e2921"),
                                     UIColor(hex : "933c2c"),
                                     UIColor(hex : "682227"),
                                     UIColor(hex : "941307"),
                                     UIColor(hex : "911612"),
                                     UIColor(hex : "811e12"),
                                     UIColor(hex : "561b1a"),
                                     UIColor(hex : "7e261d"),
                                     UIColor(hex : "CF000F"),
                                     UIColor(hex : "b63c2f"),
                                     UIColor(hex : "8f063b"),
                                     UIColor(hex : "F64747"),
                                     UIColor(hex : "663399"),
                                     UIColor(hex : "674172"),
                                     UIColor(hex : "913D88"),
                                     UIColor(hex : "9A12B3"),
                                     UIColor(hex : "013243"),
                                     UIColor(hex : "446CB3"),
                                     UIColor(hex : "176e9e"),
                                     UIColor(hex : "08568b"),
                                     UIColor(hex : "2C3E50"),
                                     UIColor(hex : "022d41"),
                                     UIColor(hex : "26535c"),
                                     UIColor(hex : "22313F"),
                                     UIColor(hex : "145f86"),
                                     UIColor(hex : "3A539B"),
                                     UIColor(hex : "34495E"),
                                     UIColor(hex : "415164"),
                                     UIColor(hex : "2574A9"),
                                     UIColor(hex : "1F3A93"),
                                     UIColor(hex : "3c607e"),
                                     UIColor(hex : "26A65B"),
                                     UIColor(hex : "179b80"),
                                     UIColor(hex : "127671"),
                                     UIColor(hex : "127864"),
                                     UIColor(hex : "025844"),
                                     UIColor(hex : "0b684e"),
                                     UIColor(hex : "357855"),
                                     UIColor(hex : "1d8f76"),
                                     UIColor(hex : "017446"),
                                     UIColor(hex : "1E824C"),
                                     UIColor(hex : "02614b"),
                                     UIColor(hex : "05311f"),
                                     UIColor(hex : "5e4418"),
                                     UIColor(hex : "331c0a"),
                                     UIColor(hex : "331806")]
    
    
    
   var colors: [UIColor] = [UIColor]() //[#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), #colorLiteral(red: 0.9568627451, green: 0.262745098, blue: 0.2117647059, alpha: 1), #colorLiteral(red: 0.9254901961, green: 0.3921568627, blue: 0.2941176471, alpha: 1), #colorLiteral(red: 0.8235294118, green: 0.3019607843, blue: 0.3411764706, alpha: 1),#colorLiteral(red: 0.9490196078, green: 0.1490196078, blue: 0.07450980392, alpha: 1), #colorLiteral(red: 0.8509803922, green: 0.1176470588, blue: 0.09411764706, alpha: 1), #colorLiteral(red: 0.5882352941, green: 0.1568627451, blue: 0.1058823529, alpha: 1), #colorLiteral(red: 0.937254902, green: 0.2823529412, blue: 0.2117647059, alpha: 1),#colorLiteral(red: 0.8392156863, green: 0.2705882353, blue: 0.2549019608, alpha: 1), #colorLiteral(red: 0.7529411765, green: 0.2235294118, blue: 0.168627451, alpha: 1), #colorLiteral(red: 0.8117647059, green: 0, blue: 0.05882352941, alpha: 1), #colorLiteral(red: 0.9058823529, green: 0.2980392157, blue: 0.2352941176, alpha: 1),#colorLiteral(red: 0.8588235294, green: 0.03921568627, blue: 0.3568627451, alpha: 1), #colorLiteral(red: 0.9647058824, green: 0.2784313725, blue: 0.2784313725, alpha: 1), #colorLiteral(red: 0.9450980392, green: 0.662745098, blue: 0.6274509804, alpha: 1), #colorLiteral(red: 0.8235294118, green: 0.3215686275, blue: 0.4980392157, alpha: 1),#colorLiteral(red: 0.8784313725, green: 0.5098039216, blue: 0.5137254902, alpha: 1), #colorLiteral(red: 0.9647058824, green: 0.1411764706, blue: 0.3490196078, alpha: 1), #colorLiteral(red: 0.8862745098, green: 0.4156862745, blue: 0.4156862745, alpha: 1), #colorLiteral(red: 0.5803921569, green: 0.4862745098, blue: 0.6901960784, alpha: 1),#colorLiteral(red: 0.862745098, green: 0.7764705882, blue: 0.8784313725, alpha: 1), #colorLiteral(red: 0.4, green: 0.2, blue: 0.6, alpha: 1), #colorLiteral(red: 0.4039215686, green: 0.2549019608, blue: 0.4470588235, alpha: 1), #colorLiteral(red: 0.6823529412, green: 0.6588235294, blue: 0.8274509804, alpha: 1),#colorLiteral(red: 0.568627451, green: 0.2392156863, blue: 0.5333333333, alpha: 1), #colorLiteral(red: 0.6039215686, green: 0.07058823529, blue: 0.7019607843, alpha: 1), #colorLiteral(red: 0.7490196078, green: 0.3333333333, blue: 0.9254901961, alpha: 1), #colorLiteral(red: 0.7450980392, green: 0.5647058824, blue: 0.831372549, alpha: 1),#colorLiteral(red: 0.5568627451, green: 0.2666666667, blue: 0.6784313725, alpha: 1), #colorLiteral(red: 0.6078431373, green: 0.3490196078, blue: 0.7137254902, alpha: 1), #colorLiteral(red: 0.003921568627, green: 0.1960784314, blue: 0.262745098, alpha: 1), #colorLiteral(red: 0.2666666667, green: 0.4235294118, blue: 0.7019607843, alpha: 1),#colorLiteral(red: 0.8941176471, green: 0.9450980392, blue: 0.9960784314, alpha: 1), #colorLiteral(red: 0.2549019608, green: 0.5137254902, blue: 0.8431372549, alpha: 1), #colorLiteral(red: 0.3490196078, green: 0.6705882353, blue: 0.8901960784, alpha: 1), #colorLiteral(red: 0.5058823529, green: 0.8117647059, blue: 0.8784313725, alpha: 1),#colorLiteral(red: 0.3215686275, green: 0.7019607843, blue: 0.8509803922, alpha: 1), #colorLiteral(red: 0.7725490196, green: 0.937254902, blue: 0.968627451, alpha: 1), #colorLiteral(red: 0.1333333333, green: 0.6549019608, blue: 0.9411764706, alpha: 1), #colorLiteral(red: 0.2039215686, green: 0.5960784314, blue: 0.8588235294, alpha: 1),#colorLiteral(red: 0.1725490196, green: 0.2431372549, blue: 0.3137254902, alpha: 1), #colorLiteral(red: 0.09803921569, green: 0.7098039216, blue: 0.9960784314, alpha: 1), #colorLiteral(red: 0.2, green: 0.431372549, blue: 0.4823529412, alpha: 1), #colorLiteral(red: 0.1333333333, green: 0.1921568627, blue: 0.2470588235, alpha: 1),#colorLiteral(red: 0.4196078431, green: 0.7254901961, blue: 0.9411764706, alpha: 1), #colorLiteral(red: 0.1176470588, green: 0.5450980392, blue: 0.7647058824, alpha: 1), #colorLiteral(red: 0.2274509804, green: 0.3254901961, blue: 0.6078431373, alpha: 1), #colorLiteral(red: 0.2039215686, green: 0.2862745098, blue: 0.368627451, alpha: 1),#colorLiteral(red: 0.4039215686, green: 0.5019607843, blue: 0.6235294118, alpha: 1), #colorLiteral(red: 0.1450980392, green: 0.4549019608, blue: 0.662745098, alpha: 1), #colorLiteral(red: 0.1215686275, green: 0.2274509804, blue: 0.5764705882, alpha: 1), #colorLiteral(red: 0.537254902, green: 0.768627451, blue: 0.9568627451, alpha: 1),#colorLiteral(red: 0, green: 0.9019607843, blue: 0.2509803922, alpha: 1), #colorLiteral(red: 0.568627451, green: 0.7058823529, blue: 0.5882352941, alpha: 1), #colorLiteral(red: 0.3058823529, green: 0.8039215686, blue: 0.768627451, alpha: 1), #colorLiteral(red: 0.6352941176, green: 0.8705882353, blue: 0.8156862745, alpha: 1), #colorLiteral(red: 0.5294117647, green: 0.8274509804, blue: 0.4862745098, alpha: 1), #colorLiteral(red: 0.5647058824, green: 0.7764705882, blue: 0.5843137255, alpha: 1), #colorLiteral(red: 0.1490196078, green: 0.6509803922, blue: 0.3568627451, alpha: 1), #colorLiteral(red: 0.01176470588, green: 0.7882352941, blue: 0.662745098, alpha: 1), #colorLiteral(red: 0.4078431373, green: 0.7647058824, blue: 0.6392156863, alpha: 1), #colorLiteral(red: 0.3960784314, green: 0.7764705882, blue: 0.7333333333, alpha: 1), #colorLiteral(red: 0.1058823529, green: 0.737254902, blue: 0.6078431373, alpha: 1), #colorLiteral(red: 0.1058823529, green: 0.6392156863, blue: 0.6117647059, alpha: 1), #colorLiteral(red: 0.4, green: 0.8, blue: 0.6, alpha: 1), #colorLiteral(red: 0.2117647059, green: 0.8431372549, blue: 0.7176470588, alpha: 1), #colorLiteral(red: 0.7843137255, green: 0.968627451, blue: 0.7725490196, alpha: 1), #colorLiteral(red: 0.5254901961, green: 0.8862745098, blue: 0.8352941176, alpha: 1), #colorLiteral(red: 0.1803921569, green: 0.8, blue: 0.4431372549, alpha: 1), #colorLiteral(red: 0.0862745098, green: 0.6274509804, blue: 0.5215686275, alpha: 1), #colorLiteral(red: 0.003921568627, green: 0.5960784314, blue: 0.4588235294, alpha: 1), #colorLiteral(red: 0.01176470588, green: 0.6509803922, blue: 0.4705882353, alpha: 1) , #colorLiteral(red: 0.3019607843, green: 0.6862745098, blue: 0.4862745098, alpha: 1), #colorLiteral(red: 0.1647058824, green: 0.7333333333, blue: 0.6078431373, alpha: 1), #colorLiteral(red: 0.5843137255, green: 0.7490196078, blue: 0.7490196078, alpha: 1), #colorLiteral(red: 0.9490196078, green: 0.9450980392, blue: 0.937254902, alpha: 1), #colorLiteral(red: 0.6705882353, green: 0.7176470588, blue: 0.7176470588, alpha: 1), #colorLiteral(red: 0.8549019608, green: 0.8745098039, blue: 0.8823529412, alpha: 1), #colorLiteral(red: 0.5843137255, green: 0.6470588235, blue: 0.5254901961, alpha: 1), #colorLiteral(red: 0.9254901961, green: 0.9411764706, blue: 0.9450980392, alpha: 1),  #colorLiteral(red: 0.7411764706, green: 0.7647058824, blue: 0.7803921569, alpha: 1), #colorLiteral(red: 0.4235294118, green: 0.4784313725, blue: 0.537254902, alpha: 1), #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1), #colorLiteral(red: 0.9764705882, green: 0.7490196078, blue: 0.231372549, alpha: 1), #colorLiteral(red: 0.9764705882, green: 0.7490196078, blue: 0.231372549, alpha: 1), #colorLiteral(red: 0.9764705882, green: 0.4117647059, blue: 0.05490196078, alpha: 1),  #colorLiteral(red: 0.9529411765, green: 0.6117647059, blue: 0.07058823529, alpha: 1), #colorLiteral(red: 0.8274509804, green: 0.3294117647, blue: 0, alpha: 1), #colorLiteral(red: 0.9607843137, green: 0.6705882353, blue: 0.2078431373, alpha: 1), #colorLiteral(red: 0.9215686275, green: 0.5921568627, blue: 0.3058823529, alpha: 1), #colorLiteral(red: 0.9490196078, green: 0.4705882353, blue: 0.2941176471, alpha: 1), #colorLiteral(red: 0.9098039216, green: 0.4941176471, blue: 0.01568627451, alpha: 1), #colorLiteral(red: 0.9215686275, green: 0.5843137255, blue: 0.1960784314, alpha: 1), #colorLiteral(red: 0.9725490196, green: 0.5803921569, blue: 0.02352941176, alpha: 1), #colorLiteral(red: 0.9921568627, green: 0.8901960784, blue: 0.6549019608, alpha: 1), #colorLiteral(red: 0.9137254902, green: 0.831372549, blue: 0.3764705882, alpha: 1), #colorLiteral(red: 0.5960784314, green: 0.2588235294, blue: 0, alpha: 1), #colorLiteral(red: 0.9803921569, green: 0.7450980392, blue: 0.3450980392, alpha: 1), #colorLiteral(red: 0.1490196078, green: 0.7607843137, blue: 0.5058823529, alpha: 1), #colorLiteral(red: 0.01568627451, green: 0.5764705882, blue: 0.4470588235, alpha: 1), #colorLiteral(red: 0.1176470588, green: 0.5098039216, blue: 0.2980392157, alpha: 1), #colorLiteral(red: 0, green: 0.6941176471, blue: 0.4156862745, alpha: 1)]
    
    var arrQKBG_images: [UIImage] = [#imageLiteral(resourceName: "gradient-1"), #imageLiteral(resourceName: "gradient-2"), #imageLiteral(resourceName: "gradient-3"), #imageLiteral(resourceName: "gradient-4"), #imageLiteral(resourceName: "gradient-5"), #imageLiteral(resourceName: "gradient-6"), #imageLiteral(resourceName: "gradient-7"), #imageLiteral(resourceName: "gradient-8"), #imageLiteral(resourceName: "gradient-9"), #imageLiteral(resourceName: "gradient-10")]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.frame = UIScreen.main.bounds

        //Collection Cell Register=======================//
        self.CollectColorView.register(UINib.init(nibName: "QKColorPickerCollectionCell", bundle: nil), forCellWithReuseIdentifier: "QKColorPickerCollectionCell")
        self.lbl_title.text = self.isImageMode ? "Select Color Shade of Background" : "Select Color"
       // self.CollectColorView.reloadData()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.CollectColorView.reloadData()
    }
    
    
    //MARK: - UICollection Delegate Datasource Method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        self.colors = self.tag == 0 ? self.arr_light_colors : self.arr_DarkColors
        
        if self.isImageMode{
            return self.arrQKBG_images.count
        }
//        else if self.tag == 0 {
//            return self.arr_light_colors.count
//        }
        return self.colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        self.lbl_title.text = self.isImageMode ? "Select Color Shade of Background" : "Select Color"
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QKColorPickerCollectionCell", for: indexPath) as! QKColorPickerCollectionCell
        
        cell.img_Backimg.isHidden = !self.isImageMode
        if self.isImageMode
        {
            cell.img_Backimg.image = self.arrQKBG_images[indexPath.row]
            if String(indexPath.row + 1) == selectImage {
                cell.img_Tickmark.image = #imageLiteral(resourceName: "done_ic").tint(with: .white)
            }
            else {
                cell.img_Tickmark.image = nil
            }
            
            
        }
        else
        {
            
            cell.backgroundColor = self.colors[indexPath.row]
            cell.layer.cornerRadius = cell.frame.height / 2
            cell.layer.borderWidth = 0.7
            cell.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            cell.clipsToBounds = true
        //for strColor in 0..<self.colors.count {
//            if self.tag == 0 {
//            }
            
            if self.hexStringFromColor(self.colors[indexPath.row]) == self.hexStringFromColor(selectedColor) {
                selectIndex = indexPath
                let color = selectedColor.isWhiteText ? UIColor.white : UIColor.black
                cell.img_Tickmark.image = #imageLiteral(resourceName: "done_ic").tint(with: color)
               
            }
            else {
                cell.img_Tickmark.image = nil
            }
         }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.isImageMode{
            let size = CGSize.init(width: (self.BaseView.bounds.width - 40)/2, height: (self.BaseView.bounds.width - 40)/2)
            return size
        }
        let size = CGSize.init(width: (self.BaseView.bounds.width - 80)/4, height: (self.BaseView.bounds.width - 80)/4)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets.init(top: 10, left: 10, bottom: 10, right: 10)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       if !self.isImageMode
       {
        let currentCell = self.CollectColorView.cellForItem(at: indexPath) as! QKColorPickerCollectionCell
        let color = self.colors[indexPath.row].isWhiteText ? UIColor.white : UIColor.black
        currentCell.img_Tickmark.image = #imageLiteral(resourceName: "done_ic").tint(with: color)
        
        self.selectedColor = (self.colors[indexPath.row])
        collectionView.reloadData()
        
        UIView.transition(with: self, duration: 0.35, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
        
           }, completion: { (finished) in
        
            DispatchQueue.main.async {
                self.removeFromSuperview()
                print(self.hexStringFromColor(self.colors[indexPath.row]))
                self.delegate?.setSelectedColor(self.hexStringFromColor(self.colors[indexPath.row]))
                self.datadelegate?.setSelectedColorQK(self.hexStringFromColor(self.colors[indexPath.row]), self.tag)
            }
        })
        }
        else
       {
        UIView.transition(with: self, duration: 0.35, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
            
        }, completion: { (finished) in
            
            DispatchQueue.main.async {
                self.removeFromSuperview()
                self.datadelegate?.setQkTabBackImage(self.arrQKBG_images[indexPath.row], String(indexPath.row + 1))
            }
        })
        }
        
        
      // collectionView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
//        cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
//        UIView.animate(withDuration: 0.3, animations: {
//            cell.transform = CGAffineTransform.identity
//        }) { (sucess) in
//        }
        
        
        cell.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
        UIView.animate(withDuration: 0.3, animations: {
            
            cell.transform = CGAffineTransform.init(scaleX: 0.01, y: 0.1)
            UIView.animate(withDuration: 0.4, animations: {
                cell.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
            }, completion: { (success) in
                cell.transform = CGAffineTransform.identity
            })
            
            
        }) { (success) in
            
        }
        
    }
    
    
    // MARK :- SCrollView Method
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset < scrollView.contentOffset.y) {
            
            self.scrollop = false
            
            // moved to top
        } else if (self.lastContentOffset > scrollView.contentOffset.y) {
            self.scrollop = true
            // moved to bottom
        } else {
            self.scrollop = true
            // didn't move
        }
    }
    
    
    
//    func colorPickerView(_ colorPickerView: ColorPickerView, didSelectItemAt indexPath: IndexPath) {
//

//
//    }
//
//    func colorPickerView(_ colorPickerView: ColorPickerView, didDeselectItemAt indexPath: IndexPath) {
//
//
//    }
//
//    func colorPickerView(_ colorPickerView: ColorPickerView, insetForSectionAt section: Int) -> UIEdgeInsets {
//
//        return UIEdgeInsets.init(top: 10, left: 10, bottom: 10, right: 10)
//    }
//
//    func colorPickerView(_ colorPickerView: ColorPickerView, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        let size = CGSize.init(width: (self.BaseView.bounds.width-80)/4, height: (self.BaseView.bounds.width-80)/4)
//
//        return size
//    }
//    func colorPickerView(_ colorPickerView: ColorPickerView, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//
//        return 10
//    }
//    //
//    func colorPickerView(_ colorPickerView: ColorPickerView, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//
//        return 0
//    }
    
    @IBAction func CancelPress(_ sender: UIButton)
    {
        UIView.transition(with: self, duration: 0.2, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
            
        }, completion: { (finished) in
            
            DispatchQueue.main.async {
                self.removeFromSuperview()
                
            }
        })
        
    }
    
    func hexStringFromColor(_ color: UIColor) -> String
    {
        let comp = color.cgColor.components
        
        return String.init(format: "%02lX%02lX%02lX", lroundf(Float(comp![0]*255)),lroundf(Float(comp![1]*255)),lroundf(Float(comp![2]*255)))
    }
    
}

extension UIColor {
    
    var redValue: CGFloat{
        return cgColor.components! [0]
    }
    
    var greenValue: CGFloat{
        return cgColor.components! [1]
    }
    
    var blueValue: CGFloat{
        return cgColor.components! [2]
    }
    
    var alphaValue: CGFloat{
        return cgColor.components! [3]
    }
    
    var isWhiteText: Bool {
        
        // non-RGB color
        if cgColor.numberOfComponents == 2 {
            return 0.0...0.5 ~= cgColor.components!.first! ? true : false
        }
        
        let red = self.redValue * 255
        let green = self.greenValue * 255
        let blue = self.blueValue * 255
        
        let yiq = ((red * 299) + (green * 587) + (blue * 114)) / 1000
        return yiq < 192
    }
    
}
extension UIColor{
    
    func isEqualToColor(color: UIColor, withTolerance tolerance: CGFloat = 0.0) -> Bool{
        
        var r1 : CGFloat = 0
        var g1 : CGFloat = 0
        var b1 : CGFloat = 0
        var a1 : CGFloat = 0
        var r2 : CGFloat = 0
        var g2 : CGFloat = 0
        var b2 : CGFloat = 0
        var a2 : CGFloat = 0
        
        self.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        color.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)
        
        return fabs(r1 - r2) <= tolerance && fabs(g1 - g2) <= tolerance && fabs(b1 - b2) <= tolerance && fabs(a1 - a2) <= tolerance
    }
    
}
