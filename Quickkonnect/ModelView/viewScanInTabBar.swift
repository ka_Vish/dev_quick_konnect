//
//  viewScanInTabBar.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 15/03/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit

class viewScanInTabBar: UIView {
    @IBOutlet var viewMainBG: UIControl!
    @IBOutlet var btnScanClk: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.setShadowInCustomScanView()
    }

    func setShadowInCustomScanView() {
       // self.viewMainBG.layer.shadowPath = UIBezierPath.init(roundedRect: self.viewMainBG.bounds, cornerRadius: self.viewMainBG.frame.height/2).cgPath
//        self.viewMainBG.layer.shadowColor = UIColor.green.withAlphaComponent(0.5).cgColor
//        self.viewMainBG.layer.shadowOffset = CGSize.init(width: 1, height: 0)
//        self.viewMainBG.layer.shadowRadius = 1
//        self.viewMainBG.layer.shadowOpacity = 0.7
        self.viewMainBG.clipsToBounds = false
        
//        self.viewMainBG.layer.cornerRadius = 40
        // drop shadow
        self.viewMainBG.layer.shadowColor = #colorLiteral(red: 0.01960784314, green: 0.4392156863, blue: 0.5137254902, alpha: 1).cgColor
        self.viewMainBG.layer.shadowOpacity = 0.6
        self.viewMainBG.layer.shadowRadius = 6.0
        self.viewMainBG.layer.shadowOffset = CGSize(width: 5, height: 5)
        self.viewMainBG.layer.masksToBounds = false
        
//        self.viewMainBG.layer.borderWidth = 2
//        self.viewMainBG.layer.borderColor = UIColor.white.cgColor
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        //self.setShadowInCustomScanView()
    }
}
