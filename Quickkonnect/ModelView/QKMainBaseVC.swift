//
//  QKMainBaseVC.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 16/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit
import SDWebImage


struct Device {
    // iDevice detection code
    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA           = UIScreen.main.scale >= 2.0
    
    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
}


class QKMainBaseVC: UIViewController {
    
    var TopViewStatus = UIView()
    let Bar = UIView.fromNib("QKNaviBar") as! QKNaviBar
    var statusHeightDefault : CGFloat = 20
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = true
        
        var TopAreHeight = 20
        
        TopViewStatus.frame = CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: 20)
        if (Device.IS_IPHONE_X){
            //iPhone X
            TopAreHeight = 44
            TopViewStatus.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44)
        }
        
        TopViewStatus.backgroundColor = UIColor.init(hex: "28AFB0")
        self.view.addSubview(TopViewStatus)
        
        self.Bar.frame = CGRect.init(x: 0, y: TopAreHeight, width: Int(self.view.bounds.width), height: 60)
        
        //self.setTopStatus(y_position: CGFloat(TopAreHeight))
        
        
        self.Bar.Menu_btn.isHidden = true
        self.Bar.Noti_btn.isHidden = true
        self.Bar.backgroundColor = UIColor.white
        self.Bar.layer.masksToBounds = false
        self.Bar.layer.shadowColor = UIColor.lightGray.cgColor
        self.Bar.layer.shadowOpacity = 0.6
        self.Bar.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        self.Bar.layer.shadowRadius = 1
        self.Bar.Noti_btn.setImage(#imageLiteral(resourceName: "ic_bottom_notification_select").tint(with: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)), for: .normal)
        
        self.Bar.Profile_Img_BG.layer.borderWidth = 2
        self.Bar.Profile_Img_BG.layer.borderColor = UIColor.white.withAlphaComponent(0.16).cgColor
        
        
        self.view.addSubview(self.Bar)
        
        self.Bar.Menu_btn.addTarget(self, action: #selector(btnSideMenuClicked(_:)), for: .touchUpInside)
        self.Bar.Noti_btn.addTarget(self, action: #selector(btnNotipress(_:)), for: .touchUpInside)
        self.Bar.btn_Chat.addTarget(self, action: #selector(btnChatPress(_:)), for: .touchUpInside)
        self.Bar.Profile_Img.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(btnProfilepress(_:))))
        self.Bar.Back_btn.addTarget(self, action: #selector(backPress(_:)), for: .touchUpInside)
        
        self.Bar.Profile_Img.layer.cornerRadius = self.Bar.Profile_Img.frame.height / 2
        self.Bar.Profile_Img.clipsToBounds = true
        
        let getTrillingForImg_Profile = UIScreen.main.bounds.width - self.Bar.Profile_Img.frame.size.width - 12
        self.Bar.constraint_img_profile_trilling.constant = getTrillingForImg_Profile
        
        
        
        
        
        //For left Side gesture
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.respondToSwipeLeftGesture))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
        
        
        //NotificationCenter.default.addObserver(self, selector: #selector(statusBarHeightChanged), name: NSNotification.Name.UIApplicationWillChangeStatusBarFrame, object: nil)
//        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeLeftGesture))
//        swipeLeft.direction = UISwipeGestureRecognizerDirection.right
//        self.view.isUserInteractionEnabled = true
//        self.view.addGestureRecognizer(swipeLeft)
//        
//        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeLeftGesture))
//        swipeRight.direction = UISwipeGestureRecognizerDirection.left
//        self.view.isUserInteractionEnabled = true
//        self.view.addGestureRecognizer(swipeRight)
        
        
       
        
        //        if kUserDefults_("session") != nil
        //        {
        //       if let userDict = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "session") as! Data) as? NSDictionary
        //       {
        //        if userDict["profile_pic"] as? String ?? "" != ""
        //        {
        //            self.Bar.Profile_Img.sd_setImage(with: URL.init(string: userDict["profile_pic"] as? String ?? "")!, placeholderImage: UIImage.init(named: "user_profile_pic"), options: SDWebImageOptions.refreshCached)
        //        }
        //       }
        //        else
        //        {
        //            self.Bar.Profile_Img.image = UIImage.init(named: "user_profile_pic")
        //        }
        //        }
        
        self.view.layoutIfNeeded()
        
    }
    
//    func setTopStatus(y_position : CGFloat) {
//        //if statusHeightDefault == 40 {
//        self.Bar.frame = CGRect.init(x: 0, y: Int(y_position), width: Int(self.view.bounds.width), height: 60)
//        //}
//    }
    
    
//    func statusBarHeightChanged() {
//        statusHeightDefault = UIApplication.shared.statusBarFrame.size.height
//        print(statusHeightDefault)
//        if statusHeightDefault == 40 {
//            self.Bar.frame = CGRect.init(x: 0, y: 40, width: self.view.bounds.width, height: 60.0)
//        }
//        else {
//            self.Bar.frame = CGRect.init(x: 0, y: 20, width: self.view.bounds.width, height: 60.0)
//        }
//
//    }
    
   
    
    
    
    
    
    
    
    
    
    
    //For Swipe left
    @objc func respondToSwipeLeftGesture(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            print("Screen edge swiped!")
            
            kUserDefults(false as AnyObject, key: "ishomeupdate")
//            if let menuItemViewController = self.tabBarController as? SideMenuItemContent {
//                menuItemViewController.showSideMenu()
//            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !KVAppDataServieces.shared.isBool(.profile_pic, "")
        {
            self.Bar.Profile_Img.image = UIImage.init(named: "user_profile_pic")
            SDWebImageManager.shared().loadImage(with: URL.init(string: KVAppDataServieces.shared.UserValue(.profile_pic) as! String)!, options: SDWebImageOptions.refreshCached, progress: { (pro1, pro2, url) in
                
            }, completed: { (img, data, err, type, finish, url) in
                
                if err == nil
                {
                    self.Bar.Profile_Img.image = (img ?? UIImage.init(named: "user_profile_pic"))
                    
                    //                    let imdata = (UIImageJPEGRepresentation(img!, 0.5)?.base64EncodedString())!
                    //
                    //                    KVAppDataServieces.shared.update(.contact_image, imdata)
                    
                }
            })
        }
        if let picURL = KVAppDataServieces.shared.UserValue(.profile_pic) as? String, picURL != "" && !KVAppDataServieces.shared.isBool(.profile_data, "")
        {
            if let datadict = (KVAppDataServieces.shared.UserValue(.profile_data)) as? NSDictionary
            {
                if let picuri = (datadict["basic_detail"] as? NSDictionary ?? [:])["profile_pic"] as? String, picuri != ""
                {
                    if picURL != picuri
                    {
                        KVAppDataServieces.shared.update(.profile_pic, picuri)
                        
                        SDWebImageManager.shared().loadImage(with: URL.init(string: picuri)!, options: SDWebImageOptions.refreshCached, progress: { (pro1, pro2, url) in
                            
                        }, completed: { (img, data, err, type, finish, url) in
                            
                            if err == nil
                            {
                                self.Bar.Profile_Img.image = (img ?? UIImage.init(named: "user_profile_pic"))
                            }
                        })
                    }
                }
            }
        }
        else
        {
            self.Bar.Profile_Img.image = UIImage.init(named: "user_profile_pic")
        }
        
    }
    //
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        //        if let profilePicURL = Singleton.sharedInstance.data_details?.basicDetails?.profile_pic, profilePicURL.count > 0  {
        //            if profilePicURL.count > 0 {
        //                self.Bar.Profile_Img.sd_setImage(with: URL.init(string: profilePicURL)!, placeholderImage: UIImage.init(named: "user_profile_pic"), options: SDWebImageOptions.refreshCached)
        //            }
        //        }
        
        if !KVAppDataServieces.shared.isBool(.profile_pic, "")
        {
            self.Bar.Profile_Img.image = UIImage.init(named: "user_profile_pic")
            SDWebImageManager.shared().loadImage(with: URL.init(string: KVAppDataServieces.shared.UserValue(.profile_pic) as! String)!, options: SDWebImageOptions.refreshCached, progress: { (pro1, pro2, url) in
                
            }, completed: { (img, data, err, type, finish, url) in
                
                if err == nil
                {
                    self.Bar.Profile_Img.image = (img ?? UIImage.init(named: "user_profile_pic"))
                    
                    //                    let imdata = (UIImageJPEGRepresentation(img!, 0.5)?.base64EncodedString())!
                    //                    
                    //                    KVAppDataServieces.shared.update(.contact_image, imdata)
                    
                }
            })
        }
        if let picURL = KVAppDataServieces.shared.UserValue(.profile_pic) as? String, picURL != "" && !KVAppDataServieces.shared.isBool(.profile_data, "")
        {
            if let datadict = (KVAppDataServieces.shared.UserValue(.profile_data)) as? NSDictionary
            {
                if let picuri = (datadict["basic_detail"] as? NSDictionary ?? [:])["profile_pic"] as? String, picuri != ""
                {
                    if picURL != picuri
                    {
                    KVAppDataServieces.shared.update(.profile_pic, picuri)
                    SDWebImageManager.shared().loadImage(with: URL.init(string: picuri)!, options: SDWebImageOptions.refreshCached, progress: { (pro1, pro2, url) in
                        
                    }, completed: { (img, data, err, type, finish, url) in
                        
                        if err == nil
                        {
                            self.Bar.Profile_Img.image = (img ?? UIImage.init(named: "user_profile_pic"))
                        }
                    })
                    }
                }
            }
        }
        else
        {
            self.Bar.Profile_Img.image = UIImage.init(named: "user_profile_pic")
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.view.layoutIfNeeded()
        
        //self.Bar.removeFromSuperview()
        if (Device.IS_IPHONE_X) {
        }
        else {
            self.Bar.frame = CGRect(x: 0, y: 20, width: self.view.bounds.width, height: 60.0)
        }
        //self.view.addSubview(self.Bar)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

    //=============================================================================================//
    //=============================================================================================//
    //MARK : - Button Action Method
    @IBAction func backPress(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSideMenuClicked(_ sender: UIButton) {
        kUserDefults(false as AnyObject, key: "ishomeupdate")
//        if let menuItemViewController = self.tabBarController as? SideMenuItemContent {
//            menuItemViewController.showSideMenu()
//        }
    }
    
    
    @IBAction func btnNotipress(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        vc.strScreenFrom = "Push"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnChatPress(_ sender: UIButton) {
        let vcChat = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.navigationController?.pushViewController(vcChat, animated: true)
    }
    
    
    @IBAction func btnProfilepress(_ sender: UITapGestureRecognizer) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        obj.isedit = false
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
}


