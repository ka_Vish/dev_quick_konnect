//
//  QKDownloadContactCell.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 12/03/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit

class QKDownloadContactCell: UITableViewCell {

    @IBOutlet weak var btnSelectDeselect: UIButton!
    @IBOutlet weak var lblContactTypeValue: UILabel!
    @IBOutlet weak var lblEmailTypeValue: UILabel!
    @IBOutlet weak var lblPhoneTypeValue: UILabel!
    @IBOutlet weak var imgPhone: UIImageView!
    @IBOutlet weak var imgEmail: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgPhone.image =  #imageLiteral(resourceName: "ic_phonecall").tint(with: UIColor.init(hex: "28AFB0"))
        self.imgEmail.image =  #imageLiteral(resourceName: "ic_message").tint(with: UIColor.init(hex: "28AFB0"))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}



