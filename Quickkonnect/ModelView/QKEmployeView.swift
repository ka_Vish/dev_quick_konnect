//
//  QKEmployeView.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 07/03/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit

class QKEmployeView: UIView {

    @IBOutlet var tbl_employe: UITableView!
    @IBOutlet var bar_search: UISearchBar!
    
    var arrEmployeeData = NSMutableArray()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       self.tbl_employe.register(UINib.init(nibName: "QKEmployeSelectCell", bundle: nil), forCellReuseIdentifier: "QKEmployeSelectCell")
        
    }
    
   
}

extension QKEmployeView: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrEmployeeData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        let cell: QKEmployeSelectCell = tableView.dequeueReusableCell(withIdentifier: "QKEmployeSelectCell") as! QKEmployeSelectCell
        cell.lbl_name.text = "99799789789788978789898978978978989789789789789897898979878978"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

extension QKEmployeView: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
}

