//
//  QKNaviBar.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 16/01/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit

class QKNaviBar: UIView {
    
    @IBOutlet var Menu_btn: UIButton!
    @IBOutlet var Title_lbl: UILabel!
    @IBOutlet var Noti_btn: UIButton!
    @IBOutlet var Profile_Img: UIImageView!
    @IBOutlet var Profile_Img_BG: UIImageView!
    @IBOutlet var Back_btn: UIButton!
    @IBOutlet var btn_Chat: UIButton!
    @IBOutlet var btn_Filter: UIButton!
    @IBOutlet var img_TopBG: UIImageView!
    @IBOutlet var constraint_img_profile_trilling: NSLayoutConstraint!
    @IBOutlet var constraint_btn_chat_trilling: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.img_TopBG.isHidden = true
        self.btn_Filter.isHidden = true
        self.Back_btn.isHidden = true
        
        
    }
    
}
