//
//  viewToolTip.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 23/04/18.
//  Copyright © 2018 com.kavish. All rights reserved.
//

import UIKit

class viewToolTip: UIView {

    @IBOutlet var lblText: UILabel!
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var imgUpload: UIImageView!
    
    
    
    func getFullFreameForToolTip(to view: UIViewController?){
        view?.view.addSubview(self)
        self.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.lblText.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
        UIView.animate(withDuration: 0.3, animations: {
            self.lblText.transform = CGAffineTransform.identity
        }) { (sucess) in
            view?.view.bringSubview(toFront: self)
        }
    }
    
    
    
    //=================================================================================================//
    // MARK: UIButton Method
    @IBAction func btnCloseViewTapped(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "ImageUploadFirstTime")
        self.removeFromSuperview()
    }
    
}
