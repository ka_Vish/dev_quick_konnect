//
// TabBarViewController.swift
//
// Copyright 2017 Handsome LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit


/*
 TabBarViewController is a controller relevant one of the side menu items. To indicate this it adopts `SideMenuItemContent` protocol.
 */
class TabBarViewController: UITabBarController, UITabBarControllerDelegate {
    
    var lastSelectIndex = 0
    var bottomTabHeight = 0
    var bottomTabY_Position = 0
    var btn_buttonScanClik = UIButton()
    var CustomTabView = UIView.fromNib("viewScanInTabBar") as! viewScanInTabBar
    
    var arr_Selectedimage = [UIImage.init(named: "ic_bottom_home_select"),UIImage.init(named: "ic_bottom_contacts_select"),UIImage.init(named: "ic_bottm_scan"),UIImage.init(named: "ic_bottom_notification_select"),UIImage.init(named: "ic_bottom_menu_select")]
    
    var arr_UnSelectedimage = [UIImage.init(named: "ic_bottom_home_unselect"),UIImage.init(named: "ic_bottom_contacts_unselect"),UIImage.init(named: "ic_bottm_scan"),UIImage.init(named: "ic_bottom_notification_unselect"),UIImage.init(named: "ic_bottom_menu_unselect")]
    
    override func viewDidLoad() {
        
        //Set Image For Bottom Tab
        self.delegate = self
        self.lastSelectIndex = 0
        
        if let count = self.tabBar.items?.count {
            for i in 0...(count-1) {
                let imageNameForSelectedState   = arr_Selectedimage[i]
                let imageNameForUnselectedState = arr_UnSelectedimage[i]

                self.tabBar.items?[i].selectedImage = imageNameForSelectedState?.withRenderingMode(.alwaysOriginal)
                self.tabBar.items?[i].image = imageNameForUnselectedState?.withRenderingMode(.alwaysOriginal)
            }
        }
        bottomTabHeight = Int(self.tabBar.frame.size.height)
        bottomTabY_Position = Int(self.tabBar.frame.origin.y)
        
        
        //===================================================================//
        //==========================Right Side Swipe=========================//
        let rightSwipe = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeRight(_:)))
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(rightSwipe)
        //===================================================================//
        //===================================================================//
        
        //===================================================================//
        //==========================Left Side Swipe=========================//
        let leftSwipe = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeLeft(_:)))
        leftSwipe.direction = .left
        self.view.addGestureRecognizer(leftSwipe)
        //===================================================================//
        //===================================================================//
        
        //===================================================================//
        //=======================Bottom Up Side Swipe========================//
        let UpSwipe = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeUp(_:)))
        UpSwipe.direction = .up
        self.tabBar.addGestureRecognizer(UpSwipe)
//        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.respondToSwipeUpGestureForProfile))
//        edgePan.edges = .bottom
//        self.tabBar.addGestureRecognizer(edgePan)
        //===================================================================//
        //===================================================================//
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        CustomTabView.removeFromSuperview()
        
        if Device.IS_IPHONE_X {
            if #available(iOS 11.0, *) {
                CustomTabView.frame = CGRect(x: 0, y: CGFloat(bottomTabY_Position) - 33 - (app_Delegate.window?.safeAreaInsets.bottom)!, width: self.view.frame.size.width, height: CGFloat(bottomTabHeight) + 25)
            }
        }
        else {
            if #available(iOS 11.0, *) {
                CustomTabView.frame = CGRect(x: 0, y: tabBar.frame.origin.y - 28 - (app_Delegate.window?.safeAreaInsets.bottom)!, width: self.view.frame.size.width, height: tabBar.frame.size.height + 20)
            } else {
                // Fallback on earlier versions
                CustomTabView.frame = CGRect(x: 0, y: tabBar.frame.origin.y - 28, width: UIScreen.main.bounds.width, height: tabBar.frame.size.height + 20)
            }
        }
        print(CustomTabView.frame)
        
        //========================================//
        //Scan Button Init and Click For Tab Bar==//
        
        btn_buttonScanClik = UIButton.init(frame: CGRect.init(x: (CustomTabView.viewMainBG.frame.origin.x), y: CustomTabView.frame.origin.y, width: CustomTabView.viewMainBG.frame.size.width, height: CustomTabView.viewMainBG.frame.size.height))
        
        print(CustomTabView.frame)
        print(CustomTabView.viewMainBG.frame)
        print(btn_buttonScanClik.frame)
        
        btn_buttonScanClik.setTitle("", for: .normal)
        btn_buttonScanClik.addTarget(self, action: #selector(self.btnClkToScanViewController), for: .touchUpInside)
        
        self.view.addSubview(CustomTabView)
        self.view.addSubview(btn_buttonScanClik)
        
//        self.viewDidLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if Device.IS_IPHONE_X {
            if #available(iOS 11.0, *) {
                CustomTabView.frame = CGRect(x: 0, y: CGFloat(bottomTabY_Position) - 33 - (app_Delegate.window?.safeAreaInsets.bottom)!, width: self.view.frame.size.width, height: CGFloat(bottomTabHeight) + 25)
            }
        }
        else {
            if #available(iOS 11.0, *) {
                CustomTabView.frame = CGRect(x: 0, y: tabBar.frame.origin.y - 28 - (app_Delegate.window?.safeAreaInsets.bottom)!, width: self.view.frame.size.width, height: tabBar.frame.size.height + 20)
            } else {
                // Fallback on earlier versions
                CustomTabView.frame = CGRect(x: 0, y: tabBar.frame.origin.y - 28, width: UIScreen.main.bounds.width, height: tabBar.frame.size.height + 20)
            }
        }
        self.btn_buttonScanClik.removeFromSuperview()
        
        btn_buttonScanClik.frame = CGRect.init(x: (CustomTabView.viewMainBG.frame.origin.x), y: CustomTabView.frame.origin.y, width: CustomTabView.viewMainBG.frame.size.width, height: CustomTabView.viewMainBG.frame.size.height)
        CustomTabView.viewMainBG.layer.cornerRadius = CustomTabView.viewMainBG.bounds.height/2
        CustomTabView.viewMainBG.layer.shadowColor = #colorLiteral(red: 0.01960784314, green: 0.4392156863, blue: 0.5137254902, alpha: 1).cgColor
        CustomTabView.viewMainBG.layer.shadowOpacity = 0.6
        CustomTabView.viewMainBG.layer.shadowRadius = 6.0
        CustomTabView.viewMainBG.layer.shadowOffset = CGSize(width: 5, height: 5)
        CustomTabView.viewMainBG.layer.masksToBounds = false
        self.view.addSubview(self.btn_buttonScanClik)
        
        print(CustomTabView.frame)
    }
    
    func btnClkToScanViewController() {
        self.selectedIndex = 2
    }
    
    
    @IBAction func swipeRight(_ sender: UISwipeGestureRecognizer)
    {
        if self.selectedIndex > 0
        {
            let tabViewControllers = viewControllers!
            let fromView = selectedViewController!.view
            let toView = tabViewControllers[self.selectedIndex - 1].view
            let fromIndex = self.selectedIndex
            let toIndex = self.selectedIndex - 1
            
            guard fromIndex != toIndex else {return}
            
            // Add the toView to the tab bar view
            fromView?.superview!.addSubview(toView!)
            
            // Position toView off screen (to the left/right of fromView)
            let screenWidth = UIScreen.main.bounds.width
            let scrollRight = toIndex > fromIndex
            let offset = (scrollRight ? screenWidth : -screenWidth)
            toView?.center = CGPoint(x: (fromView?.center.x)! + offset, y: (toView?.center.y)!)
            
            // Disable interaction during animation
            view.isUserInteractionEnabled = false
            
            
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                fromView?.center = CGPoint(x: (fromView?.center.x)! - offset, y: (fromView?.center.y)!)
                toView?.center   = CGPoint(x: (toView?.center.x)! - offset, y: (toView?.center.y)!)
                self.selectedIndex = self.selectedIndex - 1
                
            }, completion: { (finished) in
                
                fromView?.removeFromSuperview()
                //self.selectedIndex = self.selectedIndex - 1
                self.lastSelectIndex = self.selectedIndex
                self.view.isUserInteractionEnabled = true
            })
            
            
            
            
        }
    }
    
    @IBAction func swipeLeft(_ sender: UISwipeGestureRecognizer)
    {
        if self.selectedIndex < (self.viewControllers?.count)!-1
        {
            
            let tabViewControllers = viewControllers!
            let fromView = selectedViewController!.view
            let toView = tabViewControllers[self.selectedIndex + 1].view
            let fromIndex = self.selectedIndex
            let toIndex = self.selectedIndex + 1
            
            guard fromIndex != toIndex else {return}
            
            // Add the toView to the tab bar view
            fromView?.superview!.addSubview(toView!)
            
            // Position toView off screen (to the left/right of fromView)
            let screenWidth = UIScreen.main.bounds.width
            let scrollRight = toIndex > fromIndex
            let offset = (scrollRight ? screenWidth : -screenWidth)
            toView?.center = CGPoint(x: (fromView?.center.x)! + offset, y: (toView?.center.y)!)
            
            // Disable interaction during animation
            view.isUserInteractionEnabled = false
            
            
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                fromView?.center = CGPoint(x: (fromView?.center.x)! - offset, y: (fromView?.center.y)!)
                toView?.center   = CGPoint(x: (toView?.center.x)! - offset, y: (toView?.center.y)!)
                self.selectedIndex = self.selectedIndex + 1
                
            }, completion: { (finished) in
                fromView?.removeFromSuperview()
                //self.selectedIndex = self.selectedIndex + 1
                self.lastSelectIndex = self.selectedIndex
                self.view.isUserInteractionEnabled = true
            })
            
        }
    }
    
    
//    //==============================================================================================//
//    //=======================================For Open ProfileVC=====================================//
//    //For Swipe UP
//    @objc func respondToSwipeUpGestureForProfile(_ recognizer: UIScreenEdgePanGestureRecognizer) {
//        if recognizer.state == .recognized {
//            print("Screen edge swiped!")
//            
//            let objProfile : ProfileVC = self.storyboard!.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
//            objProfile.isedit = false
//            objProfile.ScreenFrom = "Present"
//            let objNav = UINavigationController(rootViewController: objProfile)
//            objNav.isNavigationBarHidden = true
//            self.navigationController?.present(objNav, animated: true, completion: nil)
//        }
//    }
//    //==============================================================================================//
//    //==============================================================================================//
    
    @IBAction func swipeUp(_ sender: UISwipeGestureRecognizer) {

        let objProfile : ProfileVC = self.storyboard!.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        objProfile.isedit = false
        objProfile.ScreenFrom = "Present"
        let objNav = UINavigationController(rootViewController: objProfile)
        objNav.isNavigationBarHidden = true
        
        
        objProfile.view.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
            objProfile.view.frame = CGRect.init(x: 0, y: 0 , width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
        }, completion: { (isfinished) in
            self.navigationController?.present(objNav, animated: true, completion: nil)
        })
        
        
        
        
//
//
//        let transition = CATransition()
//        transition.duration = 1.0
//        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
//        transition.type = kCATransitionFade
////        transition.subtype = kCATransitionFromRight
//        self.navigationController!.view.layer.add(transition, forKey: nil)
//
//
        
    }
    //==============================================================================================//
    //==============================================================================================//
    //==============================================================================================//
    //==============================================================================================//
    
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print(tabBarController.selectedIndex)
        
        if selectedViewController != nil && viewController != selectedViewController {
            
            
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        print(tabBarController.selectedIndex)
        
        if selectedViewController != nil && viewController != selectedViewController {
            
            UIView.transition(from: (selectedViewController?.view)!, to: viewController.view, duration: 0.2, options: UIViewAnimationOptions.transitionCrossDissolve, completion: { (finished) in
                
            })
            
        }
        
        return true
    }
    
    
    
    
    
    
}

