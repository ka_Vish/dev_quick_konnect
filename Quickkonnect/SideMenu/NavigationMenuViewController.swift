//
// NavigationMenuViewController.swift
//
// Copyright 2017 Handsome LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit
import SDWebImage


/*
 Menu controller is responsible for creating its content and showing/hiding menu using 'menuContainerViewController' property.
 */
class NavigationMenuViewController: UIViewController {
    
    let kCellReuseIdentifier = "customCell"
    let menuItems:[String] = ["Social Accounts","Help and Settings", "Legal", "Account Settings", "Logout"]
    let imageName:[UIImage] = [#imageLiteral(resourceName: "social_acc"), #imageLiteral(resourceName: "help_setting"), #imageLiteral(resourceName: "imgLegal"), #imageLiteral(resourceName: "account_setting"), #imageLiteral(resourceName: "imgLogout")]
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgView_BG: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var Count_collect: UICollectionView!
    @IBOutlet var imageTopContraint: NSLayoutConstraint!
    @IBOutlet var imageBottomConstraint: NSLayoutConstraint!
    @IBOutlet var layoutHeightHeaderView: NSLayoutConstraint!
    var minimumHeaderHeight: CGFloat = 90
    var maximumHeaderHeight: CGFloat = 130
    var arr_QK_ProfileList = [NSDictionary]()
    var imageScale: CGFloat = 6.2
    var QKCountTitleArr = ["0","0","0"]
    var QKCountArr = ["Contacts","This Week","This Month"]
    var arr_SectionNumber = [[String : Any]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        app_Delegate.strProfileScreenFrom = ""
        self.imgView.layer.cornerRadius = self.imgView.frame.size.width / 2
        self.imgView.clipsToBounds = true
        
        self.imgView_BG.layer.cornerRadius = self.imgView_BG.frame.size.width / 2
        self.imgView_BG.clipsToBounds = true
        self.lblEmail.text = ""
        
        self.imgView_BG.layer.borderWidth = 3
        self.imgView_BG.layer.borderColor = UIColor.white.withAlphaComponent(0.36).cgColor
        
        self.tableView.contentInset = UIEdgeInsetsMake(self.maximumHeaderHeight, 0, 0, 0)
        self.layoutHeightHeaderView.constant = self.maximumHeaderHeight
        tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.none)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.QKCountTitleArr = ["\(KVAppDataServieces.shared.UserValue(.contact_all))","\(KVAppDataServieces.shared.UserValue(.contact_week))","\(KVAppDataServieces.shared.UserValue(.contact_month))"]
        self.Count_collect.reloadData()
        
        //if app_Delegate.isBackChange == true   {
        
        self.callAPIforgetQKTagViewList()
        //}
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.imgView.layer.cornerRadius = self.imgView.frame.size.width / 2
        self.imgView.clipsToBounds = true
        self.imgView_BG.layer.cornerRadius = self.imgView_BG.frame.size.width / 2
        self.imgView_BG.clipsToBounds = true
        self.lblEmail.text = ""
        
        
        
        
//        if self.layoutHeightHeaderView.constant == self.minimumHeaderHeight{
//            self.imageTopContraint.constant = 16
//            self.imageBottomConstraint.constant = 16
//        }
        
        
        
        if !KVAppDataServieces.shared.isBool(.profile_pic, "")
        {
            self.imgView.image = UIImage.init(named: "user_profile_pic")
            SDWebImageManager.shared().loadImage(with: URL.init(string: KVAppDataServieces.shared.UserValue(.profile_pic) as! String)!, options: SDWebImageOptions.refreshCached, progress: { (pro1, pro2, url) in
                
            }, completed: { (img, data, err, type, finish, url) in
                
                if err == nil
                {
                    self.imgView.image = img
                }
            })
        }
        else
        {
            self.imgView.image = UIImage.init(named: "user_profile_pic")
        }
        
        
        self.lblName.text = KVAppDataServieces.shared.UserValue(.fullname) as? String ?? ""
        self.lblEmail.text = KVAppDataServieces.shared.UserValue(.email) as? String ?? ""
        
        let percentage = self.layoutHeightHeaderView.constant * 0.2
        self.lblName.font = UIFont.init(name: "lato-Medium", size: percentage)
        
        
        self.arr_SectionNumber.removeAll()
        self.arr_SectionNumber.append(["sec_id":"profile_info","title":"", "row":[1]])
        self.arr_SectionNumber.append(["sec_id":"badge_info","title":"", "row":[1]])
        
        self.arr_SectionNumber.append(["sec_id":"settings","title":"", "row":NSArray()])
        
        self.tableView.reloadData()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedReloadQKTagView(notification:)), name: Notification.Name("RELOADNOTIFICATION"), object: nil)
    }
    
    //================================================================================//
    //MARK :- Received Notification
    func methodOfReceivedReloadQKTagView(notification: NSNotification){
        //Take Action on Notification
        self.callAPIforgetQKTagViewList()
    }
    //================================================================================//
        

    
    
    func callAPIforgetQKTagViewList() {
        let dict = NSMutableDictionary()
        dict["user_id"] = KVAppDataServieces.shared.LoginUserID()
        KVClass.KVServiece(methodType: "POST", processView: nil, baseView: self, processLabel: "", params: dict, api: "profile/profile_list", arrFlag: false) { (json, status) in
            if status == 1 {
                print(json)
                if let dataDict = json["data"] as? [NSDictionary] {
                    self.arr_QK_ProfileList = dataDict
                    
                    KVProfileCreate.shared.arr_Profile.removeAll()
                    KVProfileCreate.shared.arr_Badges.removeAll()
                    if dataDict.count == 2 {
                        KVProfileCreate.shared.arr_Profile.append(dataDict.first!)
                    }else if dataDict.count > 2 {
                        for item in dataDict {
                            if KVProfileCreate.shared.arr_Profile.count < 2 {
                                KVProfileCreate.shared.arr_Profile.append(item)
                            }
                        }
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    
    
    @IBAction func btnProfileActionPress(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        obj.isedit = false
        self.navigationController?.pushViewController(obj, animated: true)
    }    
}


//=============================================================================================================//
//=============================================================================================================//
//=============================================================================================================//
extension NavigationMenuViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: DashCollectCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashCollectCell", for: indexPath) as! DashCollectCell
        cell.Title_lbl.text = self.QKCountArr[indexPath.row]
        cell.Count_lbl.text = self.QKCountTitleArr[indexPath.row]
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize.init(width: self.Count_collect.bounds.width/3, height: self.Count_collect.bounds.height)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 1, 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
}
//=============================================================================================================//
//=============================================================================================================//
//=============================================================================================================//


/*
 Extention of `NavigationMenuViewController` class, implements table view delegates methods.
 */
extension NavigationMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        var imgscale = self.imageScale
        
        let y = self.maximumHeaderHeight - (scrollView.contentOffset.y + self.maximumHeaderHeight)
        let height = min(max(y, self.minimumHeaderHeight), self.maximumHeaderHeight + 0)
        
        layoutHeightHeaderView.constant = height
        
        let range = self.maximumHeaderHeight - self.minimumHeaderHeight
        let openAmount = self.layoutHeightHeaderView.constant - self.minimumHeaderHeight
        let scale = (openAmount/range)
        
        imgscale = imgscale - scale
    
        self.imageBottomConstraint.constant = self.layoutHeightHeaderView.constant / imgscale
        self.imageTopContraint.constant = self.layoutHeightHeaderView.constant / imgscale
        
        
        UIView.animate(withDuration: 0.1) {
            self.view.layoutIfNeeded()
            self.imgView.layer.cornerRadius = self.imgView.frame.size.width / 2
            self.imgView.clipsToBounds = true
            self.imgView_BG.layer.cornerRadius = self.imgView_BG.frame.size.width / 2
            self.imgView_BG.clipsToBounds = true
            
            let percentage = self.layoutHeightHeaderView.constant * 0.2
            
            // if percentage > 0.7{
            
            self.lblName.font = UIFont.init(name: "lato-Medium", size: percentage)
            
//            if self.layoutHeightHeaderView.constant == self.minimumHeaderHeight{
//                self.imageTopContraint.constant = 16
//                self.imageBottomConstraint.constant = 16
//            }
//            else{
//                self.imageTopContraint.constant += percentage
//                self.imageBottomConstraint.constant += percentage
//            }
            
            //  }
            
            
        }
        self.Count_collect.reloadData()
        
        //        headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height+44)
        //headerView.updateConstraints()
        //imageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height+44)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arr_SectionNumber.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let dicti = self.arr_SectionNumber[section]
        let strID = dicti["sec_id"] as? String ?? ""
        
        switch strID {
        case "profile_info":
            if KVProfileCreate.shared.arr_Profile.count == 0{
            return 1
            }
            return KVProfileCreate.shared.arr_Profile.count + 2
        case "badge_info":
            if KVProfileCreate.shared.arr_Badges.count == 0{
                return 1
            }
            return KVProfileCreate.shared.arr_Badges.count + 2
        default:
            return self.imageName.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension//52.0
    }
    

    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let dicti = self.arr_SectionNumber[section]
        let strID = dicti["sec_id"] as? String ?? ""
        
        
        let view = UIView()
        view.backgroundColor = UIColor.groupTableViewBackground
        view.sizeToFit()
        if strID == "settings"{
            view.backgroundColor = UIColor.clear
        }
        
        
        
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        let dicti = self.arr_SectionNumber[section]
        let strID = dicti["sec_id"] as? String ?? ""
        
        switch strID{
        case "profile_info":
            if KVProfileCreate.shared.arr_Profile.count == 0{
                return 30
            }
            return 0
        case "badge_info":
            if KVProfileCreate.shared.arr_Badges.count == 0{
                return 30
            }
            return 0
        default:
            return 30
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let dicti = self.arr_SectionNumber[indexPath.section]
        let strID = dicti["sec_id"] as? String ?? ""
        
        switch strID {
        case "profile_info":
            
            if KVProfileCreate.shared.arr_Profile.count == 0{
                let cell3: createProfileInsliderCell = tableView.dequeueReusableCell(withIdentifier: "createProfileInsliderCell", for: indexPath) as! createProfileInsliderCell
                
                return cell3
            }
            else if KVProfileCreate.shared.arr_Profile.count == 1 {
                
                if indexPath.row == 0{
                    let cell2: ProListCell = tableView.dequeueReusableCell(withIdentifier: "ProListCell", for: indexPath) as! ProListCell
                    cell2.Pro_img.layer.borderWidth = 3
                    cell2.Pro_img.layer.borderColor = UIColor.white.withAlphaComponent(0.16).cgColor
                    cell2.Pro_img.layer.cornerRadius = cell2.Pro_img.frame.size.width/2
                    
                    var strProfileType = ""
                    cell2.img_approve.isHidden = true
                    let strP_Type = KVProfileCreate.shared.arr_Profile[indexPath.row]["type"] as? Int ?? 0
                    let strP_Status = KVProfileCreate.shared.arr_Profile[indexPath.row]["status"] as? String ?? ""
                    
                    if strP_Type == 1 {
                        strProfileType = "Company"
                    }
                    else if strP_Type == 2 {
                        strProfileType = "Enterprise"
                    }
                    else if strP_Type == 3 {
                        strProfileType = "Celebrity/Public Figure"
                    }
                    
                    if strP_Status == "P" {
                        cell2.img_approve.isHidden = true
                        
                        cell2.Title_lbl.text = KVProfileCreate.shared.arr_Profile[indexPath.row]["name"] as? String ?? ""
                        cell2.Type_lbl.textColor = UIColor.orange
                        cell2.Type_lbl.text = "Pending Verification"
                        cell2.Type_lbl.font = UIFont.init(name: "lato-Regular", size: 13)
                        cell2.Type_lblStatus.text = strProfileType
                        cell2.Type_lblStatus.textColor = UIColor.darkGray
                        cell2.Type_lblStatus.font = UIFont.init(name: "lato-Regular", size: 14)
                    }
                    else if strP_Status == "R" {
                        cell2.img_approve.image = #imageLiteral(resourceName: "noti_rejected")
                        cell2.img_approve.isHidden = false
                        
                        cell2.Title_lbl.text = KVProfileCreate.shared.arr_Profile[indexPath.row]["name"] as? String ?? ""
                        cell2.Type_lbl.text = "Rejected"
                        cell2.Type_lbl.textColor = UIColor.red
                        cell2.Type_lbl.font = UIFont.init(name: "lato-Regular", size: 13)
                        cell2.Type_lblStatus.text = strProfileType
                    }
                    else if strP_Status == "A" {
                        cell2.img_approve.image = #imageLiteral(resourceName: "noti_contactaprove")
                        cell2.img_approve.isHidden = false
                        cell2.Title_lbl.textColor = UIColor.black
                        cell2.Title_lbl.text = KVProfileCreate.shared.arr_Profile[indexPath.row]["name"] as? String ?? ""
                        cell2.Type_lbl.text = ""
                        cell2.Type_lblStatus.text = strProfileType
                        cell2.Type_lblStatus.textColor = UIColor.darkGray
                        cell2.Type_lblStatus.font = UIFont.init(name: "lato-Regular", size: 14)
                    }
                    else {
                        cell2.img_approve.isHidden = true
                        cell2.Title_lbl.text = ""
                        cell2.Type_lbl.text = KVProfileCreate.shared.arr_Profile[indexPath.row]["name"] as? String ?? ""
                        cell2.Type_lblStatus.text = ""
                    }
                    
                    if let imageURI = KVProfileCreate.shared.arr_Profile[indexPath.row]["logo"] as? String, imageURI != ""{
                        
                        cell2.Pro_img.setImageWith(URLRequest.init(url: URL.init(string: imageURI)!), placeholderImage: #imageLiteral(resourceName: "logo"), success: { (requesr, response, image) in
                            cell2.Pro_img.image = image
                        }, failure: { (request, response, error) in
                            cell2.Pro_img.image = #imageLiteral(resourceName: "logo")
                        })
//                        SDWebImageManager().loadImage(with: URL.init(string: imageURI)!, options: SDWebImageOptions.refreshCached, progress: { (p1, p2, uri) in
//
//                        }, completed: { (imge, dta, err, type, isfinished, uri) in
//
//                            cell2.Pro_img.image = (imge ?? #imageLiteral(resourceName: "logo"))
//                        })
                        
                    }
                    else{
                        cell2.Pro_img.image = #imageLiteral(resourceName: "logo")
                    }
                    
                    return cell2
                }else{
                   
                    if indexPath.row == 1{
                        let cell3: createProfileInsliderCell = tableView.dequeueReusableCell(withIdentifier: "createProfileInsliderCell", for: indexPath) as! createProfileInsliderCell
                        
                        return cell3
                    }else{
                        let cell: profileHeaderCell = tableView.dequeueReusableCell(withIdentifier: "profileHeaderCell", for: indexPath) as! profileHeaderCell
                        cell.lbl_title.text = "VIEW ALL ACCOUNTS"
                        
                        return cell
                    }
                    
                    
                }
                
            }
            else if KVProfileCreate.shared.arr_Profile.count == 2 {
                
                if indexPath.row < 2{
                    let cell2: ProListCell = tableView.dequeueReusableCell(withIdentifier: "ProListCell", for: indexPath) as! ProListCell
                    cell2.Pro_img.layer.borderWidth = 3
                    cell2.Pro_img.layer.borderColor = UIColor.white.withAlphaComponent(0.16).cgColor
                    cell2.Pro_img.layer.cornerRadius = cell2.Pro_img.frame.size.width/2
                    
                    var strProfileType = ""
                    cell2.img_approve.isHidden = true
                    let strP_Type = KVProfileCreate.shared.arr_Profile[indexPath.row]["type"] as? Int ?? 0
                    let strP_Status = KVProfileCreate.shared.arr_Profile[indexPath.row]["status"] as? String ?? ""
                    
                    if strP_Type == 1 {
                        strProfileType = "Company"
                    }
                    else if strP_Type == 2 {
                        strProfileType = "Enterprise"
                    }
                    else if strP_Type == 3 {
                        strProfileType = "Celebrity/Public Figure"
                    }
                    
                    if strP_Status == "P" {
                        cell2.img_approve.isHidden = true
                        
                        cell2.Title_lbl.text = KVProfileCreate.shared.arr_Profile[indexPath.row]["name"] as? String ?? ""
                        cell2.Type_lbl.textColor = UIColor.darkGray
                        cell2.Type_lbl.text = strProfileType
                        cell2.Type_lbl.font = UIFont.init(name: "lato-Regular", size: 13)
                        cell2.Type_lblStatus.text = "Pending Verification"
                        cell2.Type_lblStatus.textColor = UIColor.orange
                        cell2.Type_lblStatus.font = UIFont.init(name: "lato-Medium", size: 13)
                    }
                    else if strP_Status == "R" {
                        cell2.img_approve.image = #imageLiteral(resourceName: "noti_rejected")
                        cell2.img_approve.isHidden = false
                        
                        cell2.Title_lbl.text = KVProfileCreate.shared.arr_Profile[indexPath.row]["name"] as? String ?? ""
                        cell2.Type_lbl.text = strProfileType
                        cell2.Type_lbl.textColor = UIColor.darkGray
                        cell2.Type_lbl.font = UIFont.init(name: "lato-Regular", size: 13)
                        cell2.Type_lblStatus.textColor = UIColor.red
                        cell2.Type_lblStatus.font = UIFont.init(name: "lato-Medium", size: 13)
                        cell2.Type_lblStatus.text = "Rejected"
                    }
                    else if strP_Status == "A" {
                        cell2.img_approve.image = #imageLiteral(resourceName: "noti_contactaprove")
                        cell2.img_approve.isHidden = false
                        cell2.Title_lbl.textColor = UIColor.black
                        cell2.Title_lbl.text = KVProfileCreate.shared.arr_Profile[indexPath.row]["name"] as? String ?? ""
                        cell2.Type_lbl.text = ""
                        cell2.Type_lblStatus.text = strProfileType
                        cell2.Type_lblStatus.textColor = UIColor.darkGray
                        cell2.Type_lblStatus.font = UIFont.init(name: "lato-Regular", size: 14)
                    }
                    else {
                        cell2.img_approve.isHidden = true
                        cell2.Title_lbl.text = ""
                        cell2.Type_lbl.text = KVProfileCreate.shared.arr_Profile[indexPath.row]["name"] as? String ?? ""
                        cell2.Type_lblStatus.text = ""
                    }
                    
                    if let imageURI = KVProfileCreate.shared.arr_Profile[indexPath.row]["logo"] as? String, imageURI != ""{
                        
                        cell2.Pro_img.setImageWith(URLRequest.init(url: URL.init(string: imageURI)!), placeholderImage: #imageLiteral(resourceName: "logo"), success: { (requesr, response, image) in
                            cell2.Pro_img.image = image
                        }, failure: { (request, response, error) in
                            cell2.Pro_img.image = #imageLiteral(resourceName: "logo")
                        })
//                        SDWebImageManager().loadImage(with: URL.init(string: imageURI)!, options: SDWebImageOptions.refreshCached, progress: { (p1, p2, uri) in
//
//                        }, completed: { (imge, dta, err, type, isfinished, uri) in
//
//                            cell2.Pro_img.image = (imge ?? #imageLiteral(resourceName: "logo"))
//                        })
                        
                    }
                    else{
                        cell2.Pro_img.image = #imageLiteral(resourceName: "logo")
                    }
                    
                    
                    
                    
                    return cell2
                }else{
                    
                    if indexPath.row == 2{
                        let cell3: createProfileInsliderCell = tableView.dequeueReusableCell(withIdentifier: "createProfileInsliderCell", for: indexPath) as! createProfileInsliderCell
                        
                        return cell3
                    }
                    else{
                        let cell: profileHeaderCell = tableView.dequeueReusableCell(withIdentifier: "profileHeaderCell", for: indexPath) as! profileHeaderCell
                        cell.lbl_title.text = "VIEW ALL ACCOUNTS"
                        
                        return cell
                    }
                    
                    
                }
                
            }
            return UITableViewCell()
         
        case "badge_info":
            
            let cell: LeftMenuCell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell", for: indexPath) as! LeftMenuCell
            
            if KVProfileCreate.shared.arr_Badges.count == 0{
               
                cell.label_nodata.isHidden = false
                cell.label_title.isHidden = true
                cell.label_nodata.text = "No Badges Available"
                cell.label_nodata.textColor = UIColor.lightGray
                cell.image_icon.image = nil
            }
            else
            {
            cell.label_title.text = ""
            cell.image_icon.image = nil
            }
            return cell
            
            
        default:
           
            let cell: LeftMenuCell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell", for: indexPath) as! LeftMenuCell
            cell.label_title.text = menuItems[indexPath.row]
            cell.image_icon.image = imageName[indexPath.row]
            return cell
            
        }
        
       
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dicti = self.arr_SectionNumber[indexPath.section]
        let strID = dicti["sec_id"] as? String ?? ""
        
        if strID == "profile_info"{
            
            
            if KVProfileCreate.shared.arr_Profile.count == 0{
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddProfileVC") as! AddProfileVC
                obj.strScreenFrom = "Slider"
                self.navigationController?.pushViewController(obj, animated: true)
                
                return
                
            }
            else if KVProfileCreate.shared.arr_Profile.count == 1 {
                
                if indexPath.row == 0{
                    
                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
                    if KVProfileCreate.shared.arr_Profile[indexPath.row]["role"] as? Int ?? 0 == 1 {
                        obj.int_userID = -1
                        obj.str_Screen_From = "OnlyViewProfile"
                        obj.strBlockProfileID = KVProfileCreate.shared.arr_Profile[indexPath.row]["id"] as? Int ?? 0
                        obj.str_uniquecode = KVProfileCreate.shared.arr_Profile[indexPath.row]["unique_code"] as? String ?? ""
                    }
                    else {
                        obj.strScreenFrom = "Slider"
                        
                        if KVProfileCreate.shared.arr_Profile[indexPath.row]["role"] as? Int ?? 0 == 2 {
                            obj.str_Screen_From = "View/EditProfile"
                        }
                        
                        app_Delegate.strProfileScreenFrom = "ViewProfile"
                        obj.int_userID = KVProfileCreate.shared.arr_Profile[indexPath.row]["id"] as? Int ?? 0
                    }
                    self.navigationController?.pushViewController(obj, animated: true)
                    
                    return
                }else{
                    
                    if indexPath.row == 1{
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddProfileVC") as! AddProfileVC
                        obj.strScreenFrom = "Slider"
                        self.navigationController?.pushViewController(obj, animated: true)
                        
                        return
                    }else{
                       
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "QKProListVC") as! QKProListVC
                        obj.arr_ProfileLisData = arr_QK_ProfileList
                        self.navigationController?.pushViewController(obj, animated: true)
                        
                        return
                        
                    }
                }
            }
            else if KVProfileCreate.shared.arr_Profile.count == 2 {
                
                if indexPath.row < 2{
                    
                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "QKProfileVC") as! QKProfileVC
                    if KVProfileCreate.shared.arr_Profile[indexPath.row]["role"] as? Int ?? 0 == 1 {
                        obj.int_userID = -1
                        obj.str_Screen_From = "OnlyViewProfile"
                        obj.strBlockProfileID = KVProfileCreate.shared.arr_Profile[indexPath.row]["id"] as? Int ?? 0
                        obj.str_uniquecode = KVProfileCreate.shared.arr_Profile[indexPath.row]["unique_code"] as? String ?? ""
                    }
                    else {
                        obj.strScreenFrom = "Slider"
                        
                        if KVProfileCreate.shared.arr_Profile[indexPath.row]["role"] as? Int ?? 0 == 2 {
                            obj.str_Screen_From = "View/EditProfile"
                        }
                        
                        app_Delegate.strProfileScreenFrom = "ViewProfile"
                        obj.int_userID = KVProfileCreate.shared.arr_Profile[indexPath.row]["id"] as? Int ?? 0
                    }
                    self.navigationController?.pushViewController(obj, animated: true)
                    
                    return
                    
                }else{
                    
                    if indexPath.row == 2{
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddProfileVC") as! AddProfileVC
                        obj.strScreenFrom = "Slider"
                        self.navigationController?.pushViewController(obj, animated: true)
                        return
                    }
                    else{
                        
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "QKProListVC") as! QKProListVC
                        obj.arr_ProfileLisData = arr_QK_ProfileList
                        self.navigationController?.pushViewController(obj, animated: true)
                        return
                    }
                    
                    
                }
            }
        }
        
        if strID == "settings"{
        
        if indexPath.row == 0 {
            let objSocial = self.storyboard?.instantiateViewController(withIdentifier: "SocialVC") as! SocialVC
            objSocial.view.tag = 2211
            self.navigationController?.pushViewController(objSocial, animated: true)
        }
//        else if indexPath.row == 1 {
//            let objProfile = self.storyboard?.instantiateViewController(withIdentifier: "QKProListVC") as! QKProListVC
//            objProfile.view.tag = 2211
//            self.navigationController?.pushViewController(objProfile, animated: true)
//        }
        else if indexPath.row == 1 {
            let objSetting = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            objSetting.view.tag = 1011
            objSetting.strTitle = menuItems[indexPath.row]
            self.navigationController?.pushViewController(objSetting, animated: true)
        }
        else if indexPath.row == 2 {
            let objLeagal = self.storyboard?.instantiateViewController(withIdentifier: "LegalViewController") as! LegalViewController
            objLeagal.view.tag = 2211
            self.navigationController?.pushViewController(objLeagal, animated: true)
        }
        else if indexPath.row == 3 {
            let objSetting = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            objSetting.view.tag = 1012
            objSetting.strTitle = menuItems[indexPath.row]
            self.navigationController?.pushViewController(objSetting, animated: true)
        }
        else if indexPath.row == 4 {
            
            DispatchQueue.main.async {
                
                let alertView = UIAlertController.init(title: "Are you sure, You want to logout?", message: nil, preferredStyle: .actionSheet)
                
                let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                })
                
                let actionYes = UIAlertAction.init(title: "Logout", style: .destructive, handler: { (action) in
                    
                    //===============================================================================================//
                    //===========API Call For Logout===========================================================//
                    let strToken = kUserDefults_("fcmtoken") as? String ?? ""
                    let strUrlApi = "logout/\(KVAppDataServieces.shared.LoginUserID())/\(strToken)"
                    KVClass.KVServiece(methodType: "GET", processView: self.navigationController, baseView: self, processLabel: "", params: nil, api: strUrlApi, arrFlag: false) { (JSON, status) in
                        print(JSON)
                        AlldefaultsRemove()
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "NaviVC") as! NaviVC
                        UserDefaults.standard.removeObject(forKey: "session")
                        UserDefaults.standard.removeObject(forKey: "dashboard")
                        UIApplication.shared.keyWindow?.rootViewController = vc
                    }
                    //===============================================================================================//
                    //===============================================================================================//
                    //===============================================================================================//
                    //===============================================================================================//
                    
                    
                    
                    
                })
                
                
                alertView.addAction(actionYes)
                alertView.addAction(cancel)
                
                self.present(alertView, animated: true, completion: nil)
            }
          }
        }
    }
}

class customCells:UITableViewCell {
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
}

class LeftMenuCell: UITableViewCell {
    @IBOutlet var image_icon: UIImageView!
    @IBOutlet var label_title: UILabel!
    @IBOutlet var label_nodata: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.label_nodata.isHidden = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.image_icon.image = nil
        
    }
    
}


class createProfileInsliderCell: UITableViewCell{
    
    @IBOutlet var btn_add: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.btn_add.setImage(#imageLiteral(resourceName: "add").tint(with: .white), for: .normal)
    }
    
}

class profileHeaderCell: UITableViewCell{
    
    @IBOutlet var lbl_title: UILabel!
    
    
    
}

