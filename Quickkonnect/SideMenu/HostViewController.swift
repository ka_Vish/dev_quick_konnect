////
//// HostViewController.swift
////
//// Copyright 2017 Handsome LLC
////
//// Licensed under the Apache License, Version 2.0 (the "License");
//// you may not use this file except in compliance with the License.
//// You may obtain a copy of the License at
////
//// http://www.apache.org/licenses/LICENSE-2.0
////
//// Unless required by applicable law or agreed to in writing, software
//// distributed under the License is distributed on an "AS IS" BASIS,
//// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//// See the License for the specific language governing permissions and
//// limitations under the License.
////
//
//import UIKit
//
///*
// HostViewController is container view controller, contains menu controller and the list of relevant view controllers.
// 
// Responsible for creating and selecting menu items content controlers.
// Has opportunity to show/hide side menu.
// */
//class HostViewController: MenuContainerViewController {
//    
//    @IBOutlet var viewFull: UIView!
//    
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        
//        let TopViewStatus = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: 20))
//        if (Device.IS_IPHONE_X){
//            //iPhone X
//            TopViewStatus.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44)
//        }
//        
//        TopViewStatus.backgroundColor = UIColor.init(hex: "28AFB0")
//        self.view.addSubview(TopViewStatus)
//        
//        
//        
//        
//        let screenSize: CGRect = viewFull.bounds
//        self.transitionOptions = TransitionOptions(duration: 0.4, visibleContentWidth: screenSize.width / 5)
//        
//        // Instantiate menu view controller by identifier
//        self.menuViewController = self.storyboard!.instantiateViewController(withIdentifier: "NavigationMenu") as! MenuViewController
//        
//        // Gather content items controllers
//        self.contentViewControllers = contentControllers()
//        
//        // Select initial content controller. It's needed even if the first view controller should be selected.
//        self.selectContentViewController(contentViewControllers.first!)
//    }
//    
////    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
////        super.viewWillTransition(to: size, with: coordinator)
////        
////        /*
////         Options to customize menu transition animation.
////         */
////        var options = TransitionOptions()
////        
////        // Animation duration
////        options.duration = viewFull.bounds.size.width < viewFull.bounds.size.height ? 0.4 : 0.6
////        
////        // Part of item content remaining visible on right when menu is shown
////        options.visibleContentWidth = viewFull.bounds.size.width / 6
////        self.transitionOptions = options
////    }
//    
//    private func contentControllers() -> [UIViewController] {
//        let controllersIdentifiers = ["tabbar","SocialVC","QKProListVC","SettingsViewController", "LegalViewController"]
//        var contentList = [UIViewController]()
//        
//        /*
//         Instantiate items controllers from storyboard.
//         */
//        for identifier in controllersIdentifiers {
//            if let viewController = self.storyboard?.instantiateViewController(withIdentifier: identifier) {
//                contentList.append(viewController)
//            }
//        }
//        
//        return contentList
//    }
//}

