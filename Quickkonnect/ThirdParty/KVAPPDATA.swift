//
//  KVAppData.swift
//  RealmMapDataDemo
//
//  Created by Zignuts Technolab on 05/02/18.
//  Copyright © 2018 Zignuts Technolab. All rights reserved.
//

import Foundation
import RealmSwift


@objcMembers class KVAppData: Object {
    
    dynamic var id: Int = 0
    dynamic var fullname: String = ""
    dynamic var email: String = ""
    dynamic var firsttime: Bool = true
    dynamic var user_detail: String = ""
    dynamic var unique_code: String = ""
    dynamic var qr_code: String = ""
    dynamic var middle_tag: String = ""
    dynamic var background_color: String = "0xffFFFFFF"
    dynamic var border_color: String = "0xff000000"
    dynamic var pattern_color: String = "0xff000000"
    dynamic var background_gradient: String = "1"
    dynamic var profile_pic: String = ""
    dynamic var notification_status: Bool = false
    dynamic var facebook: Data?
    dynamic var linkedin: Data?
    dynamic var twitter: Data?
    dynamic var instagram: Data?
    dynamic var google_plus: Data?
    dynamic var profile_data: Data?
    dynamic var contact_image: Data?
    dynamic var contact_all: Int = 0
    dynamic var contact_month: Int = 0
    dynamic var contact_week: Int = 0
    dynamic var contact_user_list: Data?
    dynamic var block_user_list: Data?
    dynamic var loginFrom : String = "Normal"
    
   // dynamic var test: String = "test"
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        
        self.id = dict["id"] as? Int ?? 0
        self.fullname = "\(dict["firstname"] as? String ?? "") \(dict["lastname"] as? String ?? "")"
        self.email = dict["email"] as? String ?? ""
        self.firsttime = dict["firsttime"] as? Bool ?? false
        self.user_detail = dict["user_detail"] as? String ?? ""
        self.unique_code = dict["unique_code"] as? String ?? ""
        self.qr_code = dict["qr_code"] as? String ?? ""
        self.middle_tag = dict["middle_tag"] as? String ?? "data:image/png;base64,\((UIImagePNGRepresentation(UIImage.init(named: "logo")!)?.base64EncodedString())!)"
        self.background_color = dict["background_color"] as? String ?? "0xffFFFFFF"
        self.border_color = dict["border_color"] as? String ?? "0xff000000"
        self.pattern_color = dict["pattern_color"] as? String ?? "0xff000000"
        self.profile_pic = dict["profile_pic"] as? String ?? ""
        self.background_gradient = dict["background_gradient"] as? String ?? "1"
        self.notification_status = dict["notification_status"] as? Bool ?? false
        self.facebook = KVAppDataServieces.shared.getDatafromObject(dict["facebook"] as? NSDictionary ?? [:])
        self.twitter = KVAppDataServieces.shared.getDatafromObject(dict["twitter"] as? NSDictionary ?? [:])
        self.linkedin = KVAppDataServieces.shared.getDatafromObject(dict["linkedin"] as? NSDictionary ?? [:])
        self.instagram = KVAppDataServieces.shared.getDatafromObject(dict["instagram"] as? NSDictionary ?? [:])
        self.google_plus = KVAppDataServieces.shared.getDatafromObject(dict["google"] as? NSDictionary ?? [:])
        
        self.contact_image = UIImagePNGRepresentation(UIImage.init(named: "logo")!)
        
        if let countdatadict = dict["contact_count"] as? NSDictionary
        {
            self.contact_all = countdatadict["contact_all"] as? Int ?? 0
            self.contact_week = countdatadict["contact_week"] as? Int ?? 0
            self.contact_month = countdatadict["contact_month"] as? Int ?? 0
        }
        
//        self.contact_all = dict["contact_all"] as? Int ?? 0
//        self.contact_week = dict["contact_week"] as? Int ?? 0
//        self.contact_month = dict["contact_month"] as? Int ?? 0
        
        let profiledict = NSMutableDictionary()
        profiledict["basic_detail"] = dict["basic_detail"]
        profiledict["contact_detail"] = dict["contact_detail"]
        profiledict["education_detail"] = dict["education_detail"]
        profiledict["experience_detail"] = dict["experience_detail"]
        profiledict["language_detail"] = dict["language_detail"]
        profiledict["publication_detail"] = dict["publication_detail"]
        
        self.profile_data = KVAppDataServieces.shared.getDatafromObject(profiledict)
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

@objcMembers class KVScanUserData: Object {
    
    dynamic var ScanUniqueCode: String = ""
    dynamic var UserID: Int = 0
    dynamic var UserData: Data?
    
    
    convenience init(_ code: String, _ userid: Int, _ userdata: Data?) {
        self.init()
        
        self.ScanUniqueCode = code
        self.UserID = userid
        self.UserData = userdata
        
    }
    
    override static func primaryKey() -> String? {
        return "ScanUniqueCode"
    }
}



@objcMembers class DJProfileDetailData: Object {
    
    dynamic var profiel_ID: Int = 0
    dynamic var UserID: Int = 0
    dynamic var UserData: Data?
    
    
    convenience init(_ profileid: Int, _ userid: Int, _ userdata: Data?) {
        self.init()
        
        self.profiel_ID = profileid
        self.UserID = userid
        self.UserData = userdata
        
    }
    
    override static func primaryKey() -> String? {
        return "profiel_ID"
    }
}

@objcMembers class DJProfileDetailFromUniqueCode: Object {
    
    dynamic var unique_CODE: String = ""
    dynamic var UserID: Int = 0
    dynamic var UserData: Data?
    
    
    convenience init(_ unique_code: String, _ userid: Int, _ userdata: Data?) {
        self.init()
        
        self.unique_CODE = unique_code
        self.UserID = userid
        self.UserData = userdata
        
    }
    
    override static func primaryKey() -> String? {
        return "unique_CODE"
    }
}




@objcMembers class DJUserProfilesList: Object {
    
    dynamic var login_user_iD: Int = 0
    dynamic var UserData: Data? = nil
    
    
    convenience init(_ user_id: Int, _ userdata: Data?) {
        self.init()
        
        self.login_user_iD = user_id
        self.UserData = userdata
        
    }
    
    override static func primaryKey() -> String? {
        return "login_user_iD"
    }
}


@objcMembers class DJAll_FollowerList: Object {
    
    
    dynamic var USER_iD: Int = 0
    dynamic var UserData: Data? = nil
    
    
    convenience init(_ user_id: Int, _ userdata: Data?) {
        self.init()
        
        self.USER_iD = user_id
        self.UserData = userdata
        
    }
    
    override static func primaryKey() -> String? {
        return "USER_iD"
    }
}

@objcMembers class DJ_FollowingList: Object {
    
    
    dynamic var USER_iD: Int = 0
    dynamic var UserData: Data? = nil
    
    
    convenience init(_ user_id: Int, _ userdata: Data?) {
        self.init()
        
        self.USER_iD = user_id
        self.UserData = userdata
        
    }
    
    override static func primaryKey() -> String? {
        return "USER_iD"
    }
}

@objcMembers class DJFollowersUserList: Object {
    
   
    dynamic var profile_iD: Int = 0
    dynamic var UserData: Data? = nil
    
    
    convenience init(_ profile_id: Int, _ userdata: Data?) {
        self.init()
        
        self.profile_iD = profile_id
        self.UserData = userdata
        
    }
    
    override static func primaryKey() -> String? {
        return "profile_iD"
    }
}

@objcMembers class DJEmployeesUserList: Object {
    
    
    dynamic var profile_iD: Int = 0
    dynamic var UserData: Data? = nil
    
    
    convenience init(_ profileid: Int, _ userdata: Data?) {
        self.init()
        
        self.profile_iD = profileid
        self.UserData = userdata
        
    }
    
    override static func primaryKey() -> String? {
        return "profile_iD"
    }
}





class KVAppDataServieces {
    
    private init() {}
    static let shared = KVAppDataServieces()
    var AppUserID = kUserDefults_("kLoginusrid") as? Int ?? 0
    private var AppData: Results<KVAppData>!
    private var ScaneUserData: Results<KVScanUserData>!
    private var Profile_Detail_Data: Results<DJProfileDetailData>!
    private var ProfileDetailFromUnique_Code: Results<DJProfileDetailFromUniqueCode>!
    var arrFollowersUsers: Results<DJFollowersUserList>!
    var arr_AllFollowerList: Results<DJAll_FollowerList>!
    var arr_FollowingList: Results<DJ_FollowingList>!
    var arrEmployeesUsers: Results<DJEmployeesUserList>!
    var arrUserProfilesList: Results<DJUserProfilesList>!
    
    enum KVAppDataEnum: String {
        case loginFrom = "loginFrom"
        case fullname = "fullname"
        case email = "email"
        case user_detail = "user_detail"
        case unique_code = "unique_code"
        case qr_code = "qr_code"
        case middle_tag = "middle_tag"
        case background_color = "background_color"
        case border_color = "border_color"
        case pattern_color = "pattern_color"
        case profile_pic = "profile_pic"
        case firsttime = "firsttime"
        case notification_status = "notification_status"
        case facebook = "facebook"
        case linkedin = "linkedin"
        case twitter = "twitter"
        case instagram = "instagram"
        case google_plus = "google_plus"
        case profile_data = "profile_data"
        case contact_all = "contact_all"
        case contact_week = "contact_week"
        case contact_month = "contact_month"
        case contact_image = "contact_image"
        case contact_user_list = "contact_user_list"
        case block_user_list = "block_user_list"
        case background_gradient = "background_gradient"
    }
    
    enum KVAppDataProfileEnum: String{
        
        case basic_detail = "basic_detail"
        case contact_detail = "contact_detail"
        case education_detail = "education_detail"
        case experience_detail = "experience_detail"
        case language_detail = "language_detail"
        case publication_detail = "publication_detail"
    }
    
    
    func initializeAppData(_ dict: NSDictionary)
    {
        KVAppDataServieces.shared.AppUserID = kUserDefults_("kLoginusrid") as? Int ?? 0
        let obj = KVAppData.init(dict)
        RealmServieces.shared.create(obj)
        
        ScaneUserData = RealmServieces.shared.realm.objects(KVScanUserData.self).filter(NSPredicate.init(format: "UserID == %d", KVAppDataServieces.shared.AppUserID))
        
        arrFollowersUsers = RealmServieces.shared.realm.objects(DJFollowersUserList.self).filter(NSPredicate.init(format: "profile_iD == %d", KVAppDataServieces.shared.AppUserID))
        
        arrEmployeesUsers = RealmServieces.shared.realm.objects(DJEmployeesUserList.self).filter(NSPredicate.init(format: "profile_iD == %d", KVAppDataServieces.shared.AppUserID))
        
        
        Profile_Detail_Data = RealmServieces.shared.realm.objects(DJProfileDetailData.self).filter(NSPredicate.init(format: "profiel_ID == %d", KVAppDataServieces.shared.AppUserID))
        
        //ProfileDetailFromUnique_Code = RealmServieces.shared.realm.objects(DJProfileDetailFromUniqueCode.self).filter(NSPredicate.init(format: "unique_CODE == %d", KVAppDataServieces.shared.AppUserID))
        
        
        
        
        filterAppData()
    }
    
    func removeListData(_ key: KVAppDataEnum)
    {
        filterAppData()
        let obj = (AppData.last)
        
        try! RealmServieces.shared.realm.write {
            obj?.setValue(nil, forKey: key.rawValue)
        }
        
    }
    
    func getDatafromObject(_ dict: NSDictionary) -> Data?
    {
        let obj = NSKeyedArchiver.archivedData(withRootObject: dict)
        
        return obj
    }
    
    //    func getScanUserbyID(_ keyid: Int) -> NSDictionary?
    //    {
    //        filterAppData()
    //
    //        let valuedata = KVAppDataServieces.shared.ScaneUserData.filter("ScanUniqueCode == %@", uniquecode)
    //
    //    }
    
    
    func getScanUser(_ uniquecode: String) -> NSDictionary?
    {
        filterAppData()
        
        let valuedata = KVAppDataServieces.shared.ScaneUserData.filter("ScanUniqueCode == %@", uniquecode)
        
        if valuedata.count > 0
        {
            let data = NSKeyedUnarchiver.unarchiveObject(with: (valuedata.last?.UserData)!)
            
            return data as? NSDictionary ?? [:]
        }
        return nil
    }
    
    
    
    
    func getUserProfileDetail(_ profileid: Int) -> NSDictionary?
    {
        filterAppData()
        
        let valuedata = KVAppDataServieces.shared.Profile_Detail_Data.filter("profiel_ID == %@", profileid)
        
        if valuedata.count > 0
        {
            let data = NSKeyedUnarchiver.unarchiveObject(with: (valuedata.last?.UserData)!)
            
            return data as? NSDictionary ?? [:]
        }
        return nil
    }
    
    func getUserProfileDetailFromUniqueCode(_ uniquecode: String) -> NSDictionary?
    {
        filterAppData()
        
        let valuedata = KVAppDataServieces.shared.ProfileDetailFromUnique_Code.filter("unique_CODE == %@", uniquecode)
        
        if valuedata.count > 0
        {
            let data = NSKeyedUnarchiver.unarchiveObject(with: (valuedata.last?.UserData)!)
            
            return data as? NSDictionary ?? [:]
        }
        return nil
    }
    
    
    
    func setScanUser(_ uniqueCode: String, _ dataDict: NSDictionary)
    {
        let datavalue = getDatafromObject(dataDict)
        
        let obj = KVScanUserData.init(uniqueCode, KVAppDataServieces.shared.LoginUserID(), datavalue)
        RealmServieces.shared.create(obj)
    }
    
    
    func setUserProfileData(_ profileid: Int, _ dataDict: NSDictionary)
    {
        let datavalue = getDatafromObject(dataDict)
        
        let obj = DJProfileDetailData.init(profileid, KVAppDataServieces.shared.LoginUserID(), datavalue)
        RealmServieces.shared.create(obj)
    }
    
    func setUserProfileDataFromUniqueCode(_ uniquecode: String, _ dataDict: NSDictionary)
    {
        let datavalue = getDatafromObject(dataDict)
        
        let obj = DJProfileDetailFromUniqueCode.init(uniquecode, KVAppDataServieces.shared.LoginUserID(), datavalue)
        RealmServieces.shared.create(obj)
    }
    
    
    func unMatchScanuser(_ uniquecode: String)
    {
        let data = RealmServieces.shared.realm.objects(KVScanUserData.self).filter(NSPredicate.init(format: "ScanUniqueCode == %@", uniquecode))
        RealmServieces.shared.deleteObject(data)
    }
    
    
    
    //===========================================================================================================//
    // MARK: Set Profile List in Local Database
    func setProfileListFromLoginUser_ID(_ login_user_id: Int, _ arrData: NSDictionary)
    {
        let datavalue = getDatafromObject(arrData)
        
        let obj = DJUserProfilesList.init(login_user_id, datavalue)
        RealmServieces.shared.create(obj)
    }
    
    // MARK: Get Followers User List From Local Database
    func getUserProfiles_List(_ userId: Int) -> [NSDictionary]?
    {
        
        KVAppDataServieces.shared.arrUserProfilesList = RealmServieces.shared.realm.objects(DJUserProfilesList.self).filter(NSPredicate.init(format: "login_user_iD == %d", userId))
        
        let valuedata = KVAppDataServieces.shared.arrUserProfilesList.filter("login_user_iD == %@", userId)
        
        if valuedata.count > 0
        {
            let data = NSKeyedUnarchiver.unarchiveObject(with: (valuedata.last?.UserData)!)
            
            return (data as? NSDictionary ?? [:])["data"] as? [NSDictionary]
        }
        return nil
    }
    //===========================================================================================================//
    //===========================================================================================================//
    
    
    
    
    //===========================================================================================================//
    // MARK: Set Followers User List in Local Database
    func setFollowersUserListFromProfile_ID(_ profile_id: Int, _ arrData: NSDictionary)
    {
        let datavalue = getDatafromObject(arrData)
        
        let obj = DJFollowersUserList.init(profile_id, datavalue)
        RealmServieces.shared.create(obj)
    }
    
    // MARK: Get Followers User List From Local Database
    func getFollowersUserList(_ profileid: Int) -> [NSDictionary]?
    {
        
        KVAppDataServieces.shared.arrFollowersUsers = RealmServieces.shared.realm.objects(DJFollowersUserList.self).filter(NSPredicate.init(format: "profile_iD == %d", profileid))
        
        let valuedata = KVAppDataServieces.shared.arrFollowersUsers.filter("profile_iD == %@", profileid)
        
        if valuedata.count > 0
        {
            let data = NSKeyedUnarchiver.unarchiveObject(with: (valuedata.last?.UserData)!)
            
            return (data as? NSDictionary ?? [:])["data"] as? [NSDictionary]
        }
        return nil
    }
    //===========================================================================================================//
    //===========================================================================================================//
    
    
    //===========================================================================================================//
    // MARK: Set All Followers List in Local Database
    func setAllFollowerListFromLoginUser_ID(_ user_id: Int, _ arrData: NSDictionary)
    {
        let datavalue = getDatafromObject(arrData)
        
        let obj = DJAll_FollowerList.init(user_id, datavalue)
        RealmServieces.shared.create(obj)
    }
    
    // MARK: Get Followers User List From Local Database
    func getAll_FollowerList(_ userid: Int) -> [NSDictionary]?
    {
        
        KVAppDataServieces.shared.arr_AllFollowerList = RealmServieces.shared.realm.objects(DJAll_FollowerList.self).filter(NSPredicate.init(format: "USER_iD == %d", userid))
        
        let valuedata = KVAppDataServieces.shared.arr_AllFollowerList.filter("USER_iD == %@", userid)
        
        if valuedata.count > 0
        {
            let data = NSKeyedUnarchiver.unarchiveObject(with: (valuedata.last?.UserData)!)
            
            return (data as? NSDictionary ?? [:])["data"] as? [NSDictionary]
        }
        return nil
    }
    //===========================================================================================================//
    //===========================================================================================================//
    
    
    //===========================================================================================================//
    // MARK: Set Employees List in Local Database
    func setEmployeesUserListFromProfile_ID(_ profile_id: Int, _ arrData: NSDictionary)
    {
        let datavalue = getDatafromObject(arrData)
        
        let obj = DJEmployeesUserList.init(profile_id, datavalue)
        RealmServieces.shared.create(obj)
    }
    
    // MARK: Get Followers User List From Local Database
    func getEmployeesUserList(_ profileid: Int) -> [NSDictionary]?
    {
        KVAppDataServieces.shared.arrEmployeesUsers = RealmServieces.shared.realm.objects(DJEmployeesUserList.self).filter(NSPredicate.init(format: "profile_iD == %d", profileid))
        
        let valuedata = KVAppDataServieces.shared.arrEmployeesUsers.filter("profile_iD == %@", profileid)
        
        if valuedata.count > 0
        {
            let data = NSKeyedUnarchiver.unarchiveObject(with: (valuedata.last?.UserData)!)
            
            return (data as? NSDictionary ?? [:])["data"] as? [NSDictionary]
        }
        return nil
    }
    //===========================================================================================================//
    //===========================================================================================================//
    
    
    //===========================================================================================================//
    // MARK: Set All Following List in Local Database
    func set_FollowingListFromLoginUser_ID(_ user_id: Int, _ arrData: NSDictionary)
    {
        let datavalue = getDatafromObject(arrData)
        
        let obj = DJ_FollowingList.init(user_id, datavalue)
        RealmServieces.shared.create(obj)
    }
    
    // MARK: Get Followers User List From Local Database
    func get_FollowingList(_ userid: Int) -> [NSDictionary]?
    {
        
        KVAppDataServieces.shared.arr_FollowingList = RealmServieces.shared.realm.objects(DJ_FollowingList.self).filter(NSPredicate.init(format: "USER_iD == %d", userid))
        
        let valuedata = KVAppDataServieces.shared.arr_FollowingList.filter("USER_iD == %@", userid)
        
        if valuedata.count > 0
        {
            let data = NSKeyedUnarchiver.unarchiveObject(with: (valuedata.last?.UserData)!)
            
            return (data as? NSDictionary ?? [:])["data"] as? [NSDictionary]
        }
        return nil
    }
    //===========================================================================================================//
    //===========================================================================================================//
    
    
    
    
    func LoginUserID() -> Int
    {
        filterAppData()
        
        return (AppData.last?.id ?? 0)!
    }
    //
    func UserValue(_ detailname: KVAppDataEnum) -> AnyObject
    {
        filterAppData()
        let value = AppData.last?.value(forKey: detailname.rawValue)
        
        if detailname.rawValue == "contact_image"
        {
            return value as AnyObject
        }
        
        if let data = value as? Data
        {
            let dict = NSKeyedUnarchiver.unarchiveObject(with: data)
            
            return dict as AnyObject
        }
        
        return AppData.last?.value(forKey: detailname.rawValue) as AnyObject
    }
    
    
    
    func updateProfile(_ key: KVAppDataProfileEnum, _ value: NSDictionary,_ isupdate: Int)
    {
        filterAppData()
        
        let obj = (AppData.last)
        
        if key.rawValue == "basic_detail"
        {
            let profiledict = NSMutableDictionary.init(dictionary: UserValue(.profile_data) as! [AnyHashable : AnyHashable])
            
            profiledict[key.rawValue] = value
            
            let data = getDatafromObject(profiledict)
            
            try! RealmServieces.shared.realm.write {
                obj?.setValue(data, forKey: KVAppDataEnum.profile_data.rawValue)
            }
            return
        }
        let profiledict = NSMutableDictionary.init(dictionary: UserValue(.profile_data) as! [AnyHashable : AnyHashable])
        
        var objectarr: [NSDictionary]! = [NSDictionary]()
        
        if let valueArr = profiledict[key.rawValue] as? [NSDictionary]
        {
            for item in valueArr
            {
               objectarr.append(item)
            }
        }
            if isupdate == 5001
            {
                objectarr.append(value)
            }
            else
            {
                objectarr.remove(at: isupdate)
                objectarr.insert(value, at: isupdate)
                
            }
            
            profiledict[key.rawValue] = objectarr
            
            let data = getDatafromObject(profiledict)
            
            try! RealmServieces.shared.realm.write {
                obj?.setValue(data, forKey: KVAppDataEnum.profile_data.rawValue)
            }
            
            return
       // }
    }
    
    func updateFullProfile(_ datadict: NSDictionary)
    {
        filterAppData()
        
        let obj = (AppData.last)
        
        let data = getDatafromObject(datadict)
        
        try! RealmServieces.shared.realm.write {
            obj?.setValue(data, forKey: "profile_data")
        }
        
    }
    
    func deleteProfileData(_ key: KVAppDataProfileEnum, _ atIndex: Int)
    {
        filterAppData()
        let obj = (AppData.last)
        let profiledict = NSMutableDictionary.init(dictionary: (UserValue(.profile_data) as? NSDictionary ?? [:]) as! [AnyHashable : AnyHashable])
        
        if var objarr = profiledict[key.rawValue] as? [NSDictionary]
        {
            objarr.remove(at: atIndex)
            profiledict[key.rawValue] = objarr
            
            let data = getDatafromObject(profiledict)
            
            try! RealmServieces.shared.realm.write {
                obj?.setValue(data, forKey: KVAppDataEnum.profile_data.rawValue)
            }
            
        }
        
    }
    
    
    
    func update(_ key: KVAppDataEnum, _ value: Any)
    {
        filterAppData()
        
        let obj = (AppData.last)
        
        if let dict = value as? NSDictionary
        {
            let data = getDatafromObject(dict)
            
            try! RealmServieces.shared.realm.write {
                obj?.setValue(data, forKey: key.rawValue)
            }
            return
        }
        
        try! RealmServieces.shared.realm.write {
            obj?.setValue(value, forKey: key.rawValue)
        }
        
    }
    
    func isBool(_ detailname: KVAppDataEnum, _ valueName: String) -> Bool
    {
        filterAppData()
        
        let obj = AppData.last?.value(forKey: detailname.rawValue)
        
        if let data = obj as? Data
        {
            let dict = NSKeyedUnarchiver.unarchiveObject(with: data)
            
            if let boolvalue = (dict as? NSDictionary ?? [:]).value(forKey: valueName) as? Bool
            {
                return boolvalue
            }
            else if let nilvalue = (dict as? NSDictionary ?? [:]).value(forKey: valueName)
            {
                if nilvalue is NSNull
                {
                    return true
                }
                else if (nilvalue as? String) == nil || (nilvalue as? String) == ""
                {
                    return true
                }
                
                return false
            }
            
        }
        else if let boolvalue = obj as? Bool
        {
            return boolvalue
        }
        else if obj is NSNull
        {
            return true
        }
        else if (obj as? String) == nil || (obj as? String) == ""
        {
            return true
        }
        
        return false
    }
    
    
    func instaData() -> NSDictionary
    {
        filterAppData()
        
        let obj = AppData.last?.instagram
        
        if obj == nil
        {
            return [:]
        }
        let dict = NSKeyedUnarchiver.unarchiveObject(with: obj!)
        return dict as? NSDictionary ?? [:]
    }
    
    func localQKData() -> NSDictionary
    {
        return [:]
    }
    
    func fbData() -> NSDictionary
    {
        filterAppData()
        
        let obj = AppData.last?.facebook
        if obj == nil
        {
            return [:]
        }
        let dict = NSKeyedUnarchiver.unarchiveObject(with: obj!)
        
        return dict as? NSDictionary ?? [:]
    }
    func twitterData() -> NSDictionary
    {
        filterAppData()
        
        let obj = AppData.last?.twitter
        if obj == nil
        {
            return [:]
        }
        let dict = NSKeyedUnarchiver.unarchiveObject(with: obj!)
        
        return dict as? NSDictionary ?? [:]
    }
    func linkedinData() -> NSDictionary
    {
        filterAppData()
        
        let obj = AppData.last?.linkedin
        if obj == nil
        {
            return [:]
        }
        let dict = NSKeyedUnarchiver.unarchiveObject(with: obj!)
        
        return dict as? NSDictionary ?? [:]
    }
    func googleData() -> NSDictionary
    {
        filterAppData()
        
        let obj = AppData.last?.google_plus
        if obj == nil
        {
            return [:]
        }
        let dict = NSKeyedUnarchiver.unarchiveObject(with: obj!)
        
        return dict as? NSDictionary ?? [:]
    }
    
    fileprivate func filterAppData()
    {
        KVAppDataServieces.shared.AppData = RealmServieces.shared.realm.objects(KVAppData.self).filter(NSPredicate.init(format: "id == %d", KVAppDataServieces.shared.AppUserID))
        
        KVAppDataServieces.shared.ScaneUserData = RealmServieces.shared.realm.objects(KVScanUserData.self).filter(NSPredicate.init(format: "UserID == %d", KVAppDataServieces.shared.AppUserID))
        
        KVAppDataServieces.shared.Profile_Detail_Data = RealmServieces.shared.realm.objects(DJProfileDetailData.self).filter(NSPredicate.init(format: "UserID == %d", KVAppDataServieces.shared.AppUserID))
        
        KVAppDataServieces.shared.ProfileDetailFromUnique_Code = RealmServieces.shared.realm.objects(DJProfileDetailFromUniqueCode.self).filter(NSPredicate.init(format: "UserID == %d", KVAppDataServieces.shared.AppUserID))
        
    }
    
    func deleteAccount()
    {
        filterAppData()
        let obj = AppData.last
        
        let scanelist = KVAppDataServieces.shared.ScaneUserData.filter("UserID == %d", KVAppDataServieces.shared.LoginUserID())
        RealmServieces.shared.deleteObject(scanelist)
        RealmServieces.shared.delete(obj!)
    }
    
    
}



