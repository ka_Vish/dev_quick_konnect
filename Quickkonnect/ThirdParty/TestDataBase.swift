//
//  TestDataBase.swift
//  SolarSTC Offline
//
//  Created by Kavi on 07/12/17.
//  Copyright © 2017 chetan. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class RealmLoginUserData: Object {
    
    dynamic var id: Int = 0
    dynamic var name: String = ""
    dynamic var firstname: String = ""
    dynamic var lastname: String = ""
    dynamic var email: String = ""
    dynamic var firsttime: Bool = false
    dynamic var user_detail: String = ""
    dynamic var unique_code: String = ""
    dynamic var qr_code: String = ""
    dynamic var middle_tag: String = ""
    dynamic var background_color: String = ""
    dynamic var border_color: String = ""
    dynamic var pattern_color: String = ""
    dynamic var Status: Int = 0
    dynamic var profile_pic: String = ""
    dynamic var notification_status: Bool = false
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        
        self.id = dict["id"] as? Int ?? 0
        self.name = dict["name"] as? String ?? ""
        self.firstname = dict["firstname"] as? String ?? ""
        self.lastname = dict["lastname"] as? String ?? ""
        self.email = dict["email"] as? String ?? ""
        self.firsttime = dict["firsttime"] as? Bool ?? false
        self.user_detail = dict["user_detail"] as? String ?? ""
        self.unique_code = dict["unique_code"] as? String ?? ""
        self.qr_code = dict["qr_code"] as? String ?? ""
        self.middle_tag = dict["middle_tag"] as? String ?? ""
        self.background_color = dict["background_color"] as? String ?? "FFFFFF"
        self.border_color = dict["border_color"] as? String ?? "000000"
        self.pattern_color = dict["pattern_color"] as? String ?? "000000"
        self.Status = dict["status"] as? Int ?? 0
        self.profile_pic = dict["profile_pic"] as? String ?? ""
        self.notification_status = dict["notification_status"] as? Bool ?? false
        
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}


@objcMembers class RealmSocialLoginData: Object {
    
    dynamic var socialmediaid: Int = 0
    dynamic var username: String = ""
    dynamic var socialmedianame: String = ""
    dynamic var socialstate: Bool = false
    dynamic var userID: Int = 0
    
    
    convenience init(_ userid: Int, _ socialid: Int, _ userName: String, _ socialname: String, _ socialstatus: Bool) {
        self.init()
        
        self.userID = userid
        self.socialmediaid = socialid
        self.socialmedianame = socialname
        self.username = userName
        self.socialstate = socialstatus
    }
    
    override static func primaryKey() -> String? {
        return "socialmediaid"
    }
    
}

@objcMembers class RealmCountryList: Object {
    
    dynamic var code: Int = 0
    dynamic var name: String = ""
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        
        self.code = dict["code"] as? Int ?? 0
        self.name = dict["name"] as? String ?? ""
        
    }
    
    override static func primaryKey() -> String? {
        return "code"
    }
    
}

@objcMembers class RealmCategoryList: Object {
    
    dynamic var parent_id: Int = 0
    dynamic var id: Int = 0
    dynamic var name: String = ""
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        
        self.parent_id = dict["type"] as? Int ?? 0
        self.id = dict["id"] as? Int ?? 0
        self.name = dict["name"] as? String ?? ""
        
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

@objcMembers class RealmSubCategoryList: Object {
    
    dynamic var parent_id: Int = 0
    dynamic var id: Int = 0
    dynamic var name: String = ""
    
    convenience init(_ dict: NSDictionary) {
        self.init()
        
        self.parent_id = dict["type"] as? Int ?? 0
        self.id = dict["id"] as? Int ?? 0
        self.name = dict["name"] as? String ?? ""
        
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}







//
//
//@objcMembers class RealmAllDataList: Object {
//
//    dynamic var InstallBookingID: Int = 0
//    dynamic var BookingDate: String = ""
//    dynamic var ProjectNumber: Int = 0
//    dynamic var Customer: String = ""
//    dynamic var Project: String = ""
//    dynamic var HouseTypeABB: String = ""
//    dynamic var NumberPanels: Int = 0
//    dynamic var Installer: String = ""
//    dynamic var InstallState: String = ""
//    dynamic var InstallPostCode: String = ""
//    dynamic var ProjectID: Int = 0
//    dynamic var notes: String = ""
//
//
//    convenience init(_ dict: NSDictionary) {
//        self.init()
//
//        self.InstallBookingID = dict["InstallBookingID"] as? Int ?? 0
//        self.BookingDate = dict["BookingDate"] as? String ?? ""
//        self.ProjectNumber = dict["ProjectNumber"] as? Int ?? 0
//        self.Customer = dict["Customer"] as? String ?? ""
//        self.Project = dict["Project"] as? String ?? ""
//        self.HouseTypeABB = dict["HouseTypeABB"] as? String ?? ""
//        self.NumberPanels = dict["NumberPanels"] as? Int ?? 0
//        self.Installer = dict["Installer"] as? String ?? ""
//        self.InstallState = dict["InstallState"] as? String ?? ""
//        self.InstallPostCode = dict["InstallPostCode"] as? String ?? ""
//        self.ProjectID = dict["ProjectID"] as? Int ?? 0
//        self.notes = dict["InstallerNotes"] as? String ?? "N/A"
//
//
//    }
//
//    override static func primaryKey() -> String? {
//        return "ProjectID"
//    }
//
//}
//
//@objcMembers class RealmnUserDetailData: Object {
//
//
//    dynamic var ProjectID: String = "0"
//    dynamic var UserDetails: NSData? = nil
//
//
//    convenience init(_ dict: NSDictionary) {
//        self.init()
//
//        self.ProjectID = dict["ProjectID"] as? String ?? "0"
//        self.UserDetails = (dict["Data"] as? NSData ?? nil)!
//
//    }
//
//    override static func primaryKey() -> String? {
//        return "ProjectID"
//    }
//
//}
//
//@objcMembers class RealmnUserInverterDetailData: Object {
//
//    dynamic var IncID: Int = 0
//    dynamic var ProjectID: String = "0"
//    dynamic var Image: NSData? = nil
//    dynamic var InverterSerialNo: String = ""
//    dynamic var ProjectNumber: String = ""
//    dynamic var latitude: String = ""
//    dynamic var longitude: String = ""
//
//
//    convenience init(_ ID: String,_ Image: NSData?,_ ProNum: String,_ NO: String , _ lat: String, _ long: String) {
//        self.init()
//
//        self.IncID = self.incrementID()
//        self.ProjectID = ID
//        self.Image = Image!
//        self.ProjectNumber = ProNum
//        self.InverterSerialNo = NO
//        self.latitude = lat
//        self.longitude = long
//
//    }
//
//    override static func primaryKey() -> String? {
//        return "IncID"
//    }
//
//    func incrementID() -> Int {
//        let realm = try! Realm()
//        return (realm.objects(RealmnUserInverterDetailData.self).max(ofProperty: "IncID") as Int? ?? 0) + 1
//    }
//
//}
//
//@objcMembers class RealmnUserPanelDetailData: Object {
//
//    dynamic var IncID: Int = 0
//    dynamic var ProjectID: String = "0"
//    dynamic var ProjectNumber: String = ""
//    dynamic var PanelImageName: NSData? = nil
//    dynamic var PanelSerialNo: String = ""
//    dynamic var PanelID: String = ""
//    dynamic var latitude: String = ""
//    dynamic var longitude: String = ""
//
//    convenience init(_ ID: String,_ ProNum: String,_ Image: NSData?,_ PanelID: String,_ NO: String , _ lat: String, _ long: String) {
//        self.init()
//
//        self.IncID = self.incrementID()
//        self.ProjectID = ID
//        self.ProjectNumber = ProNum
//        self.PanelImageName = Image
//        self.PanelID = PanelID
//        self.PanelSerialNo = NO
//        self.latitude = lat
//        self.longitude = long
//
//    }
//
//    override static func primaryKey() -> String? {
//        return "IncID"
//    }
//
//    func incrementID() -> Int {
//        let realm = try! Realm()
//        return (realm.objects(RealmnUserPanelDetailData.self).max(ofProperty: "IncID") as Int? ?? 0) + 1
//    }
//
//}
//
//
//@objcMembers class RealmnUserPhotoData: Object {
//
//    dynamic var IncID: Int = 0
//    dynamic var ProjectID: String = "0"
//    dynamic var ProjectNumber: String = ""
//    dynamic var IsImage: Bool = false
//    dynamic var ImageID: Int = 0
//    dynamic var Image: NSData? = nil
//    dynamic var latitude: String = ""
//    dynamic var longitude: String = ""
//
//    convenience init(_ ID: String,_ ProNum: String,_ Image: Bool,_ ImageID: Int,_ Photo: NSData? , _ lat: String, _ long: String) {
//        self.init()
//
//        self.IncID = self.incrementID()
//        self.ProjectID = ID
//        self.ProjectNumber = ProNum
//        self.IsImage = Image
//        self.ImageID = ImageID
//        self.Image = Photo
//        self.latitude = lat
//        self.longitude = long
//
//    }
//
//    func incrementID() -> Int {
//        let realm = try! Realm()
//        return (realm.objects(RealmnUserPhotoData.self).max(ofProperty: "IncID") as Int? ?? 0) + 1
//    }
//    override static func primaryKey() -> String? {
//        return "IncID"
//    }
//}
//
//@objcMembers class RealmnUserSTCformData: Object {
//
//    dynamic var IncID: Int = 0
//    dynamic var ProjectID: String = "0"
//    dynamic var ProjectNumber: String = ""
//    dynamic var IsImage: Bool = false
//    dynamic var ImageID: Int = 0
//    dynamic var Image: NSData? = nil
//    dynamic var Ownimage: String = ""
//    dynamic var latitude: String = ""
//    dynamic var longitude: String = ""
//
//
//    convenience init(_ ID: String,_ ProNum: String,_ Image: Bool,_ ImageID: Int,_ Photo: NSData? , _ lat: String, _ long: String){
//        self.init()
//
//        self.IncID = self.incrementID()
//        self.ProjectID = ID
//        self.ProjectNumber = ProNum
//        self.IsImage = Image
//        self.ImageID = ImageID
//        self.Image = Photo
//        self.Ownimage = ""
//        self.latitude = lat
//        self.longitude = long
//
//    }
//
//    override static func primaryKey() -> String? {
//        return "IncID"
//    }
//    func incrementID() -> Int {
//        let realm = try! Realm()
//        return (realm.objects(RealmnUserSTCformData.self).max(ofProperty: "IncID") as Int? ?? 0) + 1
//    }
//}



class RealmServieces {
    
    private init() {}
    static let shared = RealmServieces()
    
    
    let config = Realm.Configuration(
        // Set the new schema version. This must be greater than the previously used
        // version (if you've never set a schema version before, the version is 0).
        schemaVersion:2,
        
        // Set the block which will be called automatically when opening a Realm with
        // a schema version lower than the one set above
        migrationBlock: { migration, oldSchemaVersion in
            // We haven’t migrated anything yet, so oldSchemaVersion == 0
            if (oldSchemaVersion < 2) {
                // Nothing to do!
                // Realm will automatically detect new properties and removed properties
                // And will update the schema on disk automatically
            }
    })
    
   
    
    
    var realm = try! Realm()
    
    
    func create<T: Object>(_ object: T)
    {
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        
        do
        {
            try realm.write {
                
                realm.add(object, update: true)
            }
        }
        catch
        {
            post(error)
        }
    }
    
    func deleteObject<T: Object>(_ object: Results<T>)
    {
        do
        {
            
            
            try realm.write {
                
                realm.delete(object)
            }
        }
        catch
        {
            post(error)
        }
    }
    
    func delete<T: Object>(_ object: T)
    {
        do
        {
            try realm.write {
                realm.delete(object)
            }
            
        }
        catch
        {
            post(error)
        }
    }
    
    func deleteAllRec()
    {
        do
        {
            try realm.write {
                realm.deleteAll()
            }
            
        }
        catch
        {
            post(error)
        }
    }
    
    
    func post(_ error: Error)
    {
        NotificationCenter.default.post(name: NSNotification.Name("RealmError"), object: error)
    }
    
    func observRealmErrors(in vc: UIViewController, completion: @escaping(Error?) -> Void)
    {
        NotificationCenter.default.addObserver(forName: NSNotification.Name("RealmError"), object: nil, queue: nil) { (notification) in
            
            completion(notification.object as? Error)
        }
    }
    
    func stopObservingErros(in vc: UIViewController)
    {
        NotificationCenter.default.removeObserver(vc, name: NSNotification.Name("RealmError"), object: nil)
    }
    
    
}



