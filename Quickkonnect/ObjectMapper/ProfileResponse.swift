//
//  ProfileResponse.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 28/12/17.
//  Copyright © 2017 com.kavish. All rights reserved.
//

import UIKit
import ObjectMapper

class contactDataArray: Mappable {
    var contactArray:[contactData]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        
        contactArray <- map["data"]
    }
}

class contactData: Mappable {
    var scan_user_id:CLongLong?
    var scan_user_name:String?
    var scan_user_profile_url:String?
    var scan_user_unique_code:String?
    var scan_date:String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        scan_user_id <- map["scan_user_id"]
        scan_user_name <- map["scan_user_name"]
        scan_user_profile_url <- map["scan_user_profile_url"]
        scan_user_unique_code <- map["scan_user_unique_code"]
        scan_date <- map["scan_date"]
    }
}

class dataDetails: Mappable {
    
    var basicDetails:basicDetails?
    var contactDetails:[contactDetails]?
    var educationDetails:[educationDetails]?
    var experienceDetail:[experienceDetail]?
    var publicationDetail:[publicationDetail]?
    var languageDetail:[languageDetail]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        basicDetails <- map["basic_detail"]
        contactDetails <- map["contact_detail"]
        educationDetails <- map["education_detail"]
        experienceDetail <- map["experience_detail"]
        publicationDetail <- map["publication_detail"]
        languageDetail <- map["language_detail"]
    }
}

class dataDetailsContact: Mappable {
    
    var basicDetails:basicDetails?
    var contactDetails:[contactDetails]?
    var educationDetails:[educationDetails]?
    var experienceDetail:[experienceDetail]?
    var publicationDetail:[publicationDetail]?
    var languageDetail:[languageDetail]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        basicDetails <- map["basic_detail"]
        contactDetails <- map["approved_contacts"]//["contact_detail"]
        educationDetails <- map["education_detail"]
        experienceDetail <- map["experience_detail"]
        publicationDetail <- map["publication_detail"]
        languageDetail <- map["language_detail"]
    }
}

class languageDetail: Mappable {
    var langauge_id:CLongLong?
    var langauge_name:String?
    var rating:String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        langauge_id <- map["langauge_id"]
        langauge_name <- map["langauge_name"]
        rating <- map["rating"]
    }
}

class publicationDetail: Mappable {
    var publication_id:CLongLong?
    var publication_title:String?
    var publisher_name:String?
    var date:String?
    var url: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        publication_id <- map["publication_id"]
        publication_title <- map["publication_title"]
        publisher_name <- map["publisher_name"]
        date <- map["date"]
        url <- map["publication_url"]
    }
}

class experienceDetail: Mappable {
    
    var experience_id:CLongLong?
    var company_name:String?
    var title:String?
    var location:String?
    var start_date:String?
    var end_date:String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        experience_id <- map["experience_id"]
        company_name <- map["company_name"]
        title <- map["title"]
        location <- map["location"]
        start_date <- map["start_date"]
        end_date <- map["end_date"]
    }
}

class educationDetails: Mappable {
    
    var education_id:CLongLong?
    var school_name:String?
    var degree:String?
    var field_of_study:String?
    var start_date:String?
    var end_date:String?
    var activities:String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        education_id <- map["education_id"]
        school_name <- map["school_name"]
        degree <- map["degree"]
        field_of_study <- map["field_of_study"]
        start_date <- map["start_date"]
        end_date <- map["end_date"]
        activities <- map["activities"]
    }
}

class contactDetails: Mappable {
    
    var contact_id:CLongLong?
    var contact_type:String?
    var email:String?
    var phone:String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        contact_id <- map["contact_id"]
        contact_type <- map["contact_type"]
        email <- map["email"]
        phone <- map["phone"]
    }
}

class basicDetails: Mappable {
    
    var userid:CLongLong?
    var username:String?
    var firstname:String?
    var lastname:String?
    var profile_pic:String?
    var birthdate:String?
    var gender:String?
    var hometown:String?
    var country_id:CLongLong?
    var country:String?
    var city_id:CLongLong?
    var city:String?
    var state_id:CLongLong?
    var state:String?
    var qk_story:String?
    var unique_code:String?
    var email:String?
    
    required init?(map: Map){
    }
    
    
    func mapping(map: Map) {
        userid <- map["user_id"]
        username <- map["username"]
        firstname <- map["firstname"]
        lastname <- map["lastname"]
        profile_pic <- map["profile_pic"]
        birthdate <- map["birthdate"]
        hometown <- map["hometown"]
        gender <- map["gender"]
        country_id <- map["country_id"]
        country <- map["country"]
        city_id <- map["city_id"]
        city <- map["city"]
        state_id <- map["state_id"]
        state <- map["state"]
        qk_story <- map["qk_story"]
        unique_code <- map["unique_code"]
        email <- map["email"]
    }
}
