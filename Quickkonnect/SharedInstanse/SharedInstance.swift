//
//  SharedInstance.swift
//  Quickkonnect
//
//  Created by Zignuts Technolab on 28/12/17.
//  Copyright © 2017 com.kavish. All rights reserved.
//

import UIKit

class Singleton {
    var data_details_contact:dataDetailsContact?
    var data_details:dataDetails?
    
    var contact_data:[contactData]?
    static let sharedInstance : Singleton = {
        let instance = Singleton()
        return instance
    }()
}
